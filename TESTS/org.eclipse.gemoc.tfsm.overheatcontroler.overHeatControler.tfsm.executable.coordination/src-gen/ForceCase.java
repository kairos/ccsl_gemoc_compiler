import org.eclipse.gemoc.moccml.compiled.library.Clock;

public class ForceCase{
	public Clock clockToForce;
	public Boolean associatedResValue; //too restrictive, I know
	public ForceCase(Clock c, boolean resValue) {
		clockToForce = c;
		associatedResValue = resValue;
	}
}