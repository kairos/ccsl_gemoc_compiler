//============================================================================
// Name        : main.cpp
// Author      : Julien Deantoni
// Version     : the best of ones I had :)
// Copyright   : http://i3s.unice.fr/~deantoni
// Description : Code generated for your CCSL specification, C++11 style
//============================================================================

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.Solver;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.*;
import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.statemachine.State;
import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.statemachine.StateMachine;
import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.statemachine.Transition;
import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.*;
import org.eclipse.gemoc.moccml.compiled.library.utils.*;
import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.ConditionalExpression.Case;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.*;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.lang.reflect.Field;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;import org.eclipse.gemoc.execution.commons.commands.DoStepCommand;
import org.eclipse.gemoc.execution.commons.commands.GetVariableCommand;
import org.eclipse.gemoc.execution.commons.commands.SetVariableCommand;
import org.eclipse.gemoc.execution.commons.commands.StopCommand;
import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.commands.StopReason;
import org.eclipse.gemoc.execution.commons.helpers.EcoreQNHelper;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;
import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;
import org.eclipse.gemoc.executionframework.engine.commons.GenericModelExecutionContext;
import org.eclipse.gemoc.executionframework.engine.commons.sequential.ISequentialRunConfiguration;
import org.eclipse.gemoc.executionframework.engine.core.AbstractCommandBasedSequentialExecutionEngine;
import com.google.common.util.concurrent.AtomicDouble;
import fr.inria.diverse.k3.al.annotationprocessor.coordination.ICoordinationManager;
import fr.inria.diverse.k3.al.annotationprocessor.coordination.Input;
import fr.inria.diverse.k3.al.annotationprocessor.coordination.Output;
import fr.inria.diverse.k3.al.annotationprocessor.coordination.Time;
import fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand;
import fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;import toools.io.JavaResource;
import toools.io.file.RegularFile;
import java.util.Collection;
import java.util.List;


public class CompiledExecutionEngine extends AbstractCommandBasedSequentialExecutionEngine<GenericModelExecutionContext<ISequentialRunConfiguration>, ISequentialRunConfiguration> 
                                     implements ICoordinationManager {

	public CompiledExecutionEngine() {
	}



	public Object execute(Object caller, String methodName, List<Object> parameters) {
		return internal_execute(caller, methodName, parameters, null);
	}

	private Object internal_execute(Object caller, String methodName, Collection<Object> parameters,
			Object mseOccurrence) throws RuntimeException {
		ArrayList<Object> staticParameters = new ArrayList<Object>();
		staticParameters.add(caller);
		if (parameters != null) {
			staticParameters.addAll(parameters);
		}
		Method bestApplicableMethod = getBestApplicableMethod(caller, methodName, staticParameters);
		if (bestApplicableMethod == null)
			throw new RuntimeException("static class not found or method not found when calling " + methodName
					+ " on " + caller + ". MSEOccurence=" + mseOccurrence);

		Object[] args = new Object[0];
		if (staticParameters != null) {
			args = staticParameters.toArray();
		}
		Object result = null;
		try {
			result = bestApplicableMethod.invoke(null, args);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException("Exception caught during execution of a call, see inner exception."+ methodName
					+ " on " + caller + ". MSEOccurence=" + mseOccurrence);
		}
		return result;
	}

	private Method getBestApplicableMethod(Object caller, String methodName, List<Object> parameters) {
		Set<Class<?>> staticHelperClasses = getStaticHelperClasses(caller);
		if (staticHelperClasses == null || staticHelperClasses.isEmpty()) {
			return null;
		}
		for (Class<?> c : staticHelperClasses) {
			Method m = getFirstApplicableMethod(c, methodName, parameters);
			if (m != null)
				return m;
		}
		return null;
	}

	/**
	 * return the first compatible method, goes up the inheritance hierarchy
	 * 
	 * @param staticHelperClass
	 * @param methodName
	 * @param parameters
	 * @return
	 */
	protected Method getFirstApplicableMethod(Class<?> staticHelperClass, String methodName, List<Object> parameters) {
		Method[] methods = staticHelperClass.getDeclaredMethods();
		for (Method method : methods) {
			Class<?>[] evaluatedMethodParamTypes = method.getParameterTypes();
			if (method.getName().equals(methodName) && evaluatedMethodParamTypes.length == parameters.size()) {
				boolean isAllParamCompatible = true;
				for (int i = 0; i < evaluatedMethodParamTypes.length; i++) {
					Object p = parameters.get(i);
					if (evaluatedMethodParamTypes[i].isPrimitive()) {

						if (evaluatedMethodParamTypes[i].equals(Integer.TYPE) && !Integer.class.isInstance(p)) {
							isAllParamCompatible = false;
							break;
						} else if (evaluatedMethodParamTypes[i].equals(Boolean.TYPE) && !Boolean.class.isInstance(p)) {
							isAllParamCompatible = false;
							break;
						}

					} else if (!evaluatedMethodParamTypes[i].isInstance(p)) {
						isAllParamCompatible = false;
						break;
					}
				}
				if (isAllParamCompatible) {
					return method;
				}
			}
		}
		// tries going in the inheritance hierarchy
		Class<?> superClass = staticHelperClass.getSuperclass();
		if (superClass != null)
			return getFirstApplicableMethod(superClass, methodName, parameters);
		else
			return null;
	}

	/**
	 * search static class by name (future version should use a map of available
	 * aspects, and deals with it as a list of applicable static classes)
	 * 
	 */
	protected Set<Class<?>> getStaticHelperClasses(Object target) {
		List<Class<?>> res = new ArrayList<>();
		for(Class<?> klass : savedAspects) {
			Aspect annot = (Aspect) klass.getAnnotation(Aspect.class);
			if (annot != null){
				if ((annot.className().isInstance(target))) {
					res.add(klass);
				}
			}
		}
		return new HashSet<Class<?>>(res);
	}


private void initializeCSV(ArrayList<ArrayList<String>> rtdCouples,
			EObject root) {
		try {
			csvFile.setContentAsASCII("");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		for(int i = 0; i < rtdCouples.size(); i++) {
			ArrayList<String> couple = rtdCouples.get(i);
			TreeIterator<EObject> it = root.eAllContents();
			while (it.hasNext()) {
				EObject eo = it.next();
				String className = couple.get(0);
				try {
					if(Class.forName(className).isAssignableFrom(eo.getClass())) {
						if (i == 0) {
							csvFile.append("timeRef".getBytes());
						}else {
							csvFile.append((","+getEObjectName(eo)).getBytes());
						}
					}
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			csvFile.append(("\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
 	private void generatePlotScript(ArrayList<ArrayList<String>> rtdCouples,
			EObject root) {
		RegularFile plotFile = new RegularFile("org.eclipse.gemoc.tfsm.overheatcontroler.overHeatControler.tfsm.executable.coordination.gnuplot");
		try {
			plotFile.setContentAsASCII("");
			plotFile.append((" set title 'result1'\n" + 
				" set ylabel 'RTD'\n" + 
				" set xlabel 'Time'\n" + 
				" set grid\n" + 
				" set term pdf\n" + 
				" set output 'org.eclipse.gemoc.tfsm.overheatcontroler.overHeatControler.tfsm.executable.coordination.pdf'\n" + 
				" plot ").getBytes());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		String sep="";
		int column = 2;
		for(int i = 0; i < rtdCouples.size(); i++) {
			ArrayList<String> couple = rtdCouples.get(i);
			TreeIterator<EObject> it = root.eAllContents();
			while (it.hasNext()) {
				EObject eo = it.next();
				String className = couple.get(0);
				try {
					if(Class.forName(className).isAssignableFrom(eo.getClass())) {
						if (i == 0) {
							//nothing to do here
						}else {
							plotFile.append((sep+"'org.eclipse.gemoc.tfsm.overheatcontroler.overHeatControler.tfsm.executable.coordination.csv' using 1:"+column+" with lines title '"+getEObjectName(eo)+"'").getBytes());
							sep=",";
							column++;
						}
					}
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			plotFile.append(("\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	private String getEObjectName(EObject eo) {
		EStructuralFeature nameF = eo.eClass().getEStructuralFeature("name");
		String name = nameF != null ? (String)eo.eGet(nameF) : null;
		if (name != null) {
			return name;
		}else {
			return "noName";
		}
	}
	
	int timeRef = 0;
	RegularFile csvFile = new RegularFile("org.eclipse.gemoc.tfsm.overheatcontroler.overHeatControler.tfsm.executable.coordination.csv");
		private void logRTDs(ArrayList<ArrayList<String>> rtdCouples,
			EObject root) {
		
		ArrayList<Object> results = new ArrayList<Object>();
		Object aResult = -1;
			for(int i = 0; i < rtdCouples.size(); i++) {
				ArrayList<String> couple = rtdCouples.get(i);
				TreeIterator<EObject> it = root.eAllContents();
				while (it.hasNext()) {
					EObject eo = it.next();
					String className = couple.get(0);
					String rtdName = couple.get(1);
					try {
						if(Class.forName(className).isAssignableFrom(eo.getClass())) {
							try{
								aResult = (Integer) this.execute(eo, rtdName, null);
							}
							catch(ClassCastException c) {
								aResult = (Float) this.execute(eo, rtdName, null);
							}
							if (i == 0) {
								timeRef = (int) aResult;
							}else {
								results.add(aResult);
							}
						}
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
			
			try {
				csvFile.append((""+timeRef).getBytes());
				for(Object i : results) {
					csvFile.append((", "+i.toString()).getBytes());
				}
				csvFile.append(("\n").getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}


	Multimap<Clock, ForceCase> forcedClock = ArrayListMultimap.create();
	ArrayList<Clock> clockToForceNext = new ArrayList<>();

	Set<Class<?>> savedAspects = new HashSet<>(Arrays.asList(           org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.TFSMAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.IntegerVariableAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.TransitionAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.StateAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.FSMEventAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.BooleanUnaryExpressionAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.GuardAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.VariableAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.OpaqueBooleanExpressionAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.ExpressionAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.EvaluateGuardAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.OpaqueIntegerExpressionAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.FSMClockAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.BooleanVariableAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.EventGuardAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.NamedElementAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.TemporalGuardAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.BooleanBinaryExpressionAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.IntegerComparisonExpressionAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.TimedSystemAspect.class
           ,org.eclipse.gemoc.example.moccml.tfsm.k3dsa.aspect.IntegerCalculationExpressionAspect.class
			));

   Map<Clock, String> clockToMethodName= new HashMap<>();
   Map<Clock, String> clockToEObject = new HashMap<>();
	String languageName="ConcurrentTFSM";
	public static void main(String[] args) {
           int nbSteps = 20000; //by default
		boolean logClocks = false;
		boolean logStepsOnly = false;
		
		Options options = new Options();

		Option opt_nbSteps = new Option("n", "nbSteps", true, "number of steps to execute (-1 for infinite)");
		opt_nbSteps.setRequired(false);
        options.addOption(opt_nbSteps);
		
        Option opt_logClocks = new Option("l", "logClocksAndSteps", false, "log clock status and execution steps");
        opt_logClocks.setRequired(false);
        options.addOption(opt_logClocks);
        
        Option opt_logStepsOnly = new Option("s", "logStepsOnly", false, "log only execution steps");
        opt_logStepsOnly.setRequired(false);
        options.addOption(opt_logStepsOnly);
        

        Option opt_rtdToLog = new Option("rtd", "runtimeDataToLog", false, "list of couple with class name and rtd name");
        opt_rtdToLog.setRequired(false);
        opt_rtdToLog.setArgs(Option.UNLIMITED_VALUES);
        opt_rtdToLog.setValueSeparator(' ');
        options.addOption(opt_rtdToLog);
        

        

        CommandLineParser parser = new PosixParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Compiled Execution Engine", options);

            System.exit(1);
        }

        if (cmd.hasOption("nbSteps")) nbSteps= Integer.parseInt(cmd.getOptionValue("nbSteps"));
        logClocks = cmd.hasOption("logClocksAndSteps");
        logStepsOnly = cmd.hasOption("logStepsOnly");


        String[] rtdList = null;
		if (cmd.hasOption("rtd")) rtdList = cmd.getOptionValues("rtd");
	    if (rtdList != null && rtdList.length%2 != 0) {
	    	System.err.println("RTD not considered since not correctly formatted. (list of couples, a couples being a class (qualified) name and a rtd name");
	    }
		ArrayList<ArrayList<String>> rtdCouples = new ArrayList<ArrayList<String>>();
		if (rtdList != null){
         for(int i = 0; i < rtdList.length; i+=2) {
			    rtdCouples.add(new ArrayList<String>(Arrays.asList(rtdList[i], rtdList[i+1])));
           }
		}


		CompiledExecutionEngine theEngine = new CompiledExecutionEngine();
	 int CPUprotection_0_mainBlock_selfLoopNormalTemp_4after3s_15_afterDuration = 3;
	 Integer[] finite_CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_seqOneInfinite = {};
	 Integer[] infinite_CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_seqOneInfinite = {1};
	 Sequence CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_seqOneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_seqOneInfinite,infinite_CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_seqOneInfinite,"CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_seqOneInfinite");
	 Integer[] finite_CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite = {1};
	 Integer[] infinite_CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite = {0};
	 Sequence CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite,infinite_CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite,"CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite");
	 Integer[] finite_CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite = {};
	 Integer[] infinite_CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite = {1};
	 Sequence CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite,infinite_CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite,"CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite");
	 Integer[] finite_CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_seqOneInfinite = {};
	 Integer[] infinite_CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_seqOneInfinite = {1};
	 Sequence CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_seqOneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_seqOneInfinite,infinite_CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_seqOneInfinite,"CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_seqOneInfinite");
	 Integer[] finite_CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_seqOneInfinite = {};
	 Integer[] infinite_CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_seqOneInfinite = {1};
	 Sequence CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_seqOneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_seqOneInfinite,infinite_CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_seqOneInfinite,"CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_seqOneInfinite");
	 Integer[] finite_CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite = {};
	 Integer[] infinite_CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite = {1};
	 Sequence CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite,infinite_CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite,"CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite");
	 Integer[] finite_CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_oneInfinite = {1};
	 Integer[] infinite_CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_oneInfinite = {0};
	 Sequence CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_oneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_oneInfinite,infinite_CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_oneInfinite,"CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_oneInfinite");
	 Integer[] finite_CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite = {1};
	 Integer[] infinite_CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite = {0};
	 Sequence CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite,infinite_CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite,"CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite");
	 Integer[] finite_CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_seqOneInfinite = {};
	 Integer[] infinite_CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_seqOneInfinite = {1};
	 Sequence CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_seqOneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_seqOneInfinite,infinite_CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_seqOneInfinite,"CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_seqOneInfinite");
	 Integer[] finite_CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite = {};
	 Integer[] infinite_CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite = {1};
	 Sequence CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite,infinite_CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite,"CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite");
	 int CPUprotection_0_mainBlock_selfLoopTooHot_2after5s_14_afterDuration = 5;
	 Integer[] finite_CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite = {};
	 Integer[] infinite_CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite = {1};
	 Sequence CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite,infinite_CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite,"CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite");
	 Integer[] finite_CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite = {};
	 Integer[] infinite_CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite = {1};
	 Sequence CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite = new Sequence(finite_CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite,infinite_CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite,"CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite");
Not CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_15 = new Not( new SeqEmpty(CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite));
SeqEmpty CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_16 = new SeqEmpty(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite);
SeqHead CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqHead = new SeqHead(CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite);
SeqEmpty CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_18 = new SeqEmpty(CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite);
Not CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_19 = new Not( new SeqEmpty(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_oneInfinite));
SeqTail CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqTail = new SeqTail(CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite);
SeqEmpty CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_21 = new SeqEmpty(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_oneInfinite);
SeqTail CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqTail = new SeqTail(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_oneInfinite);
SeqHead CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqHead = new SeqHead(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_oneInfinite);
SeqTail CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqTail = new SeqTail(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite);
Not CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_25 = new Not( new SeqEmpty(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite));
SeqHead CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqHead = new SeqHead(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_oneInfinite);
	 Clock CPUprotection_0_mainBlock_initToNormal_3_fire = new Clock("CPUprotection_0_mainBlock_initToNormal_3_fire");
	 Clock CPUprotection_0_mainBlock_switchCPUState_9_occurs = new Clock("CPUprotection_0_mainBlock_switchCPUState_9_occurs");
	 Clock CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedTrue = new Clock("CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedTrue");
	 Clock CPUprotection_0_mainBlock_init_10_entering = new Clock("CPUprotection_0_mainBlock_init_10_entering");
	 Clock CPUprotection_0_mainBlock_CPUprotection_0_start = new Clock("CPUprotection_0_mainBlock_CPUprotection_0_start");
	 Clock CPUprotection_0_mainBlock_seconds_1_ticks = new Clock("CPUprotection_0_mainBlock_seconds_1_ticks");
	 Clock CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedTrue = new Clock("CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedTrue");
	 Clock CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedFalse = new Clock("CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedFalse");
	 Clock CPUprotection_0_mainBlock_normalTemp_12_entering = new Clock("CPUprotection_0_mainBlock_normalTemp_12_entering");
	 Clock CPUprotection_0_mainBlock_overHeatProtection_13_start = new Clock("CPUprotection_0_mainBlock_overHeatProtection_13_start");
	 Clock CPUprotection_0_mainBlock_tooHot_11_entering = new Clock("CPUprotection_0_mainBlock_tooHot_11_entering");
	 Clock CPUprotection_0_mainBlock_selfLoopTooHot_2_fire = new Clock("CPUprotection_0_mainBlock_selfLoopTooHot_2_fire");
	 Clock CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate = new Clock("CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate");
	 Clock CPUprotection_0_mainBlock_normalToTooHot_5_fire = new Clock("CPUprotection_0_mainBlock_normalToTooHot_5_fire");
	 Clock CPUprotection_0_mainBlock_init_10_leaving = new Clock("CPUprotection_0_mainBlock_init_10_leaving");
	 Clock CPUprotection_0_mainBlock_normalTemp_12_leaving = new Clock("CPUprotection_0_mainBlock_normalTemp_12_leaving");
	 Clock CPUprotection_0_mainBlock_tooHot_11_leaving = new Clock("CPUprotection_0_mainBlock_tooHot_11_leaving");
	 Clock CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire = new Clock("CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire");
	 Clock CPUprotection_0_mainBlock_tooHotToNormal_6_fire = new Clock("CPUprotection_0_mainBlock_tooHotToNormal_6_fire");
	 Clock CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedFalse = new Clock("CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedFalse");
	 Clock CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate = new Clock("CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate");
	Wait CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead = new Wait(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqHead,"CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead");
	Union CPUprotection_0_mainBlock_normalTemp_12intermediate_allEvents1 = new Union("CPUprotection_0_mainBlock_normalTemp_12intermediate_allEvents1");
	Defer CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne = new Defer("CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne");
	Death CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death = new Death("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death");
	Concatenation CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait = new Concatenation("CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait");
	Union CPUprotection_0_mainBlock_normalToTooHot_5intermediate_otherFireFromTheSameState31 = new Union("CPUprotection_0_mainBlock_normalToTooHot_5intermediate_otherFireFromTheSameState31");
	Union CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition21 = new Union("CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition21");
	Death CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death = new Death("CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death");
	Wait CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead = new Wait(CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqHead,"CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead");
	Union CPUprotection_0_mainBlock_init_10intermediate_allFiredoutgoingTransition1 = new Union("CPUprotection_0_mainBlock_init_10intermediate_allFiredoutgoingTransition1");
	Union CPUprotection_0_mainBlock_normalTemp_12_Union_eventsOrLocalTime = new Union("CPUprotection_0_mainBlock_normalTemp_12_Union_eventsOrLocalTime");
	Union CPUprotection_0_mainBlock_normalToTooHot_5_Union_trueOrFalse = new Union("CPUprotection_0_mainBlock_normalToTooHot_5_Union_trueOrFalse");
	Concatenation CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait = new Concatenation("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait");
	Union CPUprotection_0_mainBlock_switchCPUState_9intermediate_AllTriggeringOccurrences1 = new Union("CPUprotection_0_mainBlock_switchCPUState_9intermediate_AllTriggeringOccurrences1");
	Defer CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne = new Defer("CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne");
	Union CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard = new Union("CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard");
	Defer CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne = new Defer("CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne");
	Union CPUprotection_0_mainBlock_tooHot_11_Union_eventsOrLocalTime = new Union("CPUprotection_0_mainBlock_tooHot_11_Union_eventsOrLocalTime");
	Union CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard = new Union("CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard");
	Union CPUprotection_0_mainBlock_selfLoopTooHot_2intermediate_otherFireFromTheSameState1 = new Union("CPUprotection_0_mainBlock_selfLoopTooHot_2intermediate_otherFireFromTheSameState1");
	Union CPUprotection_0_mainBlock_tooHotToNormal_6intermediate_otherFireFromTheSameState31 = new Union("CPUprotection_0_mainBlock_tooHotToNormal_6intermediate_otherFireFromTheSameState31");
	Union CPUprotection_0_mainBlock_tooHotToNormal_6_Union_trueOrFalse = new Union("CPUprotection_0_mainBlock_tooHotToNormal_6_Union_trueOrFalse");
	Death CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt = new Death("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt");
	Concatenation CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait = new Concatenation("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait");
	Union CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition1 = new Union("CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition1");
	Defer CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne = new Defer("CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne");
	Union CPUprotection_0_mainBlock_tooHot_11intermediate_allEvents1 = new Union("CPUprotection_0_mainBlock_tooHot_11intermediate_allEvents1");
	Union CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition1 = new Union("CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition1");
	Death CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death = new Death("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death");
	Defer CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne = new Defer("CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne");
	Union CPUprotection_0_mainBlock_selfLoopNormalTemp_4intermediate_otherFireFromTheSameState1 = new Union("CPUprotection_0_mainBlock_selfLoopNormalTemp_4intermediate_otherFireFromTheSameState1");
	Concatenation CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat = new Concatenation("CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat");
	Defer CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne = new Defer("CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne");
	Death CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt = new Death("CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt");
	Wait CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead = new Wait(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqHead,"CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead");
	Defer CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne = new Defer("CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne");
	Union CPUprotection_0_mainBlock_normalTemp_12intermediate_allFiredoutgoingTransition1 = new Union("CPUprotection_0_mainBlock_normalTemp_12intermediate_allFiredoutgoingTransition1");
	Union CPUprotection_0_mainBlock_CPUprotection_0intermediate_allStartTFSM1 = new Union("CPUprotection_0_mainBlock_CPUprotection_0intermediate_allStartTFSM1");
	Union CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition21 = new Union("CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition21");
	Union CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition23 = new Union("CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition23");
	Defer CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne = new Defer("CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne");
	Defer CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne = new Defer("CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne");
	Sampled CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled = new Sampled("CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled", true);
	Union CPUprotection_0_mainBlock_tooHot_11intermediate_allFiredoutgoingTransition1 = new Union("CPUprotection_0_mainBlock_tooHot_11intermediate_allFiredoutgoingTransition1");
	Sampled CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled = new Sampled("CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled", true);
	Death CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt = new Death("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt");
	Concatenation CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat = new Concatenation("CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat");
	Union CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition3 = new Union("CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition3");
	 ConditionalExpression CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne = new ConditionalExpression("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne");
	 ConditionalExpression CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne = new ConditionalExpression("CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne");
	 ConditionalExpression CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne = new ConditionalExpression("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne");
	 UserDefinedExpression CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst = new UserDefinedExpression("CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst");
	 UserDefinedExpression CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime = new UserDefinedExpression("CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime");
	 UserDefinedExpression CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick = new UserDefinedExpression("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick");
	 ConditionalExpression CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail");
	 UserDefinedExpression CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime = new UserDefinedExpression("CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime");
	 UserDefinedExpression CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst = new UserDefinedExpression("CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst");
	Exclusion CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime4_5 = new Exclusion(CPUprotection_0_mainBlock_initToNormal_3_fire,CPUprotection_0_mainBlock_selfLoopTooHot_2_fire);
	Exclusion CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime1_5 = new Exclusion(CPUprotection_0_mainBlock_initToNormal_3_fire,CPUprotection_0_mainBlock_normalToTooHot_5_fire);
	Exclusion CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime2_5 = new Exclusion(CPUprotection_0_mainBlock_initToNormal_3_fire,CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire);
	Exclusion CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime2_3 = new Exclusion(CPUprotection_0_mainBlock_tooHotToNormal_6_fire,CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire);
	Exclusion CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime1_2 = new Exclusion(CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire,CPUprotection_0_mainBlock_normalToTooHot_5_fire);
	Exclusion CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime1_4 = new Exclusion(CPUprotection_0_mainBlock_selfLoopTooHot_2_fire,CPUprotection_0_mainBlock_normalToTooHot_5_fire);
	Exclusion CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime3_5 = new Exclusion(CPUprotection_0_mainBlock_initToNormal_3_fire,CPUprotection_0_mainBlock_tooHotToNormal_6_fire);
	Exclusion CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime1_3 = new Exclusion(CPUprotection_0_mainBlock_tooHotToNormal_6_fire,CPUprotection_0_mainBlock_normalToTooHot_5_fire);
	Exclusion CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime3_4 = new Exclusion(CPUprotection_0_mainBlock_selfLoopTooHot_2_fire,CPUprotection_0_mainBlock_tooHotToNormal_6_fire);
	Exclusion CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime2_4 = new Exclusion(CPUprotection_0_mainBlock_selfLoopTooHot_2_fire,CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire);
	Exclusion CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_EitherFalseOrTrue = new Exclusion(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedFalse,CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedTrue);
	Exclusion CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_EitherFalseOrTrue = new Exclusion(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedFalse,CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedTrue);
	Causes CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1PrecedesC2 = new Causes(CPUprotection_0_mainBlock_tooHot_11_entering,CPUprotection_0_mainBlock_tooHot_11_leaving);
	Precedes CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c1PrecedesC2 = new Precedes(CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition3,CPUprotection_0_mainBlock_normalTemp_12_entering);
	Coincides CPUprotection_0_mainBlock_CPUprotection_0Coincides_firstOnlyOnce = new Coincides(CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst,CPUprotection_0_mainBlock_CPUprotection_0_start);
	Causes CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1PrecedesC2 = new Causes(CPUprotection_0_mainBlock_normalTemp_12_entering,CPUprotection_0_mainBlock_normalTemp_12_leaving);
	Precedes CPUprotection_0_mainBlock_CPUprotection_0Precedes_startTimedSystemBeforeAllStartTFSM = new Precedes(CPUprotection_0_mainBlock_CPUprotection_0_start,CPUprotection_0_mainBlock_CPUprotection_0intermediate_allStartTFSM1);
	Coincides CPUprotection_0_mainBlock_init_10Coincides_firingATransitionAlternatesWithLeavingState = new Coincides(CPUprotection_0_mainBlock_init_10_leaving,CPUprotection_0_mainBlock_init_10intermediate_allFiredoutgoingTransition1);
	Coincides CPUprotection_0_mainBlock_normalTemp_12Coincides_firingATransitionAlternatesWithLeavingState = new Coincides(CPUprotection_0_mainBlock_normalTemp_12_leaving,CPUprotection_0_mainBlock_normalTemp_12intermediate_allFiredoutgoingTransition1);
	Coincides CPUprotection_0_mainBlock_initToNormal_3Coincides_TransientInitTransition2 = new Coincides(CPUprotection_0_mainBlock_initToNormal_3_fire,CPUprotection_0_mainBlock_init_10_leaving);
	Coincides CPUprotection_0_mainBlock_initToNormal_3Coincides_TransientInitTransition = new Coincides(CPUprotection_0_mainBlock_init_10_leaving,CPUprotection_0_mainBlock_init_10_entering);
	Precedes CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c1PrecedesC2 = new Precedes(CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition1,CPUprotection_0_mainBlock_tooHot_11_entering);
	Precedes CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1PrecedesC2 = new Precedes(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard);
	Precedes CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c2precedesC1DelayedByOne = new Precedes(CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne);
	Precedes CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c2precedesC1DelayedByOne = new Precedes(CPUprotection_0_mainBlock_normalTemp_12_entering,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne);
	Coincides CPUprotection_0_mainBlock_normalToTooHot_5Coincides_EvaluateGuardWhenEnteringState = new Coincides(CPUprotection_0_mainBlock_normalTemp_12_entering,CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate);
	Precedes CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c1PrecedesC2 = new Precedes(CPUprotection_0_mainBlock_tooHot_11_entering,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime);
	Precedes CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c2precedesC1DelayedByOne = new Precedes(CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne);
	Precedes CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1PrecedesC2 = new Precedes(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard);
	Coincides CPUprotection_0_mainBlock_tooHot_11Coincides_firingATransitionAlternatesWithLeavingState = new Coincides(CPUprotection_0_mainBlock_tooHot_11_leaving,CPUprotection_0_mainBlock_tooHot_11intermediate_allFiredoutgoingTransition1);
	Precedes CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c2precedesC1DelayedByOne = new Precedes(CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne);
	Coincides CPUprotection_0_mainBlock_switchCPUState_9FSMEventRendezVous_occursWhenSolicitate_FSMEventRendezVousDef_RendezVous = new Coincides(CPUprotection_0_mainBlock_switchCPUState_9_occurs,CPUprotection_0_mainBlock_switchCPUState_9intermediate_AllTriggeringOccurrences1);
	Coincides CPUprotection_0_mainBlock_tooHotToNormal_6Coincides_EvaluateGuardWhenEnteringState = new Coincides(CPUprotection_0_mainBlock_tooHot_11_entering,CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate);
	Precedes CPUprotection_0_mainBlock_overHeatProtection_13Precedes_noLocalTimeBeforeStart = new Precedes(CPUprotection_0_mainBlock_overHeatProtection_13_start,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick);
	Causes CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c2precedesC1DelayedByOne = new Causes(CPUprotection_0_mainBlock_tooHot_11_leaving,CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne);
	Causes CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c2precedesC1DelayedByOne = new Causes(CPUprotection_0_mainBlock_normalTemp_12_leaving,CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne);
	Causes CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c2precedesC1DelayedByOne = new Causes(CPUprotection_0_mainBlock_init_10_leaving,CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne);
	Causes CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1PrecedesC2 = new Causes(CPUprotection_0_mainBlock_init_10_entering,CPUprotection_0_mainBlock_init_10_leaving);
	Precedes CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c1PrecedesC2 = new Precedes(CPUprotection_0_mainBlock_normalTemp_12_entering,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime);
	Precedes CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c2precedesC1DelayedByOne = new Precedes(CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne);
	Coincides CPUprotection_0_mainBlock_overHeatProtection_13Coincides_firstOnlyOnce = new Coincides(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst,CPUprotection_0_mainBlock_overHeatProtection_13_start);
	Precedes CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c2precedesC1DelayedByOne = new Precedes(CPUprotection_0_mainBlock_tooHot_11_entering,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne);
	Coincides CPUprotection_0_mainBlock_overHeatProtection_13Coincides_firstIsInitialState = new Coincides(CPUprotection_0_mainBlock_init_10_entering,CPUprotection_0_mainBlock_overHeatProtection_13_start);
StateMachine CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition = new StateMachine();
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition.allClocks.addAll(Arrays.asList(CPUprotection_0_mainBlock_selfLoopNormalTemp_4intermediate_otherFireFromTheSameState1,CPUprotection_0_mainBlock_seconds_1_ticks,CPUprotection_0_mainBlock_normalTemp_12_entering,CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire));
State CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState = new State("CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState");
State CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry = new State("CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry");
State CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse = new State("CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse");
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition.states.addAll(Arrays.asList(CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState,CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry,CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse));
Transition CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_init2waiting = new Transition(CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState, CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_init2waiting.clocks = new ArrayList<Clock>(Arrays.asList());
Transition CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToreadyToFire = new Transition(CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry, CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToreadyToFire.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_normalTemp_12_entering));
Transition CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry = new Transition(CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse, CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopNormalTemp_4intermediate_otherFireFromTheSameState1));
Transition CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry2 = new Transition(CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse, CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry2.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire));
Transition CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry = new Transition(CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry, CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_seconds_1_ticks));
Transition CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitTimeToFireToTGT_waitTimeToFire = new Transition(CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse, CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitTimeToFireToTGT_waitTimeToFire.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_seconds_1_ticks));
Transition CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry2 = new Transition(CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry, CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry2.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopNormalTemp_4intermediate_otherFireFromTheSameState1));
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_init2waiting));
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToreadyToFire,CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry,CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry2));
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry,CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry2,CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitTimeToFireToTGT_waitTimeToFire));
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition.initialState = CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState;
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition.currentState = CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState;

IntRef id_128 = new IntRef(0);
IntRef id_129 = new IntRef(3); // from TGT_duration
IntEquals id_130 = new IntEquals(id_128, id_129);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry2.guard = id_130;

IntRef id_131 = new IntRef(3); // from TGT_duration
IntInf id_132 = new IntInf(id_128, id_131);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitTimeToFireToTGT_waitTimeToFire.guard = id_132;

IntRef id_133 = new IntRef(0);
IntAssign id_134 = new IntAssign(id_128, id_133);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry2.actions.add(id_134);

IntRef id_135 = new IntRef(1);
IntAdd id_136 = new IntAdd(id_128, id_135);
IntAssign id_137 = new IntAssign(id_128, id_136);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitTimeToFireToTGT_waitTimeToFire.actions.add(id_137);

IntAssign id_138 = new IntAssign(id_128, id_133);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToreadyToFire.actions.add(id_138);

IntAssign id_139 = new IntAssign(id_128, id_133);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry.actions.add(id_139);

IntAssign id_140 = new IntAssign(id_128, id_133);
CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_init2waiting.actions.add(id_140);
StateMachine CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition = new StateMachine();
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition.allClocks.addAll(Arrays.asList(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedTrue,CPUprotection_0_mainBlock_tooHot_11_entering,CPUprotection_0_mainBlock_tooHotToNormal_6_fire,CPUprotection_0_mainBlock_tooHotToNormal_6intermediate_otherFireFromTheSameState31));
State CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState = new State("CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState");
State CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry = new State("CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry");
State CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire = new State("CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire");
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition.states.addAll(Arrays.asList(CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState,CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry,CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire));
Transition CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_init2waiting = new Transition(CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState, CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry);
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_init2waiting.clocks = new ArrayList<Clock>(Arrays.asList());
Transition CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToreadyToFire = new Transition(CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry, CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire);
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToreadyToFire.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_tooHot_11_entering));
Transition CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry = new Transition(CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire, CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry);
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_tooHotToNormal_6intermediate_otherFireFromTheSameState31));
Transition CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry2 = new Transition(CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire, CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry);
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry2.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedTrue,CPUprotection_0_mainBlock_tooHotToNormal_6_fire));
Transition CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry = new Transition(CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry, CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry);
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_tooHotToNormal_6intermediate_otherFireFromTheSameState31));
Transition CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry2 = new Transition(CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry, CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry);
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry2.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedTrue));
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_init2waiting));
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToreadyToFire,CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry,CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry2));
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry,CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry2));
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition.initialState = CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState;
CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition.currentState = CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState;
StateMachine CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition = new StateMachine();
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition.allClocks.addAll(Arrays.asList(CPUprotection_0_mainBlock_seconds_1_ticks,CPUprotection_0_mainBlock_tooHot_11_entering,CPUprotection_0_mainBlock_selfLoopTooHot_2intermediate_otherFireFromTheSameState1,CPUprotection_0_mainBlock_selfLoopTooHot_2_fire));
State CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState = new State("CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState");
State CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry = new State("CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry");
State CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse = new State("CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse");
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition.states.addAll(Arrays.asList(CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState,CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry,CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse));
Transition CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_init2waiting = new Transition(CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState, CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_init2waiting.clocks = new ArrayList<Clock>(Arrays.asList());
Transition CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToreadyToFire = new Transition(CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry, CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToreadyToFire.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_tooHot_11_entering));
Transition CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry = new Transition(CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse, CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopTooHot_2intermediate_otherFireFromTheSameState1));
Transition CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry2 = new Transition(CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse, CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry2.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopTooHot_2_fire));
Transition CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry = new Transition(CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry, CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_seconds_1_ticks));
Transition CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitTimeToFireToTGT_waitTimeToFire = new Transition(CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse, CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitTimeToFireToTGT_waitTimeToFire.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_seconds_1_ticks));
Transition CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry2 = new Transition(CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry, CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry2.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopTooHot_2intermediate_otherFireFromTheSameState1));
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_init2waiting));
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingStateEntry.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToreadyToFire,CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry,CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToTGT_waitingEntry2));
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_waitingTimerToElapse.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry,CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry2,CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitTimeToFireToTGT_waitTimeToFire));
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition.initialState = CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState;
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition.currentState = CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_initialState;

IntRef id_141 = new IntRef(0);
IntRef id_142 = new IntRef(5); // from TGT_duration
IntEquals id_143 = new IntEquals(id_141, id_142);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry2.guard = id_143;

IntRef id_144 = new IntRef(5); // from TGT_duration
IntInf id_145 = new IntInf(id_141, id_144);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitTimeToFireToTGT_waitTimeToFire.guard = id_145;

IntRef id_146 = new IntRef(0);
IntAssign id_147 = new IntAssign(id_141, id_146);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry2.actions.add(id_147);

IntRef id_148 = new IntRef(1);
IntAdd id_149 = new IntAdd(id_141, id_148);
IntAssign id_150 = new IntAssign(id_141, id_149);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitTimeToFireToTGT_waitTimeToFire.actions.add(id_150);

IntAssign id_151 = new IntAssign(id_141, id_146);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_waitingEntryToreadyToFire.actions.add(id_151);

IntAssign id_152 = new IntAssign(id_141, id_146);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_readyToFireTowaitingEntry.actions.add(id_152);

IntAssign id_153 = new IntAssign(id_141, id_146);
CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition_TGT_init2waiting.actions.add(id_153);
StateMachine CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition = new StateMachine();
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition.allClocks.addAll(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5intermediate_otherFireFromTheSameState31,CPUprotection_0_mainBlock_normalTemp_12_entering,CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedTrue,CPUprotection_0_mainBlock_normalToTooHot_5_fire));
State CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState = new State("CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState");
State CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry = new State("CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry");
State CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire = new State("CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire");
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition.states.addAll(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState,CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry,CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire));
Transition CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_init2waiting = new Transition(CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState, CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry);
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_init2waiting.clocks = new ArrayList<Clock>(Arrays.asList());
Transition CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToreadyToFire = new Transition(CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry, CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire);
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToreadyToFire.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_normalTemp_12_entering));
Transition CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry = new Transition(CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire, CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry);
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5intermediate_otherFireFromTheSameState31));
Transition CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry2 = new Transition(CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire, CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry);
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry2.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedTrue,CPUprotection_0_mainBlock_normalToTooHot_5_fire));
Transition CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry = new Transition(CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry, CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry);
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5intermediate_otherFireFromTheSameState31));
Transition CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry2 = new Transition(CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry, CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry);
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry2.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedTrue));
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_init2waiting));
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntry.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToreadyToFire,CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry,CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_waitingEntryToEGT_waitingEntry2));
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFire.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry,CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_readyToFireTowaitingEntry2));
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition.initialState = CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState;
CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition.currentState = CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition_EGT_initialState;
StateMachine CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime = new StateMachine();
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime.allClocks.addAll(Arrays.asList(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate,CPUprotection_0_mainBlock_seconds_1_ticks,CPUprotection_0_mainBlock_tooHotToNormal_6_Union_trueOrFalse));
State CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_S7 = new State("CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_S7");
State CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3 = new State("CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3");
State CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4 = new State("CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4");
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime.states.addAll(Arrays.asList(CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_S7,CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3,CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4));
Transition CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S4 = new Transition(CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3, CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4);
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S4.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate,CPUprotection_0_mainBlock_seconds_1_ticks));
Transition CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S42 = new Transition(CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3, CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4);
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S42.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate));
Transition CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S3 = new Transition(CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3, CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3);
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S3.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_seconds_1_ticks));
Transition CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4ToMSED_S3 = new Transition(CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4, CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3);
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4ToMSED_S3.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_tooHotToNormal_6_Union_trueOrFalse));
Transition CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_S7ToMSED_S3 = new Transition(CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_S7, CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3);
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_S7ToMSED_S3.clocks = new ArrayList<Clock>(Arrays.asList());
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_S7.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_S7ToMSED_S3));
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S4,CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S42,CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S3));
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4ToMSED_S3));
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime.initialState = CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_S7;
CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime.currentState = CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime_S7;
StateMachine CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime = new StateMachine();
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime.allClocks.addAll(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5_Union_trueOrFalse,CPUprotection_0_mainBlock_seconds_1_ticks,CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate));
State CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_S7 = new State("CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_S7");
State CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3 = new State("CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3");
State CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4 = new State("CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4");
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime.states.addAll(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_S7,CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3,CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4));
Transition CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S4 = new Transition(CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3, CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4);
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S4.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate,CPUprotection_0_mainBlock_seconds_1_ticks));
Transition CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S42 = new Transition(CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3, CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4);
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S42.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate));
Transition CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S3 = new Transition(CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3, CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3);
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S3.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_seconds_1_ticks));
Transition CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4ToMSED_S3 = new Transition(CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4, CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3);
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4ToMSED_S3.clocks = new ArrayList<Clock>(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5_Union_trueOrFalse));
Transition CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_S7ToMSED_S3 = new Transition(CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_S7, CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3);
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_S7ToMSED_S3.clocks = new ArrayList<Clock>(Arrays.asList());
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_S7.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_S7ToMSED_S3));
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S4,CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S42,CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S3ToMSED_S3));
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4.outgoingTransitions = new ArrayList<Transition>(Arrays.asList(CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_MSED_S4ToMSED_S3));
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime.initialState = CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_S7;
CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime.currentState = CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime_S7;
	CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead.connect(CPUprotection_0_mainBlock_overHeatProtection_13_start);
	CPUprotection_0_mainBlock_normalTemp_12intermediate_allEvents1.connect(CPUprotection_0_mainBlock_switchCPUState_9_occurs,CPUprotection_0_mainBlock_switchCPUState_9_occurs);
	CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne.connect(CPUprotection_0_mainBlock_init_10_entering,CPUprotection_0_mainBlock_init_10_entering,CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite);
	CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait.connect(CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail, true, CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqTail);
	CPUprotection_0_mainBlock_normalToTooHot_5intermediate_otherFireFromTheSameState31.connect(CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire,CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire);
	CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition21.connect(CPUprotection_0_mainBlock_initToNormal_3_fire,CPUprotection_0_mainBlock_tooHotToNormal_6_fire);
	CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead.connect(CPUprotection_0_mainBlock_CPUprotection_0_start);
	CPUprotection_0_mainBlock_init_10intermediate_allFiredoutgoingTransition1.connect(CPUprotection_0_mainBlock_initToNormal_3_fire,CPUprotection_0_mainBlock_initToNormal_3_fire);
	CPUprotection_0_mainBlock_normalTemp_12_Union_eventsOrLocalTime.connect(CPUprotection_0_mainBlock_seconds_1_ticks,CPUprotection_0_mainBlock_normalTemp_12intermediate_allEvents1);
	CPUprotection_0_mainBlock_normalToTooHot_5_Union_trueOrFalse.connect(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedTrue,CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedFalse);
	CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait.connect(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail, true, CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqTail);
	CPUprotection_0_mainBlock_switchCPUState_9intermediate_AllTriggeringOccurrences1.connect(CPUprotection_0_mainBlock_normalToTooHot_5_fire,CPUprotection_0_mainBlock_tooHotToNormal_6_fire);
	CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne.connect(CPUprotection_0_mainBlock_normalTemp_12_entering,CPUprotection_0_mainBlock_normalTemp_12_entering,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_seqOneInfinite);
	CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard.connect(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedTrue,CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedFalse);
	CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne.connect(CPUprotection_0_mainBlock_normalTemp_12_entering,CPUprotection_0_mainBlock_normalTemp_12_entering,CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite);
	CPUprotection_0_mainBlock_tooHot_11_Union_eventsOrLocalTime.connect(CPUprotection_0_mainBlock_seconds_1_ticks,CPUprotection_0_mainBlock_tooHot_11intermediate_allEvents1);
	CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard.connect(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedTrue,CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedFalse);
	CPUprotection_0_mainBlock_selfLoopTooHot_2intermediate_otherFireFromTheSameState1.connect(CPUprotection_0_mainBlock_tooHotToNormal_6_fire,CPUprotection_0_mainBlock_tooHotToNormal_6_fire);
	CPUprotection_0_mainBlock_tooHotToNormal_6intermediate_otherFireFromTheSameState31.connect(CPUprotection_0_mainBlock_selfLoopTooHot_2_fire,CPUprotection_0_mainBlock_selfLoopTooHot_2_fire);
	CPUprotection_0_mainBlock_tooHotToNormal_6_Union_trueOrFalse.connect(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedTrue,CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedFalse);
	CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait.connect(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail, true, CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_SeqTail);
	CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition1.connect(CPUprotection_0_mainBlock_initToNormal_3_fire,CPUprotection_0_mainBlock_tooHotToNormal_6_fire);
	CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne.connect(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate,CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite);
	CPUprotection_0_mainBlock_tooHot_11intermediate_allEvents1.connect(CPUprotection_0_mainBlock_switchCPUState_9_occurs,CPUprotection_0_mainBlock_switchCPUState_9_occurs);
	CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition1.connect(CPUprotection_0_mainBlock_normalToTooHot_5_fire,CPUprotection_0_mainBlock_selfLoopTooHot_2_fire);
	CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne.connect(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate,CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_seqOneInfinite);
	CPUprotection_0_mainBlock_selfLoopNormalTemp_4intermediate_otherFireFromTheSameState1.connect(CPUprotection_0_mainBlock_normalToTooHot_5_fire,CPUprotection_0_mainBlock_normalToTooHot_5_fire);
	CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat.connect(CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat, false, null);
	CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne.connect(CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition1,CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition1,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_seqOneInfinite);
	CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead.connect(CPUprotection_0_mainBlock_seconds_1_ticks);
	CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne.connect(CPUprotection_0_mainBlock_tooHot_11_entering,CPUprotection_0_mainBlock_tooHot_11_entering,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_seqOneInfinite);
	CPUprotection_0_mainBlock_normalTemp_12intermediate_allFiredoutgoingTransition1.connect(CPUprotection_0_mainBlock_normalToTooHot_5_fire,CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire);
	CPUprotection_0_mainBlock_CPUprotection_0intermediate_allStartTFSM1.connect(CPUprotection_0_mainBlock_overHeatProtection_13_start,CPUprotection_0_mainBlock_overHeatProtection_13_start);
	CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition21.connect(CPUprotection_0_mainBlock_normalToTooHot_5_fire,CPUprotection_0_mainBlock_selfLoopTooHot_2_fire);
	CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition23.connect(CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition21,CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire);
	CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne.connect(CPUprotection_0_mainBlock_tooHot_11_entering,CPUprotection_0_mainBlock_tooHot_11_entering,CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakseqOneInfinite);
	CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne.connect(CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition3,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition3,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_seqOneInfinite);
	CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled.connect(CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition23,CPUprotection_0_mainBlock_normalTemp_12_Union_eventsOrLocalTime);
	CPUprotection_0_mainBlock_tooHot_11intermediate_allFiredoutgoingTransition1.connect(CPUprotection_0_mainBlock_tooHotToNormal_6_fire,CPUprotection_0_mainBlock_selfLoopTooHot_2_fire);
	CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled.connect(CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition21,CPUprotection_0_mainBlock_tooHot_11_Union_eventsOrLocalTime);
	CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat.connect(CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat, false, null);
	CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition3.connect(CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition1,CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire);
		CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne.connect(new ArrayList<Case>(Arrays.asList(
			new Case(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death, CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_21),
			new Case(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait, CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_19))));
		CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne.connect(new ArrayList<Case>(Arrays.asList(
			new Case(CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death, CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_18),
			new Case(CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait, CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_15))));
		CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne.connect(new ArrayList<Case>(Arrays.asList(
			new Case(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death, CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_16),
			new Case(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait, CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_25))));
		CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst.connect(CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne);
		CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime.connect(CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat);
		CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick.connect(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne);
		CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime.connect(CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat);
		CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst.connect(CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne);
	SortedSet<Constraint> sortedConstraints = new TreeSet<Constraint>(Arrays.asList(CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime4_5,CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime1_5,CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime2_5,CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime2_3,CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime1_2,CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime1_4,CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime3_5,CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime1_3,CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime3_4,CPUprotection_0_mainBlock_overHeatProtection_13intermediate_oneTransitionAtATime2_4,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_EitherFalseOrTrue,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_EitherFalseOrTrue,CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1PrecedesC2,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c1PrecedesC2,CPUprotection_0_mainBlock_CPUprotection_0Coincides_firstOnlyOnce,CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1PrecedesC2,CPUprotection_0_mainBlock_CPUprotection_0Precedes_startTimedSystemBeforeAllStartTFSM,CPUprotection_0_mainBlock_init_10Coincides_firingATransitionAlternatesWithLeavingState,CPUprotection_0_mainBlock_normalTemp_12Coincides_firingATransitionAlternatesWithLeavingState,CPUprotection_0_mainBlock_initToNormal_3Coincides_TransientInitTransition2,CPUprotection_0_mainBlock_initToNormal_3Coincides_TransientInitTransition,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c1PrecedesC2,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1PrecedesC2,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c2precedesC1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c2precedesC1DelayedByOne,CPUprotection_0_mainBlock_normalToTooHot_5Coincides_EvaluateGuardWhenEnteringState,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c1PrecedesC2,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c2precedesC1DelayedByOne,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1PrecedesC2,CPUprotection_0_mainBlock_tooHot_11Coincides_firingATransitionAlternatesWithLeavingState,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c2precedesC1DelayedByOne,CPUprotection_0_mainBlock_switchCPUState_9FSMEventRendezVous_occursWhenSolicitate_FSMEventRendezVousDef_RendezVous,CPUprotection_0_mainBlock_tooHotToNormal_6Coincides_EvaluateGuardWhenEnteringState,CPUprotection_0_mainBlock_overHeatProtection_13Precedes_noLocalTimeBeforeStart,CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c2precedesC1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c2precedesC1DelayedByOne,CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c2precedesC1DelayedByOne,CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1PrecedesC2,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c1PrecedesC2,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c2precedesC1DelayedByOne,CPUprotection_0_mainBlock_overHeatProtection_13Coincides_firstOnlyOnce,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c2precedesC1DelayedByOne,CPUprotection_0_mainBlock_overHeatProtection_13Coincides_firstIsInitialState,CPUprotection_0_mainBlock_selfLoopNormalTemp_4TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition,CPUprotection_0_mainBlock_tooHotToNormal_6EventGuardedTransition_fireWhenRestrueOccursVariousTransition,CPUprotection_0_mainBlock_selfLoopTooHot_2TemporalGuardedTransition_fireWhenTemporalGuardHoldsVariousTransition,CPUprotection_0_mainBlock_normalToTooHot_5EventGuardedTransition_fireWhenRestrueOccursVariousTransition,CPUprotection_0_mainBlock_tooHotToNormal_6MicroStepEnforcement_evalGuardAnswerInNoTime,CPUprotection_0_mainBlock_normalToTooHot_5MicroStepEnforcement_evalGuardAnswerInNoTime,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_normalTemp_12intermediate_allEvents1,CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_normalToTooHot_5intermediate_otherFireFromTheSameState31,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition21,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_init_10intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_normalTemp_12_Union_eventsOrLocalTime,CPUprotection_0_mainBlock_normalToTooHot_5_Union_trueOrFalse,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_switchCPUState_9intermediate_AllTriggeringOccurrences1,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_tooHot_11_Union_eventsOrLocalTime,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard,CPUprotection_0_mainBlock_selfLoopTooHot_2intermediate_otherFireFromTheSameState1,CPUprotection_0_mainBlock_tooHotToNormal_6intermediate_otherFireFromTheSameState31,CPUprotection_0_mainBlock_tooHotToNormal_6_Union_trueOrFalse,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition1,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_tooHot_11intermediate_allEvents1,CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition1,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_selfLoopNormalTemp_4intermediate_otherFireFromTheSameState1,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_CPUprotection_0intermediate_allStartTFSM1,CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition21,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition23,CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled,CPUprotection_0_mainBlock_tooHot_11intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition3,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_normalTemp_12intermediate_allEvents1,CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_normalToTooHot_5intermediate_otherFireFromTheSameState31,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition21,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_init_10intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_normalTemp_12_Union_eventsOrLocalTime,CPUprotection_0_mainBlock_normalToTooHot_5_Union_trueOrFalse,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_switchCPUState_9intermediate_AllTriggeringOccurrences1,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_tooHot_11_Union_eventsOrLocalTime,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard,CPUprotection_0_mainBlock_selfLoopTooHot_2intermediate_otherFireFromTheSameState1,CPUprotection_0_mainBlock_tooHotToNormal_6intermediate_otherFireFromTheSameState31,CPUprotection_0_mainBlock_tooHotToNormal_6_Union_trueOrFalse,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition1,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick,CPUprotection_0_mainBlock_tooHot_11intermediate_allEvents1,CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition1,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_selfLoopNormalTemp_4intermediate_otherFireFromTheSameState1,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_CPUprotection_0intermediate_allStartTFSM1,CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition21,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition23,CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail,CPUprotection_0_mainBlock_tooHot_11intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition3));
	Set<Clock> allClocks = new HashSet<Clock>(Arrays.asList(CPUprotection_0_mainBlock_initToNormal_3_fire,CPUprotection_0_mainBlock_switchCPUState_9_occurs,CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedTrue,CPUprotection_0_mainBlock_init_10_entering,CPUprotection_0_mainBlock_CPUprotection_0_start,CPUprotection_0_mainBlock_seconds_1_ticks,CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedTrue,CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedFalse,CPUprotection_0_mainBlock_normalTemp_12_entering,CPUprotection_0_mainBlock_overHeatProtection_13_start,CPUprotection_0_mainBlock_tooHot_11_entering,CPUprotection_0_mainBlock_selfLoopTooHot_2_fire,CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate,CPUprotection_0_mainBlock_normalToTooHot_5_fire,CPUprotection_0_mainBlock_init_10_leaving,CPUprotection_0_mainBlock_normalTemp_12_leaving,CPUprotection_0_mainBlock_tooHot_11_leaving,CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire,CPUprotection_0_mainBlock_tooHotToNormal_6_fire,CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedFalse,CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_normalTemp_12intermediate_allEvents1,CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_normalToTooHot_5intermediate_otherFireFromTheSameState31,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition21,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_init_10intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_normalTemp_12_Union_eventsOrLocalTime,CPUprotection_0_mainBlock_normalToTooHot_5_Union_trueOrFalse,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_switchCPUState_9intermediate_AllTriggeringOccurrences1,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_tooHot_11_Union_eventsOrLocalTime,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard,CPUprotection_0_mainBlock_selfLoopTooHot_2intermediate_otherFireFromTheSameState1,CPUprotection_0_mainBlock_tooHotToNormal_6intermediate_otherFireFromTheSameState31,CPUprotection_0_mainBlock_tooHotToNormal_6_Union_trueOrFalse,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition1,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_tooHot_11intermediate_allEvents1,CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition1,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_selfLoopNormalTemp_4intermediate_otherFireFromTheSameState1,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_CPUprotection_0intermediate_allStartTFSM1,CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition21,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition23,CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled,CPUprotection_0_mainBlock_tooHot_11intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition3,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_normalTemp_12intermediate_allEvents1,CPUprotection_0_mainBlock_init_10WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_normalToTooHot_5intermediate_otherFireFromTheSameState31,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition21,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_init_10intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_normalTemp_12_Union_eventsOrLocalTime,CPUprotection_0_mainBlock_normalToTooHot_5_Union_trueOrFalse,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_switchCPUState_9intermediate_AllTriggeringOccurrences1,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne,CPUprotection_0_mainBlock_normalTemp_12WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_tooHot_11_Union_eventsOrLocalTime,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_ResEvalGuard,CPUprotection_0_mainBlock_selfLoopTooHot_2intermediate_otherFireFromTheSameState1,CPUprotection_0_mainBlock_tooHotToNormal_6intermediate_otherFireFromTheSameState31,CPUprotection_0_mainBlock_tooHotToNormal_6_Union_trueOrFalse,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_concatWait,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition1,CPUprotection_0_mainBlock_checkTempTooHot_7BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick,CPUprotection_0_mainBlock_tooHot_11intermediate_allEvents1,CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition1,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_death,CPUprotection_0_mainBlock_checkTempTooHot_8BooleanGuardedTransitionRule_fireEvaluationAndResult_BooleanGuardedTransitionRuleDef_evalCausesResult_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_selfLoopNormalTemp_4intermediate_otherFireFromTheSameState1,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_waitHead,CPUprotection_0_mainBlock_tooHot_11Alternates_stateEntering2_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_CPUprotection_0intermediate_allStartTFSM1,CPUprotection_0_mainBlock_tooHot_11intermediate_allInputTransition21,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_firstLocalTimeTick_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail,CPUprotection_0_mainBlock_CPUprotection_0_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition23,CPUprotection_0_mainBlock_tooHot_11WeakAlternates_enterOnceBeforeToLeave_WeakAlternatesDef_WeakAlt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12Alternates_stateEntering1_AlternatesDef_Alt_c1DelayedByOne,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_FilterBy_FilterBySeqTail,CPUprotection_0_mainBlock_tooHot_11intermediate_allFiredoutgoingTransition1,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_OneSampled,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst_OneTickAndNoMoreDef_OneTickAndNoMore_onlyOne_FilterByDef_killIt,CPUprotection_0_mainBlock_normalTemp_12_SampledOn_allInputsSampledOneventsOrTime,CPUprotection_0_mainBlock_tooHot_11_SampledOn_allInputsSampledOneventsOrTime_SampledOnDef_SampledOnrootConcat,CPUprotection_0_mainBlock_overHeatProtection_13_OneTickAndNoMore_onlyOneFirst,CPUprotection_0_mainBlock_normalTemp_12intermediate_allInputTransition3));
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_seconds_1_ticks, "//@tfsms.0/@localClock");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_seconds_1_ticks, "ticks");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_selfLoopTooHot_2_fire, "//@tfsms.0/@ownedTransitions.4");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_selfLoopTooHot_2_fire, "fire");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_initToNormal_3_fire, "//@tfsms.0/@ownedTransitions.0");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_initToNormal_3_fire, "fire");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire, "//@tfsms.0/@ownedTransitions.3");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_selfLoopNormalTemp_4_fire, "fire");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_normalToTooHot_5_fire, "//@tfsms.0/@ownedTransitions.1");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_normalToTooHot_5_fire, "fire");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_tooHotToNormal_6_fire, "//@tfsms.0/@ownedTransitions.2");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_tooHotToNormal_6_fire, "fire");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate, "//@tfsms.0/@ownedTransitions.1/@ownedGuard");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate, "evaluate");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate, "//@tfsms.0/@ownedTransitions.2/@ownedGuard");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate, "evaluate");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_switchCPUState_9_occurs, "//@globalEvents.0");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_switchCPUState_9_occurs, "occurs");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_init_10_entering, "//@tfsms.0/@ownedStates.2");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_init_10_entering, "onEnter");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_tooHot_11_entering, "//@tfsms.0/@ownedStates.1");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_tooHot_11_entering, "onEnter");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_normalTemp_12_entering, "//@tfsms.0/@ownedStates.0");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_normalTemp_12_entering, "onEnter");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_overHeatProtection_13_start, "//@tfsms.0");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_overHeatProtection_13_start, "initialize");
theEngine.clockToEObject.put(CPUprotection_0_mainBlock_CPUprotection_0_start, "/");
theEngine.clockToMethodName.put(CPUprotection_0_mainBlock_CPUprotection_0_start, "initialize");



	org.eclipse.emf.ecore.resource.ResourceSet resourceSet = new org.eclipse.emf.ecore.resource.impl.ResourceSetImpl();

	resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			"tfsm", new org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl());

	// Register the package -- only needed for stand-alone!
	 org.eclipse.gemoc.example.moccml.tfsm.tfsm.TfsmPackage tfsmpackage =  org.eclipse.gemoc.example.moccml.tfsm.tfsm.TfsmPackage.eINSTANCE;

RegularFile model = new RegularFile("overHeatControler.tfsm");
	try {
		JavaResource.exportToFile("/overHeatControler.tfsm", model);
	} catch (java.io.IOException e) {
		e.printStackTrace();
	}	org.eclipse.emf.ecore.resource.Resource resource = resourceSet.getResource(org.eclipse.emf.common.util.URI.createFileURI("overHeatControler.tfsm"), true);
	
	org.eclipse.gemoc.example.moccml.tfsm.tfsm.impl.TimedSystemImpl root = (org.eclipse.gemoc.example.moccml.tfsm.tfsm.impl.TimedSystemImpl) resource.getContents().get(0);
theEngine.forcedClock.put(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate, new ForceCase(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedTrue, true));
theEngine.forcedClock.put(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate, new ForceCase(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedTrue, true));
theEngine.forcedClock.put(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluate, new ForceCase(CPUprotection_0_mainBlock_checkTempTooHot_7_evaluatedFalse, false));
theEngine.forcedClock.put(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluate, new ForceCase(CPUprotection_0_mainBlock_checkTempTooHot_8_evaluatedFalse, false));

	theEngine.populateVariableMapFromAnnotations(root);

    if(rtdCouples.size() > 0) {
    	theEngine.initializeCSV(rtdCouples, root);
		theEngine.generatePlotScript(rtdCouples, root);
    }

   	Solver solver = new Solver(allClocks, sortedConstraints);
   	BlockingQueue<Exception> exceptionQueue = new ArrayBlockingQueue<>(1);
   
   	
   	Thread solverThread = theEngine.startSolverInThread(exceptionQueue, solver, nbSteps, logStepsOnly, logClocks, rtdCouples, resource, root, theEngine);
    solverThread.start();
    ServerSocket server = null;
 try {
	server = new ServerSocket(39635);
	
	Socket clientSocket = server.accept();
	
	ObjectInputStream cin = new ObjectInputStream(clientSocket.getInputStream());
	ObjectOutputStream cout = new ObjectOutputStream(clientSocket.getOutputStream());
	
	Object clientCommand = cin.readObject();
	while(!theEngine.simulationEnded.tryAcquire() || clientCommand instanceof StopCommand) { 
		System.out.println("Command received: "+clientCommand);
		if(clientCommand instanceof DoStepCommand) {
			theEngine.timeBeforeDoStep = theEngine.lastknownTime.doubleValue();
			System.out.println("DoStep starts @"+theEngine.timeBeforeDoStep);
			theEngine.currentPredicate = ((DoStepCommand) clientCommand).predicate;
			StopCondition stopCond = theEngine.doStep(theEngine.currentPredicate);
			System.out.println("DoStep stops @"+stopCond.timeValue+" due to "+stopCond.stopReason);
			cout.writeObject(stopCond);
		}	
		if(clientCommand instanceof GetVariableCommand) {
			String varQN = ((GetVariableCommand) clientCommand).variableQualifiedName;
			Object varValue= theEngine.getVariable(varQN);
			cout.writeObject(varValue);
		}
		if(clientCommand instanceof SetVariableCommand) {
			String varQN = ((SetVariableCommand) clientCommand).variableQualifiedName;
			Object newValue = ((SetVariableCommand) clientCommand).newValue; 
			Boolean res = theEngine.setVariable(varQN, newValue);
			cout.writeObject(res);
		}
		System.out.println("wait for a new command.");
		clientCommand = cin.readObject();
	}
    server.close();
	solverThread.join();
 	} catch (IOException | InterruptedException | ClassNotFoundException e) {
	 e.printStackTrace();
	 }
    
    
    model.delete();    
    System.out.println();
    System.out.println("/**********************\n");
    System.out.println("*     simulation ends      *\n");
    System.out.println("**********************/");
    System.out.println();	
    return;
    }
	
	Semaphore startDoStepSemaphore  = new Semaphore(0);
	Semaphore finishDoStepSemaphore = new Semaphore(0);
	Semaphore simulationEnded       = new Semaphore(0);
	Map<String, Object> qualifiedNameToEObject = new HashMap<>(); //initialized in main, used in GetVariable
	Map<String, Method> qualifiedNameToMethod = new HashMap<>(); 
	CoordinationPredicate currentPredicate;
	AtomicDouble lastknownTime = new AtomicDouble(-1.0);
	double timeBeforeDoStep = -1;
	StopCondition lastStopcondition = null;
	Object timeObject = null;
	Class<?> timeType = null;

	/**
	 * this method go through all the model and its aspects and populate the {@link #qualifiedNameToEObject} map with Variables annotated as @Input or @Output
	 */
	private void populateVariableMapFromAnnotations(EObject root) {
		TreeIterator<EObject> contentIt = root.eAllContents();
		while(contentIt.hasNext()) {
			EObject eo = contentIt.next();
//			List<Class<?>> aspects = K3DslHelper.getAspectsOn(languageName, eo.getClass());
			loopAspects: for(Class<?> aspect : savedAspects) {
				Aspect anAspect = aspect.getAnnotation(Aspect.class);
				for (Class<?> ieo: eo.getClass().getInterfaces()) {
					if (anAspect.className().getName().compareTo(ieo.getName()) != 0){
						continue loopAspects;
					}
				}
				try {
					Class<?> propertyClass = this.getClass().getClassLoader().loadClass(aspect.getTypeName()+aspect.getSimpleName()+"Properties");
					for (Field f : propertyClass.getFields()) {
						for (Method m : aspect.getMethods()) {
							if (m.getName().compareTo(f.getName()) == 0) {
								if (m.isAnnotationPresent(Input.class)) {
									qualifiedNameToEObject.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName(), eo);
									if (m.getParameterCount() == 2) {
										qualifiedNameToMethod.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName()+"::set", m);
									}
								}
								if (m.isAnnotationPresent(Output.class)) {
									qualifiedNameToEObject.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName(), eo);
									if (m.getParameterCount() == 1) {
										qualifiedNameToMethod.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName()+"::get", m);
									}
								}
								if (m.isAnnotationPresent(Time.class)) {
									try {
										timeObject = this.execute(eo, f.getName(), null);  
										timeType = f.getType();
									} catch (IllegalArgumentException e) {
										e.printStackTrace();
									}
								}
							}
						}
					}
				} catch (ClassNotFoundException | SecurityException e1) {
					e1.printStackTrace();
				}
			}
		}
			
	}
	
	public Object getVariable(String varQN) {
		Object var = qualifiedNameToEObject.get(varQN);
		Method method = qualifiedNameToMethod.get(varQN+"::get");
		if (var == null || method == null) {
			return false;
		}
		try {
			StepManagerRegistry.getInstance().unregisterManager(this);
			Object res = method.invoke(null, var);
			StepManagerRegistry.getInstance().registerManager(this);
			return res;
		} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	public boolean setVariable(String varQN, Object newValue) {
		Object var = qualifiedNameToEObject.get(varQN);
		Method method = qualifiedNameToMethod.get(varQN+"::set");
		if (var == null || method == null) {
			return false;
		}
		try {
			StepManagerRegistry.getInstance().unregisterManager(this);
			this.execute(var, method.getName(), Arrays.asList(newValue));
			StepManagerRegistry.getInstance().registerManager(this);
		} catch (SecurityException | IllegalArgumentException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	private void decrement(Semaphore sem) {
		try {
			sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void increment(Semaphore sem) {
			sem.release();
	}
	
	
	public StopCondition doStep(CoordinationPredicate predicate) {
		increment(startDoStepSemaphore);
		decrement(finishDoStepSemaphore);
		return lastStopcondition;
		//should do something Here according to what has been done during the step (according to the stopCondition)
	}
	
	public Thread startSolverInThread(BlockingQueue<Exception> exceptionQueue, Solver solver, int nbSteps, boolean logStepsOnly, 
									  Boolean logClocks, ArrayList<ArrayList<String>> rtdCouples, org.eclipse.emf.ecore.resource.Resource resource,
									  EObject root, CompiledExecutionEngine theEngine) {
		return new Thread(() ->
		// since aspect's methods are static, first arg is null
		{
			try {
				decrement(startDoStepSemaphore);
				for (int currentStep = 0; currentStep < nbSteps; currentStep++) {
					if (logStepsOnly || logClocks)
						System.out.println("-------------step " + currentStep + "\n");

					System.out.println("0 "+ clockToForceNext);
					solver.doStep(theEngine.clockToForceNext);
					
					System.out.println("1");
					
					checkLogicalStepStopPredicate();
					System.out.println("2");
					checkEventStopPredicate(solver, resource);
System.out.println("3");
					this.executeDSAsAndLog(logClocks, clockToMethodName, clockToEObject, resource, solver);
	System.out.println("4");
					if (rtdCouples.size() > 0) {
						this.logRTDs(rtdCouples, root);
					}
				}
System.out.println("5");
			} catch (/* IllegalAccessException | */IllegalArgumentException /* | InvocationTargetException */ e) {
				exceptionQueue.offer(e);
			}
			increment(simulationEnded);
		});
	}


	private void checkLogicalStepStopPredicate() {
		if (this.currentPredicate != null
			&& this.currentPredicate.contains(StopReason.LOGICALSTEP,null, null, null))
		{
			this.currentPredicate.getLogicalStepPredicate().nbStepsRequired--;
			if(this.currentPredicate.getLogicalStepPredicate().nbStepsRequired == 0) {
				// struct to communicate the stop condition to the server
				lastStopcondition = new StopCondition(StopReason.LOGICALSTEP, null, null, lastknownTime.floatValue()); // always lastKnownTime ?
				increment(finishDoStepSemaphore); // give control back to the caller.
				// wait for stuff to be done in the server before to continue
				decrement(startDoStepSemaphore); // waiting for the caller to do something
			}
		}
	}


	private void checkEventStopPredicate(Solver solver, Resource resource) {
		if (this.currentPredicate != null) {
			for (Clock pt_c : solver.clocks) {
				String uriFragment = clockToEObject.get(pt_c);
				String supportQN = "";
				if (uriFragment != null) {
					EObject support = resource.getEObject(uriFragment);
					supportQN = EcoreQNHelper.getQualifiedName(support);
				}
				if (pt_c.status == QuantumBoolean.TRUE && this.currentPredicate.contains(StopReason.EVENT,null, supportQN, pt_c.name)) {
					// struct to communicate the stop condition to the server
					lastStopcondition = new StopCondition(StopReason.EVENT, clockToEObject.get(pt_c),
							pt_c.name, lastknownTime.floatValue()); // always lastKnownTime ?
					increment(finishDoStepSemaphore); // give control back to the caller.
					// wait for stuff to be done in the server before to continue
					decrement(startDoStepSemaphore); // waiting for the caller to do something
				}
			}
		}
	}
	
	
	public void executeDSAsAndLog(boolean logClocks, Map<Clock, String> clockToMethodName, Map<Clock, String> clockToEObject,
			org.eclipse.emf.ecore.resource.Resource resource, Solver solver) {
		clockToForceNext.clear();
		StepManagerRegistry.getInstance().registerManager(this);
        for(Clock pt_c: solver.clocks) {
            if (! (pt_c instanceof Constraint)) {
               if (logClocks) System.out.println(pt_c);
            	if (pt_c.status == QuantumBoolean.TRUE) {
            		String eobjectURIFrag = clockToEObject.get(pt_c);
            		if (eobjectURIFrag != null) {
            			org.eclipse.emf.ecore.EObject eo = resource.getEObject(eobjectURIFrag);
            			Object oRes = this.execute(eo, clockToMethodName.get(pt_c), null);
            			if ((oRes instanceof Boolean) && forcedClock.containsKey(pt_c)) {
            				boolean bRes = (Boolean)oRes;
            				for(ForceCase fc : forcedClock.get(pt_c)) {
            					if (fc.associatedResValue == bRes) {
            						clockToForceNext.add(fc.clockToForce);
            					}
            				}
            			}
            		}
            	}
            }
        }        StepManagerRegistry.getInstance().unregisterManager(this);
	}

	@Override
	public void readyToRead(EObject caller, Object propContainer, StepCommand command, String className, String propertyName) {
		System.out.println("in readyToRead("+propertyName+") on "+EcoreQNHelper.getQualifiedName(caller));
		if(currentPredicate != null && currentPredicate.contains(StopReason.READYTOREAD ,caller,className,propertyName)) {
			//struct to communicate the stop condition to the server
			lastStopcondition = new StopCondition(StopReason.READYTOREAD, /*caller, propContainer, command,*/ EcoreQNHelper.getQualifiedName(caller), propertyName, lastknownTime.floatValue()); //always lastKnownTime ?
			increment(finishDoStepSemaphore); // give control back to the caller.
			//wait for stuff to be done in the server before to continue
			decrement(startDoStepSemaphore); //waiting for the caller to do something
		}
		command.execute();
	}

	

	@Override
	public void justUpdated(EObject caller, Object propContainer, StepCommand command, String className, String propertyName) {
		System.out.println("in justUpdated("+propertyName+") on "+EcoreQNHelper.getQualifiedName(caller));
		command.execute();
		if(currentPredicate != null && currentPredicate.contains(StopReason.UPDATE ,caller,className,propertyName)) {
			//struct to communicate the stop condition to the server
			lastStopcondition = new StopCondition(StopReason.UPDATE, /*caller, propContainer, command,*/ EcoreQNHelper.getQualifiedName(caller), propertyName, lastknownTime.floatValue()); //always lastKnownTime ?
			increment(finishDoStepSemaphore); // give control back to the caller.
			//Optionally waiting for the caller to do something before to continue (logically we wait for antoher doStep)
			decrement(startDoStepSemaphore); 
		}
	}

	@Override
	public void timePassed(EObject caller, Object propContainer, StepCommand command, String className, double value) {
		System.out.println("in timePassed("+value+") on "+EcoreQNHelper.getQualifiedName(caller));
		command.execute();
		lastknownTime.set(value);
		
		if (currentPredicate != null && currentPredicate.contains(StopReason.TIME ,caller,className, Double.toString(value))) {
			double delta = ((TemporalPredicate)currentPredicate.getTemporalPredicate()).deltaT;
			if ((value - timeBeforeDoStep) >= delta){
				//struct to communicate the stop condition to the server
				lastStopcondition = new StopCondition(StopReason.TIME, /*caller, propContainer, command, */EcoreQNHelper.getQualifiedName(caller), "time", lastknownTime.floatValue()); //always lastKnownTime ?
				increment(finishDoStepSemaphore); // give control back to the caller.
				//wait for stuff to be done in the server before to continue
				decrement(startDoStepSemaphore); //waiting for the caller to do something
			}
		}
	}


	@Override
	/*
	 * This is the operation called from K3 code. We use this callback to pass the
	 * command to the generic executeOperation operation. (non-Javadoc)
	 * 
	 * @see fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager#
	 * executeStep(java.lang.Object,
	 * fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand,
	 * java.lang.String)
	 */
	public void executeStep(final Object caller, final StepCommand command, String className, final String methodName) {
		System.out.println("in executeDebugStep in "+className+"::"+methodName+" on "+caller);
		if (currentPredicate != null && currentPredicate.contains(StopReason.DEBUGSTEP, null ,null , null)) {
				//struct to communicate the stop condition to the server
				lastStopcondition = new StopCondition(StopReason.DEBUGSTEP, caller.toString(), "time", lastknownTime.floatValue()); //always lastKnownTime ?
				increment(finishDoStepSemaphore); // give control back to the caller.
				//wait for stuff to be done in the server before to continue
				decrement(startDoStepSemaphore); //waiting for the caller to do something
				command.execute();
		}
	}

@Override
	public boolean canHandle(Object caller) {
		return true;
	}


	@Override
	public String engineKindName() {
		return "Compiled Coordination Engine";	}

	//unused from inheritance.... yet

	@Override
	protected void executeEntryPoint() {
	}
	@Override
	protected void initializeModel() {
	}
	@Override
	protected void prepareEntryPoint(GenericModelExecutionContext<ISequentialRunConfiguration> arg0) {
	}
	@Override
	protected void prepareInitializeModel(GenericModelExecutionContext<ISequentialRunConfiguration> arg0) {
    }
}
