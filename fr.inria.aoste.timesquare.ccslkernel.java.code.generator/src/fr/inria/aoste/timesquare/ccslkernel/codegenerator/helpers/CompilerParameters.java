/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.aoste.timesquare.ccslkernel.codegenerator.helpers;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that contains all the needed parameters and information needed to compile a CCSL
 * model using {@link Compiler#compile(org.eclipse.emf.ecore.resource.Resource)}.
 * 
 * @author nchleq
 * @see CompilerParameters#outputProjectName
 * @see CompilerParameters#outputProjectNamePrefix
 * @see CompilerParameters#packagePrefix
 */
public class CompilerParameters {
	
	/**
	 * Gives the name of the project to create or to modify. Classes will be generated in packages
	 * that have the value of the {@link #packagePrefix} field as prefix.
	 */
	public String outputProjectName = null;
	
	/**
	 * If the {@link #outputProjectName} field is null then the {@link #outputProjectNamePrefix} is
	 * used to derive a project name by using this prefix and adding the CCSL model name at the
	 * end. No dot is inserted between the two components, if you want to have one, take care that
	 * {@link #outputProjectNamePrefix} ends with a dot.
	 */
	public String outputProjectNamePrefix = null;

	/**
	 * 
	 */
	public String packagePrefix = "";
	
	static public String genSourceFolder = "src-gen";
	static public String sourceFolder = "src";
	
	static public int indentStep = 4;
	
	public boolean inlineConcatExpression = true;
	
	public boolean modelsAreSingleton = false;
	
	public boolean generateTests = false;

	public boolean generateMain = false;

	static public String FILTERBYNAME = "FilterBy";
	
	public boolean useRuntimeFilterBy = false;
	
	public static final String[] requiredDependencies = {
				"org.eclipse.gemoc.moccml.compiled.library",
				"fr.inria.diverse.k3.al.annotationprocessor.plugin" };
	
	public List<String> otherDependencies = new ArrayList<String>();

}
