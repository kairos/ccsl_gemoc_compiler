package fr.inria.aoste.timesquare.ccslkernel.codegenerator.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.moccml.constraint.ccslmoc.model.moccml.moccml.StateMachineRelationDefinition;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.AbstractAction;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.AbstractGuard;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.Guard;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.IntegerAssignement;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.State;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.Transition;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.Trigger;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
//import org.jgrapht.traverse.TopologicalOrderIterator;

import fr.inria.aoste.timesquare.ccslkernel.codegenerator.helpers.ActionHelper;
import fr.inria.aoste.timesquare.ccslkernel.codegenerator.helpers.CompilerParameters;
import fr.inria.aoste.timesquare.ccslkernel.codegenerator.helpers.GuardHelper;
import fr.inria.aoste.timesquare.ccslkernel.codegenerator.helpers.PluginProjectHelper;
import fr.inria.aoste.timesquare.ccslkernel.codegenerator.switches.ClassicalExpressionBuilder;
import fr.inria.aoste.timesquare.ccslkernel.codegenerator.switches.KernelExpressionBuilder;
import fr.inria.aoste.timesquare.ccslkernel.codegenerator.switches.KernelRelationBuilder;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import toools.io.file.RegularFile;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class CCSLJavaCodeGenerator extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public CCSLJavaCodeGenerator(String projectName, boolean withAspectManagement, boolean withCoordinationAPI) {
		this.projectName = projectName;
		this.withAspectManagement = withAspectManagement;
		this.withCoordinationAPI = withCoordinationAPI;
	}
	
	public CCSLJavaCodeGenerator(String projectName) {
		this.projectName = projectName;
		this.withAspectManagement = false;
		this.withCoordinationAPI = false;
	}
	
	/**
	 * required by UI handler :-/
	 */
	public CCSLJavaCodeGenerator() {
		this.projectName = "fr.inria.kairos.generatedCode.automaticName_TOBECHANGED";
		this.withAspectManagement = false;
		this.withCoordinationAPI = false;
	}

	public String projectName = "";
	public boolean withAspectManagement = false;
	public boolean withCoordinationAPI = false;
	
	private IFile _ccslFile;
	public HashMap<InstantiatedElement, List<InstantiatedElement>> clockToRelationExpressions = new HashMap<InstantiatedElement, List<InstantiatedElement>>();
	private Set<InstantiatedElement> allRelationsButExclusions;
	private Set<InstantiatedElement> allExclusions;
	private Set<InstantiatedElement> allClocks;
	private Set<InstantiatedElement> allKernelExpressions;
	private Set<InstantiatedElement> allConstants;
	public IFolder srcFolder;
	private RegularFile mainFile;
	private IProject proj;
	private PluginProjectHelper pluginHelper;
	private Set<InstantiatedElement> allComplexExpressions;
	private Set<InstantiatedElement> allClassicalExpression;
	private HashSet<InstantiatedElement> allConditionalRelation;
	private UnfoldModel theUnfoldModel;
	private HashSet<InstantiatedElement> allStateMachineRelations;
	
	
	public IProject getProject() {
		return proj;
	}
	
	public void putElementInRelationExpression(InstantiatedElement key, InstantiatedElement value) {
	    List<InstantiatedElement> myClassList = this.clockToRelationExpressions.get(key);
	    if(myClassList == null) {
	        myClassList = new ArrayList<InstantiatedElement>();
	        this.clockToRelationExpressions.put(key,myClassList);
	    }
	    myClassList.add(value);
	}


	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
//		generator = new CCSLKernelCPPRewritingRuleConstructor();
		
		System.out.println("Java code generation started!");
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			if (((IStructuredSelection) selection).size() == 1) {
				Object selected = ((IStructuredSelection) selection).getFirstElement();
				if (selected instanceof IFile) {
					try {
						projectName = "fr.inria.kairos.moccml.generatedcode."+((IFile)selected).getName();
						createProjectAndGenerate((IFile) selected);
					} catch (CoreException e) {
						e.printStackTrace();
					}
					generateMainHeader(srcFolder);
					generateBeginOfMain("nameLanguage NotRelevant if not a gemoc project");
					generateEndOfMain();
				}
			}
		}
		return null;
	}


	/**
	 * create a java project and set srcFolder as well as mainFile
	 * @param ccslFile
	 * @param projectName
	 * @throws CoreException 
	 */
	public void createProjectAndGenerate(IFile ccslFile) throws CoreException {
		_ccslFile = ccslFile;
		String filename = _ccslFile.getFullPath().toString();
		
		theUnfoldModel = null;
		try {
			theUnfoldModel = UnfoldModel.unfoldModel(URI.createPlatformResourceURI(filename));
		} catch (IOException | UnfoldingException e1) {
			e1.printStackTrace();
		}
		pluginHelper = new PluginProjectHelper();
		//srcFolder
		proj = pluginHelper.createPluginProject(projectName);
		srcFolder = proj.getFolder(CompilerParameters.genSourceFolder);
		pluginHelper.addRequiredBundle(proj, "org.apache.commons.cli");
		pluginHelper.addRequiredBundle(proj, "org.eclipse.gemoc.moccml.compiled.library");
		pluginHelper.addRequiredBundle(proj, " fr.inria.aoste.utils.grph");
		if(withAspectManagement || withCoordinationAPI) { 
			pluginHelper.addRequiredBundle(proj, "fr.inria.diverse.k3.al.annotationprocessor.plugin");
			pluginHelper.addRequiredBundle(proj, "org.eclipse.gemoc.executionframework.engine");
			pluginHelper.addRequiredBundle(proj, "org.eclipse.gemoc.xdsmlframework.api");
			pluginHelper.addRequiredBundle(proj, "org.eclipse.emf.transaction");
			pluginHelper.addRequiredBundle(proj, "org.eclipse.gemoc.trace.gemoc.api");
			pluginHelper.addRequiredBundle(proj, "org.eclipse.gemoc.execution.commons.dostepparameters");
			pluginHelper.addRequiredBundle(proj, "com.google.guava");
		}
		//clean if exists
		try {
			for(IResource r : srcFolder.members()){
				r.delete(true, null);
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
		
		allStateMachineRelations = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if (ie.getDefinition() instanceof StateMachineRelationDefinition) {
				allStateMachineRelations.add(ie);
			}
		}
		
		allClocks = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if (ie.isRecursiveCall()) {
//				System.out.println("##############  recursive : "+ie.getInstantiationPath().getLast());
			}
			if (ie.isClock()) {
				allClocks.add(ie);
			}
		}
		
		allConstants = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if (ie.isPrimitiveElement() || ie.getValue() instanceof SequenceElement) {
				if(ie.getInstantiationPath().getLast() instanceof IntegerElement || ie.getValue() instanceof SequenceElement) {
					allConstants.add(ie);
				}else {	
					if(ie.getValue() != null) {
						allConstants.add(ie);
					}
				}
			}
		}
		
		allClassicalExpression = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if (ie.isClassicalExpression()) {
				allClassicalExpression.add(ie);
			}
			if (ie.isConditional()) {
				for(InstantiatedElement caze: ie.getConditionalCases()) {
					InstantiatedElement test = caze.getConditionTest();
					allClassicalExpression.add(test);
				}
			}
			
		}
		
		allExclusions = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if (!ie.isClock() && ie.isKernelRelation() && "Exclusion".equals(ie.getDeclaration().getName())) {
				allExclusions.add(ie);
			}
		}
		
		allRelationsButExclusions = new HashSet<InstantiatedElement>();
		addAllKernelRelationsButExclusions(theUnfoldModel.getInstantiationTree().lookupInstances(null));
		
		allConditionalRelation = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if ((!ie.isKernelRelation()) && ie.isRelation() && ie.isConditional()) {
				allConditionalRelation.add(ie);
			}
		}
		
		
		allKernelExpressions = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			System.out.print(ie);
			if (!ie.isClock() && ie.isKernelExpression()) {
				allKernelExpressions.add(ie);
			}else {
				if ((!ie.isConditional()) && ie.getRootExpression() != null) {
					allKernelExpressions.add(ie.getRootExpression());
				}
				if (ie.isConditional()) {
					for(InstantiatedElement caze: ie.getConditionalCases()) {
						if(caze.isKernelExpression()) {
							allKernelExpressions.add(caze);
						}
					}
				}
			}
		}
		
		allComplexExpressions = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			System.out.print(ie);
			if ((!ie.isClock()) && /*(!ie.isKernelExpression()) && */ie.isExpression()) {
				allComplexExpressions.add(ie);
//				System.out.println("  addedXX ");
			}else {
				if (ie.isConditional()) {
					for(InstantiatedElement caze: ie.getConditionalCases()) {
						if(!caze.isKernelExpression() && caze.isExpression()) {
							allComplexExpressions.add(caze);
						}
					}
				}
			}
		}
		

		mainFile = new RegularFile(srcFolder.getLocation().toPortableString()+"/CompiledExecutionEngine.java");
	}


	private void addAllKernelRelationsButExclusions(List<InstantiatedElement> list) {
		for(InstantiatedElement ie: list) {
			if (ie.isKernelRelation() && !"Exclusion".equals(ie.getDeclaration().getName())) {
				allRelationsButExclusions.add(ie);
			}
		}
	}
	
	
	
	
	public void generateMainHeader(IFolder srcFolder) {
		
		try {
			mainFile.append(("//============================================================================\n"
					+ "// Name        : main.cpp\n"
					+ "// Author      : Julien Deantoni\n"
					+ "// Version     : the best of ones I had :)\n"
					+ "// Copyright   : http://i3s.unice.fr/~deantoni\n"
					+ "// Description : Code generated for your CCSL specification, C++11 style\n"
					+ "//============================================================================\n"
					+ "\n" +
					"import java.util.ArrayList;\n" + 
					"import java.util.Arrays;\n" + 
					"import java.util.HashMap;\n" + 
					"import java.util.HashSet;\n" + 
					"import java.util.Map;\n" + 
					"import java.util.Set;\n" +
					"import java.util.SortedSet;\n" + 
					"import java.util.TreeSet;" + 
					"\n" + 
					"import org.apache.commons.cli.CommandLine;\n" + 
					"import org.apache.commons.cli.CommandLineParser;\n" + 
					"import org.apache.commons.cli.HelpFormatter;\n" + 
					"import org.apache.commons.cli.Option;\n" + 
					"import org.apache.commons.cli.Options;\n" + 
					"import org.apache.commons.cli.ParseException;\n" + 
					"import org.apache.commons.cli.PosixParser;\n" + 
					"import org.eclipse.gemoc.moccml.compiled.library.Clock;\n" + 
					"import org.eclipse.gemoc.moccml.compiled.library.Solver;\n" + 
					"import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;\n" + 
					"import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.*;\n" + 
					"import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.statemachine.State;\n" + 
					"import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.statemachine.StateMachine;\n" + 
					"import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.statemachine.Transition;\n" + 
					"import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.*;\n" + 
					"import org.eclipse.gemoc.moccml.compiled.library.utils.*;\n" + 
					"import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.ConditionalExpression.Case;\n" + 
					"import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.*;\n").getBytes());
			
			if(withCoordinationAPI) { 
				mainFile.append((
					"\n" +
					"import java.util.concurrent.ArrayBlockingQueue;\n" + 
					"import java.util.concurrent.BlockingQueue;\n" + 
					"import java.util.concurrent.Semaphore;\n" + 
					"import java.lang.reflect.Field;\n" +
					"import java.io.ObjectInputStream;\n" + 
					"import java.io.ObjectOutputStream;\n" + 
					"import java.net.ServerSocket;\n" + 
					"import java.net.Socket;" + 
					"import org.eclipse.gemoc.execution.commons.commands.DoStepCommand;\n" + 
					"import org.eclipse.gemoc.execution.commons.commands.GetVariableCommand;\n" + 
					"import org.eclipse.gemoc.execution.commons.commands.SetVariableCommand;\n" + 
					"import org.eclipse.gemoc.execution.commons.commands.StopCommand;\n" + 
					"import org.eclipse.gemoc.execution.commons.commands.StopCondition;\n" + 
					"import org.eclipse.gemoc.execution.commons.commands.StopReason;\n" + 
					"import org.eclipse.gemoc.execution.commons.helpers.EcoreQNHelper;\n" + 
					"import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;\n" + 
					"import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;\n" + 
					"import org.eclipse.gemoc.executionframework.engine.commons.GenericModelExecutionContext;\n" + 
					"import org.eclipse.gemoc.executionframework.engine.commons.sequential.ISequentialRunConfiguration;\n" + 
					"import org.eclipse.gemoc.executionframework.engine.core.AbstractCommandBasedSequentialExecutionEngine;\n" + 
					"import com.google.common.util.concurrent.AtomicDouble;\n" + 
					"import fr.inria.diverse.k3.al.annotationprocessor.coordination.ICoordinationManager;\n" + 
					"import fr.inria.diverse.k3.al.annotationprocessor.coordination.Input;\n" + 
					"import fr.inria.diverse.k3.al.annotationprocessor.coordination.Output;\n" + 
					"import fr.inria.diverse.k3.al.annotationprocessor.coordination.Time;\n" + 
					"import fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand;\n" + 
					"import fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry;" +
					"\n").getBytes());
			}
			if(withAspectManagement || withCoordinationAPI) {
				mainFile.append((					
					"import org.eclipse.emf.common.util.TreeIterator;\n" + 
					"import org.eclipse.emf.ecore.EObject;\n" + 
					"import org.eclipse.emf.ecore.EStructuralFeature;\n" + 
					"import org.eclipse.emf.ecore.resource.Resource;\n" +
					"import java.io.IOException;\n" + 
					"import java.lang.reflect.InvocationTargetException;\n" + 
					"import java.lang.reflect.Method;\n" + 
					"import fr.inria.diverse.k3.al.annotationprocessor.Aspect;\n\n" +
					"import com.google.common.collect.ArrayListMultimap;\n" + 
					"import com.google.common.collect.Multimap;" + 
					"import toools.io.JavaResource;\n" + 
					"import toools.io.file.RegularFile;\n" + 
					"import java.util.Collection;\n" + 
					"import java.util.List;\n\n").getBytes()); 
			}
			if(withCoordinationAPI) {
				mainFile.append(("\n"+
						 		 "public class CompiledExecutionEngine extends AbstractCommandBasedSequentialExecutionEngine<GenericModelExecutionContext<ISequentialRunConfiguration>, ISequentialRunConfiguration> \n"+
							     "                                     implements ICoordinationManager {\n").getBytes());
			}else {
				mainFile.append(("\n"+
				 		 		 "public class CompiledExecutionEngine {\n").getBytes());
			}
			
			mainFile.append(("\n" + 
					"	public CompiledExecutionEngine() {\n" + 
					"	}\n" + 
					"\n\n" + 
					"\n").getBytes());
			
			if(withAspectManagement) { 
				mainFile.append(("	public Object execute(Object caller, String methodName, List<Object> parameters) {\n" + 
					"		return internal_execute(caller, methodName, parameters, null);\n" + 
					"	}\n" + 
					"\n" + 
					"	private Object internal_execute(Object caller, String methodName, Collection<Object> parameters,\n" + 
					"			Object mseOccurrence) throws RuntimeException {\n" + 
					"		ArrayList<Object> staticParameters = new ArrayList<Object>();\n" + 
					"		staticParameters.add(caller);\n" + 
					"		if (parameters != null) {\n" + 
					"			staticParameters.addAll(parameters);\n" + 
					"		}\n" + 
					"		Method bestApplicableMethod = getBestApplicableMethod(caller, methodName, staticParameters);\n" + 
					"		if (bestApplicableMethod == null)\n" + 
					"			throw new RuntimeException(\"static class not found or method not found when calling \" + methodName\n" + 
					"					+ \" on \" + caller + \". MSEOccurence=\" + mseOccurrence);\n" + 
					"\n" + 
					"		Object[] args = new Object[0];\n" + 
					"		if (staticParameters != null) {\n" + 
					"			args = staticParameters.toArray();\n" + 
					"		}\n" + 
					"		Object result = null;\n" + 
					"		try {\n" + 
					"			result = bestApplicableMethod.invoke(null, args);\n" + 
					"		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {\n" + 
					"			e.printStackTrace();\n" + 
					"			throw new RuntimeException(\"Exception caught during execution of a call, see inner exception.\"+ methodName\n" + 
					"					+ \" on \" + caller + \". MSEOccurence=\" + mseOccurrence);\n" + 
					"		}\n" + 
					"		return result;\n" + 
					"	}\n" + 
					"\n" + 
					"	private Method getBestApplicableMethod(Object caller, String methodName, List<Object> parameters) {\n" + 
					"		Set<Class<?>> staticHelperClasses = getStaticHelperClasses(caller);\n" + 
					"		if (staticHelperClasses == null || staticHelperClasses.isEmpty()) {\n" + 
					"			return null;\n" + 
					"		}\n" + 
					"		for (Class<?> c : staticHelperClasses) {\n" + 
					"			Method m = getFirstApplicableMethod(c, methodName, parameters);\n" + 
					"			if (m != null)\n" + 
					"				return m;\n" + 
					"		}\n" + 
					"		return null;\n" + 
					"	}\n" + 
					"\n" + 
					"	/**\n" + 
					"	 * return the first compatible method, goes up the inheritance hierarchy\n" + 
					"	 * \n" + 
					"	 * @param staticHelperClass\n" + 
					"	 * @param methodName\n" + 
					"	 * @param parameters\n" + 
					"	 * @return\n" + 
					"	 */\n" + 
					"	protected Method getFirstApplicableMethod(Class<?> staticHelperClass, String methodName, List<Object> parameters) {\n" + 
					"		Method[] methods = staticHelperClass.getDeclaredMethods();\n" + 
					"		for (Method method : methods) {\n" + 
					"			Class<?>[] evaluatedMethodParamTypes = method.getParameterTypes();\n" + 
					"			if (method.getName().equals(methodName) && evaluatedMethodParamTypes.length == parameters.size()) {\n" + 
					"				boolean isAllParamCompatible = true;\n" + 
					"				for (int i = 0; i < evaluatedMethodParamTypes.length; i++) {\n" + 
					"					Object p = parameters.get(i);\n" + 
					"					if (evaluatedMethodParamTypes[i].isPrimitive()) {\n" + 
					"\n" + 
					"						if (evaluatedMethodParamTypes[i].equals(Integer.TYPE) && !Integer.class.isInstance(p)) {\n" + 
					"							isAllParamCompatible = false;\n" + 
					"							break;\n" + 
					"						} else if (evaluatedMethodParamTypes[i].equals(Boolean.TYPE) && !Boolean.class.isInstance(p)) {\n" + 
					"							isAllParamCompatible = false;\n" + 
					"							break;\n" + 
					"						}\n" + 
					"\n" + 
					"					} else if (!evaluatedMethodParamTypes[i].isInstance(p)) {\n" + 
					"						isAllParamCompatible = false;\n" + 
					"						break;\n" + 
					"					}\n" + 
					"				}\n" + 
					"				if (isAllParamCompatible) {\n" + 
					"					return method;\n" + 
					"				}\n" + 
					"			}\n" + 
					"		}\n" + 
					"		// tries going in the inheritance hierarchy\n" + 
					"		Class<?> superClass = staticHelperClass.getSuperclass();\n" + 
					"		if (superClass != null)\n" + 
					"			return getFirstApplicableMethod(superClass, methodName, parameters);\n" + 
					"		else\n" + 
					"			return null;\n" + 
					"	}\n" + 
					"\n" + 
					"	/**\n" + 
					"	 * search static class by name (future version should use a map of available\n" + 
					"	 * aspects, and deals with it as a list of applicable static classes)\n" + 
					"	 * \n" + 
					"	 */\n" + 
					"	protected Set<Class<?>> getStaticHelperClasses(Object target) {\n" + 
					"		List<Class<?>> res = new ArrayList<>();\n" + 
					"		for(Class<?> klass : savedAspects) {\n" + 
					"			Aspect annot = (Aspect) klass.getAnnotation(Aspect.class);\n" + 
					"			if (annot != null){\n" + 
					"				if ((annot.className().isInstance(target))) {\n"+
					"					res.add(klass);\n" + 
					"				}\n" + 
					"			}\n" + 
					"		}\n" + 
					"		return new HashSet<Class<?>>(res);\n" + 
					"	}\n\n\n"+
					"private void initializeCSV(ArrayList<ArrayList<String>> rtdCouples,\n" + 
					"			EObject root) {\n" + 
					"		try {\n" + 
					"			csvFile.setContentAsASCII(\"\");\n" + 
					"		} catch (IOException e1) {\n" + 
					"			e1.printStackTrace();\n" + 
					"		}\n" + 
					"		for(int i = 0; i < rtdCouples.size(); i++) {\n" + 
					"			ArrayList<String> couple = rtdCouples.get(i);\n" + 
					"			TreeIterator<EObject> it = root.eAllContents();\n" + 
					"			while (it.hasNext()) {\n" + 
					"				EObject eo = it.next();\n" + 
					"				String className = couple.get(0);\n" + 
					"				try {\n" + 
					"					if(Class.forName(className).isAssignableFrom(eo.getClass())) {\n" + 
					"						if (i == 0) {\n" + 
					"							csvFile.append(\"timeRef\".getBytes());\n" + 
					"						}else {\n" + 
					"							csvFile.append((\",\"+getEObjectName(eo)).getBytes());\n" + 
					"						}\n" + 
					"					}\n" + 
					"				} catch (ClassNotFoundException | IOException e) {\n" + 
					"					e.printStackTrace();\n" + 
					"				}\n" + 
					"			}\n" + 
					"		}\n" + 
					"		try {\n" + 
					"			csvFile.append((\"\\n\").getBytes());\n" + 
					"		} catch (IOException e) {\n" + 
					"			e.printStackTrace();\n" + 
					"		}\n" + 
					"	}\n" + 
					"	\n" + 
					" 	private void generatePlotScript(ArrayList<ArrayList<String>> rtdCouples,\n" + 
					"			EObject root) {\n" + 
					"		RegularFile plotFile = new RegularFile(\""+projectName+".gnuplot\");\n" + 
					"		try {\n" + 
					"			plotFile.setContentAsASCII(\"\");\n" + 
					"			plotFile.append((\" set title 'result1'\\n\" + \n" + 
					"				\" set ylabel 'RTD'\\n\" + \n" + 
					"				\" set xlabel 'Time'\\n\" + \n" + 
					"				\" set grid\\n\" + \n" + 
					"				\" set term pdf\\n\" + \n" + 
					"				\" set output '"+projectName+".pdf'\\n\" + \n" + 
					"				\" plot \").getBytes());\n" + 
					"		} catch (IOException e1) {\n" + 
					"			e1.printStackTrace();\n" + 
					"		}\n" + 
					"		String sep=\"\";\n" + 
					"		int column = 2;\n" + 
					"		for(int i = 0; i < rtdCouples.size(); i++) {\n" + 
					"			ArrayList<String> couple = rtdCouples.get(i);\n" + 
					"			TreeIterator<EObject> it = root.eAllContents();\n" + 
					"			while (it.hasNext()) {\n" + 
					"				EObject eo = it.next();\n" + 
					"				String className = couple.get(0);\n" + 
					"				try {\n" + 
					"					if(Class.forName(className).isAssignableFrom(eo.getClass())) {\n" + 
					"						if (i == 0) {\n" + 
					"							//nothing to do here\n" + 
					"						}else {\n" + 
					"							plotFile.append((sep+\"'"+projectName+".csv' using 1:\"+column+\" with lines title '\"+getEObjectName(eo)+\"'\").getBytes());\n" + 
					"							sep=\",\";\n" + 
					"							column++;\n" + 
					"						}\n" + 
					"					}\n" + 
					"				} catch (ClassNotFoundException | IOException e) {\n" + 
					"					e.printStackTrace();\n" + 
					"				}\n" + 
					"			}\n" + 
					"		}\n" + 
					"		try {\n" + 
					"			plotFile.append((\"\\n\").getBytes());\n" + 
					"		} catch (IOException e) {\n" + 
					"			e.printStackTrace();\n" + 
					"		}\n" + 
					"	}	private String getEObjectName(EObject eo) {\n" + 
					"		EStructuralFeature nameF = eo.eClass().getEStructuralFeature(\"name\");\n" + 
					"		String name = nameF != null ? (String)eo.eGet(nameF) : null;\n" + 
					"		if (name != null) {\n" + 
					"			return name;\n" + 
					"		}else {\n" + 
					"			return \"noName\";\n" + 
					"		}\n" + 
					"	}\n" + 
					"	\n" + 
					"	int timeRef = 0;\n" + 
					"	RegularFile csvFile = new RegularFile(\""+projectName+".csv\");\n" + 
					"		private void logRTDs(ArrayList<ArrayList<String>> rtdCouples,\n" + 
					"			EObject root) {\n" + 
					"		\n" + 
					"		ArrayList<Object> results = new ArrayList<Object>();\n" + 
					"		Object aResult = -1;\n" + 
					"			for(int i = 0; i < rtdCouples.size(); i++) {\n" + 
					"				ArrayList<String> couple = rtdCouples.get(i);\n" + 
					"				TreeIterator<EObject> it = root.eAllContents();\n" + 
					"				while (it.hasNext()) {\n" + 
					"					EObject eo = it.next();\n" + 
					"					String className = couple.get(0);\n" + 
					"					String rtdName = couple.get(1);\n" + 
					"					try {\n" + 
					"						if(Class.forName(className).isAssignableFrom(eo.getClass())) {\n" + 
					"							try{\n" + 
					"								aResult = (Integer) this.execute(eo, rtdName, null);\n" + 
					"							}\n" + 
					"							catch(ClassCastException c) {\n" + 
					"								aResult = (Float) this.execute(eo, rtdName, null);\n" + 
					"							}\n" + 
					"							if (i == 0) {\n" + 
					"								timeRef = (int) aResult;\n" + 
					"							}else {\n" + 
					"								results.add(aResult);\n" + 
					"							}\n" + 
					"						}\n" + 
					"					} catch (ClassNotFoundException e) {\n" + 
					"						e.printStackTrace();\n" + 
					"					}\n" + 
					"				}\n" + 
					"			}\n" + 
					"			\n" + 
					"			try {\n" + 
					"				csvFile.append((\"\"+timeRef).getBytes());\n" + 
					"				for(Object i : results) {\n" + 
					"					csvFile.append((\", \"+i.toString()).getBytes());\n" + 
					"				}\n" + 
					"				csvFile.append((\"\\n\").getBytes());\n" + 
					"			} catch (IOException e) {\n" + 
					"				e.printStackTrace();\n" + 
					"			}\n" + 
					"			\n" + 
					"		}\n" + 
					"\n\n"+
					"	Multimap<Clock, ForceCase> forcedClock = ArrayListMultimap.create();\n" + 
					"	ArrayList<Clock> clockToForceNext = new ArrayList<>();\n\n").getBytes());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
			}
			
			
			public void generateAspectList(List<Class<?>> aspectList) {
				try{
//					pluginHelper.addRequiredBundle(proj, aspectList.get(0).getName().substring(0, aspectList.get(0).getName().lastIndexOf('.')));
					mainFile.append(("	Set<Class<?>> savedAspects = new HashSet<>(Arrays.asList(").getBytes());
					String sep ="";
					for(Class<?> klass : aspectList) {
						mainFile.append(("           "+sep+klass.getCanonicalName()+".class\n").getBytes());
						sep = ",";
					}
					mainFile.append("			));\n".getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
			
			public void injectMethod(StringBuilder sb) {
				try {
					mainFile.append(sb.toString().getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
			public void generateBeginOfMain(String languageName) {
				try {
					mainFile.append(("\n" + 
							"   Map<Clock, String> clockToMethodName= new HashMap<>();\n" + 
							"   Map<Clock, String> clockToEObject = new HashMap<>();\n" + 
							"	String languageName=\""+languageName+"\";" +
							"\n" +
							"	public static void main(String[] args) {\n" + 
							"           int nbSteps = 20000; //by default\n" + 
							"		boolean logClocks = false;\n" + 
							"		boolean logStepsOnly = false;\n" + 
							"		\n" + 
							"		Options options = new Options();\n" + 
							"\n" + 
							"		Option opt_nbSteps = new Option(\"n\", \"nbSteps\", true, \"number of steps to execute (-1 for infinite)\");\n" + 
							"		opt_nbSteps.setRequired(false);\n" + 
							"        options.addOption(opt_nbSteps);\n" + 
							"		\n" + 
							"        Option opt_logClocks = new Option(\"l\", \"logClocksAndSteps\", false, \"log clock status and execution steps\");\n" + 
							"        opt_logClocks.setRequired(false);\n" + 
							"        options.addOption(opt_logClocks);\n" + 
							"        \n" + 
							"        Option opt_logStepsOnly = new Option(\"s\", \"logStepsOnly\", false, \"log only execution steps\");\n" + 
							"        opt_logStepsOnly.setRequired(false);\n" + 
							"        options.addOption(opt_logStepsOnly);\n" + 
							"        \n").getBytes());
					if(withAspectManagement) {
						mainFile.append(("\n" + 
								"        Option opt_rtdToLog = new Option(\"rtd\", \"runtimeDataToLog\", false, \"list of couple with class name and rtd name\");\n" + 
								"        opt_rtdToLog.setRequired(false);\n" + 
								"        opt_rtdToLog.setArgs(Option.UNLIMITED_VALUES);\n" + 
								"        opt_rtdToLog.setValueSeparator(' ');\n" + 
								"        options.addOption(opt_rtdToLog);\n" + 
								"        \n").getBytes()); 
					}
					
					mainFile.append(("\n" +  
							"        \n" + 
							"\n" + 
							"        CommandLineParser parser = new PosixParser();\n" + 
							"        HelpFormatter formatter = new HelpFormatter();\n" + 
							"        CommandLine cmd = null;\n" + 
							"\n" + 
							"        try {\n" + 
							"            cmd = parser.parse(options, args);\n" + 
							"        } catch (ParseException e) {\n" + 
							"            System.out.println(e.getMessage());\n" + 
							"            formatter.printHelp(\"Compiled Execution Engine\", options);\n" + 
							"\n" + 
							"            System.exit(1);\n" + 
							"        }\n" + 
							"\n" + 
							"        if (cmd.hasOption(\"nbSteps\")) nbSteps= Integer.parseInt(cmd.getOptionValue(\"nbSteps\"));\n" + 
							"        logClocks = cmd.hasOption(\"logClocksAndSteps\");\n" + 
							"        logStepsOnly = cmd.hasOption(\"logStepsOnly\");\n" +
							"\n").getBytes());
					if(withAspectManagement) {
						mainFile.append(("\n" +
							"        String[] rtdList = null;\n" + 
							"		if (cmd.hasOption(\"rtd\")) rtdList = cmd.getOptionValues(\"rtd\");\n" + 
							"	    if (rtdList != null && rtdList.length%2 != 0) {\n" + 
							"	    	System.err.println(\"RTD not considered since not correctly formatted. (list of couples, a couples being a class (qualified) name and a rtd name\");\n" + 
							"	    }\n" + 
							"		ArrayList<ArrayList<String>> rtdCouples = new ArrayList<ArrayList<String>>();\n" + 
							"		if (rtdList != null){\n" + 
							"         for(int i = 0; i < rtdList.length; i+=2) {\n" + 
							"			    rtdCouples.add(new ArrayList<String>(Arrays.asList(rtdList[i], rtdList[i+1])));\n" + 
							"           }\n" + 
							"		}\n\n").getBytes());
					}
					mainFile.append(("\n" +
							"		CompiledExecutionEngine theEngine = new CompiledExecutionEngine();\n"
							).getBytes());
					
					for(InstantiatedElement ie : allConstants){
						if(ie.getValue() != null) {
							if (ie.getValue() instanceof IntegerElement) {
								mainFile.append(("\t int "+cleanQualifiedName(ie)+" = "+((IntegerElement)ie.getValue()).getValue()+";\n").getBytes());
							}
							if (ie.getValue() instanceof BooleanElement) {
								mainFile.append(("\t boolean "+cleanQualifiedName(ie)+" = "+((BooleanElement)ie.getValue()).getValue()+";\n").getBytes());
							}
							if(ie.getValue() instanceof SequenceElement) {
								mainFile.append(("\t Integer[] finite_"+cleanQualifiedName(ie)+" = {").getBytes());
								String sep="";
								for(PrimitiveElement i : ((SequenceElement)ie.getValue()).getFinitePart()){
									if(i instanceof IntegerElement) {
										mainFile.append((sep+((IntegerElement)i).getValue()).getBytes());
									}else{
										mainFile.append((sep+((IntegerElement)ie.getParent().resolveAbstractEntity(((IntegerVariableRef)i).getReferencedVar()).getValue()).getValue()).getBytes());
									}
									sep=",";
								}
								mainFile.append(("};\n").getBytes());
								
								mainFile.append(("\t Integer[] infinite_"+cleanQualifiedName(ie)+" = {").getBytes());
								sep="";
								for(PrimitiveElement i : ((SequenceElement)ie.getValue()).getNonFinitePart()){
									if(i instanceof IntegerElement) {
										mainFile.append((sep+((IntegerElement)i).getValue()).getBytes());
									}else{
										mainFile.append((sep+((IntegerElement)ie.getParent().resolveAbstractEntity(((IntegerVariableRef)i).getReferencedVar()).getInstantiationPath().getLast()).getValue()).getBytes());
									}
									sep=",";
								}
								mainFile.append(("};\n").getBytes());
								
								mainFile.append(("\t Sequence "+cleanQualifiedName(ie)+" = new Sequence(finite_"+cleanQualifiedName(ie)+",infinite_"+cleanQualifiedName(ie)+",\""+cleanQualifiedName(ie)+"\");\n").getBytes());
							}
						}else {
							mainFile.append(("\t int "+cleanQualifiedName(ie)+" = "+((IntegerElement)ie.getInstantiationPath().getLast()).getValue()+";\n").getBytes());
						}
					}
					
					for(InstantiatedElement ie : allClassicalExpression){
						if(ie == null) {
							continue;
						}
						ClassicalExpressionBuilder builder = new ClassicalExpressionBuilder(ie, mainFile, this);
						builder.doSwitch(ie.getInstantiationPath().getLast());
					}
					
					for(InstantiatedElement ie : allClocks){
						mainFile.append(("\t Clock "+cleanQualifiedName(ie)+" = new Clock(\""+cleanQualifiedName(ie)+"\");\n").getBytes());
					}
					
					StringBuilder connectCode = new StringBuilder();
					
					for(InstantiatedElement ie : allKernelExpressions){
						KernelExpressionBuilder builder = new KernelExpressionBuilder(ie, mainFile, this);
						builder.doSwitch(ie.getDeclaration());
						connectCode.append(builder.connectStringBuilder);
					}
					
					
					
					for(InstantiatedElement ie : allComplexExpressions ){
						if (!ie.isConditional() && ie.getRootExpression() != null) {
							mainFile.append(("\t UserDefinedExpression "+cleanQualifiedName(ie)+" = new UserDefinedExpression(\""+cleanQualifiedName(ie)+"\");\n").getBytes());
							connectCode.append("\t\t"+cleanQualifiedName(ie)+".connect("+cleanQualifiedName(ie.getRootExpression())+");\n");
						}else 
						if (ie.isConditional()) {
							mainFile.append(("\t ConditionalExpression "+cleanQualifiedName(ie)+" = new ConditionalExpression(").getBytes());
							mainFile.append(("\""+cleanQualifiedName(ie)+"\");\n").getBytes());
							connectCode.append("\t\t"+cleanQualifiedName(ie)+".connect(new ArrayList<Case>(Arrays.asList(");
									String sep = "";
									for(InstantiatedElement caze: ie.getConditionalCases()) {
										InstantiatedElement test = caze.getConditionTest();
										connectCode.append((sep+"\n\t\t\tnew Case("+cleanQualifiedName(caze)+", "+cleanQualifiedName(test)+")"));
										sep = ",";
									}
									connectCode.append(")));\n");
						}else {
//							System.out.println("Complex not managed: "+ie);
						}
					}
					
					
					for(InstantiatedElement ie : allExclusions){
						KernelRelationBuilder builder = new KernelRelationBuilder(ie, mainFile, this);
						builder.doSwitch(ie.getDeclaration());
					}
					for(InstantiatedElement ie : allRelationsButExclusions){
						KernelRelationBuilder builder = new KernelRelationBuilder(ie, mainFile, this);
						builder.doSwitch(ie.getDeclaration());
					}	
					
					
					StringBuilder desactivedRelations = new StringBuilder();
					for(InstantiatedElement ie : allConditionalRelation ){
						mainFile.append(("\t ConditionalRelation "+cleanQualifiedName(ie)+" = new ConditionalRelation();\n").getBytes());
						
						connectCode.append("\t\t"+cleanQualifiedName(ie)+".connect(");
						boolean defaultHolds = true;
								for(int i = 0; i < ie.getConditionalCases().size(); i++) {
									InstantiatedElement test = ie.getConditionalCases().get(i); //WARNING: these are not actually the cases but only the tests :-(
									if (((BooleanElement)test.getValue()).getValue() == true) {
										defaultHolds = false;
										Relation rem = ((ConditionalRelationDefinition)ie.getDefinition()).getRelCases().get(i).getRelation().get(0); //hope the collection are correctly sorted
										connectCode.append(cleanQualifiedName(theUnfoldModel.getInstantiationTree().lookupInstance(test.getParent().getInstantiationPath()+"::"+test.getParent().getDefinition().getName()+"::"+rem.getName()))+");");
									}else {
										Relation rem = ((ConditionalRelationDefinition)ie.getDefinition()).getRelCases().get(i).getRelation().get(0); //hope the collection are correctly sorted
										desactivedRelations.append(cleanQualifiedName(theUnfoldModel.getInstantiationTree().lookupInstance(test.getParent().getInstantiationPath()+"::"+test.getParent().getDefinition().getName()+"::"+rem.getName()))+".isActive = false;\n");
									}
									
								}
								if(defaultHolds) {
									connectCode.append(cleanQualifiedName(ie.getDefaultCase())+");");
								}else {
									desactivedRelations.append(cleanQualifiedName(ie.getDefaultCase())+".isActive = false;\n");

								}
						
					}
					
					int localUID = uid;
					for(InstantiatedElement ie : allStateMachineRelations) {
						StateMachineRelationDefinition def = (StateMachineRelationDefinition) ie.getDefinition();
						String machineName = cleanQualifiedName(ie);
						mainFile.append(("StateMachine "+machineName+" = new StateMachine();\n").getBytes());
						

						
						Set<String> allClockNames = new HashSet<String>();
						for(Transition t : def.getTransitions()) {							
							if(t.getTrigger() != null) {
								for(BindableEntity bindable : ((Trigger)t.getTrigger()).getTrueTriggers()){
									allClockNames.add(cleanQualifiedName(ie.resolveAbstractEntity((AbstractEntity) bindable)));
								}
							}
						}
						
						String clockList = "";
						String sep ="";
						for(String clockName : allClockNames) {
//		clocks should already be compiled
//							mainFile.append(("Clock "+clockName+" = new Clock(\""+clockName+"\");\n").getBytes());
							clockList+=(sep+clockName);
							sep=",";
						}
						
						
						mainFile.append((machineName+".allClocks.addAll(Arrays.asList(").getBytes());
						mainFile.append(clockList.getBytes());
						mainFile.append(( "));\n").getBytes());
						
						String stateList = "";
						sep="";
						for(State s : def.getStates()) {
							String stateName = cleanQualifiedName(ie)+'_'+s.getName();
							mainFile.append(("State "+stateName+" = new State(\""+stateName+"\");\n").getBytes());
							stateList+=(sep+stateName);
							sep=",";
						}
						
						mainFile.append((machineName+".states.addAll(Arrays.asList(").getBytes());
						mainFile.append(stateList.getBytes());
						mainFile.append(( "));\n").getBytes());
							
						
						for(Transition t : def.getTransitions()) {
							String tName = cleanQualifiedName(ie)+"_"+t.getName();
							String tSource = cleanQualifiedName(ie)+"_"+t.getSource().getName();
							String tTarget = cleanQualifiedName(ie)+"_"+t.getTarget().getName();
							mainFile.append(("Transition "+tName+" = new Transition("+tSource+", "+tTarget+");\n").getBytes());
							mainFile.append((tName+".clocks = new ArrayList<Clock>(Arrays.asList(").getBytes());
							clockList = "";
							sep="";
							if(t.getTrigger() != null) {
								for(BindableEntity bindable : ((Trigger)t.getTrigger()).getTrueTriggers()){
									String clockName = cleanQualifiedName(ie.resolveAbstractEntity((AbstractEntity) bindable));
									clockList+=(sep+clockName);
									sep=",";
								}
							}
							mainFile.append(( clockList+"));\n").getBytes());
					
							System.out.println("\t\tguard:"+t.getGuard());
							System.out.println("\t\twhen:"+t.getTrigger());
							System.out.println("\t\taction:"+t.getActions());
						}
						
						
						for(State s : def.getStates()) {
							String stateName = cleanQualifiedName(ie)+'_'+s.getName();
							String transitionList = "";
							sep="";
							for(Transition outT : s.getOutputTransitions()) {
								String tName = cleanQualifiedName(ie)+"_"+outT.getName();
								transitionList+=(sep+tName);
								sep=",";
							}
							mainFile.append((stateName+".outgoingTransitions = new ArrayList<Transition>(Arrays.asList(").getBytes());
							mainFile.append(transitionList.getBytes());
							mainFile.append(( "));\n").getBytes());
							
						}
						String initName = cleanQualifiedName(ie)+"_"+def.getInitialStates().get(0).getName();
						mainFile.append((machineName+".initialState = "+initName+";\n").getBytes());
						mainFile.append((machineName+".currentState = "+initName+";\n").getBytes());
						
						//variable stuff
// done on the fly during guard stuff.						
//						for(ConcreteEntity v : def.getDeclarationBlock().getConcreteEntities()) {
//							if(v instanceof IntegerElement) {
//								mainFile.append(("IntRef id_"+getUid(v)+" = new IntRef("+((IntegerElement)v).getValue().intValue()+");\n").getBytes());
//							}else{
//								throw new RuntimeException("Varibale not supported: "+v.getClass());
//							}
//						}
						
						
						//guard stuff
						Map<EObject, Integer> fsmToUID = new HashMap<>();
						GuardHelper guardHelper = new GuardHelper(localUID, fsmToUID);
						Set<Transition> uniqueTransitions = new HashSet<Transition>(def.getTransitions());
						for(Transition t : uniqueTransitions) {
							AbstractGuard g = t.getGuard();
							if (g == null) {
								continue;
							}
							Guard guard = (Guard)g;
							String guardString = "";
							
							guardString = guardHelper.dispatchGenerateExpressionMethod(guard.getValue(), ie, "\n");
							mainFile.append(guardString.getBytes());
							
							String tName = cleanQualifiedName(ie)+"_"+t.getName();
							mainFile.append((tName+".guard = id_"+guardHelper.getUid(guard.getValue())+";\n").getBytes());
							
						}
						localUID = guardHelper.uid;

						ActionHelper actionHelper = new ActionHelper(localUID, fsmToUID);
						for(Transition t : uniqueTransitions) {
							List<AbstractAction> actions = t.getActions();
							
							for (AbstractAction a : actions) {
								if (a == null) {
									continue;
								}
								IntegerAssignement ia = (IntegerAssignement) a; //warning we are missing death and birth
								String actionString = "";
								actionString = actionHelper.dispatchGenerateExpressionMethod(ia, ie, "\n");
								mainFile.append(actionString.getBytes());
								
								String tName = cleanQualifiedName(ie)+"_"+t.getName();
								mainFile.append((tName+".actions.add(id_"+actionHelper.getUid(ia)+");\n").getBytes());
							
							}
						}
						
						localUID = actionHelper.uid;
						
					}//fin all state machines
					
					
					
					 mainFile.append(desactivedRelations.toString().getBytes());
					 mainFile.append(connectCode.toString().getBytes());
					
					
					 mainFile.append("\tSortedSet<Constraint> sortedConstraints = new TreeSet<Constraint>(Arrays.asList(".getBytes());
					 String sep = "";
					 for(InstantiatedElement ie : allExclusions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ",";
					 }
					 for(InstantiatedElement ie : allRelationsButExclusions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ",";
					 }
					 for(InstantiatedElement ie : allStateMachineRelations) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ",";
					 }
					 for(InstantiatedElement ie : allKernelExpressions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ",";
					 }
					 for(InstantiatedElement ie : allComplexExpressions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ",";
					 }
					 mainFile.append("));\n".getBytes());
					 
					 
		
					 mainFile.append("\tSet<Clock> allClocks = new HashSet<Clock>(Arrays.asList(".getBytes());
					 sep = "";
					 for(InstantiatedElement ie : allClocks) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ",";
					 }
					 for(InstantiatedElement ie : allKernelExpressions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ",";
					 }
					 for(InstantiatedElement ie : allComplexExpressions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ",";
					 }
					 mainFile.append("));\n".getBytes());
					 
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				try {
					proj.refreshLocal(IResource.DEPTH_INFINITE, null);
				} catch (CoreException e) {
					e.printStackTrace();
				}
					
	}


		
			


			public void generateEndOfMain() {
				try {
					mainFile.append(("\n" + 
							"   Solver solver = new Solver(allClocks, sortedConstraints);\n" + 
							"	for (int currentStep = 0; currentStep < nbSteps; currentStep++) {\n" + 
							"		if (logStepsOnly || logClocks)\n" + 
							"			System.out.println(\"-------------step \" + currentStep + \"\\n\");\n" + 
							"\n" + 
							"		solver.doStep();\n"
							+ "		if (logClocks) {\n" + 
							"			for(Clock pt_c: solver.clocks) {\n" + 
							"	           if (! (pt_c instanceof Constraint)) {\n" + 
							"	             System.out.println(pt_c);\n" + 
							"	           }\n" + 
							"			}\n" + 
							"		}" + 
							"\n" + 
							"	}\n" +  
							"    System.out.println();" + 
							"    System.out.println(\"/**********************\\n\");\n" + 
							"    System.out.println(\"*     simulation ends      *\\n\");\n" + 
							"    System.out.println(\"**********************/\");\n" + 
							"    System.out.println();" + 
							"\n" + 
							"    return;\n").getBytes());
				   mainFile.append(("    }\n}\n").getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			public void generateEndOfMainWithAspectManagement() {
				try {
					mainFile.append(("\n" + 
							" if(rtdCouples.size() > 0) {\n" + 
							"    	theEngine.initializeCSV(rtdCouples, root);\n" + 
							"		theEngine.generatePlotScript(rtdCouples, root);\n" + 
							"    }\n" + 
							"\n" + 
							"   	Solver solver = new Solver(allClocks, sortedConstraints);\n" + 
							"	\n" + 
							"   	for (int currentStep = 0; currentStep < nbSteps; currentStep++) {\n" + 
							"		if (logStepsOnly || logClocks)\n" + 
							"			System.out.println(\"-------------step \" + currentStep + \"\\n\");\n" + 
							"\n" + 
							"		solver.doStep(theEngine.clockToForceNext);\n" + 
							"		theEngine.executeDSAsAndLog(logClocks, theEngine.clockToMethodName, theEngine.clockToEObject, resource, solver);\n" + 
							"\n" + 
							"		if (rtdCouples.size() > 0) {\n" + 
							"			theEngine.logRTDs(rtdCouples, root);\n" + 
							"		}\n" + 
							"	}\n" + 
							"    \n" + 
							"    \n" + 
							"    model.delete();    \n" + 
							"    System.out.println();\n" + 
							"    System.out.println(\"/**********************\\n\");\n" + 
							"    System.out.println(\"*     simulation ends      *\\n\");\n" + 
							"    System.out.println(\"**********************/\");\n" + 
							"    System.out.println();	\n" + 
							"    return;\n" + 
							"    }\n" + 
							"	\n" + 
							"\n" + 
							"\n" + 
							"	\n" + 
							"	public void executeDSAsAndLog(boolean logClocks, Map<Clock, String> clockToMethodName, Map<Clock, String> clockToEObject,\n" + 
							"			org.eclipse.emf.ecore.resource.Resource resource, Solver solver) {\n" + 
							"            clockToForceNext.clear();" +
							"			 for(Clock pt_c: solver.clocks) {\n" + 
							"            if (! (pt_c instanceof Constraint)) {\n" + 
							"               if (logClocks) System.out.println(pt_c);\n" + 
							"            	if (pt_c.status == QuantumBoolean.TRUE) {\n" + 
							"            		String eobjectURIFrag = clockToEObject.get(pt_c);\n" + 
							"            		if (eobjectURIFrag != null) {\n" + 
							"            			org.eclipse.emf.ecore.EObject eo = resource.getEObject(eobjectURIFrag);\n" + 
							"            			Object oRes = this.execute(eo, clockToMethodName.get(pt_c), null);\n" + 
							"            			if ((oRes instanceof Boolean) && forcedClock.containsKey(pt_c)) {\n" + 
							"            				boolean bRes = (Boolean)oRes;\n" + 
							"            				for(ForceCase fc : forcedClock.get(pt_c)) {\n" + 
							"            					if (fc.associatedResValue == bRes) {\n" + 
							"            						clockToForceNext.add(fc.clockToForce);\n" + 
							"            					}\n" + 
							"            				}\n" + 
							"            			}\n" + 
							"            		}\n" + 
							"            	}\n" + 
							"            }\n" + 
							"        }\n" + 
							"	}\n" + 
							"\n" + 
							"}\n"
							).getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		
			public void generateEndOfMainWithAspectManagementAndCoordination() {
				try {
					mainFile.append(("\n" + 
							"	theEngine.populateVariableMapFromAnnotations(root);\n" + 
							"\n" + 
							"    if(rtdCouples.size() > 0) {\n" + 
							"    	theEngine.initializeCSV(rtdCouples, root);\n" + 
							"		theEngine.generatePlotScript(rtdCouples, root);\n" + 
							"    }\n" + 
							"\n" + 
							"   	Solver solver = new Solver(allClocks, sortedConstraints);\n" + 
							"   	BlockingQueue<Exception> exceptionQueue = new ArrayBlockingQueue<>(1);\n" + 
							"   \n" + 
							"   	\n" + 
							"   	Thread solverThread = theEngine.startSolverInThread(exceptionQueue, solver, nbSteps, logStepsOnly, logClocks, rtdCouples, resource, root, theEngine);\n" + 
							"    solverThread.start();\n" + 
							"    ServerSocket server = null;\n" + 
							" try {\n" + 
							"	server = new ServerSocket(39635);\n" + 
							"	\n" + 
							"	Socket clientSocket = server.accept();\n" + 
							"	\n" + 
							"	ObjectInputStream cin = new ObjectInputStream(clientSocket.getInputStream());\n" + 
							"	ObjectOutputStream cout = new ObjectOutputStream(clientSocket.getOutputStream());\n" + 
							"	\n" + 
							"	Object clientCommand = cin.readObject();\n" + 
							"	while(!theEngine.simulationEnded.tryAcquire() || clientCommand instanceof StopCommand) { \n" + 
							"		System.out.println(\"Command received: \"+clientCommand);\n" + 
							"		if(clientCommand instanceof DoStepCommand) {\n" + 
							"			theEngine.timeBeforeDoStep = theEngine.lastknownTime.doubleValue();\n" + 
							"			System.out.println(\"DoStep starts @\"+theEngine.timeBeforeDoStep);\n" + 
							"			theEngine.currentPredicate = ((DoStepCommand) clientCommand).predicate;\n" + 
							"			StopCondition stopCond = theEngine.doStep(theEngine.currentPredicate);\n" + 
							"			System.out.println(\"DoStep stops @\"+stopCond.timeValue+\" due to \"+stopCond.stopReason);\n" + 
							"			cout.writeObject(stopCond);\n" + 
							"		}	\n" + 
							"		if(clientCommand instanceof GetVariableCommand) {\n" + 
							"			String varQN = ((GetVariableCommand) clientCommand).variableQualifiedName;\n" + 
							"			Object varValue= theEngine.getVariable(varQN);\n" + 
							"			cout.writeObject(varValue);\n" + 
							"		}\n" + 
							"		if(clientCommand instanceof SetVariableCommand) {\n" + 
							"			String varQN = ((SetVariableCommand) clientCommand).variableQualifiedName;\n" + 
							"			Object newValue = ((SetVariableCommand) clientCommand).newValue; \n" + 
							"			Boolean res = theEngine.setVariable(varQN, newValue);\n" + 
							"			cout.writeObject(res);\n" + 
							"		}\n" + 
							"		System.out.println(\"wait for a new command.\");\n" + 
							"		clientCommand = cin.readObject();\n" + 
							"	}\n" + 
							"    server.close();\n" + 
							"	solverThread.join();\n" + 
							" 	} catch (IOException | InterruptedException | ClassNotFoundException e) {\n" + 
							"	 e.printStackTrace();\n" + 
							"	 }\n" + 
							"    \n" + 
							"    \n" + 
							"    model.delete();    \n" + 
							"    System.out.println();\n" + 
							"    System.out.println(\"/**********************\\n\");\n" + 
							"    System.out.println(\"*     simulation ends      *\\n\");\n" + 
							"    System.out.println(\"**********************/\");\n" + 
							"    System.out.println();	\n" + 
							"    return;\n" + 
							"    }\n" + 
							"	\n" + 
							"	Semaphore startDoStepSemaphore  = new Semaphore(0);\n" + 
							"	Semaphore finishDoStepSemaphore = new Semaphore(0);\n" + 
							"	Semaphore simulationEnded       = new Semaphore(0);\n" + 
							"	Map<String, Object> qualifiedNameToEObject = new HashMap<>(); //initialized in main, used in GetVariable\n" + 
							"	Map<String, Method> qualifiedNameToMethod = new HashMap<>(); \n" + 
							"	CoordinationPredicate currentPredicate;\n" + 
							"	AtomicDouble lastknownTime = new AtomicDouble(0.0);\n" + 
							"	double timeBeforeDoStep = 0;\n" + 
							"	StopCondition lastStopcondition = null;\n" + 
							"	Object timeObject = null;\n" + 
							"	Class<?> timeType = null;\n" + 
							"\n" + 
							"	/**\n" + 
							"	 * this method go through all the model and its aspects and populate the {@link #qualifiedNameToEObject} map with Variables annotated as @Input or @Output\n" + 
							"	 */\n" + 
							"	private void populateVariableMapFromAnnotations(EObject root) {\n" + 
							"		TreeIterator<EObject> contentIt = root.eAllContents();\n" + 
							"		while(contentIt.hasNext()) {\n" + 
							"			EObject eo = contentIt.next();\n" + 
							"//			List<Class<?>> aspects = K3DslHelper.getAspectsOn(languageName, eo.getClass());\n" + 
							"			loopAspects: for(Class<?> aspect : savedAspects) {\n" + 
							"				Aspect anAspect = aspect.getAnnotation(Aspect.class);\n" + 
							"				for (Class<?> ieo: eo.getClass().getInterfaces()) {\n" + 
							"					if (anAspect.className().getName().compareTo(ieo.getName()) != 0){\n" + 
							"						continue loopAspects;\n" + 
							"					}\n" + 
							"				}\n"+
							"				try {\n" + 
							"					Class<?> propertyClass = this.getClass().getClassLoader().loadClass(aspect.getTypeName()+aspect.getSimpleName()+\"Properties\");\n" + 
							"					for (Field f : propertyClass.getFields()) {\n" + 
							"						for (Method m : aspect.getMethods()) {\n" + 
							"							if (m.getName().compareTo(f.getName()) == 0) {\n" + 
							"								if (m.isAnnotationPresent(Input.class)) {\n" + 
							"									qualifiedNameToEObject.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+\"::\"+f.getName(), eo);\n" + 
							"									if (m.getParameterCount() == 2) {\n" + 
							"										qualifiedNameToMethod.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+\"::\"+f.getName()+\"::set\", m);\n" + 
							"									}\n" + 
							"								}\n" + 
							"								if (m.isAnnotationPresent(Output.class)) {\n" + 
							"									qualifiedNameToEObject.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+\"::\"+f.getName(), eo);\n" + 
							"									if (m.getParameterCount() == 1) {\n" + 
							"										qualifiedNameToMethod.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+\"::\"+f.getName()+\"::get\", m);\n" + 
							"									}\n" + 
							"								}\n" + 
							"								if (m.isAnnotationPresent(Time.class)) {\n" + 
							"									try {\n" + 
							"										timeObject = this.execute(eo, f.getName(), null);  \n" + 
							"										timeType = f.getType();\n" + 
							"									} catch (IllegalArgumentException e) {\n" + 
							"										e.printStackTrace();\n" + 
							"									}\n" + 
							"								}\n" + 
							"							}\n" + 
							"						}\n" + 
							"					}\n" + 
							"				} catch (ClassNotFoundException | SecurityException e1) {\n" + 
							"					e1.printStackTrace();\n" + 
							"				}\n" + 
							"			}\n" + 
							"		}\n" + 
							"			\n" + 
							"	}\n" + 
							"	\n" + 
							"	public Object getVariable(String varQN) {\n" + 
							"		Object var = qualifiedNameToEObject.get(varQN);\n" + 
							"		Method method = qualifiedNameToMethod.get(varQN+\"::get\");\n" + 
							"		if (var == null || method == null) {\n" + 
							"			return false;\n" + 
							"		}\n" + 
							"		try {\n" + 
							"			StepManagerRegistry.getInstance().unregisterManager(this);\n" + 
							"			Object res = method.invoke(null, var);\n" + 
							"			StepManagerRegistry.getInstance().registerManager(this);\n" + 
							"			return res;\n" + 
							"		} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {\n" + 
							"			e.printStackTrace();\n" + 
							"		}\n" + 
							"		\n" + 
							"		return true;\n" + 
							"	}\n" + 
							"	\n" + 
							"	public boolean setVariable(String varQN, Object newValue) {\n" + 
							"		Object var = qualifiedNameToEObject.get(varQN);\n" + 
							"		Method method = qualifiedNameToMethod.get(varQN+\"::set\");\n" + 
							"		if (var == null || method == null) {\n" + 
							"			return false;\n" + 
							"		}\n" + 
							"		try {\n" + 
							"			StepManagerRegistry.getInstance().unregisterManager(this);\n" + 
							"			this.execute(var, method.getName(), Arrays.asList(newValue));\n" + 
							"			StepManagerRegistry.getInstance().registerManager(this);\n" + 
							"		} catch (SecurityException | IllegalArgumentException e) {\n" + 
							"			e.printStackTrace();\n" + 
							"		}\n" + 
							"		\n" + 
							"		return true;\n" + 
							"	}\n" + 
							"	\n" + 
							"	private void decrement(Semaphore sem) {\n" + 
							"		try {\n" + 
							"			sem.acquire();\n" + 
							"		} catch (InterruptedException e) {\n" + 
							"			e.printStackTrace();\n" + 
							"		}\n" + 
							"	}\n" + 
							"	\n" + 
							"	private void increment(Semaphore sem) {\n" + 
							"			sem.release();\n" + 
							"	}\n" + 
							"	\n" + 
							"	\n" + 
							"	public StopCondition doStep(CoordinationPredicate predicate) {\n" + 
							"		increment(startDoStepSemaphore);\n" + 
							"		decrement(finishDoStepSemaphore);\n" + 
							"		return lastStopcondition;\n" + 
							"		//should do something Here according to what has been done during the step (according to the stopCondition)\n" + 
							"	}\n" + 
							"	\n" + 
							"	public Thread startSolverInThread(BlockingQueue<Exception> exceptionQueue, Solver solver, int nbSteps, boolean logStepsOnly, \n" + 
							"									  Boolean logClocks, ArrayList<ArrayList<String>> rtdCouples, org.eclipse.emf.ecore.resource.Resource resource,\n" + 
							"									  EObject root, CompiledExecutionEngine theEngine) {\n" + 
							"		return new Thread(() ->\n" + 
							"		// since aspect's methods are static, first arg is null\n" + 
							"		{\n" + 
							"			try {\n" + 
							"				decrement(startDoStepSemaphore);\n" + 
							"				for (int currentStep = 0; currentStep < nbSteps; currentStep++) {\n" + 
							"					if (logStepsOnly || logClocks)\n" + 
							"						System.out.println(\"-------------step \" + currentStep + \"\\n\");\n" + 
							"\n" + 
							"					solver.doStep(theEngine.clockToForceNext);\n" + 
							"					\n" + 
							"					checkLogicalStepStopPredicate();\n" + 
							"					checkEventStopPredicate(solver, resource);\n" + 
							"\n" + 
							"					this.executeDSAsAndLog(logClocks, clockToMethodName, clockToEObject, resource, solver);\n" + 
							"	\n" + 
							"					if (rtdCouples.size() > 0) {\n" + 
							"						this.logRTDs(rtdCouples, root);\n" + 
							"					}\n" + 
							"				}\n" + 
							"\n" + 
							"			} catch (/* IllegalAccessException | */IllegalArgumentException /* | InvocationTargetException */ e) {\n" + 
							"				exceptionQueue.offer(e);\n" + 
							"			}\n" + 
							"			increment(simulationEnded);\n" + 
							"		});\n" + 
							"	}\n" + 
							"\n" + 
							"\n" + 
							"	private void checkLogicalStepStopPredicate() {\n" + 
							"		if (this.currentPredicate != null\n" + 
							"			&& this.currentPredicate.contains(StopReason.LOGICALSTEP,null, null, null))\n" + 
							"		{\n" + 
							"			this.currentPredicate.getLogicalStepPredicate().nbStepsRequired--;\n" + 
							"			if(this.currentPredicate.getLogicalStepPredicate().nbStepsRequired == 0) {\n" + 
							"				// struct to communicate the stop condition to the server\n" + 
							"				lastStopcondition = new StopCondition(StopReason.LOGICALSTEP, null, null, lastknownTime.floatValue()); // always lastKnownTime ?\n" + 
							"				increment(finishDoStepSemaphore); // give control back to the caller.\n" + 
							"				// wait for stuff to be done in the server before to continue\n" + 
							"				decrement(startDoStepSemaphore); // waiting for the caller to do something\n" + 
							"			}\n" + 
							"		}\n" + 
							"	}\n" + 
							"\n" + 
							"\n" + 
							"	private void checkEventStopPredicate(Solver solver, Resource resource) {\n" + 
							"		if (this.currentPredicate != null) {\n" + 
							"			for (Clock pt_c : solver.clocks) {\n" + 
							"				String uriFragment = clockToEObject.get(pt_c);\n" + 
							"				String supportQN = \"\";\n" + 
							"				if (uriFragment != null) {\n" + 
							"					EObject support = resource.getEObject(uriFragment);\n" + 
							"					supportQN = EcoreQNHelper.getQualifiedName(support);\n" + 
							"				}\n" + 
							"				if (pt_c.status == QuantumBoolean.TRUE && this.currentPredicate.contains(StopReason.EVENT,null, supportQN, pt_c.name)) {\n" + 
							"					// struct to communicate the stop condition to the server\n" + 
							"					lastStopcondition = new StopCondition(StopReason.EVENT, clockToEObject.get(pt_c),\n" + 
							"							pt_c.name, lastknownTime.floatValue()); // always lastKnownTime ?\n" + 
							"					increment(finishDoStepSemaphore); // give control back to the caller.\n" + 
							"					// wait for stuff to be done in the server before to continue\n" + 
							"					decrement(startDoStepSemaphore); // waiting for the caller to do something\n" + 
							"				}\n" + 
							"			}\n" + 
							"		}\n" + 
							"	}\n" + 
							"	\n" + 
							"	\n" + 
							"	public void executeDSAsAndLog(boolean logClocks, Map<Clock, String> clockToMethodName, Map<Clock, String> clockToEObject,\n" + 
							"			org.eclipse.emf.ecore.resource.Resource resource, Solver solver) {\n" + 
							"		StepManagerRegistry.getInstance().registerManager(this);\n" + 
							"        clockToForceNext.clear();\n" +
							"        for(Clock pt_c: solver.clocks) {\n" + 
							"            if (! (pt_c instanceof Constraint)) {\n" + 
							"               if (logClocks) System.out.println(pt_c);\n" + 
							"            	if (pt_c.status == QuantumBoolean.TRUE) {\n" + 
							"            		String eobjectURIFrag = clockToEObject.get(pt_c);\n" + 
							"            		if (eobjectURIFrag != null) {\n" + 
							"            			org.eclipse.emf.ecore.EObject eo = resource.getEObject(eobjectURIFrag);\n" + 
							"            			Object oRes = this.execute(eo, clockToMethodName.get(pt_c), null);\n" + 
							"            			if ((oRes instanceof Boolean) && forcedClock.containsKey(pt_c)) {\n" + 
							"            				boolean bRes = (Boolean)oRes;\n" + 
							"            				for(ForceCase fc : forcedClock.get(pt_c)) {\n" + 
							"            					if (fc.associatedResValue == bRes) {\n" + 
							"            						clockToForceNext.add(fc.clockToForce);\n" + 
							"            					}\n" + 
							"            				}\n" + 
							"            			}\n" + 
							"            		}\n" + 
							"            	}\n" + 
							"            }\n" + 
							"        }" + 
							"        StepManagerRegistry.getInstance().unregisterManager(this);\n" + 
							"	}\n" + 
							"\n" + 
							"	@Override\n" + 
							"	public void readyToRead(EObject caller, Object propContainer, StepCommand command, String className, String propertyName) {\n" + 
							"		System.out.println(\"in readyToRead(\"+propertyName+\") on \"+EcoreQNHelper.getQualifiedName(caller));\n" + 
							"		if(currentPredicate != null && currentPredicate.contains(StopReason.READYTOREAD ,caller,className,propertyName)) {\n" + 
							"			//struct to communicate the stop condition to the server\n" + 
							"			lastStopcondition = new StopCondition(StopReason.READYTOREAD, /*caller, propContainer, command,*/ EcoreQNHelper.getQualifiedName(caller), propertyName, lastknownTime.floatValue()); //always lastKnownTime ?\n" + 
							"			increment(finishDoStepSemaphore); // give control back to the caller.\n" + 
							"			//wait for stuff to be done in the server before to continue\n" + 
							"			decrement(startDoStepSemaphore); //waiting for the caller to do something\n" + 
							"		}\n" + 
							"		command.execute();\n" + 
							"	}\n" + 
							"\n" + 
							"	\n" + 
							"\n" + 
							"	@Override\n" + 
							"	public void justUpdated(EObject caller, Object propContainer, StepCommand command, String className, String propertyName) {\n" + 
							"		System.out.println(\"in justUpdated(\"+propertyName+\") on \"+EcoreQNHelper.getQualifiedName(caller));\n" + 
							"		command.execute();\n" + 
							"		if(currentPredicate != null && currentPredicate.contains(StopReason.UPDATE ,caller,className,propertyName)) {\n" + 
							"			//struct to communicate the stop condition to the server\n" + 
							"			lastStopcondition = new StopCondition(StopReason.UPDATE, /*caller, propContainer, command,*/ EcoreQNHelper.getQualifiedName(caller), propertyName, lastknownTime.floatValue()); //always lastKnownTime ?\n" + 
							"			increment(finishDoStepSemaphore); // give control back to the caller.\n" + 
							"			//Optionally waiting for the caller to do something before to continue (logically we wait for antoher doStep)\n" + 
							"			decrement(startDoStepSemaphore); \n" + 
							"		}\n" + 
							"	}\n" + 
							"\n" + 
							"	@Override\n" + 
							"	public void timePassed(EObject caller, Object propContainer, StepCommand command, String className, double value) {\n" + 
							"		System.out.println(\"in timePassed(\"+value+\") on \"+EcoreQNHelper.getQualifiedName(caller));\n" + 
							"		command.execute();\n" + 
							"		lastknownTime.set(value);\n" + 
							"		\n" + 
							"		if (currentPredicate != null && currentPredicate.contains(StopReason.TIME ,caller,className, Double.toString(value))) {\n" + 
							"			double delta = ((TemporalPredicate)currentPredicate.getTemporalPredicate()).deltaT;\n" + 
							"			if ((value - timeBeforeDoStep) >= delta){\n" + 
							"				//struct to communicate the stop condition to the server\n" + 
							"				lastStopcondition = new StopCondition(StopReason.TIME, /*caller, propContainer, command, */EcoreQNHelper.getQualifiedName(caller), \"time\", lastknownTime.floatValue()); //always lastKnownTime ?\n" + 
							"				increment(finishDoStepSemaphore); // give control back to the caller.\n" + 
							"				//wait for stuff to be done in the server before to continue\n" + 
							"				decrement(startDoStepSemaphore); //waiting for the caller to do something\n" + 
							"			}\n" + 
							"		}\n" + 
							"	}\n" + 
							"\n" + 
							"\n" + 
							"	@Override\n" + 
							"	/*\n" + 
							"	 * This is the operation called from K3 code. We use this callback to pass the\n" + 
							"	 * command to the generic executeOperation operation. (non-Javadoc)\n" + 
							"	 * \n" + 
							"	 * @see fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager#\n" + 
							"	 * executeStep(java.lang.Object,\n" + 
							"	 * fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand,\n" + 
							"	 * java.lang.String)\n" + 
							"	 */\n" + 
							"	public void executeStep(final Object caller, final StepCommand command, String className, final String methodName) {\n" + 
							"		System.out.println(\"in executeDebugStep in \"+className+\"::\"+methodName+\" on \"+caller);\n" + 
							"		if (currentPredicate != null && currentPredicate.contains(StopReason.DEBUGSTEP, null ,null , null)) {\n" + 
							"				//struct to communicate the stop condition to the server\n" + 
							"				lastStopcondition = new StopCondition(StopReason.DEBUGSTEP, caller.toString(), \"time\", lastknownTime.floatValue()); //always lastKnownTime ?\n" + 
							"				increment(finishDoStepSemaphore); // give control back to the caller.\n" + 
							"				//wait for stuff to be done in the server before to continue\n" + 
							"				decrement(startDoStepSemaphore); //waiting for the caller to do something\n" + 
							"				command.execute();\n" + 
							"		}\n" + 
							"	}\n" + 
							"\n" + 
							"@Override\n" + 
							"	public boolean canHandle(Object caller) {\n" + 
							"		return true;\n" + 
							"	}\n" + 
							"\n" + 
							"\n" + 
							"	@Override\n" + 
							"	public String engineKindName() {\n" + 
							"		return \"Compiled Coordination Engine\";" + 
							"	}\n" + 
							"\n" + 
							"	//unused from inheritance.... yet\n" + 
							"\n" + 
							"	@Override\n" + 
							"	protected void executeEntryPoint() {\n" + 
							"	}\n" + 
							"	@Override\n" + 
							"	protected void initializeModel() {\n" + 
							"	}\n" + 
							"	@Override\n" + 
							"	protected void prepareEntryPoint(GenericModelExecutionContext<ISequentialRunConfiguration> arg0) {\n" + 
							"	}\n" + 
							"	@Override\n" + 
							"	protected void prepareInitializeModel(GenericModelExecutionContext<ISequentialRunConfiguration> arg0) {\n"
							).getBytes());
				   mainFile.append(("    }\n}\n").getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			
			
			
			
//	private IFolder createProject(String projectName) {
//		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
//		IProject project = root.getProject(projectName);
//		try {
//			project.create(null);	
//			project.open(null);
//		} catch (CoreException e) {
//			e.printStackTrace();
//		}
//		IProjectDescription description;
//		try {
//			description = project.getDescription();
//			description.setNatureIds(new String[] {"org.eclipse.jdt.core.javanature"});
//			project.setDescription(description, null);
//			
//		} catch (CoreException e1) {
//			e1.printStackTrace();
//		}
//		IJavaProject javaProject = JavaCore.create(project); 
//		IFolder srcFolder = project.getFolder("src");
//		try {
//			srcFolder.create(false, true, null);
//		} catch (CoreException e) {
//			e.printStackTrace();
//		}
//		IFolder binFolder = project.getFolder("bin");
//		try {
//			binFolder.create(false, true, null);
//		} catch (CoreException e) {
//			e.printStackTrace();
//		}
//		try {
//			javaProject.setOutputLocation(binFolder.getFullPath(), null);
//		} catch (JavaModelException e) {
//			e.printStackTrace();
//		}
//		return srcFolder;
//	}


	Map<Object, InstantiatedElement> objToIe = new HashMap<Object, InstantiatedElement>();	
	Map<Object, Integer> objToUID = new HashMap<Object, Integer>();	
	int uid = 0;
	
	public String cleanQualifiedName(InstantiatedElement ie) {
		objToIe.put(ie.getInstantiationPath().getLast(), ie);
		if (objToUID.containsKey(ie)) {
			return ie.getQualifiedName("_").replaceAll("<NoName>", objToUID.get(ie).toString()).replaceAll("-", "_");
		}else {
			objToUID.put(ie, ++uid);
			return ie.getQualifiedName("_").replaceAll("<NoName>", new Integer(uid).toString()).replaceAll("-", "_");
		}
	}
	
	public String cleanQualifiedName(Object o) {
		return cleanQualifiedName(objToIe.get(o));
	}
	
}
