package fr.inria.aoste.timesquare.ccslkernel.codegenerator.switches;

import java.io.IOException;

import fr.inria.aoste.timesquare.ccslkernel.clocktree.generator.CoincidentClocks;
import fr.inria.aoste.timesquare.ccslkernel.codegenerator.handlers.CCSLJavaCodeGenerator;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Coincidence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Exclusion;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.NonStrictPrecedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.Precedence;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.SubClock;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelRelation.util.KernelRelationSwitch;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import toools.io.file.RegularFile;

public class KernelRelationBuilder extends KernelRelationSwitch<Boolean> {

	private InstantiatedElement concrete;
	private RegularFile mainFile;
	public CCSLJavaCodeGenerator codeExecutor;

	public KernelRelationBuilder(InstantiatedElement concrete, RegularFile mainFile, CCSLJavaCodeGenerator executor) {
		this.mainFile = mainFile;
		this.concrete = concrete;
		codeExecutor = executor;
	}

	@Override
	public Boolean caseCoincidence(Coincidence object) {
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftEntity());
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightEntity());
		try {
			mainFile.append(("\tCoincides "+codeExecutor.cleanQualifiedName(concrete)+" = new Coincides("+codeExecutor.cleanQualifiedName(right)+","+codeExecutor.cleanQualifiedName(left)+");\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean caseExclusion(Exclusion object) {
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightEntity());
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftEntity());
		try {
			mainFile.append(("\tExclusion "+codeExecutor.cleanQualifiedName(concrete)+" = new Exclusion("+codeExecutor.cleanQualifiedName(right)+","+codeExecutor.cleanQualifiedName(left)+");\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean caseNonStrictPrecedence(NonStrictPrecedence object) {
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightEntity());
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftEntity());
		try {
			mainFile.append(("\tCauses "+codeExecutor.cleanQualifiedName(concrete)+" = new Causes("+codeExecutor.cleanQualifiedName(left)+","+codeExecutor.cleanQualifiedName(right)+");\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean casePrecedence(Precedence object) {
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightEntity());
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftEntity());
		try {
			mainFile.append(("\tPrecedes "+codeExecutor.cleanQualifiedName(concrete)+" = new Precedes("+codeExecutor.cleanQualifiedName(left)+","+codeExecutor.cleanQualifiedName(right)+");\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean caseSubClock(SubClock object) {
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightEntity());
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftEntity());
		try {
			mainFile.append(("\tSubClock "+codeExecutor.cleanQualifiedName(concrete)+" = new SubClock("+codeExecutor.cleanQualifiedName(left)+","+codeExecutor.cleanQualifiedName(right)+");\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

}
