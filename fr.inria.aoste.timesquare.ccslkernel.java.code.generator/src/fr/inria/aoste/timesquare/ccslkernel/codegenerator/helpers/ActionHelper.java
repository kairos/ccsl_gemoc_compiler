package fr.inria.aoste.timesquare.ccslkernel.codegenerator.helpers;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.IntegerAssignement;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntMinus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntPlus;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;

public class ActionHelper {

	public ActionHelper(int idCounter, Map<EObject, Integer> map) {
		uid = idCounter;
		fsmObjToUid = map;
	}
	
	public int uid;
	
	public String dispatchGenerateExpressionMethod(ClassicalExpression expr, InstantiatedElement ie, String res) {
		synchronized (fsmObjToUid) {
			String guardString = "";
			if(fsmObjToUid.containsKey(expr)) {
				return res;
			}/*else {
				getUid(expr); //ensure uid is assigned.
			}*/
			if(expr instanceof IntPlus) {
				guardString = generateIntegerExpression(((IntPlus)expr), ie, res);
			}else if(expr instanceof IntMinus) {
				guardString = generateIntegerExpression(((IntMinus)expr), ie, res);
			}else if(expr instanceof IntegerAssignement) {
				guardString = generateIntegerExpression(((IntegerAssignement)expr), ie, res);
			}else if(expr instanceof IntegerRef) {
				if(fsmObjToUid.containsKey(((IntegerRef)expr).getIntegerElem())) {
					return res;
				}
				guardString = generateIntegerExpression(((IntegerRef)expr), ie, res);
			}else if(expr instanceof IntegerVariableRef) {
				guardString = generateIntegerExpression(((IntegerVariableRef)expr), ie, res);
			}else {
				guardString = generateIntegerExpression(expr, ie, res);
			}
			return guardString;
		}
		}

		Map<EObject, Integer> fsmObjToUid = null;

		public Integer getUid(EObject eo) {
				if (eo instanceof IntegerRef) {
					eo = ((IntegerRef) eo).getIntegerElem();
				}
//			if((eo instanceof IntegerVariableRef)) {
//				eo = ((IntegerVariableRef) eo).getReferencedVar();
//			}
			if (fsmObjToUid.containsKey(eo)) {
				int returnedInt = fsmObjToUid.get(eo);
				return returnedInt;
			}else {
				uid++;
				fsmObjToUid.put(eo, uid);
				return uid;
			}
		}
		
//		public String generateIntegerExpression(IntegerExpression expr) {
//			return "NOPE ! "+expr.getClass().getName();//throw new RuntimeException("CCSLJavaCodeGenerator: FSM guard traduction: "+expr.getClass().getName()+ " not supported, yet");
//		}
//		
		
		public String generateIntegerExpression(IntPlus expr, InstantiatedElement ie, String res) {
			res= dispatchGenerateExpressionMethod(expr.getLeftValue(), ie, res);
			res= dispatchGenerateExpressionMethod(expr.getRightValue(), ie, res);
			String exprName = "id_"+getUid(expr);
			String leftName ="";
			leftName = "id_"+getUid(expr.getLeftValue());
			String rightName ="";
			rightName = "id_"+getUid(expr.getRightValue());
			
			res += "IntAdd "+exprName+" = new IntAdd("+leftName+", "+rightName+");\n"; 
			return res;
		}
		
		public String generateIntegerExpression(IntMinus expr, InstantiatedElement ie, String res) {
			res= dispatchGenerateExpressionMethod(expr.getLeftValue(), ie, res);
			res= dispatchGenerateExpressionMethod(expr.getRightValue(), ie, res);
			String exprName = "id_"+getUid(expr);
			String leftName ="";
			leftName = "id_"+getUid(expr.getLeftValue());
			String rightName ="";
			rightName = "id_"+getUid(expr.getRightValue());
			res += "IntMinus "+exprName+" = new IntMinus("+leftName+", "+rightName+");\n"; 
			return res;
		}
		
		public String generateIntegerExpression(IntegerRef expr, InstantiatedElement ie, String res) {
//			String res = "id_"+getUid(expr.getIntegerElem());
			res += "IntRef id_"+getUid(expr.getIntegerElem())+" = new IntRef("+((IntegerElement)expr.getIntegerElem()).getValue().intValue()+");\n";
			return res;
		}
		
		public String generateIntegerExpression(IntegerVariableRef expr, InstantiatedElement ie, String res) {
			AbstractEntity ae = expr.getReferencedVar();
			IntegerElement ielem = (IntegerElement) ie.resolveAbstractEntity(ae).getInstantiationPath().getLast();
//			if(fsmObjToUid.containsKey(ae)) {
//				return res;
//			}
			res += "IntRef id_"+getUid(expr)+" = new IntRef("+ielem.getValue().intValue()+"); // from "+ae.getName()+"\n" ;
			return res;
		}
		
		public String generateIntegerExpression(ClassicalExpression expr, InstantiatedElement ie, String res) {
			throw new RuntimeException("CCSLJavaCodeGenerator: FSM guard traduction: "+expr.getClass().getName()+ " not supported, yet");
		}
		
		public String generateIntegerExpression(IntegerAssignement expr, InstantiatedElement ie, String res) {
			res= dispatchGenerateExpressionMethod(expr.getLeftValue(), ie, res);
			res= dispatchGenerateExpressionMethod(expr.getRightValue(), ie, res);
			String exprName = "id_"+getUid(expr);
			String leftName ="";
			 leftName = "id_"+getUid(expr.getLeftValue());
			String rightName ="";
			rightName = "id_"+getUid(expr.getRightValue());
			res += "IntAssign "+exprName+" = new IntAssign("+leftName+", "+rightName+");\n"; 
			return res;
		}
	
}
