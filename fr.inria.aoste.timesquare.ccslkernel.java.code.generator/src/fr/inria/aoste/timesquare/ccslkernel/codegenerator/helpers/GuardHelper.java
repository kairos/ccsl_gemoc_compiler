package fr.inria.aoste.timesquare.ccslkernel.codegenerator.helpers;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.editionextension.IntInfEqual;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.editionextension.IntSupEqual;

import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.And;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.ClassicalExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntEqual;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntInf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntSup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Or;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;

public class GuardHelper {

	public GuardHelper(int idCounter, Map<EObject, Integer> map) {
		uid = idCounter;
		fsmObjToUid = map;
	}
	public int uid;
	
	public String dispatchGenerateExpressionMethod(ClassicalExpression expr, InstantiatedElement ie, String res) {
		synchronized (fsmObjToUid) {
			String guardString = "";
			if(fsmObjToUid.containsKey(expr)) {
				return res;
			}/*else {
				getUid(expr); //ensure uid is assigned.
			}*/
			if(expr instanceof And) {
				guardString = generateBooleanExpression(((And)expr), ie, res);
			}else if(expr instanceof Or) {
				guardString = generateBooleanExpression(((Or)expr), ie, res);
			}else if(expr instanceof IntEqual) {
				guardString = generateBooleanExpression(((IntEqual)expr), ie, res);
			}else if(expr instanceof IntSup) {
				guardString = generateBooleanExpression(((IntSup)expr), ie, res);
			}else if(expr instanceof IntSupEqual) {
				guardString = generateBooleanExpression(((IntSupEqual)expr), ie, res);
			}else if(expr instanceof IntInf) {
				guardString = generateBooleanExpression(((IntInf)expr), ie, res);
			}else if(expr instanceof IntInfEqual) {
				guardString = generateBooleanExpression(((IntInfEqual)expr), ie, res);
			}else if(expr instanceof IntegerRef) {
				if(fsmObjToUid.containsKey(((IntegerRef)expr).getIntegerElem())) {
					return res;
				}
				guardString = generateBooleanExpression(((IntegerRef)expr), ie, res);
			}else if(expr instanceof IntegerVariableRef) {
				guardString = generateBooleanExpression(((IntegerVariableRef)expr), ie, res);
			}else {
				guardString = generateBooleanExpression(expr, ie, res);
			}
			return guardString;
		}
	}

		Map<EObject, Integer> fsmObjToUid = null;

		public Integer getUid(EObject eo) {
				if (eo instanceof IntegerRef) {
					eo = ((IntegerRef) eo).getIntegerElem();
				}
//			if((eo instanceof IntegerVariableRef)) {
//				eo = ((IntegerVariableRef) eo).getReferencedVar();
//			}
			if (fsmObjToUid.containsKey(eo)) {
				int returnedInt = fsmObjToUid.get(eo);
				return returnedInt;
			}else {
				uid++;
				fsmObjToUid.put(eo, uid);
				return uid;
			}
		}
		
//		public String generateBooleanExpression(IntegerExpression expr) {
//			return "NOPE ! "+expr.getClass().getName();//throw new RuntimeException("CCSLJavaCodeGenerator: FSM guard traduction: "+expr.getClass().getName()+ " not supported, yet");
//		}
//		
		
		public String generateBooleanExpression(And expr, InstantiatedElement ie, String res) {
			res= dispatchGenerateExpressionMethod(expr.getLeftValue(), ie, res);
			res= dispatchGenerateExpressionMethod(expr.getRightValue(), ie, res);
			String exprName = "id_"+getUid(expr);
			String leftName ="";
			leftName = "id_"+getUid(expr.getLeftValue());
			String rightName ="";
			rightName = "id_"+getUid(expr.getRightValue());
			
				res += "And "+exprName+" = new And("+leftName+", "+rightName+");\n"; 
			return res;
		}
		
		public String generateBooleanExpression(Or expr, InstantiatedElement ie, String res) {
			res= dispatchGenerateExpressionMethod(expr.getLeftValue(), ie, res);
			res= dispatchGenerateExpressionMethod(expr.getRightValue(), ie, res);
			String exprName = "id_"+getUid(expr);
			String leftName ="";
			leftName = "id_"+getUid(expr.getLeftValue());
			String rightName ="";
			rightName = "id_"+getUid(expr.getRightValue());
			
				res += "Or "+exprName+" = new Or("+leftName+", "+rightName+");\n"; 
			return res;
		}
		
		public String generateBooleanExpression(IntegerRef expr, InstantiatedElement ie, String res) {
//			String res = "id_"+getUid(expr.getIntegerElem());
			res += "IntRef id_"+getUid(expr.getIntegerElem())+" = new IntRef("+((IntegerElement)expr.getIntegerElem()).getValue().intValue()+");\n";
			return res;
		}
		
		public String generateBooleanExpression(IntegerVariableRef expr, InstantiatedElement ie, String res) {
			AbstractEntity ae = expr.getReferencedVar();
			IntegerElement ielem = (IntegerElement) ie.resolveAbstractEntity(ae).getInstantiationPath().getLast();
//			if(fsmObjToUid.containsKey(ae)) {
//				return res;
//			}
			res += "IntRef id_"+getUid(expr)+" = new IntRef("+ielem.getValue().intValue()+"); // from "+ae.getName()+"\n" ;
			return res;
		}
		
		public String generateBooleanExpression(ClassicalExpression expr, InstantiatedElement ie, String res) {
			throw new RuntimeException("CCSLJavaCodeGenerator: FSM guard traduction: "+expr.getClass().getName()+ " not supported, yet");
		}
		
		public String generateBooleanExpression(IntEqual expr, InstantiatedElement ie, String res) {
			res= dispatchGenerateExpressionMethod(expr.getLeftValue(), ie, res);
			res= dispatchGenerateExpressionMethod(expr.getRightValue(), ie, res);
			String exprName = "id_"+getUid(expr);
			String leftName ="";
			 leftName = "id_"+getUid(expr.getLeftValue());
			String rightName ="";
			rightName = "id_"+getUid(expr.getRightValue());
			res += "IntEquals "+exprName+" = new IntEquals("+leftName+", "+rightName+");\n"; 
			return res;
		}
		
		public String generateBooleanExpression(IntInfEqual expr, InstantiatedElement ie, String res) {
			res= dispatchGenerateExpressionMethod(expr.getLeftValue(), ie, res);
			res= dispatchGenerateExpressionMethod(expr.getRightValue(), ie, res);
			String exprName = "id_"+getUid(expr);
			String leftName ="";
			leftName = "id_"+getUid(expr.getLeftValue());
			String rightName ="";
			rightName = "id_"+getUid(expr.getRightValue());
			res += "IntInfEqual "+exprName+" = new IntInfEqual("+leftName+", "+rightName+");\n"; 
			return res;
		}
		
		public String generateBooleanExpression(IntSupEqual expr, InstantiatedElement ie, String res) {
			res= dispatchGenerateExpressionMethod(expr.getLeftValue(), ie, res);
			res= dispatchGenerateExpressionMethod(expr.getRightValue(), ie, res);
			String exprName = "id_"+getUid(expr);
			String leftName ="";
			leftName = "id_"+getUid(expr.getLeftValue());
			String rightName ="";
			rightName = "id_"+getUid(expr.getRightValue());
			res += "IntSupEqual "+exprName+" = new IntSupEqual("+leftName+", "+rightName+");\n"; 
			return res;
		}
		
		public String generateBooleanExpression(IntSup expr, InstantiatedElement ie, String res) {
			res= dispatchGenerateExpressionMethod(expr.getLeftValue(), ie, res);
			res= dispatchGenerateExpressionMethod(expr.getRightValue(), ie, res);
			String exprName = "id_"+getUid(expr);
			String leftName ="";
			leftName = "id_"+getUid(expr.getLeftValue());
			String rightName ="";
			rightName = "id_"+getUid(expr.getRightValue());
			res += "IntSup "+exprName+" = new IntSup("+leftName+", "+rightName+");\n"; 
			return res;
		}
		
		public String generateBooleanExpression(IntInf expr, InstantiatedElement ie, String res) {
			res= dispatchGenerateExpressionMethod(expr.getLeftValue(), ie, res);
			res= dispatchGenerateExpressionMethod(expr.getRightValue(), ie, res);
			String exprName = "id_"+getUid(expr);
			String leftName ="";
			leftName = "id_"+getUid(expr.getLeftValue());
			String rightName ="";
			rightName = "id_"+getUid(expr.getRightValue());
			res += "IntInf "+exprName+" = new IntInf("+leftName+", "+rightName+");\n"; 
			return res;
		}
		
	
}
