/*******************************************************************************
 * Copyright (c) 2017 INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     INRIA - initial API and implementation
 *     I3S Laboratory - API update and bug fix
 *******************************************************************************/
package org.eclipse.gemoc.execution.concurrent.compiler.commons;

import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.gemoc.execution.concurrent.ccsljavaengine.commons.MoccmlExecutionPlatform;
import org.eclipse.gemoc.execution.concurrent.ccsljavaengine.extensions.k3.dsa.api.IK3DSAExecutorClassLoader;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.extensions.languages.MoccmlLanguageAdditionExtension;
import org.eclipse.gemoc.xdsmlframework.api.core.IRunConfiguration;
import org.eclipse.gemoc.xdsmlframework.api.extensions.languages.LanguageDefinitionExtension;

public class ConcurrentCompilerExecutionPlatform extends MoccmlExecutionPlatform  {
	
//	private ICodeExecutor _codeExecutor;
//	private Collection<IMSEStateController> _clockControllers;
	public Set<Class<?>> allAspects;
	
	public ConcurrentCompilerExecutionPlatform(MoccmlLanguageAdditionExtension _moccmlLanguageAddition,
			LanguageDefinitionExtension _languageDefinition, IRunConfiguration runConfiguration) throws CoreException 
	{
		super(_moccmlLanguageAddition, _languageDefinition, runConfiguration);	
//		_codeExecutor = _languageDefinition.instanciateCodeExecutor();
		K3DSLCodeExecutor k3ce = new K3DSLCodeExecutor((IK3DSAExecutorClassLoader) _codeExecutor, _languageDefinition.getName());
		allAspects = k3ce.allAspects;
//		_clockControllers = _languageDefinition.instanciateMSEStateControllers();
		
	}


//	@Override
//	public ICodeExecutor getCodeExecutor() 
//	{
//		return _codeExecutor;
//	}
//
////	@Override
////	public Iterable<IEngineAddon> getEngineAddons() 
////	{
////		synchronized(_addonLock)
////		{
////			return Collections.unmodifiableCollection(new ArrayList<IEngineAddon>(_addons));
////		}
////	}
//
//	@Override
//	public Collection<IMSEStateController> getMSEStateControllers() 
//	{
//		return _clockControllers;
//	}
//
//	@Override
//	public void dispose() 
//	{
//		super.dispose();
//		_clockControllers.clear();
//	}
//
//	@Override
//	public IConcurrentExecutionPlatform asConcurrentExecutionPlatform() {
//		if( this instanceof IConcurrentExecutionPlatform) return this;
//		else return null;
//	}
//
////	private Object _addonLock = new Object();
////	
////	@Override
////	public void addEngineAddon(IEngineAddon addon) 
////	{
////		synchronized (_addonLock) 
////		{
////			_addons.add(addon);
////		}
////	}
////
////	@Override
////	public void removeEngineAddon(IEngineAddon addon) 
////	{
////		synchronized (_addonLock) 
////		{
////			_addons.remove(addon);
////		}
////	}
	
}
