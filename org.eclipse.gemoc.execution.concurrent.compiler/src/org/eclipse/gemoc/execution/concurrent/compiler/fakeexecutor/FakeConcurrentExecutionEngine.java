package org.eclipse.gemoc.execution.concurrent.compiler.fakeexecutor;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gemoc.execution.concurrent.ccsljavaengine.commons.MoccmlModelExecutionContext;
import org.eclipse.gemoc.execution.concurrent.ccsljavaengine.engine.AbstractSolverCodeExecutorConcurrentEngine;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.core.IConcurrentRunConfiguration;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.core.IMoccmlRunConfiguration;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.dsa.executors.CodeExecutionException;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.dsa.executors.ICodeExecutor;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.moc.ICCSLSolver;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.moc.ISolver;
import org.eclipse.gemoc.execution.concurrent.compiler.Activator;
import org.eclipse.gemoc.execution.concurrent.compiler.commons.ConcurrentCompilationContext;
import org.eclipse.gemoc.executionframework.engine.core.AbstractExecutionEngine;
import org.eclipse.gemoc.executionframework.engine.core.CommandExecution;
import org.eclipse.gemoc.trace.commons.model.trace.SmallStep;
import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionContext;

import fr.inria.diverse.k3.al.annotationprocessor.InitializeModel;

public class FakeConcurrentExecutionEngine extends AbstractSolverCodeExecutorConcurrentEngine<MoccmlModelExecutionContext, IMoccmlRunConfiguration, ICCSLSolver> {

	ISolver _ccslSolver;
	
	public FakeConcurrentExecutionEngine(MoccmlModelExecutionContext concurrentexecutionContext, ISolver s) throws CoreException 
	{
		super();
		_solver = s;
		initialize(concurrentexecutionContext);
	}

	

	protected Step<?> _selectedLogicalStep;

	

	/**
	 * 
	 * @return the IConcurrenExecutionContext or null if no such context is available
	 */
	public MoccmlModelExecutionContext getConcurrentExecutionContext() {

		IExecutionContext<?,?,?> context = getExecutionContext();
		if (context instanceof MoccmlModelExecutionContext) {
			return (MoccmlModelExecutionContext) context;
		} else
			return null;
	}

	protected ISolver _solver;

	

	@Override
	public String toString() {
		return this.getClass().getName() + "@[FAKE Executor=" +" ; ModelResource=" + _executionContext.getResourceModel() + "]";
	}

	
//	@Override
//	public final void performInitialize(MoccmlModelExecutionContext executionContext) {
//
//		if (!(executionContext instanceof MoccmlModelExecutionContext))
//			throw new IllegalArgumentException(
//					"executionContext must be an IConcurrentExecutionContext when used in ConcurrentExecutionEngine");
//
//		IConcurrentExecutionContext concurrentExecutionContext = getConcurrentExecutionContext();
//		_solver.setExecutableModelResource(concurrentExecutionContext.getResourceModel());
//		executeInitializeModelMethod(executionContext);
//		
//		((ConcurrentCompilationContext)executionContext).setUpMSEModel();
//		((ConcurrentCompilationContext)executionContext).setUpFeedbackModel();
//		
//		Activator.getDefault().debug("*** Engine initialization done. ***");
//	}
	
	@Override
	protected void performSpecificInitialize(MoccmlModelExecutionContext executionContext) {

		MoccmlModelExecutionContext concurrentExecutionContext = getConcurrentExecutionContext();
		((ICCSLSolver) _solver).setExecutableModelResource(concurrentExecutionContext.getResourceModel());
		executeInitializeModelMethod(executionContext);
		
		((MoccmlModelExecutionContext)executionContext).setUpMSEModel();
		((MoccmlModelExecutionContext)executionContext).setUpFeedbackModel();
		
	}

	protected void executeInitializeModelMethod(MoccmlModelExecutionContext executionContext) {

		String modelInitializationMethodQName = executionContext.getRunConfiguration().getModelInitializationMethod();
		if (!modelInitializationMethodQName.isEmpty()) {
			Object target = executionContext.getResourceModel().getContents().get(0);
			String modelInitializationMethodName = modelInitializationMethodQName
					.substring(modelInitializationMethodQName.lastIndexOf(".") + 1);
			Activator.getDefault()
					.debug("*** Calling Model initialization method " + modelInitializationMethodName + "(). ***");
			

			final ArrayList<Object> modelInitializationParameters = new ArrayList<>();
			ICodeExecutor codeExecutor = getConcurrentExecutionContext().getExecutionPlatform().getCodeExecutor(); 
			ArrayList<Object> parameters = new ArrayList<Object>();
			// try with String[] args			
			parameters.add(new String[1]);
			List<Method> methods = codeExecutor.findCompatibleMethodsWithAnnotation(target, parameters, InitializeModel.class);
			if(!methods.isEmpty()){
				modelInitializationParameters.add(executionContext.getRunConfiguration().getModelInitializationArguments().split("\\r?\\n"));
			} else {
				// try with List<String>
				parameters.clear();
				parameters.add(new ArrayList<String>());
				methods.addAll(codeExecutor.findCompatibleMethodsWithAnnotation(target, parameters, InitializeModel.class));
				if(!methods.isEmpty()){
					ArrayList<String> modelInitializationArgs = new ArrayList<>();
					for (String s : executionContext.getRunConfiguration().getModelInitializationArguments().split("\\r?\\n")) {
						modelInitializationArgs.add(s);
					}
					modelInitializationParameters.add(modelInitializationArgs);
				} else {
					// try with EList<String>
					parameters.clear();
					parameters.add(new BasicEList<String>());
					methods.addAll(codeExecutor.findCompatibleMethodsWithAnnotation(target, parameters, InitializeModel.class));
					if(!methods.isEmpty()){
						BasicEList<String> modelInitializationArgs = new BasicEList<>();
						for (String s : executionContext.getRunConfiguration().getModelInitializationArguments().split("\\r?\\n")) {
							modelInitializationArgs.add(s);
						}
						modelInitializationParameters.add(modelInitializationArgs);
					}	
				}
			}
			final TransactionalEditingDomain editingDomain = TransactionalEditingDomain.Factory.INSTANCE
					.getEditingDomain(getExecutionContext().getResourceModel().getResourceSet());
			if (editingDomain != null) {
				final RecordingCommand command = new RecordingCommand(editingDomain,
						"execute  " + modelInitializationMethodQName) {
					private List<Object> result = new ArrayList<Object>();

					@Override
					protected void doExecute() {
						try {
							result.add(codeExecutor.execute(target, modelInitializationMethodName, modelInitializationParameters));
							Activator.getDefault().debug("*** Model initialization done. ***");
						} catch (CodeExecutionException e) {
							Activator.getDefault().error("Exception while initializing model " + e.getMessage(), e);
						}
					}

					@Override
					public Collection<?> getResult() {
						return result;
					}
				};
				CommandExecution.execute(editingDomain, command);
			} else {
				try {
					codeExecutor.execute(target,
							modelInitializationMethodName, modelInitializationParameters);
					Activator.getDefault().debug("*** Model initialization done. ***");
				} catch (CodeExecutionException e) {
					Activator.getDefault().error("Exception while initializing model " + e.getMessage(), e);
				}
			}
		} else {
			Activator.getDefault().debug(
					"*** Model initialization done. (no modelInitialization method defined for the language) ***");
		}

	}

	@Override
	public String engineKindName() {
		return "GEMOC Concurrent Compiler";
	}

	@Override
	protected void beforeStart() {
	}


	@Override
	protected void beforeUpdatePossibleLogicalSteps() {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void doAfterLogicalStepExecution(Step<?> logicalStep) {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void executeSmallStep(SmallStep<?> smallStep) throws CodeExecutionException {
		// TODO Auto-generated method stub
		
	}


//
//	@Override
//	protected void performStart() {
//	}
//
//
//	@Override
//	protected void performStop() {
//	}
//
//
//	@Override
//	protected void finishDispose() {
//	}

}