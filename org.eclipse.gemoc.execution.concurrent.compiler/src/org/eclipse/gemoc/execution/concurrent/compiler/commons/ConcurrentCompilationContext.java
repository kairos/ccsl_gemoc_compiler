/*******************************************************************************
 * Copyright (c) 2017 INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     INRIA - initial API and implementation
 *     I3S Laboratory - API update and bug fix
 *******************************************************************************/
package org.eclipse.gemoc.execution.concurrent.compiler.commons;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.gemoc.execution.concurrent.ccsljavaengine.commons.MoccmlModelExecutionContext;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.extensions.languages.MoccmlLanguageAdditionExtension;
import org.eclipse.gemoc.executionframework.engine.commons.EngineContextException;
import org.eclipse.gemoc.xdsmlframework.api.core.ExecutionMode;
import org.eclipse.gemoc.xdsmlframework.api.extensions.languages.LanguageDefinitionExtension;


public class ConcurrentCompilationContext extends MoccmlModelExecutionContext {

	public CompilerMode compilerMode= CompilerMode.COORDINATION;
	public String resultProjectName= "res";
	
	public ConcurrentCompilationContext(ConcurrentCompilerRunConfiguration runConfiguration)
			throws EngineContextException
	{
		super(runConfiguration, ExecutionMode.Run);
		if(runConfiguration.getCompilerMode().contains("execution")) {
			compilerMode = CompilerMode.EXECUTION;
		}else
		if(runConfiguration.getCompilerMode().contains("MoCCML")) {
			compilerMode = CompilerMode.MOCCML_ONLY;
		}
		resultProjectName = runConfiguration.getResultProjectName();
		
	}

	
//	public MoccmlLanguageAdditionExtension getMoccmlLanguageAddition(String languageName)
//			throws EngineContextException {
//		MoccmlLanguageAdditionExtension languageAddition = MoccmlLanguageAdditionExtensionPoint.findMoccmlLanguageAdditionForLanguage(languageName);
//		if (languageAddition == null) {
//			String message = "Cannot find moccml addition for the language "
//					+ _runConfiguration.getLanguageName() + ", please verify that is is correctly deployed.";
//			throw new EngineContextException(message);
//		}
//		return languageAddition;
//	}
//
//	public MoccmlLanguageAdditionExtension getMoccmlLanguageAdditionExtension() {
//		return _moccmlLanguageAdditionExtension;
//	}
//	
	@Override
	protected ConcurrentCompilerExecutionPlatform createExecutionPlatform() throws CoreException{
		LanguageDefinitionExtension moccmlLangDef;
		try {
			moccmlLangDef = getLanguageDefinitionExtension(this._runConfiguration.getLanguageName());
			MoccmlLanguageAdditionExtension languageAddition = getMoccmlLanguageAddition(this._runConfiguration.getLanguageName());
			ConcurrentCompilerExecutionPlatform platform = new ConcurrentCompilerExecutionPlatform(languageAddition, moccmlLangDef, _runConfiguration);
			platform.setCodeExecutor(languageAddition.instanciateCodeExecutor());
			return platform;
		} catch (EngineContextException e) {
			// TODO avoid runtime exception
			throw new RuntimeException(e);
		}
	}
//	
//	@Override
//	protected LanguageDefinitionExtension getLanguageDefinitionExtension(String languageName) throws EngineContextException
//	{
//		LanguageDefinitionExtension languageDefinition = LanguageDefinitionExtensionPoint
//				.findDefinition(_runConfiguration.getLanguageName());
//		if (languageDefinition == null)
//		{
//			String message = "Cannot find concurrent xdsml definition for the language " + _runConfiguration.getLanguageName()
//					+ ", please verify that is is correctly deployed.";
//			EngineContextException exception = new EngineContextException(message);
//			throw exception;
//		}
//		return languageDefinition;
//	}
//
//
//	public void setUpFeedbackModel()
//	{
//		URI feedbackPlatformURI = URI.createPlatformResourceURI(getWorkspace().getMSEModelPath().removeFileExtension().addFileExtension("feedback").toString(),
//				true);
//		try
//		{
//			Resource resource = this.getResourceModel().getResourceSet().getResource(feedbackPlatformURI, true);
//			_feedbackModel = (ActionModel) resource.getContents().get(0);
//		} catch (Exception e)
//		{
//			// file will be created later
//		}
//	}
//	
//	@Override
//	public void dispose()
//	{
//		super.dispose();
//		_logicalStepDecider.dispose();
//	}
//
//
//	@Override
//	protected String getDefaultRunDeciderName() {
//		return LogicalStepDeciderFactory.DECIDER_SOLVER;
//	}
//
//	
//
//	





	




}
