package org.eclipse.gemoc.execution.concurrent.compiler.commons;

public enum CompilerMode {
	COORDINATION,
	EXECUTION,
	MOCCML_ONLY
}
