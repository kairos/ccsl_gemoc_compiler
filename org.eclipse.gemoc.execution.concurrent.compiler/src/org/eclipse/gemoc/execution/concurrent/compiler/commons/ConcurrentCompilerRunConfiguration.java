/*******************************************************************************
 * Copyright (c) 2017 INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     INRIA - initial API and implementation
 *     I3S Laboratory - API update and bug fix
 *******************************************************************************/
package org.eclipse.gemoc.execution.concurrent.compiler.commons;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.gemoc.execution.concurrent.ccsljavaengine.commons.ConcurrentRunConfiguration;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.core.IMoccmlRunConfiguration;

public class ConcurrentCompilerRunConfiguration extends ConcurrentRunConfiguration implements IMoccmlRunConfiguration{

	public static final String COMPILER_MODE = "executable with coordination API";
	public static final String RESULT_PROJECT = "";
	private String _compilerMode;
	private String _resultProjectName;
	
	public ConcurrentCompilerRunConfiguration(ILaunchConfiguration launchConfiguration)
			throws CoreException {
		super(launchConfiguration);
		// TODO Auto-generated constructor stub
	}
	
	protected void extractInformation() throws CoreException {
		super.extractInformation();
		_compilerMode = getAttribute(COMPILER_MODE, "");
		_resultProjectName = getAttribute(RESULT_PROJECT, "");
	}
	
	public String getCompilerMode() {
		return _compilerMode;
	}
	
	public String getResultProjectName() {
		return _resultProjectName;
	}

	@Override
	public boolean getIsExhaustiveSimulation() {
		return false;
	}

	@Override
	public String getExecutionModelPath() {
		return null;
	}

}
