//
// Created by jdeanton on 5/28/17.
//
package org.eclipse.gemoc.moccml.compiled.library.constraints.relations;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class Exclusion extends Relation{
	
	public Clock c1;
	public Clock c2;

	public Exclusion(Clock l, Clock r){
    	c1 = l; 
    	c2 = r;
	}

	public boolean evaluate() {
	    return false;
	}

	public void rewrite() {
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesChoice() {
//    	    cout ).append( "Exclusion::propagatesChoice " ).append( " c1 = ").append( c1.status ).append( " c2 = " ).append( c2.status ).append( endl;

	    if(  (c1.status == QuantumBoolean.TRUE && c2.status == QuantumBoolean.FALSE)
	       ||(c1.status == QuantumBoolean.FALSE && c2.status == QuantumBoolean.TRUE)
	       ||(c1.status == QuantumBoolean.POSSIBLY && c2.status == QuantumBoolean.POSSIBLY)
	       ||(c1.status == QuantumBoolean.POSSIBLY && c2.status == QuantumBoolean.FALSE)
	       ||(c1.status == QuantumBoolean.FALSE && c2.status == QuantumBoolean.POSSIBLY)
	       ||(c1.status == QuantumBoolean.FALSE && c2.status == QuantumBoolean.FALSE)
	    ){
	        return true;
	    }

	    if (c1.status == QuantumBoolean.TRUE && c2.status == QuantumBoolean.POSSIBLY){
//    	        cout ).append( c2 ).append( endl;
//    	        assert(c2.status != QuantumBoolean.TRUE);
	        c2.status = QuantumBoolean.FALSE;
	        return false;
	    }
	    if (c2.status == QuantumBoolean.TRUE && c1.status == QuantumBoolean.POSSIBLY){
//    	        assert(c1.status != QuantumBoolean.TRUE);
	        c1.status = QuantumBoolean.FALSE;
	        return false;
	    }
	    
	    //backtrack
	    if (c2.status == QuantumBoolean.TRUE && c1.status == QuantumBoolean.TRUE){
	    	System.exit(-1);
	    	return false;
	    }

	    System.out.println(new StringBuilder("ERROR: in Exclusion, a case is missing: c1 = ").append( c1.status ).append( " c2 = " ).append( c2.status ).append("\n"));
	    System.exit(-1);
	    return true;
	}
	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesDeath(){
	    return true;
	}

	@Override
	public void reset() {
	}
	
	@Override
	public int compareTo(Constraint c) {
		return -1;
	}

}