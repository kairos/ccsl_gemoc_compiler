//
// Created by jdeanton on 6/2/17.
//
package org.eclipse.gemoc.moccml.compiled.library.constraints.expressions;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.ClassicalExpression;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.SeqHead;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.SequenceExpression;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;


public class Concatenation extends Clock implements Constraint {
	
	public Clock leftClock;
	public Clock rightClock;

    public boolean rightIsAConstraint = false;
    public boolean leftIsAConstraint = false;
    public boolean isRecursive = false;
    public boolean followLeft = true;
    
    public boolean rightIsRecursiveCall = false;
    public ClassicalExpression recursionExpression = null;

    public Concatenation(Clock lc, Clock rc, String name, boolean rightIsRecursiveCall){
    	super(name);
    	leftClock = lc;
    	rightClock = rc; 
	    if (lc instanceof Constraint){
	        leftIsAConstraint = true;
	    }
	    if (rc instanceof Constraint){
	        rightIsAConstraint = true;
	    }

	    if (rightClock == this){
	        isRecursive = true;
//	        System.out.println("--> "+ this.name + " is recursive");
	    }
	    
	    this.rightIsRecursiveCall= rightIsRecursiveCall; 

	}

    public Concatenation(String name){
    	super(name);
	}
    
    public void connect(Clock lc, Clock rc, boolean rightIsRecursiveCall, ClassicalExpression recursionParam){
    	leftClock = lc;
    	rightClock = rc; 
	    if (lc instanceof Constraint){
	        leftIsAConstraint = true;
	    }
	    if (rc instanceof Constraint){
	        rightIsAConstraint = true;
	    }

	    if (rightClock == this){
	        isRecursive = true;
	    }
	    
	    this.rightIsRecursiveCall= rightIsRecursiveCall; 
	    this.recursionExpression = recursionParam;

	}

	public boolean evaluate() {
		canBeRewrote = true;
	    if(isDead){
	        status = QuantumBoolean.FALSE;
	        return false;
	    }

	    if(followLeft && rightIsAConstraint  && rightClock != leftClock  && !isRecursive){
	        rightClock.status = QuantumBoolean.FALSE;
	    }
	    return false;
	}

	public void rewrite() {
		if (!canBeRewrote) return;
//		System.out.println("--> concat rewrite");
        if (leftClock.isDead && rightIsRecursiveCall && recursionExpression != null) {
        	leftClock.isDead = false;
        	if(recursionExpression instanceof SequenceExpression) {
        		//TODO check this ugly stuff here
        		if (leftClock instanceof Wait) {
//        			System.out.print("--> conRel rewrite strange: ");
//        			System.out.println("  wait before == "+((Wait)leftClock));
            		((Wait)leftClock).theExpr = new SeqHead(((SequenceExpression)recursionExpression).evaluate());
//            		System.out.println("  new  expr == "+((Wait)leftClock).theExpr );

            	}else {
            		((SequenceExpression)recursionExpression).evaluate();
            	}
        	}else {
        		throw new RuntimeException("Concatenation::rewrite() -> reursion expression is not implemented, yet: "+recursionExpression);
        	}
        	leftClock.reset();
        }
        if (leftClock.isDead && isRecursive){
     	    leftClock.isDead = false; //should be in reset of leftClock
        	leftClock.reset();
        
	    }
        if (leftClock.isDead && rightIsRecursiveCall){
     	    leftClock.isDead = false;
        	leftClock.reset();
	    }
	    if (leftClock.isDead && followLeft){
	        followLeft = false;
	        if(rightClock == leftClock){
	            rightClock.reset();
	        }
	    }
	    if (!followLeft && rightClock.isDead){
	        isDead = true;
	    }
	    canBeRewrote = false;
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesChoice() {
	    if(followLeft){ //follow left
	        if (leftClock.status != QuantumBoolean.POSSIBLY && status == QuantumBoolean.POSSIBLY){
	            status = leftClock.status;
	            return false;
	        }
	        if (leftClock.status == QuantumBoolean.POSSIBLY && status != QuantumBoolean.POSSIBLY){
	            leftClock.status = status;
	            return false;
	        }
	        if (leftClock.status == status ){
	            return true;
	        }
	        
	        //there is a conflict, propagates FALSE
			if(leftClock.status != QuantumBoolean.POSSIBLY &&  this.status != QuantumBoolean.POSSIBLY) {
				leftClock.status = QuantumBoolean.FALSE;
				this.status = QuantumBoolean.FALSE;
//				System.out.println("warning, concat back track !");
				return false;
			}
	        
	    }else{ //follow right
	        if (rightClock.status != QuantumBoolean.POSSIBLY && status == QuantumBoolean.POSSIBLY){
	            status = rightClock.status;
	            return false;
	        }
	        if (rightClock.status == QuantumBoolean.POSSIBLY && status != QuantumBoolean.POSSIBLY){
	            rightClock.status = status;
	            return false;
	        }
	        
	        if (rightClock.status == status ){
	            return true;
	        }
	        
	        //there is a conflict, propagates FALSE
			if(rightClock.status != QuantumBoolean.POSSIBLY &&  this.status != QuantumBoolean.POSSIBLY) {
				rightClock.status = QuantumBoolean.FALSE;
				this.status = QuantumBoolean.FALSE;
//				System.out.println("warning, back track !");
				return false;
			}
	    }


	    System.out.println(new StringBuilder("ERROR: in Concatenation Expression ").append(name).append(" a case is missing: left = ").append(leftClock.status).append(" right = ").append(rightClock.status).append(" def = ").append(status).append("\n"));
	    System.exit(-1);
	    return true;
	}

	public void reset(){
	    super.reset();
	    followLeft = true;
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesDeath(){
	    if (!followLeft && rightClock.isDead && !isDead){
	        isDead = true;
	        return false;
	    }
	    return true;
	}
	
	@Override
	public String toString() {
			return "Concat follows"+ (followLeft? leftClock.toString() : rightClock.toString());
	}
}

