package org.eclipse.gemoc.moccml.compiled.library;

import java.util.Random;

import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

/*
 * Clock.h
 *
 *  Created on: Aug 17, 2016
 *      Author: jdeanton
 */

public class Clock extends NamedElement{
	
	public int nbTick = 0;
	public Random random = new Random();
	public boolean isDead = false;
	public QuantumBoolean status = QuantumBoolean.POSSIBLY;
	public boolean canBeRewrote = true;
	public Clock(String name){
		super(name);
	}
	
	public void reset() {
		this.isDead = false;
	}
	
	public void ticks() {
		this.nbTick++;
	}
	
	/**
	 * @note in this method we can implement a simulation policy. For instance to choose true or false according to some probabilities
	 */
	public void chooseStatus(){
//	  int r = (random.nextInt(2));
//		if (r == 1 ){
			status = QuantumBoolean.TRUE;
//		}else{
//			status = QuantumBoolean.FALSE;
//		}
	}
	
	public String toString() {
		StringBuilder os = new StringBuilder();
		os.append( "Clock ").append(name).append( ": " ).append( nbTick ).append( ", " ).append( status);
	    if(isDead){
	        os.append(" (is dead)");
	    }
		return os.toString();
	}

};
