package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

public class IntAssign extends IntegerExpression {

	IntRef leftOperand;
	IntegerExpression rightOperand;
	
	public IntAssign(IntRef lint, IntegerExpression riexpr) {
		leftOperand = lint;
		rightOperand = riexpr;
	}
	
	@Override
	public Integer evaluate() {
		leftOperand.ref = rightOperand.evaluate().intValue();
		return 0; //by convention
	}
	
	@Override
	public String prettyPrint() {
		return "("+leftOperand.prettyPrint()+") := ("+rightOperand.prettyPrint()+")";
	}
	
}
