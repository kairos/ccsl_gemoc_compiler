package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

public abstract class ClassicalExpression {
	
	public abstract String prettyPrint();
	
}
