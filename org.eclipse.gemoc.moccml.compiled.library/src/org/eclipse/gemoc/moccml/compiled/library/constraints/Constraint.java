package org.eclipse.gemoc.moccml.compiled.library.constraints;


public interface Constraint extends Comparable<Constraint>{
	
	public abstract void reset();
	public abstract boolean evaluate();
	public abstract boolean propagatesChoice();
	public abstract void rewrite();
	public abstract boolean propagatesDeath();
	public default int compareTo(Constraint c) {
		return Integer.MAX_VALUE;
	}
}


