package org.eclipse.gemoc.moccml.compiled.library.constraints.expressions;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

//
// Created by jdeanton on 2/16/19.
//


public class UpTo extends Clock implements Constraint {

	public Clock clockToFollow;
	public Clock killer;

	public UpTo(Clock toFollow, Clock theKiller, String name) {
		super(name);
		clockToFollow =toFollow;
		killer = theKiller;
	}
	
	public UpTo(String name) {
		super(name);
	}
	
	public void connect(Clock toFollow, Clock theKiller) {
		clockToFollow =toFollow;
		killer = theKiller;
	}

	public void reset(){
	    super.reset();
	}

	public boolean evaluate() {
		canBeRewrote = true;
	    if(isDead){
	        status = QuantumBoolean.FALSE;
	        return false;
	    }
	    return false;
	}

	public void rewrite() {
		if (!canBeRewrote) return;
		
	    if(killer.status == QuantumBoolean.TRUE){
	        isDead = true;
	    }
	    canBeRewrote = false;
	    return;
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesChoice() {
	    if(isDead){
	        return true;
	    }

	    if( status == clockToFollow.status && killer.status != QuantumBoolean.TRUE){
	        return true;
	    }

	    if( status == QuantumBoolean.FALSE && killer.status == QuantumBoolean.TRUE){
	        return true;
	    }

	    if( status == QuantumBoolean.POSSIBLY && clockToFollow.status == QuantumBoolean.POSSIBLY && killer.status != QuantumBoolean.TRUE){
	        return true;
	    }

	    if( status == QuantumBoolean.POSSIBLY && clockToFollow.status != QuantumBoolean.POSSIBLY && killer.status != QuantumBoolean.TRUE){
	        status = clockToFollow.status;
	        return false;
	    }

	    if( status == QuantumBoolean.FALSE && clockToFollow.status == QuantumBoolean.POSSIBLY && killer.status == QuantumBoolean.FALSE){
	        clockToFollow.status = QuantumBoolean.FALSE;
	        return false;
	    }

	    if( status == QuantumBoolean.FALSE && clockToFollow.status == QuantumBoolean.POSSIBLY && killer.status == QuantumBoolean.POSSIBLY){
	        clockToFollow.status = QuantumBoolean.FALSE;
	        return false;
	    }

	    if( killer.status == QuantumBoolean.TRUE){
	        status = QuantumBoolean.FALSE;
	        return false;
	    }

	    System.out.println(new StringBuilder().append("ERROR: in UpTo Expression " ).append( name ).append( " a case is missing: clock to follow = ").append( clockToFollow.status ).append( " killer = " ).append( killer.status ).append( " def = " ).append( status ).append("\n").toString());
	    System.exit(-1);
	   return true;
	}


	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesDeath(){
	    if (clockToFollow.isDead && !isDead){
	        isDead = true;
	        return false;
	    }

	    return true;
	}


};


