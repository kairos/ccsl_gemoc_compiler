//
// Created by jdeanton on 2/14/19.
//
package org.eclipse.gemoc.moccml.compiled.library.constraints.expressions;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class Intersection extends Clock implements Constraint {
	
	public Clock c1;
	public Clock c2;

	public Intersection(Clock c11, Clock c22,String name){
    	super(name);
    	c1 = c11;
    	c2 = c22;
	}
	
	public Intersection(String name){
    	super(name);
	}
	
	public void connect(Clock c11, Clock c22){
    	c1 = c11;
    	c2 = c22;
	}


	public boolean evaluate() {
		canBeRewrote = true;
	    if(isDead){
	        status = QuantumBoolean.FALSE;
	        return false;
	    }
	    return false;
	}

	public void rewrite() {
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesChoice() {
//    	    cout).append("Intersection::propagatesChoice ").append(" c1 = ").append(c1.status).append(" c2 = ").append(c2.status).append(" def = ").append(status).append(endl;

	    if (status == QuantumBoolean.TRUE && (c1.status == QuantumBoolean.TRUE && c2.status == QuantumBoolean.TRUE)) {
	        return true;
	    }
	    if (status == QuantumBoolean.FALSE && (c1.status == QuantumBoolean.FALSE || c2.status == QuantumBoolean.FALSE)) {
	        return true;
	    }
	    if (status == QuantumBoolean.POSSIBLY
	        && (c1.status == QuantumBoolean.POSSIBLY  || c2.status == QuantumBoolean.POSSIBLY)) {
	        return true;
	    }

	    if (status == QuantumBoolean.FALSE && (c1.status == QuantumBoolean.POSSIBLY && c2.status == QuantumBoolean.POSSIBLY)) {
	        return true;
	    }

	    //if here, something must be done

	    if (c1.status == QuantumBoolean.TRUE  && c2.status == QuantumBoolean.TRUE){
	        status = QuantumBoolean.TRUE;
	        return false;
	    }
	    if (c1.status == QuantumBoolean.FALSE || c2.status == QuantumBoolean.FALSE) {
	        status = QuantumBoolean.FALSE;
	        return false;
	    }
	    if(status == QuantumBoolean.TRUE){
	        assert(c1.status != QuantumBoolean.FALSE);
	        c1.status = QuantumBoolean.TRUE;
	        assert(c2.status != QuantumBoolean.FALSE);
	        c2.status = QuantumBoolean.TRUE;
	        return false;
	    }

	    if (status == QuantumBoolean.FALSE && (c1.status == QuantumBoolean.TRUE && c2.status == QuantumBoolean.POSSIBLY)) {
	        c2.status = QuantumBoolean.FALSE;
	        return false;
	    }
	    if (status == QuantumBoolean.FALSE && (c1.status == QuantumBoolean.POSSIBLY && c2.status == QuantumBoolean.TRUE)) {
	        c1.status = QuantumBoolean.FALSE;
	        return false;
	    }


	    System.out.println(new StringBuilder("ERROR: in Intersection Expression ").append(name).append(" a case is missing: c1 = ").append(c1.status).append(" c2 = ").append(c2.status).append(" def = ").append(status).append("\n"));
	    System.exit(-1);
	    return true;
	}

	public void reset(){
	    super.reset();
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesDeath(){
	    if ((c1.isDead || c2.isDead) && !isDead){
	        isDead = true;
	        return false;
	    }
	    return true;
	}
}