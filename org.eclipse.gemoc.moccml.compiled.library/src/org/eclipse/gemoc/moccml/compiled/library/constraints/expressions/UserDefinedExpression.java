package org.eclipse.gemoc.moccml.compiled.library.constraints.expressions;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class UserDefinedExpression extends Clock implements Constraint {

	Clock root;
	
	public UserDefinedExpression(Clock rootExpression, String name) {
		super(name);
		root = rootExpression;
	}

	public UserDefinedExpression(String name) {
		super(name);
	}
	
	public void connect(Clock rootExpression) {
		root = rootExpression;
	}
	
	@Override
	public String toString() {
		return "User Defined: "+ root.toString();
	}
	
	@Override
	public void reset() {
	}

	@Override
	public boolean evaluate() {
		canBeRewrote = true;
	    if(isDead){
	        status = QuantumBoolean.FALSE;
	        return false;
	    }
		return false;
	}

	@Override
	public boolean propagatesChoice() {
		if(root.status == this.status) {
			return true;
		}
		
		//something to do here
		if(root.status == QuantumBoolean.POSSIBLY &&  this.status != QuantumBoolean.POSSIBLY) {
			root.status = this.status;
			return false;
		}
		
		if(root.status != QuantumBoolean.POSSIBLY &&  this.status == QuantumBoolean.POSSIBLY) {
			this.status = root.status;
			return false;
		}
		
		//there is a conflict, propagates FALSE
		if(root.status != QuantumBoolean.POSSIBLY &&  this.status != QuantumBoolean.POSSIBLY) {
			root.status = QuantumBoolean.FALSE;
			this.status = QuantumBoolean.FALSE;
//			System.out.println("warning, back track !");
			return false;
		}
		
		System.out.println(new StringBuilder("ERROR: in User Defined Expression ").append(name).append(" a case is missing: root = ").append(root.status).append(" def = ").append(status).append("\n"));
		System.exit(-1);
	return true;
	}

	@Override
	public void rewrite() {
	}

	@Override
	public boolean propagatesDeath() {
		if (root.isDead && this.isDead) {
			return true;
		}
		if (!root.isDead && !this.isDead) {
			return true;
		}
		root.isDead = true;
		this.isDead = true;
		return false;
	}
	
	@Override
	public void chooseStatus() {
		super.chooseStatus();
		propagatesChoice();
		if (root instanceof Constraint) {
			((Constraint) root).propagatesChoice();
		}
	}

}
