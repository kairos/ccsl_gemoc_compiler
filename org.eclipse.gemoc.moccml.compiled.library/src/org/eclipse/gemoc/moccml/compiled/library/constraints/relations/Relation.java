package org.eclipse.gemoc.moccml.compiled.library.constraints.relations;

import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;

abstract public class Relation  implements Constraint {
	public boolean canBeRewrote = false;
	public boolean isActive = true;
}
