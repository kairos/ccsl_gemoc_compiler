package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

public class IntInfEqual extends BooleanExpression {

	IntegerExpression leftOperand;
	IntegerExpression rightOperand;
	
	public IntInfEqual(IntegerExpression liexpr, IntegerExpression riexpr) {
		leftOperand = liexpr;
		rightOperand = riexpr;
	}
	
	@Override
	public boolean evaluate() {
		return leftOperand.evaluate() <= rightOperand.evaluate();
	}
	
	
	@Override
	public String prettyPrint() {
		return "("+leftOperand.prettyPrint()+") <= ("+rightOperand.prettyPrint()+")";
	}
}
