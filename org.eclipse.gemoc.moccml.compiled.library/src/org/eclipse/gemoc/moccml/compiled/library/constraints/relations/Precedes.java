/*
 * PrecedesRelation.h
 *
 *  Created on: Aug 17, 2016
 *      Author: jdeanton
 */
package org.eclipse.gemoc.moccml.compiled.library.constraints.relations;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class Precedes extends Relation{
	
	public int delta = 0;
    public Clock left;
    public Clock right;


	public Precedes(Clock l, Clock r){
		left =l;
		right = r;
	}


	public boolean evaluate() {
		if (!isActive) return false;
		
		canBeRewrote = true;
	    if (delta == 0){
	        right.status = QuantumBoolean.FALSE;
	    }
		return false;
	}

	public void rewrite() {
		if (!canBeRewrote) return;
		
	    if (left.status == QuantumBoolean.TRUE){
	        delta++;
	    }
	    if(right.status == QuantumBoolean.TRUE){
	        delta--;
	    }
	    canBeRewrote = false;
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesChoice() {
	    return true; //never propagates anything...
	}


	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesDeath(){
	    if (left.isDead && !right.isDead && delta == 0){
	        right.isDead = true;
	        return false;
	    }
	    return true;
	}


	@Override
	public void reset() {
	}
}