package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

public class IntMinus extends IntegerExpression {

	IntegerExpression leftOperand;
	IntegerExpression rightOperand;
	
	public IntMinus(IntegerExpression liexpr, IntegerExpression riexpr) {
		leftOperand = liexpr;
		rightOperand = riexpr;
	}
	
	@Override
	public Integer evaluate() {
		return leftOperand.evaluate() - rightOperand.evaluate();
	}
	
	@Override
	public String prettyPrint() {
		return "("+leftOperand.prettyPrint()+") - ("+rightOperand.prettyPrint()+")";
	}
	
}
