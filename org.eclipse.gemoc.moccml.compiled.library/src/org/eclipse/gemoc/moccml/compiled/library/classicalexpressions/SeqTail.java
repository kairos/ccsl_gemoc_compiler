package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

import org.eclipse.gemoc.moccml.compiled.library.utils.Sequence;

public class SeqTail extends SequenceExpression {

	Sequence seq;
	
	public SeqTail(Sequence s) {
		seq = s;
	}
	
	@Override
	public Sequence evaluate() {
		return seq.getTail();
	}
	
	@Override
	public String prettyPrint() {
		return "tail("+seq.name+")" ;
	}
	
}
