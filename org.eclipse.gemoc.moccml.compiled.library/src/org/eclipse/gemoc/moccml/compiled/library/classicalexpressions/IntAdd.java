package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

public class IntAdd extends IntegerExpression {

	IntegerExpression leftOperand;
	IntegerExpression rightOperand;
	
	public IntAdd(IntegerExpression liexpr, IntegerExpression riexpr) {
		leftOperand = liexpr;
		rightOperand = riexpr;
	}
	
	@Override
	public Integer evaluate() {
		return leftOperand.evaluate() + rightOperand.evaluate();
	}
	
	@Override
	public String prettyPrint() {
		return "("+leftOperand.prettyPrint()+") + ("+rightOperand.prettyPrint()+")";
	}
	
}
