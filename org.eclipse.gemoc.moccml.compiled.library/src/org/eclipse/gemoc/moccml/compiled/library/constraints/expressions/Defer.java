//
// Created by jdeanton on 5/29/17.
//
package org.eclipse.gemoc.moccml.compiled.library.constraints.expressions;

import java.util.ArrayList;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;
import org.eclipse.gemoc.moccml.compiled.library.utils.Sequence;

public class Defer extends Clock implements Constraint {
	
	public Clock baseClock;
	public Clock  delayClock;
    public Sequence sigma;
    private ArrayList<java.lang.Integer> ds = new ArrayList<java.lang.Integer>();

    public Defer(Clock bc, Clock dc, Sequence seq, String name){
    	super(name);
    	baseClock = bc;
    	delayClock = dc;
    	sigma = seq;
    }
    
    public Defer(String name){
    	super(name);
    }
    public void connect(Clock bc, Clock dc, Sequence seq){
    	baseClock = bc;
    	delayClock = dc;
    	sigma = seq;
    }
    
    public void reset(){
        super.reset();
        ds.clear();
    }

    public boolean getBeta() {
        boolean beta = false;
        if (ds.isEmpty()) {
            beta = false;
        }
        else {
            beta = (getDelay() == 1 || getDelay() == 0);
        }
        return beta;
    }

    public boolean evaluate() {
    	canBeRewrote = true;
        if(isDead){
            status = QuantumBoolean.FALSE;
            return false;
        }
        boolean beta = getBeta();
        if (! beta) {
            status = QuantumBoolean.FALSE;
        }

        return false;
    }


    public void rewrite() {
    	if (!canBeRewrote) return;
    	
        if (isDead){
        }

        if (baseClock.status == QuantumBoolean.TRUE) {
            if (delayClock.status == QuantumBoolean.TRUE) {
                // RWDefer3
                nextDelay();
                int next = sigma.nextHead();
                if (next != -1) {
                    sched(next, 0);
                }
            }
            else {
                // RWDefer2
                int next = sigma.nextHead();
                if (next != -1) {
                    sched(next, 0);
                }
            }
        }
        else if (delayClock.status == QuantumBoolean.TRUE) {
            // RWDefer1 nextDelay()
            nextDelay();
        }

        if ( (ds.isEmpty() && sigma.isEmpty())) {
            isDead = true;
        }
        
        canBeRewrote = false;
    }


    private int getDelay() {
		if (ds.isEmpty()) {
			return java.lang.Integer.MAX_VALUE;
		}
		return ds.get(0);
	}
	

    private void sched(int next, int start) {
		if (ds.size() == start) {
			ds.add(start, next);
		}
		else {
			int head = ds.get(start);
			if (next == head) {
				return;
			}
			else if (next < head) {
				int rem = head - next;
				ds.set(start, next);
				ds.add(start + 1, rem);
			}
			else { // next > head
				int rem = next - head;
				sched(rem, start + 1);
			}
		}
	}


	private void nextDelay() {
		if (! ds.isEmpty()) {
			int head = ds.get(0);
			if (head == 1 || head == 0) {
				ds.remove(0);
			}
			else {
				ds.set(0, head - 1);
			}
		}
	}


    /**
     *
     * @return true is stability is reached, false otherwise
     */
    public boolean propagatesChoice() {
        boolean beta = getBeta();
        if (beta) {
//            cout << "beta is true" << endl;
           if (delayClock.status != QuantumBoolean.POSSIBLY && status == QuantumBoolean.POSSIBLY){
               status = delayClock.status;
               return false;
           }
           if (delayClock.status == QuantumBoolean.POSSIBLY && status != QuantumBoolean.POSSIBLY){
               delayClock.status = status;
               return false;
           }
           //there is conflict, propagate false
           if (delayClock.status != QuantumBoolean.POSSIBLY && status != QuantumBoolean.POSSIBLY && delayClock.status != status){
               delayClock.status = QuantumBoolean.FALSE;
               status = QuantumBoolean.FALSE;
               return false;
           }
        }
        return true;
    }



    /**
     *
     * @return true is stability is reached, false otherwise
     */
    public boolean propagatesDeath(){
        if (delayClock.isDead && !isDead){
            isDead = true;
            return false;
        }

        if (baseClock.isDead && ds.isEmpty()){
            isDead = true;
            return false;
        }

        return true;
    }
}