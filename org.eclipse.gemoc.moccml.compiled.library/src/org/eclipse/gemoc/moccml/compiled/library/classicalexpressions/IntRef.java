package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

public class IntRef extends IntegerExpression {

	public Integer ref;
	
	public IntRef(Integer op) {
		ref = op;
	}
	
	@Override
	public Integer evaluate() {
		return ref;
	}
	
	@Override
	public String prettyPrint() {
		return ref.toString();
	}

}
