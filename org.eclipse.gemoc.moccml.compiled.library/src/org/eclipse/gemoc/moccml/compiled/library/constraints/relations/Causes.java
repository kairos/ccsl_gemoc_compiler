//
// Created by jdeanton on 2/14/19.
//
package org.eclipse.gemoc.moccml.compiled.library.constraints.relations;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class Causes extends Relation{
	
	public int delta = 0;
    public Clock left;
    public Clock right;

    public Causes(Clock l, Clock r){
    	left =l;
    	right = r;
    }


    public boolean evaluate() {
    	canBeRewrote = true;
        return false;
    }

    public void rewrite() {
    	if (!canBeRewrote) return;
    	
        if (left.status == QuantumBoolean.TRUE){
            delta++;
        }
        if(right.status == QuantumBoolean.TRUE){
            delta--;
        }
        canBeRewrote = false;
    }

    /**
     *
     * @return true is stability is reached, false otherwise
     */
    public boolean propagatesChoice() {
        if (delta == 0){
            if (left.status == QuantumBoolean.FALSE && right.status == QuantumBoolean.POSSIBLY){
                right.status = QuantumBoolean.FALSE;
                return false;
            };
            if (left.status == QuantumBoolean.POSSIBLY && right.status == QuantumBoolean.TRUE){
                left.status = QuantumBoolean.TRUE;
                return false;
            };
        }
        return true; //never propagates anything...
    }

    /**
     *
     * @return true is stability is reached, false otherwise
     */
    public boolean propagatesDeath(){
        if (left.isDead && !right.isDead && delta == 0){
            right.isDead = true;
            return false;
        }
        return true;
    }


	@Override
	public void reset() {
		delta = 0;
	}
}