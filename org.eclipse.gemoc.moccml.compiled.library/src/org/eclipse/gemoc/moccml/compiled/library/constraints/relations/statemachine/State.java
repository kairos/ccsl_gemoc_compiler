package org.eclipse.gemoc.moccml.compiled.library.constraints.relations.statemachine;

import java.util.ArrayList;

public class State {
	public String name = ""; 
	public ArrayList<Transition> outgoingTransitions = new ArrayList<Transition>();

	public State(String n) {
		name =n;
	}
	
}
