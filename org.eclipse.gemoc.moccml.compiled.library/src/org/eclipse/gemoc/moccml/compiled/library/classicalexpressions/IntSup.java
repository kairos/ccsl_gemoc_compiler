package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

public class IntSup extends BooleanExpression {

	IntegerExpression leftOperand;
	IntegerExpression rightOperand;
	
	public IntSup(IntegerExpression liexpr, IntegerExpression riexpr) {
		leftOperand = liexpr;
		rightOperand = riexpr;
	}
	
	@Override
	public boolean evaluate() {
		return leftOperand.evaluate() > rightOperand.evaluate();
	}
	
	@Override
	public String prettyPrint() {
		return "("+leftOperand.prettyPrint()+") > ("+rightOperand.prettyPrint()+")";
	}
	
}
