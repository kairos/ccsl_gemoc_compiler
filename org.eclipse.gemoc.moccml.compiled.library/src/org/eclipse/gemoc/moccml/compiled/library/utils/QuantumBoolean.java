package org.eclipse.gemoc.moccml.compiled.library.utils;

/*
 * QuanticBoolean.h
 *
 *  Created on: Aug 25, 2016
 *      Author: jdeanton
 */


public enum QuantumBoolean{
		TRUE,
		POSSIBLY,
		FALSE
};

