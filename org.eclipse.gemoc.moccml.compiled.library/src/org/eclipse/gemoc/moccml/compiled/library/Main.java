//============================================================================
// Name        : main.cpp
// Author      : Julien Deantoni
// Version     : the best of ones I had :)
// Copyright   : http://i3s.unice.fr/~deantoni
// Description : Code generated for your CCSL specification, C++11 style
//============================================================================

package org.eclipse.gemoc.moccml.compiled.library;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.Precedes;

public class Main{

	public static void main(String[] args) {

//	    int simpleTest1_main_b_PeriodicDef_plusOne = 4;
//	    int CCSL_CCSL_Expressions_three = 3;
//	    int CCSL_CCSL_Expressions_two = 2;
	    Clock a = new Clock("a");
	    Clock b= new Clock("b");
	    Clock c= new Clock("c");
	    Clock d= new Clock("d");
	    Clock e= new Clock("e");
	  
	    Precedes p1 = new Precedes(a,b);
	    Precedes p2 = new Precedes(b, c);
	    Precedes p3 = new Precedes(c, d);
	    Precedes p4 = new Precedes(d, e);
	    
	    SortedSet<Constraint> allExclusions = new TreeSet<Constraint>();
	    SortedSet<Constraint> allConstraintsButExclusions = new TreeSet<Constraint>(Arrays.asList(p1,p2,p3,p4));
	    Set<Clock> allClocks = new HashSet<Clock>(Arrays.asList(
	    											 a,
	    											 b,
	    											 c,
	    											 d,
	    											 e
	    											 ));
	
	    allConstraintsButExclusions.addAll(allExclusions);
	    Solver solver = new Solver(allClocks, allConstraintsButExclusions);
	    solver.simulate(50);
	    System.out.println(
	         "/**********************\n" +
	         "*     simulation ends      *\n" +
	         "**********************/\n");
	
	    return;
	}
}
