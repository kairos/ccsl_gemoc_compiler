package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

abstract public class IntegerExpression extends ClassicalExpression {

	public abstract Integer evaluate();
	
}
