//
// Created by jdeanton on 2/15/19.
//
package org.eclipse.gemoc.moccml.compiled.library.constraints.expressions;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class Sampled extends Clock implements Constraint {
	
	public Clock sampledClock;
	public Clock samplingClock;
    public boolean sampledSeen = false;
    public boolean isStrict = false;

    public Sampled(Clock sampled, Clock sampler,String name, boolean strict){
    	super(name);
    	isStrict = strict;
    	sampledSeen = false;
    	sampledClock = sampled;
    	samplingClock = sampler;
    }
    
    public Sampled(String name, boolean strict){
    	super(name);
    	isStrict = strict;
    	sampledSeen = false;
    }
    
    public void connect(Clock sampled, Clock sampler){
    	sampledClock = sampled;
    	samplingClock = sampler;
    }

    public void reset(){
        super.reset();
        sampledSeen = false;
    }

    public boolean evaluate() {
    	canBeRewrote = true;
        if(isDead){
            status = QuantumBoolean.FALSE;
            return false;
        }
        if (!sampledSeen && isStrict){
            status = QuantumBoolean.FALSE;
        }
        return false;
    }

    public void rewrite() {
    	if (!canBeRewrote) return;
    	
        if(status == QuantumBoolean.TRUE){
            sampledSeen = false;
            isDead = true;
//            cout).append("sampledSeen set to false" <<endl;
        }

        if (sampledClock.status == QuantumBoolean.TRUE && isStrict){
            sampledSeen = true;
//            cout).append("sampledSeen set to true" <<endl;
        }

        if (sampledClock.status == QuantumBoolean.TRUE && !isStrict && samplingClock.status != QuantumBoolean.TRUE){
            sampledSeen = true;
//            cout).append("sampledSeen set to true" <<endl;
        }
        canBeRewrote = false;
        return;
    }

    /**
     *
     * @return true is stability is reached, false otherwise
     */
    public boolean propagatesChoice() {
//        cout).append("Sampled::propagatesChoice ").append(" sampled = ").append(sampledClock.status).append(" sampling = ").append(samplingClock.status).append(" def = ").append(status).append(" sampledSeen ").append(sampledSeen <<endl;

        if( status == QuantumBoolean.TRUE && sampledSeen && samplingClock.status == QuantumBoolean.TRUE){
            return true;
        }

        if( status == QuantumBoolean.TRUE && sampledClock.status == QuantumBoolean.TRUE && samplingClock.status == QuantumBoolean.TRUE && !isStrict){
            return true;
        }

        if( status == QuantumBoolean.TRUE && !sampledSeen && !isStrict && samplingClock.status == QuantumBoolean.TRUE && sampledClock.status == QuantumBoolean.TRUE){
            return true;
        }

        if( status == QuantumBoolean.POSSIBLY && samplingClock.status == QuantumBoolean.POSSIBLY){
            return true;
        }
        if( status == QuantumBoolean.POSSIBLY && !sampledSeen && !isStrict && samplingClock.status == QuantumBoolean.TRUE && sampledClock.status == QuantumBoolean.POSSIBLY){
            return true;
        }

        if( status == QuantumBoolean.FALSE && !sampledSeen && isStrict){
            return true;
        }

        if( status == QuantumBoolean.FALSE && !sampledSeen && !isStrict && (sampledClock.status != QuantumBoolean.TRUE || samplingClock.status != QuantumBoolean.TRUE)){
            return true;
        }

        if( status == QuantumBoolean.FALSE && !sampledSeen && !isStrict && sampledClock.status == QuantumBoolean.POSSIBLY && samplingClock.status == QuantumBoolean.POSSIBLY){
            return true;
        }

        if( status == QuantumBoolean.FALSE && sampledSeen && samplingClock.status == QuantumBoolean.FALSE){
            return true;
        }

        //do something from here
        if( status == QuantumBoolean.POSSIBLY && sampledSeen && samplingClock.status == QuantumBoolean.TRUE){
            status = QuantumBoolean.TRUE;
            return false;
        }

        if( status == QuantumBoolean.POSSIBLY && samplingClock.status == QuantumBoolean.TRUE && !isStrict && sampledClock.status == QuantumBoolean.TRUE){
            status = QuantumBoolean.TRUE;
            return false;
        }

        if( status == QuantumBoolean.POSSIBLY && samplingClock.status == QuantumBoolean.FALSE){
            status = QuantumBoolean.FALSE;
            return false;
        }

        if( status == QuantumBoolean.POSSIBLY && sampledSeen && samplingClock.status == QuantumBoolean.FALSE){
            status = QuantumBoolean.FALSE;
            return false;
        }

        if( status == QuantumBoolean.TRUE && sampledSeen && samplingClock.status == QuantumBoolean.POSSIBLY){
            samplingClock.status = QuantumBoolean.TRUE;
            return false;
        }

        if( status == QuantumBoolean.TRUE && !sampledSeen && samplingClock.status == QuantumBoolean.POSSIBLY && sampledClock.status == QuantumBoolean.TRUE && !isStrict){
            samplingClock.status = QuantumBoolean.TRUE;
            return false;
        }
        
        if( status == QuantumBoolean.TRUE && !sampledSeen && samplingClock.status == QuantumBoolean.TRUE && sampledClock.status == QuantumBoolean.POSSIBLY && !isStrict){
            sampledClock.status = QuantumBoolean.TRUE;
            return false;
        }

        if( status == QuantumBoolean.FALSE && sampledSeen && samplingClock.status == QuantumBoolean.POSSIBLY){
            samplingClock.status = QuantumBoolean.FALSE;
            return false;
        }

        if( status == QuantumBoolean.FALSE && !sampledSeen && !isStrict && samplingClock.status == QuantumBoolean.POSSIBLY && sampledClock.status == QuantumBoolean.TRUE){
            samplingClock.status = QuantumBoolean.FALSE;
            return false;
        }

        if( status == QuantumBoolean.FALSE && !sampledSeen && !isStrict && samplingClock.status == QuantumBoolean.TRUE && sampledClock.status == QuantumBoolean.POSSIBLY){
            sampledClock.status = QuantumBoolean.FALSE;
            return false;
        }
        
        //backtrack
        if( status == QuantumBoolean.TRUE && sampledSeen && samplingClock.status == QuantumBoolean.FALSE){
            status = QuantumBoolean.FALSE;
            return false;
        }
        
        if( status == QuantumBoolean.FALSE && sampledSeen && samplingClock.status == QuantumBoolean.TRUE){
            samplingClock.status = QuantumBoolean.FALSE;
            return false;
        }
        
       //TODO: deal with non strict backward...
        
        

        System.out.println( new StringBuilder("ERROR: in Sampled Expression ").append(name).append(" a case is missing: sampled = ").append(sampledClock.status).append(" sampler = ").append(samplingClock.status).append(" def = ").append(status).append("\n"));
        System.exit(-1);
        return true;
    }

    /**
     *
     * @return true is stability is reached, false otherwise
     */
    public boolean propagatesDeath(){
           if (!sampledSeen && sampledClock.isDead && !isDead){
               isDead = true;
               return false;
           }
           if (sampledSeen && samplingClock.isDead && !isDead){
               isDead = true;
               return false;
           }

        return true;
    }
}