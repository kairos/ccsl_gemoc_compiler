package org.eclipse.gemoc.moccml.compiled.library.constraints.expressions;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

/*
 * UnionExpression.h
 *
 *  Created on: Aug 19, 2016
 *      Author: jdeanton
 */


public class Union extends Clock implements Constraint {
	
	public Clock c1;
	public Clock c2;

	public Union( Clock c11, Clock c22, String name){
		super(name);
    	c1 = c11;
    	c2 = c22;
    }

	public Union( String name){
		super(name);
    }

	public void connect( Clock c11, Clock c22){
    	c1 = c11;
    	c2 = c22;
    }
	
	public boolean evaluate() {
		canBeRewrote = true;
	    if(isDead){
	        status = QuantumBoolean.FALSE;
	        return false;
	    }
		return false;
	}

	public void rewrite() {
	}
	


	/**
	 *
	 * @return true is stability is reached, QuantumBoolean.FALSE otherwise
	 */
	public boolean propagatesChoice() {

//	    cout << "Union::propagatesChoice " << " c1 = "<< c1.status << " c2 = " << c2.status << " def = " << status << endl;

	    if (status == QuantumBoolean.TRUE && (c1.status == QuantumBoolean.TRUE || c2.status == QuantumBoolean.TRUE)) {
	        return true;
	    }
	    if ((status == QuantumBoolean.FALSE) && (c1.status == QuantumBoolean.FALSE) && (c2.status == QuantumBoolean.FALSE)) {
	        return true;
	    }
	    if ((status == QuantumBoolean.POSSIBLY)
	        && ((c1.status == QuantumBoolean.FALSE  && c2.status == QuantumBoolean.POSSIBLY)
	            || (c2.status == QuantumBoolean.FALSE && c1.status == QuantumBoolean.POSSIBLY)
	            || (c2.status == QuantumBoolean.POSSIBLY && c1.status == QuantumBoolean.POSSIBLY))) {
	        return true;
	    }
	    if (status == QuantumBoolean.TRUE && (c1.status == QuantumBoolean.POSSIBLY && c2.status == QuantumBoolean.POSSIBLY)) {
	        return true;
	    }
	    //if here, something must be done

	    if ((c1.status == QuantumBoolean.TRUE || c2.status == QuantumBoolean.TRUE) && status == QuantumBoolean.POSSIBLY){
	        status = QuantumBoolean.TRUE;
	        return false;
	    }
	    if (c1.status == QuantumBoolean.FALSE && c2.status == QuantumBoolean.FALSE && status == QuantumBoolean.POSSIBLY) {
	        status = QuantumBoolean.FALSE;
	        return false;
	    }
	    if(status == QuantumBoolean.FALSE && 
	    		((c1.status == QuantumBoolean.POSSIBLY && c2.status == QuantumBoolean.FALSE)
	    		  || 
	    		  (c2.status == QuantumBoolean.POSSIBLY && c1.status == QuantumBoolean.FALSE)
	    		  || 
	    		  (c2.status == QuantumBoolean.POSSIBLY && c1.status == QuantumBoolean.POSSIBLY)
	    		  )
	    	){
	        c1.status = QuantumBoolean.FALSE;
	        c2.status = QuantumBoolean.FALSE;
	        return false;
	    }

	    if (status == QuantumBoolean.TRUE && (c1.status == QuantumBoolean.FALSE) && c2.status == QuantumBoolean.POSSIBLY) {
	        c2.status = QuantumBoolean.TRUE;
	        return false;
	    }
	    if (status == QuantumBoolean.TRUE && (c2.status == QuantumBoolean.FALSE)&& c1.status == QuantumBoolean.POSSIBLY) {
	        c1.status = QuantumBoolean.TRUE;
	        return false;
	    }

	    //here there is a conflict
	    if ((c1.status == QuantumBoolean.TRUE || c2.status == QuantumBoolean.TRUE) && status == QuantumBoolean.FALSE){
	    	c1.status = QuantumBoolean.FALSE;
	    	c2.status = QuantumBoolean.FALSE;
	        return false;
	    }
	    
	    if ((c1.status == QuantumBoolean.FALSE && c2.status == QuantumBoolean.FALSE) && status == QuantumBoolean.TRUE){
	    	status = QuantumBoolean.FALSE;
	        return false;
	    }
	    
	    
	    System.out.println(new StringBuilder().append("ERROR: in Union Expression ").append(name).append(" a case is missing: c1 = ").append(c1.status).append(" c2 = ").append(c2.status).append(" def = ").append(status).append("\n").toString());;
		System.exit(-1);
		return true;
	}

	public void reset(){
	    super.reset();
	}

	/**
	 *
	 * @return true is stability is reached, QuantumBoolean.FALSE otherwise
	 */
	@Override
	public boolean propagatesDeath() {
		if (c1.isDead && c2.isDead && !isDead){
	        isDead = true;
	        return false;
	    }
	    return true;
	}
};

