//
// Created by jdeanton on 2/14/19.
//
package org.eclipse.gemoc.moccml.compiled.library.constraints.relations;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class Coincides extends Relation{
	
    public Clock left;
    public Clock right;

    public Coincides(Clock l, Clock r){
    	left =l;
    	right = r;
    }


    public boolean evaluate() {
        return false;
    }

    public void rewrite() {
    	return;
    }

    /**
     *
     * @return true is stability is reached, false otherwise
     */
    public boolean propagatesChoice() {
        if (left.status == right.status){
            return true;
        }
        
        if (left.status == QuantumBoolean.POSSIBLY && right.status != QuantumBoolean.POSSIBLY){
            left.status = right.status;
            return false;
        }
        
        if (left.status != QuantumBoolean.POSSIBLY && right.status == QuantumBoolean.POSSIBLY){
            right.status = left.status;
            return false;
        }
        
      //there is a conflict, propagates FALSE
  		if(left.status != QuantumBoolean.POSSIBLY &&  right.status != QuantumBoolean.POSSIBLY) {
  			left.status = QuantumBoolean.FALSE;
  			right.status = QuantumBoolean.FALSE;
//  			System.out.println("warning, back track !");
  			return false;
  		}
        System.out.println("missing a case in Coincides::propagates: left = "+left.status+" and right = "+right.status);
        System.exit(-1);
        return true; //never propagates anything...
    }

    /**
     *
     * @return true is stability is reached, false otherwise
     */
    public boolean propagatesDeath(){
        if (left.isDead && !right.isDead){
            right.isDead = true;
            return false;
        }
        if (right.isDead && !left.isDead){
            left.isDead = true;
            return false;
        }
        return true;
    }


	@Override
	public void reset() {
	}
	
	@Override
	public String toString() {
		String res = super.toString();
		res += "\n\t->  "+left.name+" === "+right.name;
		return res;
	}
	
	@Override
	public int compareTo(Constraint c) {
		return -1;
	}
}