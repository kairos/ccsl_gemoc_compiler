package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

public class Not extends BooleanExpression {

	BooleanExpression operand;
	
	public Not(BooleanExpression op) {
		operand = op;
	}
	
	@Override
	public boolean evaluate() {
		return !operand.evaluate();
	}
	
	@Override
	public String prettyPrint() {
		return "not("+operand.prettyPrint()+")";
	}
	
}
