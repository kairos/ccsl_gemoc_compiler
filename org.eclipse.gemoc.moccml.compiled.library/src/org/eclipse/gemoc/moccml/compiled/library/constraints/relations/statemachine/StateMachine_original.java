package org.eclipse.gemoc.moccml.compiled.library.constraints.relations.statemachine;

import java.util.ArrayList;
import java.util.Random;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.IntegerExpression;
import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.Relation;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class StateMachine_original extends Relation {

	public ArrayList<State> states = new ArrayList<State>();
	public ArrayList<Clock> allClocks = new ArrayList<Clock>();
	
	public State currentState;
	public State initialState;
	
	public ArrayList<Integer> vars = new ArrayList<Integer>();
	
	private ArrayList<Transition> guardEnabledTrantions;
	private Random random = new Random();
	
	@Override
	public void reset() {
		currentState = initialState;
	}

	@Override
	public boolean evaluate() {
		dealWithInitialTransition();
		
		guardEnabledTrantions = new ArrayList<Transition>();
 		for(Transition t : currentState.outgoingTransitions) {
			if (t.guard == null || t.guard.evaluate()) {
				if(t.guard != null)System.out.println("TRUE:"+ t.guard.prettyPrint());
				guardEnabledTrantions.add(t);
			}else {
				if(t.guard != null)System.out.println("FALSE:"+ t.guard.prettyPrint());
			}
		}
		
		ArrayList<Clock> usedClock = new ArrayList<Clock>();
		for(Transition t : guardEnabledTrantions) {
			usedClock.addAll(t.clocks);
		}
		
		ArrayList<Clock> falseClocks = new ArrayList<Clock>(allClocks);
		falseClocks.removeAll(usedClock);
		for(Clock c : falseClocks) {
			c.status = QuantumBoolean.FALSE;
		}
		
		return false;
	}

	private void dealWithInitialTransition() {
		for(Transition t : currentState.outgoingTransitions) {
			if (t.guard == null && t.clocks.isEmpty()) {
				for(IntegerExpression expr: t.actions) {
					expr.evaluate();
				}
				currentState = t.target;
				return;
			}
		}
		return;
	}

	@Override
	public boolean propagatesChoice() {
		ArrayList<Transition> fireableTransition = new ArrayList<Transition>();
		for(Transition t : guardEnabledTrantions) {
			if (t.clocks.stream().filter(c -> c.status == QuantumBoolean.TRUE).count() == t.clocks.size()) {
				fireableTransition.add(t);
			}
		}
		//as soon as one transition is enabled, others should be disabled
		ArrayList<Transition> enabledNotFireableTransitions = new ArrayList<Transition>(guardEnabledTrantions);
		enabledNotFireableTransitions.removeAll(fireableTransition);
		boolean fireableAlreadyfound = fireableTransition.size()>0 ? true : false;
		
		for(Transition t : enabledNotFireableTransitions) {
			if (t.clocks.stream().filter(c -> c.status == QuantumBoolean.TRUE).count() == t.clocks.size()) {
				System.err.println("Problem here, multiple fireable transitions");
				System.exit(-1);
				return true;
			}
			//something to do here
			if (fireableAlreadyfound && t.clocks.stream().filter(c -> c.status == QuantumBoolean.POSSIBLY).count() == 1) { //last chance to disallow
				t.clocks.stream().filter(c -> c.status == QuantumBoolean.POSSIBLY).forEach(c -> c.status = QuantumBoolean.FALSE);
				return false;
			}
		}
		return true;
//        System.out.println("missing a case in StateMachine::propagates: left = ");
//        guardEnabledTrantions.stream().forEach(t -> {
//        	System.out.println("\t"); 
//        	t.clocks.stream().forEach(c -> {
//        		System.out.println("\t\t"+c.status);	
//        	});
//        });
//        System.exit(-1);
//		return false;
	}

	@Override
	public void rewrite() {
		ArrayList<Transition> enabledTransitions = new ArrayList<Transition>();
		for(Transition t : guardEnabledTrantions) {
			if (t.clocks.stream().filter(c -> c.status == QuantumBoolean.TRUE).count() == t.clocks.size()) {
				enabledTransitions.add(t);
			}
		}
		if(enabledTransitions.size()>0) {
			Transition randomTransition = enabledTransitions.get(random.nextInt(enabledTransitions.size()));
			for(IntegerExpression expr : randomTransition.actions) {
				expr.evaluate();
			}
			currentState = randomTransition.target;
		}
	}

	@Override
	public boolean propagatesDeath() {
		return true;
	}
	

}
