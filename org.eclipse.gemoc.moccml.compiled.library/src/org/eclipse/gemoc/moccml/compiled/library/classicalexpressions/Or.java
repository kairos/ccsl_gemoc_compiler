package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

public class Or extends BooleanExpression {

	BooleanExpression leftOperand;
	BooleanExpression rightOperand;
	
	public Or(BooleanExpression liexpr, BooleanExpression riexpr) {
		leftOperand = liexpr;
		rightOperand = riexpr;
	}
	
	@Override
	public boolean evaluate() {
		return leftOperand.evaluate() || rightOperand.evaluate();
	}
	
	@Override
	public String prettyPrint() {
		return "("+leftOperand.prettyPrint()+") or ("+rightOperand.prettyPrint()+")";
	}
	
}
