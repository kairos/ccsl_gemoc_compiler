//
// Created by jdeanton on 2/20/19.
//
package org.eclipse.gemoc.moccml.compiled.library;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.Concatenation;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

public class Solver {

	public Set<Clock> clocks;
	public SortedSet<Constraint> sortedConstraints; //exclusion and coincides first for propagation purpose
//	public Set<Constraint> exclusions;

    public Solver(Set<Clock> allClocks, SortedSet<Constraint> allConstraintsButExclusions){
    	clocks = allClocks;
    	sortedConstraints = allConstraintsButExclusions;
	}

	public void solve() {
		SortedSet<Constraint> all = new TreeSet<Constraint>(sortedConstraints);
	    for(Constraint pt_r: all) {
	        pt_r.evaluate();
	    }

	    propagatesChoice();
	    
	    ArrayList<Clock> clockShuffledList = new ArrayList<Clock>(clocks);
	    java.util.Collections.shuffle(clockShuffledList);
	    for(Clock pt_c : clockShuffledList) {
	        if (pt_c.status == QuantumBoolean.POSSIBLY) {
	            pt_c.chooseStatus();
	            propagatesChoice();
//	            System.out.println("--------" + pt_c); 
	        }
	    }
       
	    for(Clock pt_c : clocks){
	        if(pt_c.status == QuantumBoolean.TRUE){
	            pt_c.ticks();
	        }
	    }
	}


	public void simulate(int nbSteps) {
	    for (int currentStep= 0; currentStep < nbSteps; currentStep++) {
	        System.out.println("-------------step " + currentStep +"\n");
	        doStep();
			for(Clock pt_c: clocks) {
			    if (! (pt_c instanceof Constraint)) 
			    	System.out.println(pt_c+"\n");
			}
	    }
	}

	/**
	 * realize a full solving step  
	 */
	public void doStep() {
		
		for(Clock pt_c : clocks){
		    pt_c.status = QuantumBoolean.POSSIBLY;
		}
		solve();
		rewrite();
		propagatesDeath();
	
	}
	
	/**
	 * realize a full solving step with clock force mechanism 
	 */
	public void doStep(ArrayList<Clock> clockToForce) {
		
		for(Clock pt_c : clocks){
		    pt_c.status = QuantumBoolean.POSSIBLY;
		}
		for(Clock c: clockToForce) {
			c.status = QuantumBoolean.TRUE;
		}
		solve();
		rewrite();
		propagatesDeath();
	
	}

	public void propagatesChoice() {
	    boolean fixPointReached = true;
	    SortedSet<Constraint> all = new TreeSet<Constraint>(sortedConstraints);
	    do{
	        fixPointReached = true;
	        for(Constraint pt_c : all){
	            boolean isStable = pt_c.propagatesChoice();
	            fixPointReached = fixPointReached && isStable;
	            if(! isStable ) {
	            }
	        }
	    }while (! fixPointReached);
	}

	public void rewrite() {
		SortedSet<Constraint> all = new TreeSet<Constraint>(sortedConstraints);
		for(Constraint pt_r: all) {
            if (!(pt_r instanceof Concatenation)) {
				pt_r.rewrite();
            }
        }
		for(Constraint pt_r: all) {
            if ((pt_r instanceof Concatenation)) {
				pt_r.rewrite();
            }
        }
	}
	
	public void propagatesDeath() {
	    boolean fixPointReached = true;
	    SortedSet<Constraint> all = new TreeSet<Constraint>(sortedConstraints);
	    do{
	        fixPointReached = true;
	        for(Constraint pt_c : all){
	            boolean isStable = pt_c.propagatesDeath();
	            fixPointReached = fixPointReached && isStable;
	        }
	    }while (! fixPointReached);
	}
}