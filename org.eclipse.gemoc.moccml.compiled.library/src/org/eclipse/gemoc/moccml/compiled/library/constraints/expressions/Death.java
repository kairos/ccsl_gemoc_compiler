//
// Created by jdeanton on 2/16/19.
//
package org.eclipse.gemoc.moccml.compiled.library.constraints.expressions;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class Death extends Clock implements Constraint {
	
	
    public Death(String name){
    	super(name);
    	isDead = true;
	}

	public void reset(){
	    super.reset();
	    isDead = true;
	}

	public boolean evaluate() {
		canBeRewrote = true;
        status = QuantumBoolean.FALSE;
        return false;
	}

	public void rewrite() {
	    return;
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesChoice() {
	        return true;
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesDeath(){
	    return true;
	}	
}
