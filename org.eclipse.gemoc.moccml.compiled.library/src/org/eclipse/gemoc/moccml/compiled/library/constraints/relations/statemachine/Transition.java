package org.eclipse.gemoc.moccml.compiled.library.constraints.relations.statemachine;

import java.util.ArrayList;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.BooleanExpression;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.IntegerExpression;

public class Transition {

	public ArrayList<Clock> clocks = new ArrayList<Clock>();
	public BooleanExpression guard = null; 
	public ArrayList<IntegerExpression> actions = new ArrayList<IntegerExpression>();
	
	public State source;
	public State target;
	
	public Transition(State src, State tgt) {
		source =src;
		target = tgt;
	}
	
}
