//
// Created by jdeanton on 5/24/17.
//
package org.eclipse.gemoc.moccml.compiled.library.constraints.relations;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class SubClock extends Relation{
	
	public Clock left;
	public Clock right;

	public SubClock(Clock l, Clock r){
    	left = l; 
    	right = r;
	}


	public boolean evaluate() {
	    return false;
	}

	public void rewrite() {
	}

	/**
	 *
	 * @return true if stability is reached, false otherwise
	 */
	public boolean propagatesChoice() {
//		System.out.println("Sublock::propagatesChoice() "+ this+ "left = "+left.status+" and right is "+right.status);
		if (right.status == QuantumBoolean.POSSIBLY && left.status == QuantumBoolean.POSSIBLY){
			return true;
		}

		if (right.status == QuantumBoolean.FALSE && left.status == QuantumBoolean.FALSE){
			return true;
		}
		if (right.status == QuantumBoolean.TRUE && left.status == QuantumBoolean.FALSE){
			return true;
		}

		if (right.status == QuantumBoolean.TRUE && left.status == QuantumBoolean.POSSIBLY){
			return true;
		}

		if (right.status == QuantumBoolean.TRUE && left.status == QuantumBoolean.TRUE){
			return true;
		}
		if (right.status == QuantumBoolean.POSSIBLY && left.status == QuantumBoolean.FALSE){
			return true;
		}
		
	    if (right.status == QuantumBoolean.FALSE){
	        if (left.status == QuantumBoolean.POSSIBLY){
	            left.status = QuantumBoolean.FALSE;
	            return false;
	        }
	    }
	    if (left.status == QuantumBoolean.TRUE){
	        if (right.status == QuantumBoolean.POSSIBLY){
	            right.status = QuantumBoolean.TRUE;
	            return false;
	        }
	    }
	    //backtrack
	    if (left.status == QuantumBoolean.TRUE){
	    	System.out.println("Backtrack since left is "+left.status+" and right is "+right.status);
	        if (right.status == QuantumBoolean.FALSE){
	            left.status = QuantumBoolean.FALSE;
	            return false;
	        }
	    }
	    
	   
	    
	    //backtrack
	    System.out.println("Missing a case left is "+left.status+" and right is "+right.status);
	    System.exit(-1);
	    return true; //never propagates anything...
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesDeath(){
	    if (right.isDead && !left.isDead){
	        left.isDead = true;
	        return false;
	    }
	    return true;
	}


	@Override
	public void reset() {
	}
}