package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

import org.eclipse.gemoc.moccml.compiled.library.utils.Sequence;

public class SeqEmpty extends BooleanExpression {

	Sequence seq;
	
	public SeqEmpty(Sequence s) {
		seq = s;
	}
	
	@Override
	public boolean evaluate() {
		return seq.isEmpty();
	}
	
	@Override
	public String prettyPrint() {
		return "seqEmpty("+seq.name+")" ;
	}
}
