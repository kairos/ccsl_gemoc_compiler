//
// Created by jdeanton on 2/20/19.
//
package org.eclipse.gemoc.moccml.compiled.library.utils;

import java.util.ArrayList;

import org.eclipse.gemoc.moccml.compiled.library.Clock;

public class Utils{
	
	public String toString(ArrayList<Clock> vect_pt_c){
		StringBuilder sb = new StringBuilder();
		for(Clock pt_c : vect_pt_c){
	        sb.append(pt_c.toString());
	    }
		return sb.toString();
	}
}

