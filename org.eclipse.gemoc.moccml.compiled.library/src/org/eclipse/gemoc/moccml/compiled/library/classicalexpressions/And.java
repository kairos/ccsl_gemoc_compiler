package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

public class And extends BooleanExpression {

	BooleanExpression leftOperand;
	BooleanExpression rightOperand;
	
	public And(BooleanExpression liexpr, BooleanExpression riexpr) {
		leftOperand = liexpr;
		rightOperand = riexpr;
	}
	
	@Override
	public boolean evaluate() {
		return leftOperand.evaluate() && rightOperand.evaluate();
	}

	@Override
	public String prettyPrint() {
		return "("+leftOperand.prettyPrint()+") and ("+rightOperand.prettyPrint()+")";
	}
	
}
