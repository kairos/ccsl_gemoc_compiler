package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

import org.eclipse.gemoc.moccml.compiled.library.utils.Sequence;

abstract public class SequenceExpression extends ClassicalExpression {

	public abstract Sequence evaluate();
	
}
