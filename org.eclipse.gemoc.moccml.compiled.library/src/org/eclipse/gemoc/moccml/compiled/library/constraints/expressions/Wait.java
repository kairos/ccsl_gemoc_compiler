package org.eclipse.gemoc.moccml.compiled.library.constraints.expressions;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.IntegerExpression;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class Wait extends Clock implements Constraint {
	
	public Clock c1;
	public int N;
	public int initialN;
	public IntegerExpression theExpr = null;

	public Wait(Clock c11, int waitValue, String name){
		super(name);
    	c1 = c11;
    	N = waitValue;
    	initialN = waitValue;
    }
	
	public Wait(int waitValue, String name){
		super(name);
    	N = waitValue;
    	initialN = waitValue;
    }
	
	public Wait(Clock c11, IntegerExpression expr, String name){
		super(name);
    	c1 = c11;
    	theExpr = expr;
    	N = expr.evaluate();
    	initialN = N;
    }
	
	public Wait(IntegerExpression expr, String name){
		super(name);
		theExpr = expr;
    	N = expr.evaluate();
    	initialN = N;
    }
	
	
	public void connect(Clock c11){
    	c1 = c11;
    }

	public void reset(){
		super.reset();
		if(theExpr != null) {
			N = theExpr.evaluate();
		}else {
			N = initialN;
		}
	}

	public boolean evaluate() {
		canBeRewrote = true;
	    if(isDead){
	        status = QuantumBoolean.FALSE;
	        return false;
	    }
	    if (N > 1 || N <= 0){
	        status = QuantumBoolean.FALSE;
	    }
	    return false;
	}

	public void rewrite() {
		if (!canBeRewrote) return;
		
	    if (isDead){
	    }

	    if (status == QuantumBoolean.TRUE){
	        isDead = true;
	        N = -1;
	    }

	    if (c1.status == QuantumBoolean.TRUE){
	        N--;
//	        cout << "N-- : " <<N << endl;
	    }
	    canBeRewrote = false;
	    return;
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesChoice() {
		if (isDead) {
			return true;
		}
		
		if (N >1 && status == QuantumBoolean.FALSE) {
			return true;
		}
		
		if (N <= 0 && status == QuantumBoolean.FALSE) {
			return true;
		}
		
		if ((N == 1) && status == c1.status) {
			return true;
		}
		
		
	    if ((N == 1)) {
	        if (status != QuantumBoolean.POSSIBLY && c1.status == QuantumBoolean.POSSIBLY) {
	            c1.status = status;
	            return false;
	        }
	        if (status == QuantumBoolean.POSSIBLY && c1.status != QuantumBoolean.POSSIBLY) {
	            status = c1.status;
	            return false;
	        }
	    }
	    
	    //there is a conflict, propagates FALSE
	    if ((N == 1) && status != c1.status) {
	    	c1.status = QuantumBoolean.FALSE;
			this.status = QuantumBoolean.FALSE;
//			System.out.println("warning, wait back track ! (N=="+N+")");
			return false;
        }
	    
       System.out.println("missing a case in Wait::propagates: N = "+N+"  c1 = "+c1.status+" and def = "+status+ "(is dead ="+isDead+" )");
       System.exit(-1);
        
	   return true;
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesDeath(){
	    if (c1.isDead && !isDead){
	        isDead = true;
	        return false;
	    }

	    return true;
	}
	
	@Override
	public String toString() {
		return "Wait "+N+" on "+c1+" (ticked "+nbTick+")";
	}

};


