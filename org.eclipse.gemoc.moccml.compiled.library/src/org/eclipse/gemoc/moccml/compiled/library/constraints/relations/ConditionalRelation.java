package org.eclipse.gemoc.moccml.compiled.library.constraints.relations;

public class ConditionalRelation extends Relation{
	
	Relation root = null;
	
	public ConditionalRelation(Relation trueCase) {
			root = trueCase;
	}
	
	public ConditionalRelation() {
	}
	
	public void connect(Relation trueCase) {
		root = trueCase;
	}

	@Override
	public String toString() {
		return "Cond: "+ root.toString();
	}
	
	@Override
	public void reset() {
		root.reset();
	}

	@Override
	public boolean evaluate() {
		canBeRewrote = true;
		return false;
	}

	@Override
	public boolean propagatesChoice() {
		return root.propagatesChoice();
	}

	@Override
	public void rewrite() {
		root.rewrite();
		canBeRewrote = false;
	}

	@Override
	public boolean propagatesDeath() {
		return root.propagatesDeath();
	}

}





//conditions can be resoklved at compile time since no recursion occurs.
//public class ConditionalRelation extends Relation{
//	
//	static public class RelationCase{
//		public RelationCase(Relation r, BooleanExpression be){
//			rel = r;
//			test = be;
//		}
//		public Relation rel;
//		public BooleanExpression test;
//	}
//
//	ArrayList<RelationCase> allCases;
//	Relation defaultRel;
//	Relation root = null;
//	
//	public ConditionalRelation(ArrayList<RelationCase> cases, Relation defaultRelation) {
//		allCases = cases;
//		defaultRel = defaultRelation;
//		for(RelationCase c : allCases) {
//			if (c.test.evaluate()) {
//				root = c.rel;
//				break;
//			}else {
//				isActive = false;
//			}
//		}
//		if (root == null) {
//			root = defaultRelation;
//		}else {
//			defaultRelation.isActive = false;
//		}
//	}
//	
//	public ConditionalRelation() {
//	}
//	
//	public void connect(ArrayList<RelationCase> cases, Relation defaultRelation) {
//		allCases = cases;
//		defaultRel = defaultRelation;
//		for(RelationCase c : allCases) {
//			if (c.test.evaluate()) {
//				root = c.rel;
//				break;
//			}else{
//				isActive = false;
//			}
//		}
//		if (root == null) {
//			root = defaultRelation;
//		}else {
//			defaultRelation.isActive = false;
//		}
//	}
//
//	@Override
//	public String toString() {
//		return "Cond: "+ root.toString();
//	}
//	
//	@Override
//	public void reset() {
//		for(RelationCase c : allCases) {
//			if (c.test.evaluate()) {
//				root = c.rel;
//				break;
//			}else{
//				isActive = false;
//			}
//		}
//		if (root == null) {
//			root = defaultRel;
//		}else {
//			defaultRel.isActive = false;
//		}
//	}
//
//	@Override
//	public boolean evaluate() {
//		canBeRewrote = true;
//		return false;
//	}
//
//	@Override
//	public boolean propagatesChoice() {
//		return root.propagatesChoice();
//	}
//
//	@Override
//	public void rewrite() {
//		root.rewrite();
//		canBeRewrote = false;
//	}
//
//	@Override
//	public boolean propagatesDeath() {
//		return root.propagatesDeath();
//	}
//
//}
