package org.eclipse.gemoc.moccml.compiled.library.constraints.relations.statemachine;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.IntegerExpression;
import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.Relation;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class StateMachine extends Relation {

	public ArrayList<State> states = new ArrayList<State>();
	public ArrayList<Clock> allClocks = new ArrayList<Clock>();
	
	public State currentState;
	public State initialState;
	
	public ArrayList<Integer> vars = new ArrayList<Integer>();
	
	private ArrayList<Transition> guardEnabledTrantions;
	private Random random = new Random();
	
	@Override
	public void reset() {
		currentState = initialState;
	}

	@Override
	public boolean evaluate() {
		dealWithInitialTransition();
//		System.out.println("current state = "+this.currentState.name);
		guardEnabledTrantions = new ArrayList<Transition>();
 		for(Transition t : currentState.outgoingTransitions) {
 			if (t.guard == null || t.guard.evaluate()) {
//				if(t.guard != null) {System.out.print("TRUE:"+ t.guard.prettyPrint()+ "when "); t.clocks.stream().forEach(c -> System.out.print(c.name+";")); System.out.println("");}
				guardEnabledTrantions.add(t);
			}else {
//				if(t.guard != null) {System.out.print("FALSE:"+ t.guard.prettyPrint()+ "when "); t.clocks.stream().forEach(c -> System.out.print(c.name+";")); System.out.println("");}
			}
		}
		
		ArrayList<Clock> usedClock = new ArrayList<Clock>();
		for(Transition t : guardEnabledTrantions) {
			usedClock.addAll(t.clocks);
		}
		
		ArrayList<Clock> falseClocks = new ArrayList<Clock>(allClocks);
		falseClocks.removeAll(usedClock);
		for(Clock c : falseClocks) {
			c.status = QuantumBoolean.FALSE;
		}
		fireableTransition =  new HashSet<Transition>();
		return false;
	}

	private void dealWithInitialTransition() {
		for(Transition t : currentState.outgoingTransitions) {
			if (t.guard == null && t.clocks.isEmpty()) {
				for(IntegerExpression expr: t.actions) {
					expr.evaluate();
				}
				currentState = t.target;
				return;
			}
		}
		return;
	}
	public HashSet<Transition> fireableTransition;
	@Override
	public boolean propagatesChoice() {
//		System.out.println("enter propagate of "+this.currentState.name);
		boolean isStable = true;
//		System.out.println(this.hashCode()+" enter: fireable size = "+fireableTransition.size());
		for(Transition t : fireableTransition) {
			//changed occurs so check if correct
			if ((t.clocks.stream().filter(c -> c.status == QuantumBoolean.TRUE).count() != t.clocks.size())) 
//					&& (t.clocks.stream().filter(c -> c.status == QuantumBoolean.TRUE).count() > 0)
//					&& (t.clocks.stream().filter(c -> c.status == QuantumBoolean.FALSE).count() > 0) 
//					&& (t.clocks.stream().filter(c -> c.status == QuantumBoolean.FALSE).count() != t.clocks.size())) 
			{ //not enabled anymore but not disabled completely
				fireableTransition.remove(t);
//				System.out.println("old enabled disabled "+t.source.name+" -> "+t.target.name +"("+fireableTransition.size()+")");
				t.clocks.stream().forEach(c -> c.status = QuantumBoolean.FALSE);
				isStable = false;
			}
		}
		
		for(Transition t : guardEnabledTrantions) {
			if (t.clocks.stream().filter(c -> c.status == QuantumBoolean.TRUE).count() == t.clocks.size()) {
				if(fireableTransition.add(t)) { //if not already there
//					System.out.println("new fireable "+t.source.name+" -> "+t.target.name);
					//here we directly disallow all others
					ArrayList<Clock> clockToDisallow = new ArrayList<Clock>(allClocks);
					clockToDisallow.removeAll(t.clocks);
					for(Clock c : clockToDisallow) {
						if (c.status != QuantumBoolean.FALSE) {
							c.status = QuantumBoolean.FALSE;
						}
					}
					isStable = false;
				}
				
			}
		}
		
		//check all mixed "FALSE" and "TRUE"

		if(fireableTransition.size() == 0 && (!allClocks.stream().anyMatch(c -> c.status == QuantumBoolean.POSSIBLY)) && (allClocks.stream().filter(c -> c.status == QuantumBoolean.FALSE).count() != allClocks.size()))
		{
			isStable = false;
			allClocks.stream().forEach(c -> c.status = QuantumBoolean.FALSE);
//			for(Transition t : guardEnabledTrantions) {
//				if (t.clocks.stream().filter(c -> c.status == QuantumBoolean.TRUE).count() > 0 
//					&& t.clocks.stream().filter(c -> c.status == QuantumBoolean.FALSE).count() > 0
//					) {
//					for(Clock c : t.clocks) {
//						System.out.println("false all ("+fireableTransition.size()+")");
//						if (c.status != QuantumBoolean.FALSE) {
//							c.status = QuantumBoolean.FALSE;
//							isStable = false;
//						}
//					}
//				}
//			}
		}
//		System.out.println(this.hashCode()+" leave: fireable size = "+fireableTransition.size());

		return isStable;
		
//		//as soon as one transition is enabled, others should be disabled
//		ArrayList<Transition> enabledNotFireableTransitions = new ArrayList<Transition>(guardEnabledTrantions);
//		enabledNotFireableTransitions.removeAll(fireableTransition);
//		boolean fireableAlreadyfound = fireableTransition.size()>0 ? true : false;
//		
//		
//		
//		if(fireableTransition.size()>1) {
////			System.out.println("PBE !!! multiple fireable transition");
//			int randomInt = random.nextInt(fireableTransition.size());
//			ArrayList<Transition> transitionsToBlock = new ArrayList<Transition>(fireableTransition);
//			Transition selectedTransition = transitionsToBlock.get(randomInt);
//			transitionsToBlock.remove(selectedTransition);
//			
//			for(Transition toBlock : transitionsToBlock) {
//				ArrayList<Clock> clocksFromSelected = new ArrayList<Clock>(toBlock.clocks); //random.nextInt(fireableTransition.size())
//				clocksFromSelected.removeAll(selectedTransition.clocks);
//				clocksFromSelected.stream().filter(c -> c.status == QuantumBoolean.TRUE).forEach(c -> c.status = QuantumBoolean.FALSE);
//			}
//			fireableTransition.clear();
//			fireableTransition.add(selectedTransition);
//			isStable= false;
//		}
//		
//		
////		for(Transition t : fireableTransition) {
////			ArrayList<Transition> outT = new ArrayList<Transition>(t.source.outgoingTransitions);
////			outT.remove(t);
////			for(Transition out : outT) {
////				if (out.clocks.stream().filter(c -> c.status == QuantumBoolean.POSSIBLY).count() == 1) { //block all other outgoing t
////					out.clocks.stream().filter(c -> c.status == QuantumBoolean.POSSIBLY).forEach(c -> c.status = QuantumBoolean.FALSE);
////					System.out.print(" block a transition ");
////					isStable = false;
////				}
////			}
////		}
//		
////		if(fireableTransition.size()>0) {
////			System.out.println("one fireable transition");
////		}
//		
//		for(Transition t : enabledNotFireableTransitions) {
//			if (t.clocks.stream().filter(c -> c.status == QuantumBoolean.TRUE).count() == t.clocks.size()) {
//				System.err.println("Problem here, multiple fireable transitions");
//				System.exit(-1);
//				return true;
//			}
//			//something to do here
////	too light
////			if (fireableAlreadyfound && t.clocks.stream().filter(c -> c.status == QuantumBoolean.POSSIBLY).count() == 1) { //last chance to disallow
////				t.clocks.stream().filter(c -> c.status == QuantumBoolean.POSSIBLY).forEach(c -> c.status = QuantumBoolean.FALSE);
////				isStable = false;
////			}
//			if (fireableAlreadyfound) { //we disable all others, a minima
//				ArrayList<Clock> clocksFromT = new ArrayList<Clock>(t.clocks); //random.nextInt(fireableTransition.size())
//				clocksFromT.removeAll(fireableTransition.get(0).clocks);
//				if (clocksFromT.stream().filter(c -> c.status == QuantumBoolean.POSSIBLY).count() >= 1) { //we add a false
//					clocksFromT.stream().filter(c -> c.status == QuantumBoolean.POSSIBLY).findAny().get().status = QuantumBoolean.FALSE;
////					System.out.print(" block a transition ");
//					isStable = false;
//				}
//			}
//			
			
			
			
			
//		}
//		return isStable;
//        System.out.println("missing a case in StateMachine::propagates: left = ");
//        guardEnabledTrantions.stream().forEach(t -> {
//        	System.out.println("\t"); 
//        	t.clocks.stream().forEach(c -> {
//        		System.out.println("\t\t"+c.status);	
//        	});
//        });
//        System.exit(-1);
//		return false;
	}

	@Override
	public void rewrite() {
//		ArrayList<Transition> enabledTransitions = new ArrayList<Transition>();
//		for(Transition t : guardEnabledTrantions) {
//			if (t.clocks.stream().filter(c -> c.status == QuantumBoolean.TRUE).count() == t.clocks.size()) {
//				enabledTransitions.add(t);
//			}
//		}
//		if(enabledTransitions.size()>0) {
//			if(enabledTransitions.size()>1) {
//				System.out.print("&&&&&&&&&&&&&&&&&&&&&&&&&  should never happen  several Transition in "+this.currentState.name+"\n\t\t");
//				for(Transition t : enabledTransitions) {
//					System.out.print(t.target.name+  ",");
//				}
//				System.out.println("__");
//			}
//			Transition randomTransition = enabledTransitions.get(random.nextInt(enabledTransitions.size()));
//			for(IntegerExpression expr : randomTransition.actions) {
//				expr.evaluate();
//			}
//			currentState = randomTransition.target;
//		}
		
		for(Transition t : fireableTransition) {
//			System.out.println("fire "+t.source.name+" -> "+t.target.name);
			for(IntegerExpression expr : t.actions) {
				expr.evaluate();
			}
			currentState = t.target;
		}
	}

	@Override
	public boolean propagatesDeath() {
		return true;
	}
	

}
