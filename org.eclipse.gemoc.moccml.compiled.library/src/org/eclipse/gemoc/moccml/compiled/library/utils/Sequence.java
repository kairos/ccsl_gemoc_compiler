package org.eclipse.gemoc.moccml.compiled.library.utils;

import java.util.Arrays;

import org.eclipse.gemoc.moccml.compiled.library.NamedElement;

//
// Created by jdeanton on 5/29/17.
//


public class Sequence extends NamedElement {
	
	
	public Sequence(Integer[] finitePart, Integer[] infinitePart, String name) {
		super(name);
		this.finitePart = finitePart;
		this.infinitePart = infinitePart;
		finiteIndex = 0;
		infiniteIndex = 0;
	}
	
	public Sequence(Sequence original) {
		super(original.name);
		if (original.finitePart != null) {
			this.finitePart = Arrays.copyOf(original.finitePart, original.finitePart.length);
		}
		else {
			this.finitePart = null;
		}
		this.finiteIndex = original.finiteIndex;
		if (original.infinitePart != null) {
			this.infinitePart = Arrays.copyOf(original.infinitePart, original.infinitePart.length);
		}
		else {
			this.infinitePart = null;
		}
		this.infiniteIndex = original.infiniteIndex;
	}

	public Integer[] finitePart;
	public Integer[] infinitePart;
	
	public int finiteIndex;
	public int infiniteIndex;
	
	public void setFinitePart(Integer[] arg) {
		finitePart = arg;
		finiteIndex = 0;
	}
	
	public void setInfinitePart(Integer[] arg) {
		infinitePart = arg;
		infiniteIndex = 0;
	}
	
	public Integer popHead() {
		return popNext();
	}
	
	public Sequence getTail() {
//		Sequence res = new Sequence(this);
//		res.popHead(); // pops the head, only modifies the indexes.
		this.popNext();
		return this;
	}
	
	public Integer nextHead() {
		if (finitePart != null && finiteIndex < finitePart.length) {
			return finitePart[finiteIndex];
		}
		else if (infinitePart != null && infiniteIndex < infinitePart.length) {
			return infinitePart[infiniteIndex];
		}
		return null;
	}
	
	public Integer popNext() {
		if (finitePart != null && finiteIndex < finitePart.length) {
			Integer result = finitePart[finiteIndex];
			finiteIndex++;
			return result;
		}
		else if (infinitePart != null && infiniteIndex < infinitePart.length) {
			Integer result = infinitePart[infiniteIndex];
			infiniteIndex++;
			if (infiniteIndex == infinitePart.length) {
				infiniteIndex = 0;
			}
			return result;
		}
		return null;
	}
	
	public boolean hasNext() {
		return ( (finitePart != null && finiteIndex < finitePart.length)
				|| isInfinite() );
	}
	
	public boolean isFinite() {
		return (infinitePart == null || infinitePart.length == 0);
	}
	
	public boolean isInfinite() {
		return (infinitePart != null && infinitePart.length > 0);
	}
	
	public boolean isEmpty() {
		return ((finitePart == null || finitePart.length == 0)
				&& (infinitePart == null && infinitePart.length == 0));
	}


//	public List<Integer> finitePart;
//    public ArrayList<Integer> infinitePart;
//    public Iterator<Integer> infinitePartIterator;
//
//    /*
//     * @return -1 if the sequence is empty
//     */
//    public int nextHead() {
//        if (! finitePart.isEmpty()){
//            int nextInt = finitePart.get(0);
//            finitePart.remove(0);
//            return nextInt;
//        }
//        if (infinitePart.isEmpty()){
//            return -1;
//        }
//
//        if (! infinitePartIterator.hasNext()){
//            infinitePartIterator = infinitePart.iterator();
//        }
//        
//        int nextInt = infinitePartIterator.next();
//        return nextInt;
//    }
//
//    Sequence(List<Integer> finitePart, ArrayList<Integer> infinitePart, String name){
//    	super(name);
//    	this.finitePart = finitePart;
//    	this.infinitePart =infinitePart;
//        infinitePartIterator = infinitePart.iterator();
//    }
//
//    public boolean empty() {
//        return infinitePart.isEmpty() && finitePart.isEmpty();
//    }
}