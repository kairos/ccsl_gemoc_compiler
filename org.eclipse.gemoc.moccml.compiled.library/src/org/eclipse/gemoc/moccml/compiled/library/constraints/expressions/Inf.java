//
// Created by jdeanton on 2/16/19.
//
package org.eclipse.gemoc.moccml.compiled.library.constraints.expressions;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class Inf extends Clock implements Constraint {
	
	public Clock leftClock;
	public Clock rightClock;
    public int delta = 0;

    public Inf(Clock lc, Clock rc,String name){
    	super(name);
    	leftClock = lc;
    	rightClock = rc;
	}
    
    public Inf(String name){
    	super(name);
    }
    public void connect(Clock lc, Clock rc){
    	leftClock = lc;
    	rightClock = rc;
	}

	public void reset(){
	    super.reset();
	    delta = 0;
	}

	public boolean evaluate() {
		canBeRewrote = true;
	    if(isDead){
	        status = QuantumBoolean.FALSE;
	        return false;
	    }
	    return false;
	}

	public void rewrite() {
		if (!canBeRewrote) return;
		
	    if(leftClock.status == QuantumBoolean.TRUE){
	        delta--;
	    }
	    if(rightClock.status == QuantumBoolean.TRUE){
	        delta++;
	    }
	    canBeRewrote = false;
	    return;
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesChoice() {
	    if(isDead){
	        return true;
	    }

	    if (status == QuantumBoolean.POSSIBLY && (leftClock.status == QuantumBoolean.POSSIBLY && rightClock.status == QuantumBoolean.POSSIBLY)){
	        return true;
	    }
	    
	    if( delta < 0 && status == leftClock.status){
	        return true;
	    }

	    if( delta > 0 && status == rightClock.status){
	        return true;
	    }

	    if (delta == 0 && status != QuantumBoolean.POSSIBLY && (leftClock.status == QuantumBoolean.POSSIBLY && rightClock.status == QuantumBoolean.POSSIBLY)){
	        return true;
	    }
	    
	    if( delta == 0
	           && ((status == QuantumBoolean.TRUE && (leftClock.status == QuantumBoolean.TRUE || rightClock.status == QuantumBoolean.TRUE))
	           || (status == QuantumBoolean.FALSE && (leftClock.status == QuantumBoolean.FALSE && rightClock.status == QuantumBoolean.FALSE))
	           || (status == QuantumBoolean.POSSIBLY && (leftClock.status == QuantumBoolean.POSSIBLY && rightClock.status == QuantumBoolean.FALSE))
	           || (status == QuantumBoolean.POSSIBLY && (leftClock.status == QuantumBoolean.FALSE && rightClock.status == QuantumBoolean.POSSIBLY)))
	      ){
	        return true;
	    }

	    if( delta < 0 && leftClock.status != QuantumBoolean.POSSIBLY){
	        status = leftClock.status;
	        return false;
	    }

	    if( delta > 0 && rightClock.status != QuantumBoolean.POSSIBLY){
	        status = rightClock.status;
	        return false;
	    }

	    if( delta < 0 && status != QuantumBoolean.POSSIBLY){
	        leftClock.status = status;
	        return false;
	    }

	    if( delta > 0 && status != QuantumBoolean.POSSIBLY){
	        rightClock.status = status;
	        return false;
	    }

	    if( delta == 0 && (status != QuantumBoolean.POSSIBLY)){
	        if (rightClock.status != QuantumBoolean.POSSIBLY){
	            leftClock.status = status;
	            return false;
	        }
	        if (leftClock.status != QuantumBoolean.POSSIBLY){
	            rightClock.status = status;
	            return false;
	        }
//	        //here we have to choose randomly
//	        int r = (random.nextInt()%2);
//	        if (r == 1 ){
//	            leftClock.status = status;
//	        }else{
//	            rightClock.status = status;
//	        }
	        return false;
	    }

	    if( delta == 0 && (rightClock.status == QuantumBoolean.TRUE || leftClock.status == QuantumBoolean.TRUE)){
	        status = QuantumBoolean.TRUE;
	        return false;
	    }

	    if( delta == 0 && rightClock.status == QuantumBoolean.FALSE && leftClock.status == QuantumBoolean.FALSE){
	        status = QuantumBoolean.FALSE;
	        return false;
	    }

	    System.out.println(new StringBuilder("ERROR: in Inf Expression ").append(name).append(" a case is missing: leftClock = ").append(leftClock.status).append(" rightClock = ").append(rightClock.status).append(" def = ").append(status).append("\n"));
	    System.exit(-1);
	    return true;
	}

	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	public boolean propagatesDeath(){
	    if( delta > 0 && rightClock.isDead && !isDead){
	        isDead = true;
	        return false;
	    }

	    if( delta < 0 && leftClock.isDead && !isDead){
	        isDead = true;
	        return false;
	    }

	    if( delta == 0 && leftClock.isDead && rightClock.isDead && !isDead){
	        isDead = true;
	        return false;
	    }

	    return true;
	}	
}
