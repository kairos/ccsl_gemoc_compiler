package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

abstract public class BooleanExpression extends ClassicalExpression {

	public abstract boolean evaluate();
	
}
