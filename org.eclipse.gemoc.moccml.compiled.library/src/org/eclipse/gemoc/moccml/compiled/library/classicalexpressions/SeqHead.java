package org.eclipse.gemoc.moccml.compiled.library.classicalexpressions;

import org.eclipse.gemoc.moccml.compiled.library.utils.Sequence;

public class SeqHead extends IntegerExpression {

	Sequence seq;
	
	public SeqHead(Sequence s) {
		seq = s;
	}
	
	@Override
	public Integer evaluate() {
		return seq.nextHead();
	}
	
	@Override
	public String prettyPrint() {
		return "seqHead("+seq.name+")" ;
	}
	
}
