package org.eclipse.gemoc.moccml.compiled.library.constraints.expressions;

import java.util.ArrayList;

import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.BooleanExpression;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;

public class ConditionalExpression extends Clock implements Constraint {
	
	static public class Case{
		public Case(Clock c, BooleanExpression be){
			constraint = c;
			test = be;
		}
		public Clock constraint;
		public BooleanExpression test;
	}

	ArrayList<Case> allCases;
	Clock root;
	
	public ConditionalExpression(ArrayList<Case> cases, String name) {
		super(name);
		allCases = cases;
		for(Case c : allCases) {
			if (c.test.evaluate()) {
				root = c.constraint;
			}else {
				c.constraint.isDead = true;
			}
		}
	}
	
	public ConditionalExpression(String name) {
		super(name);
	}
	
	public void connect(ArrayList<Case> cases) {
		allCases = cases;
		for(Case c : allCases) {
			if (c.test.evaluate()) {
				root = c.constraint;
			}else {
				c.constraint.isDead = true;
			}
		}
	}

	@Override
	public String toString() {
		if(root != null) {
			return "Cond: "+ root.toString();
		}
		return "";
	}
	
	@Override
	public void reset() {
		super.reset();
		for(Case c : allCases) {
			if (c.test.evaluate()) {
				root = c.constraint;
//				root.reset();
			}else {
				c.constraint.isDead = true;
			}
		}
	}

	@Override
	public boolean evaluate() {
		canBeRewrote = true;
	    if(isDead){
	        status = QuantumBoolean.FALSE;
	        return false;
	    }
		return false;
	}

	@Override
	public boolean propagatesChoice() {
		if(root == null) {
			return true; //recursiveCall
		}
		if(root.status == this.status) {
			return true;
		}
		
		//something to do here
		if(root.status == QuantumBoolean.POSSIBLY &&  this.status != QuantumBoolean.POSSIBLY) {
			root.status = this.status;
			return false;
		}
		
		if(root.status != QuantumBoolean.POSSIBLY &&  this.status == QuantumBoolean.POSSIBLY) {
			this.status = root.status;
			return false;
		}
		
		//there is a conflict, propagates FALSE
		if(root.status != QuantumBoolean.POSSIBLY &&  this.status != QuantumBoolean.POSSIBLY) {
			root.status = QuantumBoolean.FALSE;
			this.status = QuantumBoolean.FALSE;
//			System.out.println("warning, back track !");
			return false;
		}
		
		System.out.println(new StringBuilder("ERROR: in Conditional Expression ").append(name).append(" a case is missing: root = ").append(root.status).append(" def = ").append(status).append("\n"));
		System.exit(-1);
	return true;
	}

	@Override
	public void rewrite() {
		if (!canBeRewrote) return;
		
		for(Case c : allCases) {
			if (c.test.evaluate()) {
				root = c.constraint;
//				((Constraint)c.constraint).rewrite(); --> done in a main loop
				break;
			}
		}
		canBeRewrote = false;
	}

	@Override
	public boolean propagatesDeath() {
		if(root == null) {
			return true; //recursiveCall
		}
		if (root.isDead && this.isDead) {
			return true;
		}
		if (!root.isDead && !this.isDead) {
			return true;
		}
		root.isDead = true;
		this.isDead = true;
		return false;
	}
	
	@Override
	public void chooseStatus() {
		super.chooseStatus();
		propagatesChoice();
		if (root instanceof Constraint) {
			((Constraint) root).propagatesChoice();
		}
	}

}
