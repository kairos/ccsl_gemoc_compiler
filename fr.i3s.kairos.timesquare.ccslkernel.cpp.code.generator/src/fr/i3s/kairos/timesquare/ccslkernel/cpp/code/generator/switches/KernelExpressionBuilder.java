package fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.switches;


import java.io.IOException;

import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.handlers.CCSLCPPCodeGenerator;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Concatenation;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Death;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Defer;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Inf;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Intersection;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.NonStrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.StrictSampling;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Sup;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Union;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.UpTo;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.Wait;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.KernelExpression.util.KernelExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import toools.io.file.RegularFile;

public class KernelExpressionBuilder extends KernelExpressionSwitch<Boolean> {

	private InstantiatedElement concrete;
	private RegularFile mainFile;
	public StringBuilder connectStringBuilder = new StringBuilder();
	private CCSLCPPCodeGenerator codeExecutor;
	
	public KernelExpressionBuilder(InstantiatedElement concrete, RegularFile mainFile, CCSLCPPCodeGenerator codeExec) {
		this.mainFile = mainFile;
		this.concrete = concrete;
		this.codeExecutor = codeExec;
	}
	
	@Override
	public Boolean caseConcatenation(Concatenation object) {
		InstantiatedElement right = concrete.resolveAbstractEntity(object.getRightClock());
		InstantiatedElement left = concrete.resolveAbstractEntity(object.getLeftClock());
		try {
				mainFile.append(("\tConcatenation "+concrete.getQualifiedName("_")+"{"+"\""+concrete.getQualifiedName("_")+"\"};\n").getBytes());
				if(right.isRecursiveCall()) {
					InstantiatedElement ie_expr = right.getAbstractMapping().getLocalValue("FilterBySeq");
					if(ie_expr == null) {
						throw new RuntimeException("CCSL compiler, righ to f concatenation is a recursive call but not a filter by... not supported yet: "+right.getInstantiationPath().getLast());
					}
					connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+left.getQualifiedName("_")+","+right.getQualifiedName("_")+", true, "+codeExecutor.cleanQualifiedName(ie_expr)+");\n");
				}else {
					connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+left.getQualifiedName("_")+","+right.getQualifiedName("_")+");\n");
				}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean caseDefer(Defer object) {
		InstantiatedElement baseClock = concrete.resolveAbstractEntity(object.getBaseClock());
		InstantiatedElement delayClock = concrete.resolveAbstractEntity(object.getDelayClock());
		InstantiatedElement pattern = concrete.resolveAbstractEntity(object.getDelayPattern());
		try {
				mainFile.append(("\tDefer "+concrete.getQualifiedName("_")+"{"+"\""+concrete.getQualifiedName("_")+"\"};\n").getBytes());
				connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+baseClock.getQualifiedName("_")+","+delayClock.getQualifiedName("_")+","+pattern.getQualifiedName("_")+");\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	@Override
	public Boolean caseInf(Inf object) {
		InstantiatedElement c1 = concrete.resolveAbstractEntity(object.getClock1());
		InstantiatedElement c2 = concrete.resolveAbstractEntity(object.getClock2());
		try {
				mainFile.append(("\tInf "+concrete.getQualifiedName("_")+"{"+"\""+concrete.getQualifiedName("_")+"\"};\n").getBytes());
				connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+c1.getQualifiedName("_")+","+c2.getQualifiedName("_")+");\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean caseSup(Sup object) {
		InstantiatedElement c1 = concrete.resolveAbstractEntity(object.getClock1());
		InstantiatedElement c2 = concrete.resolveAbstractEntity(object.getClock2());
		try {
				mainFile.append(("\tSup "+concrete.getQualifiedName("_")+"{\""+concrete.getQualifiedName("_")+"\"};\n").getBytes());
				connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+c1.getQualifiedName("_")+","+c2.getQualifiedName("_")+");\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean caseIntersection(Intersection object) {
		InstantiatedElement c1 = concrete.resolveAbstractEntity(object.getClock1());
		InstantiatedElement c2 = concrete.resolveAbstractEntity(object.getClock2());
		try {
				mainFile.append(("\tIntersection "+concrete.getQualifiedName("_")+"{\""+concrete.getQualifiedName("_")+"\"};\n").getBytes());
				connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+c1.getQualifiedName("_")+","+c2.getQualifiedName("_")+"\");\n");
				
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean caseNonStrictSampling(NonStrictSampling object) {
		InstantiatedElement sampledClock = concrete.resolveAbstractEntity(object.getSampledClock());
		InstantiatedElement samplingClock = concrete.resolveAbstractEntity(object.getSamplingClock());
		try {
				mainFile.append(("\tSampled "+concrete.getQualifiedName("_")+"{\""+concrete.getQualifiedName("_")+"\"};\n").getBytes());
				connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+sampledClock.getQualifiedName("_")+","+samplingClock.getQualifiedName("_")+");\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean caseStrictSampling(StrictSampling object) {
		InstantiatedElement sampledClock = concrete.resolveAbstractEntity(object.getSampledClock());
		InstantiatedElement samplingClock = concrete.resolveAbstractEntity(object.getSamplingClock());
		try {
				mainFile.append(("\tSampled "+concrete.getQualifiedName("_")+"{\""+concrete.getQualifiedName("_")+"\", true};\n").getBytes());
				connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+sampledClock.getQualifiedName("_")+","+samplingClock.getQualifiedName("_")+");\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	

	
	@Override
	public Boolean caseUnion(Union object) {
		InstantiatedElement c1 = concrete.resolveAbstractEntity(object.getClock1());
		InstantiatedElement c2 = concrete.resolveAbstractEntity(object.getClock2());
		try {
				mainFile.append(("\tUnion "+concrete.getQualifiedName("_")+"{\""+concrete.getQualifiedName("_")+"\"};\n").getBytes());
				connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+c1.getQualifiedName("_")+","+c2.getQualifiedName("_")+");\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean caseUpTo(UpTo object) {
		InstantiatedElement clockToFollow = concrete.resolveAbstractEntity(object.getClockToFollow());		
		InstantiatedElement killerClock= concrete.resolveAbstractEntity(object.getKillerClock());
		try {
				mainFile.append(("\tUpTo "+concrete.getQualifiedName("_")+"{\""+concrete.getQualifiedName("_")+"\"};\n").getBytes());
				connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+clockToFollow.getQualifiedName("_")+","+killerClock.getQualifiedName("_")+");\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean caseWait(Wait object) {
		InstantiatedElement waitingClock = concrete.resolveAbstractEntity(object.getWaitingClock());		
		InstantiatedElement waitingValue= concrete.resolveAbstractEntity(object.getWaitingValue());
		try {
//			if(!waitingValue.isClassicalExpression()) {
					mainFile.append(("\tWait "+concrete.getQualifiedName("_")+"{"+waitingValue.getQualifiedName("_")+",\""+concrete.getQualifiedName("_")+"\"};\n").getBytes());
					connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+waitingClock.getQualifiedName("_")+");\n");
//			}else {
//					mainFile.append((codeExecutor.cleanQualifiedName(concrete)).getBytes());
//					connectStringBuilder.append("\t"+concrete.getQualifiedName("_")+".connect("+waitingClock.getQualifiedName("_")+");\n");
//				}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@Override
	public Boolean caseDeath(Death object) {
		try {
				mainFile.append(("\tDeath "+concrete.getQualifiedName("_")+"{\""+concrete.getQualifiedName("_")+"\"};\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
}



