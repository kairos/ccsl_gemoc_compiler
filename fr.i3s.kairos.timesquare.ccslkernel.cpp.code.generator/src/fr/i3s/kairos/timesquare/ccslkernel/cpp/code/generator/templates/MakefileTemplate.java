package fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.templates;

public class MakefileTemplate {
	
	public static String content = "" +
			"INCLUDES=-I.\n" + 
			"INCLUDES+= -I../includes\n" + 
			"INCLUDES+= -I../includes/utils\n" + 
			"INCLUDES+= -I../includes/constraints/relations/statemachine\n" + 
			"INCLUDES+= -I../includes/constraints/relations\n" + 
			"INCLUDES+= -I../includes/constraints/expressions\n" + 
			"INCLUDES+= -I../includes/constraints\n" + 
			"INCLUDES+= -I../includes/classicalexpressions\n" + 
			"INCLUDES+= -I../includes/includeAVR_STL\n" + 
			"\n" + 
			"\n" + 
			"CC=avr-gcc\n" + 
			"CPP=avr-g++\n" + 
			"\n" + 
			"BUILD_DIR=Build\n" + 
			"\n" + 
			"#MMCU=-mmcu=atmega328p\n" + 
			"MMCU=-mmcu=atmega2560\n" + 
			"\n" + 
			"CFLAGS= -Os -w -std=gnu11 -ffunction-sections -fdata-sections -MMD -flto -fno-fat-lto-objects ${MMCU} -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_UNO -DARDUINO_ARCH_AVR\n" + 
			"CPPFLAGS= -Os -w -std=gnu++11 -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -Wno-error=narrowing -MMD -flto -w -x c++ -CC ${MMCU} -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_UNO -DARDUINO_ARCH_AVR \n" + 
			"\n" + 
			"PROGRAM=CompiledCCSL\n" + 
			"\n" + 
			"\n" + 
			"all: ${PROGRAM}.elf ${PROGRAM}.hex \n" + 
			"	rm -f *.d\n" + 
			"\n" + 
			"${PROGRAM}.elf: Build/userCode.o Build/ClockDefinition.o Build/GeneratedSystem.o\n" + 
			"	${CPP} -w -Os -g -flto -fuse-linker-plugin -Wl,--gc-sections ${INCLUDES} ${MMCU} -L../lib/ $^ -o ${BUILD_DIR}/$@ -l${PROGRAM}\n" + 
			"\n" + 
			"Build/%.o: %.cpp\n" + 
			"	mkdir -p ${BUILD_DIR}/\n" + 
			"	${CPP} -c ${CPPFLAGS} ${INCLUDES} $^\n" + 
			"	mv *.o ${BUILD_DIR}/\n" + 
			"\n" + 
			"OBJCOPY=avr-objcopy\n" + 
			" \n" + 
			"${PROGRAM}.hex: ${BUILD_DIR}/${PROGRAM}.elf\n" + 
			"	${OBJCOPY} -O ihex -R .eeprom  ${BUILD_DIR}/${PROGRAM}.elf ${BUILD_DIR}/$@\n" + 
			"\n" + 
			"PORT=/dev/ttyACM0\n" + 
			" \n" + 
			"upload: ${BUILD_DIR}/${PROGRAM}.hex\n" + 
			"	avrdude -F -c wiring -b 115200 -p atmega2560 -P ${PORT} -D -U flash:w:${BUILD_DIR}/${PROGRAM}.hex;\n" + 
			"\n" + 
			"clean:\n" + 
			"	rm -Rf ${BUILD_DIR}\n" + 
			"";

}
