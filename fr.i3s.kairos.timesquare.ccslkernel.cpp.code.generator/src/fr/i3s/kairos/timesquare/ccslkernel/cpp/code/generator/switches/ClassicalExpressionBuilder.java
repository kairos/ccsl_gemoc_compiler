package fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.switches;

import java.io.IOException;

import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.handlers.CCSLCPPCodeGenerator;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.BooleanExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.Not;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.NumberSeqVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqExpression;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetHead;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqGetTail;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.SeqIsEmpty;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.util.ClassicalExpressionSwitch;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import toools.io.file.RegularFile;

public class ClassicalExpressionBuilder extends ClassicalExpressionSwitch<Boolean> {

	private InstantiatedElement concrete;
	private RegularFile mainFile;
	private CCSLCPPCodeGenerator codeExecutor;
	private static boolean noName = false;
	

	public ClassicalExpressionBuilder(InstantiatedElement concrete, RegularFile mainFile, CCSLCPPCodeGenerator codeExec) {
		this.mainFile = mainFile;
		this.concrete = concrete;
		this.codeExecutor = codeExec;
	}
	
	@Override
	public Boolean caseSeqGetHead(SeqGetHead object) {
//		SeqExpression seqExpr = object.getOperand();//concrete.resolveAbstractEntity(object.getOperand());
		InstantiatedElement ieSeq = concrete.getAbstractMapping().getLocalValue("FilterBySeq");
//		SequenceElement seq = null;
//		if (seqExpr instanceof NumberSeqVariableRef) {
//			seq = (SequenceElement) concrete.resolveAbstractEntity(((NumberSeqVariableRef)seqExpr).getReferencedVar()).getValue();
//		}
//		if (seqExpr instanceof NumberSeqRef) {
//			seq = ((NumberSeqRef)seqExpr).getReferencedNumberSeq();
//		}
		try {
			//mainFile.append(("\tSeqHead "+ concrete.getQualifiedName("_")+" = new SeqHead("+seq.getName()+");\n").getBytes());
			mainFile.append(("SeqHead "+codeExecutor.cleanQualifiedName(concrete)+" = {"+ieSeq.getQualifiedName("_")+"};\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	return true;
	}
	
	@Override
	public Boolean caseSeqGetTail(SeqGetTail object){
		InstantiatedElement ieSeq = concrete.getAbstractMapping().getLocalValue("FilterBySeq");
//		SeqExpression seqExpr = object.getOperand();//concrete.resolveAbstractEntity(object.getOperand());
//		SequenceElement seq = null;
//		if (seqExpr instanceof NumberSeqVariableRef) {
//			seq = (SequenceElement) concrete.resolveAbstractEntity(((NumberSeqVariableRef)seqExpr).getReferencedVar()).getValue();
//		}
//		if (seqExpr instanceof NumberSeqRef) {
//			seq = ((NumberSeqRef)seqExpr).getReferencedNumberSeq();
//		}
		try {
//			mainFile.append(("\tSeqTail "+ concrete.getQualifiedName("_")+" = new SeqTail("+seq.getName()+");\n").getBytes());
			mainFile.append(("SeqTail "+codeExecutor.cleanQualifiedName(concrete)+" = {"+ieSeq.getQualifiedName("_")+"};\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	return true;
	}
	
	@Override
	public Boolean caseSeqIsEmpty(SeqIsEmpty object){
		InstantiatedElement ieSeq = concrete.getAbstractMapping().getLocalValue("FilterBySeq");
//		SeqExpression seqExpr = object.getOperand();//concrete.resolveAbstractEntity(object.getOperand());
//		SequenceElement seq = null;
//		if (seqExpr instanceof NumberSeqVariableRef) {
//			seq = (SequenceElement) concrete.resolveAbstractEntity(((NumberSeqVariableRef)seqExpr).getReferencedVar()).getValue();
//		}
//		if (seqExpr instanceof NumberSeqRef) {
//			seq = ((NumberSeqRef)seqExpr).getReferencedNumberSeq();
//		}
		try {
//			mainFile.append(("\tSeqEmpty "+ concrete.getQualifiedName("_")+" = new SeqEmpty("+seq.getName()+");\n").getBytes());
			if(!noName) {
				mainFile.append(("SeqEmpty "+codeExecutor.cleanQualifiedName(concrete)+" =").getBytes());
			}
			mainFile.append((" SeqEmpty{"+ieSeq.getQualifiedName("_")+"}").getBytes());
			if(!noName) {
				mainFile.append((";\n").getBytes());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	return true;
	}
	
	@Override
	public Boolean caseNot(Not object){
		BooleanExpression boolExpr = object.getOperand();//concrete.resolveAbstractEntity(object.getOperand());
		try {
//			mainFile.append(("\tSeqEmpty "+ concrete.getQualifiedName("_")+" = new SeqEmpty("+seq.getName()+");\n").getBytes());
			mainFile.append(("Not "+codeExecutor.cleanQualifiedName(concrete)+"{").getBytes());
			noName = true;
			this.doSwitch(boolExpr);
			noName = false;
			mainFile.append(("};\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	return true;
	}


}
