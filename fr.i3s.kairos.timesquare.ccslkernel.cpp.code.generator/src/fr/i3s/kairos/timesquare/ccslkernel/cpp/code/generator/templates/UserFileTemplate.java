package fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.templates;

public class UserFileTemplate {

		public static String content="" +
				"#include \"GeneratedSystem.h\"\n" + 
				"\n" + 
				"void fooTask(){\n" + 
				"    //some finite code\n" + 
				"}\n" + 
				"\n" + 
				"\n" + 
				"void setupClockFunction(){\n" + 
				"    //TODO: add co,nection between clock and code here\n\n"+
				"	 //e.g., " +
				"	 //clock1.assignFunction(fooTask);\n" + 
				"}";
	
}
