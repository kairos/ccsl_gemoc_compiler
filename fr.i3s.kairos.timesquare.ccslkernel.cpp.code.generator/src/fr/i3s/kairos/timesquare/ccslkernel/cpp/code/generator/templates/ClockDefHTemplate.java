package fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.templates;

public class ClockDefHTemplate {

	public static String begin = ""+
			"#ifndef COMPILEDCCSL_CLOCKDEFINITIONS_H\n" + 
			"#define COMPILEDCCSL_CLOCKDEFINITIONS_H\n" + 
			"\n" + 
			"#include \"Clock.h\"\n\n";
	
	public static String end = ""+
			"\n" + 
			"#endif //COMPILEDCCSL_CLOCKDEFINITIONS_H\n";
	
	
}
