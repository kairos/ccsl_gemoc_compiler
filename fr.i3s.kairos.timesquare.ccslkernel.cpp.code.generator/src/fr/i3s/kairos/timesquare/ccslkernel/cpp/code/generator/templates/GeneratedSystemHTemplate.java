package fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.templates;

public class GeneratedSystemHTemplate {
	
	public static String content = "" +
			"#ifndef COMPILEDCCSL_GENERATEDSYSTEM_H\n" + 
			"#define COMPILEDCCSL_GENERATEDSYSTEM_H\n" + 
			"\n" + 
			"#include \"CCSLUtils.h\"\n" + 
			"#include \"ClockDefinitions.h\"\n" + 
			"\n" + 
			"void run();\n" + 
			"void setupClockFunction();\n" + 
			"\n" + 
			"#endif //COMPILEDCCSL_GENERATEDSYSTEM_H";

}
