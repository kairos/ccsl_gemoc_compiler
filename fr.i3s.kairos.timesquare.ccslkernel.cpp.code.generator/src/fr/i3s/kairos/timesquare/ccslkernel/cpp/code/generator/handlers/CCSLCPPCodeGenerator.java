package fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.moccml.constraint.ccslmoc.model.moccml.moccml.StateMachineRelationDefinition;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.AbstractAction;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.AbstractGuard;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.Guard;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.IntegerAssignement;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.State;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.Transition;
import org.eclipse.gemoc.moccml.constraint.fsmkernel.model.FSMModel.Trigger;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
//import org.jgrapht.traverse.TopologicalOrderIterator;

import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.helpers.ActionHelper;
import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.helpers.CompilerParameters;
import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.helpers.GuardHelper;
import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.helpers.PluginProjectHelper;
import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.switches.ClassicalExpressionBuilder;
import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.switches.KernelExpressionBuilder;
import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.switches.KernelRelationBuilder;
import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.templates.ClockDefHTemplate;
import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.templates.GeneratedSystemHTemplate;
import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.templates.MakefileTemplate;
import fr.i3s.kairos.timesquare.ccslkernel.cpp.code.generator.templates.UserFileTemplate;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.BooleanElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.IntegerElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.PrimitiveElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.BasicType.SequenceElement;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClassicalExpression.IntegerVariableRef;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.AbstractEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.BindableEntity;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.ConditionalRelationDefinition;
import fr.inria.aoste.timesquare.ccslkernel.model.TimeModel.CCSLModel.ClockExpressionAndRelation.Relation;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.InstantiatedElement;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.UnfoldModel;
import fr.inria.aoste.timesquare.ccslkernel.modelunfolding.exception.UnfoldingException;
import toools.io.file.RegularFile;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class CCSLCPPCodeGenerator extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public CCSLCPPCodeGenerator(String projectName, boolean withAspectManagement, boolean withCoordinationAPI) {
		this.projectName = projectName;
		this.withAspectManagement = withAspectManagement;
		this.withCoordinationAPI = withCoordinationAPI;
	}
	
	public CCSLCPPCodeGenerator(String projectName) {
		this.projectName = projectName;
		this.withAspectManagement = false;
		this.withCoordinationAPI = false;
	}
	
	/**
	 * required by UI handler :-/
	 */
	public CCSLCPPCodeGenerator() {
		this.projectName = "fr.inria.kairos.generatedCode.automaticName_TOBECHANGED";
		this.withAspectManagement = false;
		this.withCoordinationAPI = false;
	}

	public String projectName = "";
	public boolean withAspectManagement = false;
	public boolean withCoordinationAPI = false;
	
	private IFile _ccslFile;
	public HashMap<InstantiatedElement, List<InstantiatedElement>> clockToRelationExpressions = new HashMap<InstantiatedElement, List<InstantiatedElement>>();
	private Set<InstantiatedElement> allRelationsButExclusions;
	private Set<InstantiatedElement> allExclusions;
	private Set<InstantiatedElement> allClocks;
	private Set<InstantiatedElement> allKernelExpressions;
	private Set<InstantiatedElement> allConstants;
	public IFolder srcFolder;
	/**
	 * this is the file were the main is (i.e., GeneratedSystem.cpp)
	 */
	private RegularFile mainFile; 
	private RegularFile generatedSystem_h;
	private RegularFile clockDefinition_h;
	private RegularFile clockDefinition_cpp;
	private RegularFile userFile_cpp;
	private RegularFile makefile;
	private IProject proj;
	private PluginProjectHelper pluginHelper;
	private Set<InstantiatedElement> allComplexExpressions;
	private Set<InstantiatedElement> allClassicalExpression;
	private HashSet<InstantiatedElement> allConditionalRelation;
	private UnfoldModel theUnfoldModel;
	private HashSet<InstantiatedElement> allStateMachineRelations;
	
	
	public IProject getProject() {
		return proj;
	}
	
	public void putElementInRelationExpression(InstantiatedElement key, InstantiatedElement value) {
	    List<InstantiatedElement> myClassList = this.clockToRelationExpressions.get(key);
	    if(myClassList == null) {
	        myClassList = new ArrayList<InstantiatedElement>();
	        this.clockToRelationExpressions.put(key,myClassList);
	    }
	    myClassList.add(value);
	}


	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
//		generator = new CCSLKernelCPPRewritingRuleConstructor();
		
		System.out.println("C++ code generation started!");
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			if (((IStructuredSelection) selection).size() == 1) {
				Object selected = ((IStructuredSelection) selection).getFirstElement();
				if (selected instanceof IFile) {
					try {
						projectName = "fr.inria.kairos.moccml.cpp.generatedcode."+((IFile)selected).getName();
						createProjectAndGenerate((IFile) selected);
					} catch (CoreException e) {
						e.printStackTrace();
					}
					generateMainHeader(srcFolder);
					generateBeginOfMain("nameLanguage NotRelevant if not a gemoc project");
					generateEndOfMain();
					
					try {
						clockDefinition_h.append(ClockDefHTemplate.end.getBytes());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}


	/**
	 * create a java project and set srcFolder as well as mainFile
	 * @param ccslFile
	 * @param projectName
	 * @throws CoreException 
	 */
	public void createProjectAndGenerate(IFile ccslFile) throws CoreException {
		_ccslFile = ccslFile;
		String filename = _ccslFile.getFullPath().toString();
		
		theUnfoldModel = null;
		try {
			theUnfoldModel = UnfoldModel.unfoldModel(URI.createPlatformResourceURI(filename));
		} catch (IOException | UnfoldingException e1) {
			e1.printStackTrace();
		}
		pluginHelper = new PluginProjectHelper();
		//srcFolder
		proj = pluginHelper.createPluginProject(projectName);
		srcFolder = proj.getFolder(CompilerParameters.genSourceFolder);
//		pluginHelper.addRequiredBundle(proj, "org.apache.commons.cli");
//		pluginHelper.addRequiredBundle(proj, "org.eclipse.gemoc.moccml.compiled.library");
//		pluginHelper.addRequiredBundle(proj, " fr.inria.aoste.utils.grph");
//		if(withAspectManagement || withCoordinationAPI) { 
//			pluginHelper.addRequiredBundle(proj, "fr.inria.diverse.k3.al.annotationprocessor.plugin");
//			pluginHelper.addRequiredBundle(proj, "org.eclipse.gemoc.executionframework.engine");
//			pluginHelper.addRequiredBundle(proj, "org.eclipse.gemoc.xdsmlframework.api");
//			pluginHelper.addRequiredBundle(proj, "org.eclipse.emf.transaction");
//			pluginHelper.addRequiredBundle(proj, "org.eclipse.gemoc.trace.gemoc.api");
//			pluginHelper.addRequiredBundle(proj, "org.eclipse.gemoc.execution.commons.dostepparameters");
//		}
		//clean if exists
		try {
			for(IResource r : srcFolder.members()){
				r.delete(true, null);
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
		
		allStateMachineRelations = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if (ie.getDefinition() instanceof StateMachineRelationDefinition) {
				allStateMachineRelations.add(ie);
			}
		}
		
		allClocks = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if (ie.isRecursiveCall()) {
//				System.out.println("##############  recursive : "+ie.getInstantiationPath().getLast());
			}
			if (ie.isClock()) {
				allClocks.add(ie);
			}
		}
		
		allConstants = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if (ie.isPrimitiveElement() || ie.getValue() instanceof SequenceElement) {
				if(ie.getInstantiationPath().getLast() instanceof IntegerElement || ie.getValue() instanceof SequenceElement) {
					allConstants.add(ie);
				}else {	
					if(ie.getValue() != null) {
						allConstants.add(ie);
					}
				}
			}
		}
		
		allClassicalExpression = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if (ie.isClassicalExpression()) {
				allClassicalExpression.add(ie);
			}
			if (ie.isConditional()) {
				for(InstantiatedElement caze: ie.getConditionalCases()) {
					InstantiatedElement test = caze.getConditionTest();
					allClassicalExpression.add(test);
				}
			}
			
		}
		
		allExclusions = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if (!ie.isClock() && ie.isKernelRelation() && "Exclusion".equals(ie.getDeclaration().getName())) {
				allExclusions.add(ie);
			}
		}
		
		allRelationsButExclusions = new HashSet<InstantiatedElement>();
		addAllKernelRelationsButExclusions(theUnfoldModel.getInstantiationTree().lookupInstances(null));
		
		allConditionalRelation = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			if ((!ie.isKernelRelation()) && ie.isRelation() && ie.isConditional()) {
				allConditionalRelation.add(ie);
			}
		}
		
		
		allKernelExpressions = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			System.out.print(ie);
			if (!ie.isClock() && ie.isKernelExpression()) {
				allKernelExpressions.add(ie);
			}else {
				if ((!ie.isConditional()) && ie.getRootExpression() != null) {
					allKernelExpressions.add(ie.getRootExpression());
				}
				if (ie.isConditional()) {
					for(InstantiatedElement caze: ie.getConditionalCases()) {
						if(caze.isKernelExpression()) {
							allKernelExpressions.add(caze);
						}
					}
				}
			}
		}
		
		allComplexExpressions = new HashSet<InstantiatedElement>();
		for(InstantiatedElement ie: theUnfoldModel.getInstantiationTree().lookupInstances(null)) {
			System.out.print(ie);
			if ((!ie.isClock()) && /*(!ie.isKernelExpression()) && */ie.isExpression()) {
				allComplexExpressions.add(ie);
//				System.out.println("  addedXX ");
			}else {
				if (ie.isConditional()) {
					for(InstantiatedElement caze: ie.getConditionalCases()) {
						if(!caze.isKernelExpression() && caze.isExpression()) {
							allComplexExpressions.add(caze);
						}
					}
				}
			}
		}
		

		mainFile = new RegularFile(srcFolder.getLocation().toPortableString()+"/GeneratedSystem.cpp");
		generatedSystem_h = new RegularFile(srcFolder.getLocation().toPortableString()+"/GeneratedSystem.h");
		clockDefinition_cpp = new RegularFile(srcFolder.getLocation().toPortableString()+"/ClockDefinition.cpp");
		clockDefinition_h = new RegularFile(srcFolder.getLocation().toPortableString()+"/ClockDefinition.h");
		makefile = new RegularFile(srcFolder.getLocation().toPortableString()+"/Makefile");
		userFile_cpp = new RegularFile(srcFolder.getLocation().toPortableString()+"/userCode.cpp");
		
		try {
			makefile.setContent(MakefileTemplate.content.getBytes());
			generatedSystem_h.setContent(GeneratedSystemHTemplate.content.getBytes());
			userFile_cpp.setContent(UserFileTemplate.content.getBytes());
			clockDefinition_h.setContent(ClockDefHTemplate.begin.getBytes());
			clockDefinition_cpp.setContent("#include \"ClockDefinitions.h\"\n\n".getBytes());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private void addAllKernelRelationsButExclusions(List<InstantiatedElement> list) {
		for(InstantiatedElement ie: list) {
			if (ie.isKernelRelation() && !"Exclusion".equals(ie.getDeclaration().getName())) {
				allRelationsButExclusions.add(ie);
			}
		}
	}
	
	
	
	
	public void generateMainHeader(IFolder srcFolder) {
		
		try {
			mainFile.append(("//============================================================================\n"
					+ "// Name        : main.cpp\n"
					+ "// Author      : Julien Deantoni\n"
					+ "// Version     : the best of ones I had :)\n"
					+ "// Copyright   : http://i3s.unice.fr/~deantoni\n"
					+ "// Description : Code generated for your CCSL specification, modern C++ style\n"
					+ "//============================================================================\n"
					+ "\n" +
					"\n" + 
					"\n").getBytes());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
			
			
			public void injectMethod(StringBuilder sb) {
				try {
					mainFile.append(sb.toString().getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
			public void generateBeginOfMain(String languageName) {
				try {
					mainFile.append(("\n" + 
							"\n" + 
							"#include \"GeneratedSystem.h\"\n" + 
							"\n" + 
							"extern void setupClockFunction();\n" + 
							"\n" + 
							"int main(int argc, char ** argv)\n" + 
							"{\n" + 
							"    setupClockFunction();\n" + 
							"    run();\n" + 
							"    return 0;\n" + 
							"}\n" + 
							"\n" + 
							"void run() {\n" + 
							"\n" + 
							"    int nbSteps = 200; //by default\"\n" + 
							"    bool logClocks = false;\n" + 
							"    bool logStepsOnly = false;\n" + 
							"\n" + 
							"#if not (defined (__AVR__))\n" + 
							"    manageCommandLineArguments(argc,argv,nbSteps,logClocks,logStepsOnly);\n" + 
							"#endif"+
							"\n").getBytes());
					
					for(InstantiatedElement ie : allConstants){
						if(ie.getValue() != null) {
							if (ie.getValue() instanceof IntegerElement) {
								mainFile.append(("\t int "+cleanQualifiedName(ie)+" = "+((IntegerElement)ie.getValue()).getValue()+";\n").getBytes());
							}
							if (ie.getValue() instanceof BooleanElement) {
								mainFile.append(("\t bool "+cleanQualifiedName(ie)+" = "+((BooleanElement)ie.getValue()).getValue()+";\n").getBytes());
							}
							if(ie.getValue() instanceof SequenceElement) {
								mainFile.append(("\t std::vector<int> finite_"+cleanQualifiedName(ie)+";\n").getBytes());
								for(PrimitiveElement i : ((SequenceElement)ie.getValue()).getFinitePart()){
									if(i instanceof IntegerElement) {
										mainFile.append(("\t finite_"+cleanQualifiedName(ie)+".push_back("+((IntegerElement)i).getValue()+");\n").getBytes());
									}else{
										mainFile.append(("\t finite_"+cleanQualifiedName(ie)+".push_back("+((IntegerElement)ie.getParent().resolveAbstractEntity(((IntegerVariableRef)i).getReferencedVar()).getValue()).getValue()+");\n").getBytes());
									}
								}
								mainFile.append(("\t std::vector<int> infinite_"+cleanQualifiedName(ie)+";\n").getBytes());
								for(PrimitiveElement i : ((SequenceElement)ie.getValue()).getNonFinitePart()){
									if(i instanceof IntegerElement) {
										mainFile.append(("\t infinite_"+cleanQualifiedName(ie)+".push_back("+((IntegerElement)i).getValue()+");\n").getBytes());
									}else{
										mainFile.append(("\t infinite_"+cleanQualifiedName(ie)+".push_back("+((IntegerElement)ie.getParent().resolveAbstractEntity(((IntegerVariableRef)i).getReferencedVar()).getInstantiationPath().getLast()).getValue()+");\n").getBytes());
									}
								}
								mainFile.append(("\t Sequence "+cleanQualifiedName(ie)+" = {finite_"+cleanQualifiedName(ie)+",infinite_"+cleanQualifiedName(ie)+",\""+cleanQualifiedName(ie)+"\"};\n").getBytes());
							}
						}else {
							mainFile.append(("\t int "+cleanQualifiedName(ie)+" = "+((IntegerElement)ie.getInstantiationPath().getLast()).getValue()+";\n").getBytes());
						}
					}
					
					for(InstantiatedElement ie : allClassicalExpression){
						if(ie == null) {
							continue;
						}
						ClassicalExpressionBuilder builder = new ClassicalExpressionBuilder(ie, mainFile, this);
						builder.doSwitch(ie.getInstantiationPath().getLast());
					}
					
					for(InstantiatedElement ie : allClocks){
						clockDefinition_cpp.append(("\t Clock "+cleanQualifiedName(ie)+" = {\""+cleanQualifiedName(ie)+"\"};\n").getBytes());
						clockDefinition_h.append(("\textern Clock "+cleanQualifiedName(ie)+";\n").getBytes());
					}
					
					StringBuilder connectCode = new StringBuilder();
					
					for(InstantiatedElement ie : allKernelExpressions){
						KernelExpressionBuilder builder = new KernelExpressionBuilder(ie, mainFile, this);
						builder.doSwitch(ie.getDeclaration());
						connectCode.append(builder.connectStringBuilder);
					}
					
					
					
					for(InstantiatedElement ie : allComplexExpressions ){
						if (!ie.isConditional() && ie.getRootExpression() != null) {
							mainFile.append(("\t UserDefinedExpression "+cleanQualifiedName(ie)+"{\""+cleanQualifiedName(ie)+"\"};\n").getBytes());
							connectCode.append("\t\t"+cleanQualifiedName(ie)+".connect("+cleanQualifiedName(ie.getRootExpression())+");\n");
						}else 
						if (ie.isConditional()) {
							mainFile.append(("\t ConditionalExpression "+cleanQualifiedName(ie)+"{").getBytes());
							mainFile.append(("\""+cleanQualifiedName(ie)+"\"};\n").getBytes());
							connectCode.append("\t\t"+cleanQualifiedName(ie)+".connect({");
									String sep = "";
									for(InstantiatedElement caze: ie.getConditionalCases()) {
										InstantiatedElement test = caze.getConditionTest();
										connectCode.append((sep+"\n\t\t\tCase{"+cleanQualifiedName(caze)+", "+cleanQualifiedName(test)+"}"));
										sep = ",";
									}
									connectCode.append("});\n");
						}else {
//							System.out.println("Complex not managed: "+ie);
						}
					}
					
					
					for(InstantiatedElement ie : allExclusions){
						KernelRelationBuilder builder = new KernelRelationBuilder(ie, mainFile, this);
						builder.doSwitch(ie.getDeclaration());
					}
					for(InstantiatedElement ie : allRelationsButExclusions){
						KernelRelationBuilder builder = new KernelRelationBuilder(ie, mainFile, this);
						builder.doSwitch(ie.getDeclaration());
					}	
					
					
					StringBuilder desactivedRelations = new StringBuilder();
					for(InstantiatedElement ie : allConditionalRelation ){
						mainFile.append(("\t ConditionalRelation "+cleanQualifiedName(ie)+";\n").getBytes());
						
						connectCode.append("\t\t"+cleanQualifiedName(ie)+".connect(");
						boolean defaultHolds = true;
								for(int i = 0; i < ie.getConditionalCases().size(); i++) {
									InstantiatedElement test = ie.getConditionalCases().get(i); //WARNING: these are not actually the cases but only the tests :-(
									if (((BooleanElement)test.getValue()).getValue() == true) {
										defaultHolds = false;
										Relation rem = ((ConditionalRelationDefinition)ie.getDefinition()).getRelCases().get(i).getRelation().get(0); //hope the collection are correctly sorted
										connectCode.append(cleanQualifiedName(theUnfoldModel.getInstantiationTree().lookupInstance(test.getParent().getInstantiationPath()+"::"+test.getParent().getDefinition().getName()+"::"+rem.getName()))+");");
									}else {
										Relation rem = ((ConditionalRelationDefinition)ie.getDefinition()).getRelCases().get(i).getRelation().get(0); //hope the collection are correctly sorted
										desactivedRelations.append(cleanQualifiedName(theUnfoldModel.getInstantiationTree().lookupInstance(test.getParent().getInstantiationPath()+"::"+test.getParent().getDefinition().getName()+"::"+rem.getName()))+".isActive = false;\n");
									}
									
								}
								if(defaultHolds) {
									connectCode.append(cleanQualifiedName(ie.getDefaultCase())+");");
								}else {
									desactivedRelations.append(cleanQualifiedName(ie.getDefaultCase())+".isActive = false;\n");

								}
						
					}
					
					int localUID = uid;
					for(InstantiatedElement ie : allStateMachineRelations) {
						StateMachineRelationDefinition def = (StateMachineRelationDefinition) ie.getDefinition();
						String machineName = cleanQualifiedName(ie);
						mainFile.append(("StateMachine "+machineName+";\n").getBytes());
						

						
						Set<String> allClockNames = new HashSet<String>();
						for(Transition t : def.getTransitions()) {							
							if(t.getTrigger() != null) {
								for(BindableEntity bindable : ((Trigger)t.getTrigger()).getTrueTriggers()){
									allClockNames.add(cleanQualifiedName(ie.resolveAbstractEntity((AbstractEntity) bindable)));
								}
							}
						}
						
						for(String clockName : allClockNames) {
							//		clocks should already be compiled
							mainFile.append((machineName+".allClocks.push_back(&"+clockName+");\n").getBytes());
						}
						
						for(State s : def.getStates()) {
							String stateName = cleanQualifiedName(ie)+'_'+s.getName();
							mainFile.append(("State "+stateName+"{\""+stateName+"\"});\n").getBytes());	
							mainFile.append((machineName+".states.emplace_back("+stateName+");\n").getBytes());
						}
						
						for(Transition t : def.getTransitions()) {
							String tName = cleanQualifiedName(ie)+"_"+t.getName();
							String tSource = cleanQualifiedName(ie)+"_"+t.getSource().getName();
							String tTarget = cleanQualifiedName(ie)+"_"+t.getTarget().getName();
							mainFile.append(("Transition "+tName+"{"+tSource+", "+tTarget+"};\n").getBytes());
							mainFile.append((tName+".clocks;").getBytes());
							if(t.getTrigger() != null) {
								for(BindableEntity bindable : ((Trigger)t.getTrigger()).getTrueTriggers()){
									String clockName = cleanQualifiedName(ie.resolveAbstractEntity((AbstractEntity) bindable));
									mainFile.append((tName+".clocks.push_back(&"+clockName+");\n").getBytes());
								}
							}
					
//							System.out.println("\t\tguard:"+t.getGuard());
//							System.out.println("\t\twhen:"+t.getTrigger());
//							System.out.println("\t\taction:"+t.getActions());
						}
						
						
						for(State s : def.getStates()) {
							String stateName = cleanQualifiedName(ie)+'_'+s.getName();
							for(Transition outT : s.getOutputTransitions()) {
								String tName = cleanQualifiedName(ie)+"_"+outT.getName();
								mainFile.append((stateName+".outgoingTransitions.push_back("+tName+");\n").getBytes());
							}
							
						}
						String initName = cleanQualifiedName(ie)+"_"+def.getInitialStates().get(0).getName();
						mainFile.append((machineName+".initialState = &"+initName+";\n").getBytes());
						mainFile.append((machineName+".currentState = &"+initName+";\n").getBytes());
						
						//variable stuff
// done on the fly during guard stuff.						
//						for(ConcreteEntity v : def.getDeclarationBlock().getConcreteEntities()) {
//							if(v instanceof IntegerElement) {
//								mainFile.append(("IntRef id_"+getUid(v)+" = new IntRef("+((IntegerElement)v).getValue().intValue()+");\n").getBytes());
//							}else{
//								throw new RuntimeException("Varibale not supported: "+v.getClass());
//							}
//						}
						
						
						//guard stuff
						Map<EObject, Integer> fsmToUID = new HashMap<>();
						GuardHelper guardHelper = new GuardHelper(localUID, fsmToUID);
						Set<Transition> uniqueTransitions = new HashSet<Transition>(def.getTransitions());
						for(Transition t : uniqueTransitions) {
							AbstractGuard g = t.getGuard();
							if (g == null) {
								continue;
							}
							Guard guard = (Guard)g;
							String guardString = "";
							
							guardString = guardHelper.dispatchGenerateExpressionMethod(guard.getValue(), ie, "\n");
							mainFile.append(guardString.getBytes());
							
							String tName = cleanQualifiedName(ie)+"_"+t.getName();
							mainFile.append((tName+".guard = &id_"+guardHelper.getUid(guard.getValue())+";\n").getBytes());
							
						}
						localUID = guardHelper.uid;

						ActionHelper actionHelper = new ActionHelper(localUID, fsmToUID);
						for(Transition t : uniqueTransitions) {
							List<AbstractAction> actions = t.getActions();
							
							for (AbstractAction a : actions) {
								if (a == null) {
									continue;
								}
								IntegerAssignement ia = (IntegerAssignement) a; //warning we are missing death and birth
								String actionString = "";
								actionString = actionHelper.dispatchGenerateExpressionMethod(ia, ie, "\n");
								mainFile.append(actionString.getBytes());
								
								String tName = cleanQualifiedName(ie)+"_"+t.getName();
								mainFile.append((tName+".actions.push_back(id_"+actionHelper.getUid(ia)+");\n").getBytes());
							
							}
						}
						
						localUID = actionHelper.uid;
						
					}//fin all state machines
					
					
					
					 mainFile.append(desactivedRelations.toString().getBytes());
					 mainFile.append(connectCode.toString().getBytes());
					
					
					 mainFile.append(("\tstd::set<Constraint *> allExclusions;\n"+
						 		"{\n" + 
						 		"        Constraint * array[] = {").getBytes());
					 String sep = "&";
					 for(InstantiatedElement ie : allExclusions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ", &";
					 }
					 mainFile.append(("};\n"
						 		+ "        for(Constraint * ptc : array){\n" + 
						 		"            allExclusions.insert(ptc);\n" + 
						 		"        }\n" + 
						 		"    }").getBytes());
					 
					 mainFile.append(("\tstd::set<Constraint *> allConstraintsButExclusions;\n"+
					 		"{\n" + 
					 		"        Constraint * array[] = {").getBytes());
					 
					 sep = "&";
					 for(InstantiatedElement ie : allRelationsButExclusions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ", &";
					 }
					 for(InstantiatedElement ie : allKernelExpressions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ", &";
					 }
					 for(InstantiatedElement ie : allComplexExpressions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ", &";
					 }
					 for(InstantiatedElement ie : allStateMachineRelations) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ", &";
					 }
					 mainFile.append(("};\n"
					 		+ "        for(Constraint * ptc : array){\n" + 
					 		"            allConstraintsButExclusions.insert(ptc);\n" + 
					 		"        }\n" + 
					 		"    }").getBytes());
					 
					 
		
					 mainFile.append(("\tstd::vector<Clock *> allClocks;" +
					 		"\n" + 
					 		"    {\n" + 
					 		"        Clock* array[] = {").getBytes());
					 sep = "&";
					 for(InstantiatedElement ie : allClocks) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ", &";
					 }
					 for(InstantiatedElement ie : allKernelExpressions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ", &";
					 }
					 for(InstantiatedElement ie : allComplexExpressions) {
						 mainFile.append((sep+cleanQualifiedName(ie)).getBytes());
						 sep = ", &";
					 }
					 mainFile.append(("} ;\n" +
					 		"        for(Clock* ptc : array){\n" + 
					 		"            allClocks.push_back(ptc);\n" + 
					 		"        }\n" + 
					 		"    }").getBytes());
					 
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				try {
					proj.refreshLocal(IResource.DEPTH_INFINITE, null);
				} catch (CoreException e) {
					e.printStackTrace();
				}
					
	}


		
			


			public void generateEndOfMain() {
				try {
					mainFile.append(("\n" + 
							" Solver solver = {allClocks, allConstraintsButExclusions, allExclusions};\n" + 
							"\n" +
							"    #if not(defined (__AVR__))\n" + 
							"	    std::cout << std::vector<NamedElement*>(allConstraintsButExclusions.begin(),allConstraintsButExclusions.end())<< std::endl;\n" + 
							"\n" + 
							"	    solver.simulate(nbSteps, logStepsOnly, logClocks);\n" + 
							"	    std::cout <<\n" + 
							"	         \"/**********************\\n\" <<\n" + 
							"	         \"*     simulation ends      *\\n\" <<\n" + 
							"	         \"**********************/\" << std::endl;\n" + 
							"	\n" +
							"    #else\n" + 
							"        while(true){\n" + 
							"            solver.doStep();\n" + 
							"        }\n" + 
							"    #endif\n\n" + 
							"	    return 0;\n" + 
							"}\n").getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			public void generateEndOfMainWithAspectManagement() {
				generateEndOfMain();
				try {
					mainFile.append("//TODO ? aspect mangement in C++ ?\n".getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
					
			}
		
			public void generateEndOfMainWithAspectManagementAndCoordination() {
				generateEndOfMain();
				try {
					mainFile.append("//TODO ? aspect management and coordination in C++ ?\n".getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			
			
			
			
//	private IFolder createProject(String projectName) {
//		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
//		IProject project = root.getProject(projectName);
//		try {
//			project.create(null);	
//			project.open(null);
//		} catch (CoreException e) {
//			e.printStackTrace();
//		}
//		IProjectDescription description;
//		try {
//			description = project.getDescription();
//			description.setNatureIds(new String[] {"org.eclipse.jdt.core.javanature"});
//			project.setDescription(description, null);
//			
//		} catch (CoreException e1) {
//			e1.printStackTrace();
//		}
//		IJavaProject javaProject = JavaCore.create(project); 
//		IFolder srcFolder = project.getFolder("src");
//		try {
//			srcFolder.create(false, true, null);
//		} catch (CoreException e) {
//			e.printStackTrace();
//		}
//		IFolder binFolder = project.getFolder("bin");
//		try {
//			binFolder.create(false, true, null);
//		} catch (CoreException e) {
//			e.printStackTrace();
//		}
//		try {
//			javaProject.setOutputLocation(binFolder.getFullPath(), null);
//		} catch (JavaModelException e) {
//			e.printStackTrace();
//		}
//		return srcFolder;
//	}


	Map<Object, InstantiatedElement> objToIe = new HashMap<Object, InstantiatedElement>();	
	Map<Object, Integer> objToUID = new HashMap<Object, Integer>();	
	int uid = 0;
	
	public String cleanQualifiedName(InstantiatedElement ie) {
		objToIe.put(ie.getInstantiationPath().getLast(), ie);
		if (objToUID.containsKey(ie)) {
			return ie.getQualifiedName("_").replaceAll("<NoName>", objToUID.get(ie).toString()).replaceAll("-", "_");
		}else {
			objToUID.put(ie, ++uid);
			return ie.getQualifiedName("_").replaceAll("<NoName>", new Integer(uid).toString()).replaceAll("-", "_");
		}
	}
	
	public String cleanQualifiedName(Object o) {
		return cleanQualifiedName(objToIe.get(o));
	}
	
}
