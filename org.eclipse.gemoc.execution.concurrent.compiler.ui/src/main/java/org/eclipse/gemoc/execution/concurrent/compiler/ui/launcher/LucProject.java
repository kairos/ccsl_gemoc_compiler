package org.eclipse.gemoc.execution.concurrent.compiler.ui.launcher;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

//import org.apache.maven.artifact.repository.metadata.Plugin;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.core.ClasspathEntry;
import org.eclipse.pde.core.project.IBundleProjectDescription;
import org.osgi.framework.Bundle;
import org.xml.sax.SAXException;

//import miapita.BuildListener;
import toools.Version;
import toools.Version.Level;
import toools.collections.Collections;
import toools.collections.Filter;
import toools.collections.relation.HashRelation;
import toools.collections.relation.Relation;
import toools.config.ConfigurationException;
import toools.extern.Proces;
import toools.io.FileUtilities;
import toools.io.file.AbstractFile;
import toools.io.file.Directory;
import toools.io.file.FileFilter;
import toools.io.file.RegularFile;
import toools.text.xml.XMLUtilities;

public class LucProject
{
	public LucWorkspace workspace;
	public String projectName;
	public Directory directory;
	public List<Directory> sourceDirectories = new ArrayList<>();
	public Directory classesDirectory;
	public Set<LucProject> projectDependancies = new HashSet<>();
	public Set<RegularFile> libDependancies = new HashSet<>();
	public boolean sourceIsPublic;
	public String[] scpDestination;
	public String svnDestination;
	public String mavenRepository;
	public Version version;
	public RegularFile versionFile;
	public RegularFile changesFile;
	public Collection<AbstractFile> documentations = new ArrayList<>();
	public Set<String> excludedLibNames = new HashSet<>();
	public static ArrayList<String> addedProjectName = new ArrayList<String>();

	public LucProject(LucWorkspace w, String projectName, IJavaProject eclipseProject)
			throws IOException, SAXException, ConfigurationException, JavaModelException
	{
		
		this.workspace = w;
		this.projectName = projectName;
		this.directory = w.getLocation().getChildDirectory(projectName);

		if (directory == null)
			throw new NullPointerException("null base directory");

		if ( ! directory.exists())
			throw new IllegalArgumentException(directory.getPath() + " does not exist");

		w.getProjects().add(this);

		if ( ! getEclipseClasspathFile().exists())
			throw new IllegalArgumentException(
					getEclipseClasspathFile().getPath() + " not found");

		RegularFile miapitaFile = new RegularFile(getDirectory(), "miapita.conf");

		if (miapitaFile.exists())
		{
			InputStream r = miapitaFile.createReadingStream();
			Properties config = new Properties();
			config.load(r);

			this.sourceIsPublic = Boolean
					.valueOf(getValue(config, "source_is_public", false, "false"));

			this.scpDestination = getValues(config, "scp_destination", false, null);
			this.svnDestination = getValue(config, "svn_repository", false, null);
			this.mavenRepository = getValue(config, "maven_repository", false, null);

			{
				for (String n : getValue(config, "exclude_libs", false, "")
						.split("[, ]+"))
				{
					this.excludedLibNames.add(n);
				}
			}

			for (int i = 1;; ++i)
			{
				String s = getValue(config, "manual" + (i == 1 ? "" : i), false, null);

				if (s == null)
				{
					break;
				}
				else
				{
					if ( ! s.isEmpty())
					{
						AbstractFile f = AbstractFile
								.map(directory.getPath() + "/doc/" + s, null);
						this.documentations.add(f);
					}
				}
			}

			r.close();
		}
		else
		{
			this.mavenRepository = null;
			this.scpDestination = null;
			this.svnDestination = null;
		}

//		String xml = new String(getEclipseClasspathFile().getContent());
//		DNode root = XMLUtilities.xml2node(xml, false);
//		List<DNode> nodes = root.findChildren("classpathentry", false);
//
		this.classesDirectory = new Directory(this.directory,"bin");
		while(!classesDirectory.exists()) {
			Thread.yield(); //waiting eclipse to compile it :-/
		}
//
//		for (int i = 0; i < nodes.size() - 1; ++i)
//		{
//			DNode node = nodes.get(i);
//			String kind = node.getAttributes().get("kind");
//
//			if (kind.equals("src"))
//			{
//
//				String srcPath = node.getAttributes().get("path");
//
//				// if this is a dependency to another project
//				if (srcPath.startsWith("/"))
//				{
				IClasspathEntry[] requiredProjectNameTab = eclipseProject.getResolvedClasspath(true);
				ArrayList<IClasspathEntry> requiredProjectNames = new ArrayList<IClasspathEntry>(Arrays.asList(requiredProjectNameTab));
				requiredProjectNames.removeIf(cpe ->cpe.getEntryKind() == IClasspathEntry.CPE_PROJECT);
				ArrayList<IClasspathEntry> requiredLibNames = new ArrayList<IClasspathEntry>(Arrays.asList(requiredProjectNameTab));
				requiredLibNames.removeIf(cpe ->cpe.getEntryKind() == IClasspathEntry.CPE_LIBRARY);
				
//				for(IClasspathEntry cpe : requiredProjectNames) {
//					LucProject project = null;
//					try {
//						project = getWorkspace().findProject(cpe.getPath().toString());
//					}catch (IllegalArgumentException e) {
//					}
//
//					if (project == null)
//					{
//						throw new IllegalStateException("when scanning: " + getName()
//								+ " project: " + getName() + ": project not found: "
//								+ cpe);
//					}
//
//					projectDependancies.add(project);
//				}
//				}
//				// this is a source directory for this project
//				else
//				{
//					sourceDirectories.add(new Directory(directory, srcPath));
//				}

//			}
//			else if (kind.equals("lib"))
//			{
//			for(IClasspathEntry srcFolder : requiredLibNames) {
//				sourceDirectories.add(new Directory(srcFolder.getOutputLocation().toString()));
//			}
//			
			for(IClasspathEntry cpeLib : requiredProjectNames) {
//				Bundle bundle = Platform.getBundle("com.feat.eclipse.plugin");
				if ("jar".equals(cpeLib.getPath().getFileExtension())) {
					if(!cpeLib.getPath().toString().contains("jre/lib")) {
						RegularFile libFile = new RegularFile(cpeLib.getPath().toString());
						libDependancies.add(libFile);
					}
				}else {
					if(cpeLib.getContentKind() == 2) {
						
						String projDir = cpeLib.getSourceAttachmentPath().removeLastSegments(1).toString();
						String depProjectName = cpeLib.getSourceAttachmentPath().lastSegment().toString();
						if(addedProjectName.contains(depProjectName)) 
						{ continue;}
						addedProjectName.add(depProjectName);
						LucProject lp = new LucProject(new LucWorkspace(new Directory(projDir), eclipseProject), depProjectName , eclipseProject);
						projectDependancies.add(lp);
					}
				}
				
			}
				

//				if ( ! libFile.exists())
//				{
//					libFile = new RegularFile(getWorkspace().getLocation(), libName);
//				}
//
//				if ( ! libFile.exists())
//				{
//					libFile = new RegularFile(getDirectory(), libName);
//				}
//
//				if ( ! libFile.exists())
//				{
//					libFile = new RegularFile(libName);
//				}
//
//				if ( ! libFile.exists())
//				{
//					throw new IllegalStateException("project: " + getName()
//							+ ": file not found " + libFile.getPath());
//				}

				
//			}
//		}

//		changesFile = new RegularFile(sourceDirectories.get(0), "CHANGES.txt");
//
//		versionFile = new RegularFile(sourceDirectories.get(0),
//				getName() + "-version.txt");

		// versionOnDisk = computeCurrentVersionBasedOnMostRecentFile();
		// versionOnDisk = new String(getVersionFile().getContent()).trim();

		// versionOfTheWebFile = new RegularFile(getDirectory(),
		// "version-on-the-web.txt");

		// if (versionOfTheWebFile.exists())
//		{
//
//			version = new Version();
//
//			if (versionFile.exists())
//			{
//				version.set(new String(versionFile.getContent()).trim());
//			}
//			else
//			{
//				version.set("0.0.0");
//			}
//		}
	}

	private String[] getValues(Properties config, String entryName, boolean mandatory,
			String defaultValue) throws ConfigurationException
	{
		String s = getValue(config, entryName, mandatory, defaultValue);

		return s == null ? null : s.split(" *, *");
	}

	private String getValue(Properties config, String entryName, boolean mandatory,
			String defaultValue) throws ConfigurationException
	{
		String v = (String) config.get(entryName);

		if (v == null || v.isEmpty())
		{
			if (mandatory)
			{
				throw new ConfigurationException("project " + getName()
						+ ": mandatory entry '" + entryName + "' not found", null);
			}
			else
			{
				System.err.println("Warning: project " + getName() + "mandatory entry '"
						+ entryName + "' not found");

				return defaultValue;
			}
		}
		else
		{
			return v;
		}
	}

//	private Set<RegularFile> ensureCompatibleLibDepandancies(Set<RegularFile> filesToZip,
//			RegularFile zipfile)
//	{
//		Set<RegularFile> result = new HashSet<RegularFile>();
//		Relation<String, RegularFile> r = new HashRelation();
//
//		for (RegularFile f : filesToZip)
//		{
//			r.add(f.getName(), f);
//		}
//
//		for (String filename : r.keySet())
//		{
//			Collection<RegularFile> files = r.getValues(filename);
//
//			// need to check that they are all the same
//			if (FileUtilities.ensureSameFile(files))
//			{
//				result.add(files.iterator().next());
//			}
//			else
//			{
//				zipfile.delete();
//				throw new IllegalStateException(
//						"files have same name but different content: " + files);
//			}
//		}
//
//		return result;
//	}

	public Version getVersion()
	{
		return version;
	}

	public boolean isSourceIsPublic()
	{
		return sourceIsPublic;
	}

	public void setSourceIsPublic(boolean sourceIsPublic)
	{
		this.sourceIsPublic = sourceIsPublic;
	}

	@Override
	public String toString()
	{
		return getName();
	}

	public Directory getDirectory()
	{
		return directory;
	}

	public RegularFile getVersionFile()
	{
		return versionFile;
	}


	public String getName()
	{
		return projectName;
	}

	public List<Directory> getSourceDirectory()
	{
		return this.sourceDirectories;
	}

	public Directory getClassesDirectory()
	{
		return this.classesDirectory;
	}

	public Collection<LucProject> getDirectProjectDependancies()
	{
		return projectDependancies;
	}

	public Set<LucProject> computeIndirectProjectDependancies()
	{
		Set<LucProject> r = new HashSet<LucProject>();
		List<LucProject> queue = new ArrayList<LucProject>();
		queue.addAll(getDirectProjectDependancies());
		Set<LucProject> visitedProject = new HashSet<LucProject>();

		while ( ! queue.isEmpty())
		{
			LucProject thisProject = queue.remove(0);

			if ( ! visitedProject.contains(thisProject))
			{
				visitedProject.add(thisProject);
				r.add(thisProject);
				queue.addAll(thisProject.getDirectProjectDependancies());
			}
		}

		return Collections.difference(r, getDirectProjectDependancies());
	}

	public Set<RegularFile> computeIndirectLibDependancies()
	{
		Set<RegularFile> directLibsOfDependantProjects = new HashSet<RegularFile>();

		for (LucProject p : getDirectAndIndirectProjectDependancies())
		{
			directLibsOfDependantProjects.addAll(p.getDirectLibDependancies());
		}

		Set<RegularFile> r = Collections.difference(directLibsOfDependantProjects,
				getDirectLibDependancies());

		Iterator<RegularFile> libIterator = r.iterator();

		while (libIterator.hasNext())
		{
			RegularFile lib = libIterator.next();

			for (String excludedLibName : excludedLibNames)
			{
				if (lib.getName().startsWith(excludedLibName + "-"))
				{
					System.out.println("excluding " + lib.getName());
					libIterator.remove();
					break;
				}
			}
		}

		return r;
	}

	public Set<RegularFile> getDirectLibDependancies()
	{
		return libDependancies;
	}

	public LucWorkspace getWorkspace()
	{
		return this.workspace;
	}

	public String buildDirectoryPath = "build";
	public Directory getBuildDirectory()
	{
		Directory projectBuildDir = new Directory(buildDirectoryPath);

		if ( ! projectBuildDir.exists())
		{
			projectBuildDir.mkdirs();
		}

		return projectBuildDir;
	}

	public RegularFile getEclipseClasspathFile()
	{
		return new RegularFile(getDirectory(), ".classpath");
	}

	public RegularFile getJAR(boolean versionIt)
	{
		if (versionIt)
		{
			return new RegularFile(getBuildDirectory(),
					getName() + "-" + getVersion() + ".jar");
		}
		else
		{
			return new RegularFile(getBuildDirectory(), getName() + ".jar");
		}
	}

	public void createJARFile(boolean versionIt) throws IOException
	{
//		l.creating(getJAR(versionIt));

		// store them so that we can delete at the end
		Collection<RegularFile> tempSourceFiles = new ArrayList<RegularFile>();

		if (sourceIsPublic)
		{
			for (Directory sd : getSourceDirectory())
			{
				for (RegularFile javaFile : (Collection<RegularFile>) (Collection) sd
						.retrieveTree(new FileFilter.RegexFilter(".*\\.java$"), null))
				{
					if ( ! javaFile.exists())
						throw new IllegalStateException();

					String relativeName = javaFile.getNameRelativeTo(sd);
					RegularFile destinationFile = new RegularFile(getClassesDirectory(),
							relativeName);
					tempSourceFiles.add(destinationFile);
					javaFile.copyTo(destinationFile, true);
				}
			}
		}
		else
		{
			for (RegularFile javaFile : (Collection<RegularFile>) (Collection) getClassesDirectory()
					.retrieveTree(new FileFilter.RegexFilter(".*\\.java$"), null))
			{
				javaFile.delete();
			}
		}

//		if (versionIt)
//		{
//			// write the version file
//			new RegularFile(getClassesDirectory(), getVersionFile().getName())
//					.setContent(version.toString().getBytes());
//		}

		// zip it all in the jar file
		FileUtilities.zip(getJAR(versionIt), getClassesDirectory(), null);
//		l.fileCreated(getJAR(versionIt));

		// delete the source files that were in the bin directory
		for (RegularFile tmpFile : tempSourceFiles)
		{
			tmpFile.delete();
		}
	}

	@Override
	public boolean equals(Object obj)
	{
		return obj instanceof LucProject
				&& ((LucProject) obj).getDirectory().equals(getDirectory());
	}

	@Override
	public int hashCode()
	{
		return directory.hashCode();
	}

	public Collection<RegularFile> getJavaFiles()
	{
		return Collections.filter(getSourceFiles(), new Filter<RegularFile>()
		{
			@Override
			public boolean accept(RegularFile o)
			{
				return o.getName().endsWith("java");
			}
		});
	}

	public Collection<RegularFile> getClassFiles()
	{
		return (Collection<RegularFile>) (Collection) getClassesDirectory()
				.retrieveTree(new FileFilter()
				{
					@Override
					public boolean accept(AbstractFile pathname)
					{
						return pathname.getName().endsWith(".class");
					}
				}, null);
	}

	public Collection<RegularFile> getSourceFiles()
	{
		Set<RegularFile> files = new HashSet<>();

		for (Directory sd : getSourceDirectory())
		{
			files.addAll((Collection<RegularFile>) (Collection) sd
					.retrieveTree(new FileFilter()
					{
						@Override
						public boolean accept(AbstractFile f)
						{
							return ! (f instanceof Directory)
									&& ! f.getPath().contains(".svn");
						}
					}, null));
		}

		return files;
	}

	public Collection<RegularFile> getResourceFiles()
	{
		return Collections.difference(getSourceFiles(), getJavaFiles(), getClassFiles());
	}

	public int getNumberOfSourceLines() throws IOException
	{
		int n = 0;

		for (RegularFile f : getJavaFiles())
		{

			for (String line : Arrays.asList(new String(f.getContent()).split("\n")))
			{
				if ( ! line.trim().isEmpty())
				{
					++n;
				}
			}

		}

		return n;
	}

	public Set<LucProject> getDirectAndIndirectProjectDependancies()
	{
		return Collections.union(getDirectProjectDependancies(),
				computeIndirectProjectDependancies());
	}

	public Set<RegularFile> getDirectAndIndirectLibDependancies()
	{
		return Collections.union(getDirectLibDependancies(),
				computeIndirectLibDependancies());
	}

//	public int computeNumberOfLinesAlsoInProjectDependancies()
//	{
//		int n = 0;
//
//		for (LucProject p : getDirectProjectDependancies())
//		{
//			n += p.getNumberOfSourceLines();
//		}
//
//		return n + getNumberOfSourceLines();
//	}
	
	
	public void createAllJarArchive()
			throws IOException
	{
		ensureAllIndividualJarFilesCreated();
		RegularFile targetBigJar = getSelfContainedBinariesFile();
		Set<File> allJars = new HashSet<File>();

		for (LucProject p : getDirectAndIndirectProjectDependancies())
		{
			allJars.add(p.getJAR(false).toFile());

		}

		for (RegularFile jar : getDirectAndIndirectLibDependancies())
		{
			System.out.println("create jar " + jar);
			allJars.add(jar.toFile());
		}
		allJars.add(getJAR(true).toFile());
		try {
			assemble.assemble(targetBigJar.toFile(), "CompiledExecutionEngine", new ArrayList<File>(allJars));
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void createSelfContainedBinariesArchive()
			throws IOException
	{
		ensureAllIndividualJarFilesCreated();
		RegularFile zipWithDependancies = getSelfContainedBinariesFile();
//		listener.creating(zipWithDependancies);
		Set<RegularFile> filesToZip = new HashSet<RegularFile>();

		for (LucProject p : getDirectAndIndirectProjectDependancies())
		{
			filesToZip.add(p.getJAR(false));

		}

		for (RegularFile jar : getDirectAndIndirectLibDependancies())
		{
			System.out.println("zipping jar " + jar);
			filesToZip.add(jar);
		}

		filesToZip.add(getJAR(true));
		filesToZip.addAll(getDirectLibDependancies());
//		listener.fileCreated(zipWithDependancies);

//		filesToZip = ensureCompatibleLibDepandancies(filesToZip, zipWithDependancies);

		FileUtilities.zip(zipWithDependancies, filesToZip);
//		listener.fileCreated(zipWithDependancies);
	}

	private boolean allIndividualJarFilesCreated = false;

	public void ensureAllIndividualJarFilesCreated()
			throws IOException
	{
		if ( ! allIndividualJarFilesCreated)
		{
			for (LucProject thisProject : getDirectAndIndirectProjectDependancies())
			{
				thisProject.createJARFile(false);
			}

			if ( ! getJAR(true).exists())
			{
				createJARFile( true);
			}

			allIndividualJarFilesCreated = true;
		}
	}

	
	
	
	public void createBigJar() throws IOException
	{
		ensureAllIndividualJarFilesCreated();
		RegularFile bigzip = getBigJAR();
//		listener.creating(bigzip);

		Map<String, byte[]> map = new HashMap<String, byte[]>();

		for (LucProject thisProject : getDirectAndIndirectProjectDependancies())
		{
			// System.out.println("putall " + thisProject.getJAR());
			map.putAll(FileUtilities.unzip(thisProject.getJAR(false)));
		}

		for (RegularFile lib : getDirectAndIndirectLibDependancies())
		{
			// System.out.println("putall " + lib);
			map.putAll(FileUtilities.unzip(lib));
		}

		// System.out.println("putall " + getJAR());
		map.putAll(FileUtilities.unzip(getJAR(true)));

		// removing all META-INF information
//		Iterator<Entry<String, byte[]>> i = map.entrySet().iterator();
//
//		while (i.hasNext())
//		{
//			if (i.next().getKey().startsWith("META-INF/"))
//			{
//				i.remove();
//			}
//		}
		// System.out.println(1);
		// System.out.println(map.size());
		FileUtilities.zip(bigzip, map);
		// System.out.println(2);
//		listener.fileCreated(bigzip);
	}

	public RegularFile getBigJAR()
	{
		return new RegularFile(getBuildDirectory(),
				getName() + "-" + getVersion() + "-big.jar");
	}

	public RegularFile getSelfContainedBinariesFile()
	{
		return new RegularFile(getBuildDirectory(),
				getName() + "-allInOne.jar");
	}

	public String[] getSCPDestination()
	{
		return this.scpDestination;
	}

	public String getSVNDestination()
	{
		return this.svnDestination;
	}

//	public void release(BuildListener buildListener, boolean putOnTheWeb, boolean javadoc,
//			Level upgradeLevel) throws IOException, SAXException
//	{
//		if (upgradeLevel == null)
//			throw new NullPointerException();
//
//		version.upgrade(upgradeLevel);
//		createSelfContainedBinariesArchive();
//		createBigJar();
//		versionFile.setContent(version.toString().getBytes());
//
//		if (javadoc)
//		{
//			createJavadoc(buildListener);
//		}
//
//		if ( ! putOnTheWeb)
//			return;
//
//		if (scpDestination == null)
//		{
//			buildListener.message("No SCP repository specified, skipping");
//		}
//		else
//		{
//			for (String dest : getSCPDestination())
//			{
//				String[] sshDestination = dest.split(":");
//				Proces.exec("ssh", sshDestination[0], "mkdir", "-p",
//						sshDestination[1] + "/releases");
//
//				for (RegularFile f : new RegularFile[] { getJAR(true),
//						getSelfContainedBinariesFile(), getBigJAR() })
//				{
//					Proces.exec("rsync", f.getPath(), dest + "/releases/" + f.getName());
//					buildListener.uploading(f, dest);
//				}
//
//				// uploads manuals, if any
//				{
//					for (AbstractFile docPath : getDocumentationPaths())
//					{
//						buildListener.uploading(docPath, dest);
//
//						docPath.rsyncTo(dest + "/doc/" + docPath.getName());
//					}
//				}
//
//				// uploads javadoc
//				if (javadoc)
//				{
//					buildListener.uploading(getJavadocDirectory(), dest);
//
//					Proces.exec("ssh", sshDestination[0], "mkdir", "-p",
//							sshDestination[1] + "/doc/javadoc");
//					Proces.exec("rsync", "-a", getJavadocDirectory().getPath() + "/",
//							dest + "/doc/javadoc");
//				}
//			}
//
//			// update maven repository
//			if (mavenRepository == null)
//			{
//				buildListener.message("No MAVEN repository specified, skipping");
//			}
//			else
//			{
//				buildListener.mavenuploading(mavenRepository);
//				String mavenServer = mavenRepository.replaceAll(":.*", "");
//				String repositoryDir = mavenRepository.replaceAll(".*:", "");
//				String dir = repositoryDir + "/" + getName() + "/" + getName() + "/"
//						+ version;
//
//				// create the directory on the maven repository
//				Proces.exec("ssh", mavenServer, "mkdir", "-p", dir);
//
//				String xml = "<project>\n\t<modelVersion>4.0.0</modelVersion>\n\t<groupId>"
//						+ getName() + "</groupId>\n\t<artifactId>" + getName()
//						+ "</artifactId>\n\t<version>" + version
//						+ "</version>\n</project>";
//
//				// creates the pom file in the repository
//				Proces.exec("ssh", xml.getBytes(), mavenServer, "cat", ">",
//						dir + "/" + getName() + "-" + version + ".pom");
//
//				// uploads the big JAR
//				Proces.exec("scp", getBigJAR().getPath(), mavenServer + ":" + dir + "/"
//						+ getName() + "-" + version + ".jar");
//			}
//
//			{
//				for (String dest : getSCPDestination())
//				{
//					String scpServer = dest.replaceAll(":.*", "");
//					String scpDir = dest.replaceAll(".*:", "");
//
//					// maven users will update patch correcting releases
//					Proces.exec("ssh",
//							version.toString().replaceFirst("\\.[0-9]*$", "").trim()
//									.getBytes(),
//							scpServer, "cat",
//							">" + scpDir + "/releases/maven-public-version.txt");
//				}
//			}
//
//			if (svnDestination == null)
//			{
//				buildListener.message("No SVN repository specified, skipping");
//			}
//			else
//			{
//				String src = getSVNDestination() + "/trunk/";
//				String dest = getSVNDestination() + "/tags/" + getName() + "-"
//						+ getVersion();
//				buildListener.svnuploading(dest);
//
//				Proces.exec("svn", "copy", "-rHEAD", src, dest, "--message",
//						"\"This is release " + getVersion() + "\"");
//			}
//
//			// timestamps the "CHANGES" file
//			Calendar rightNow = Calendar.getInstance();
//			DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
//			String formattedDate = df.format(rightNow.getTime());
//			changesFile.append(("\n\nVersion " + getVersion() + "\treleased on "
//					+ formattedDate + "\n\n").getBytes());
//
//			for (String dest : getSCPDestination())
//			{
//				buildListener.uploading(changesFile, dest);
//				Proces.exec("scp", changesFile.getPath(), dest);// uploads last
//																// version
//																// information
//				buildListener.uploading(versionFile, dest + "/releases/last-version.txt");
//				Proces.exec("scp", versionFile.getPath(),
//						dest + "/releases/last-version.txt");
//			}
//
//		}
//	}

	public Directory getJavadocDirectory()
	{
		return new Directory(getDirectory(), "doc/javadoc");
	}

//	public List<Class<?>> getClasses()
//	{
//		List<Class<?>> l = new ArrayList<Class<?>>();
//
//		for (Directory sd : getSourceDirectory())
//		{
//			for (RegularFile sf : (Collection<RegularFile>) (Collection) sd
//					.retrieveTree(new FileFilter.RegexFilter("\\.java$"), null))
//			{
//				String classname = sf.getName().substring(sd.getPath().length() + 1)
//						.replace('\\', '.');
//				l.add(Clazz.findClassOrFail(classname));
//			}
//		}
//
//		return l;
//	}

//	public void createJavadoc(BuildListener l)
//	{
//		String srcPaths = "";
//
//		for (Directory sd : getSourceDirectory())
//		{
//			srcPaths += " " + sd.getPath();
//		}
//
//		l.creating(getJavadocDirectory());
//		Proces.exec("bash", "-c",
//				"javadoc -Xdoclint:none -d " + getJavadocDirectory().getPath()
//						+ " $(find " + srcPaths + "  -type f -name '*.java')");
//	}

	private Collection<AbstractFile> getDocumentationPaths()
	{
		return this.documentations;
	}

	public int computeNumberOfFileAlsoInProjectDependancies()
	{
		int n = 0;

		for (LucProject p : getDirectProjectDependancies())
		{
			n += p.getJavaFiles().size();
		}

		return n + getJavaFiles().size();
	}

	public String getMavenRepository()
	{
		return mavenRepository;
	}

}
