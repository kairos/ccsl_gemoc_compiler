/*******************************************************************************
 * Copyright (c) 2017 INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     INRIA - initial API and implementation
 *     I3S Laboratory - API update and bug fix
 *******************************************************************************/
package org.eclipse.gemoc.execution.concurrent.compiler.ui.launcher;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.ILaunchGroup;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem;
import org.eclipse.gemoc.dsl.debug.ide.adapter.IDSLCurrentInstructionListener;
import org.eclipse.gemoc.execution.concurrent.ccsljavaengine.commons.MoccmlModelExecutionContext;
import org.eclipse.gemoc.execution.concurrent.ccsljavaengine.extensions.timesquare.moc.impl.CcslSolver;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.moc.ISolver;
import org.eclipse.gemoc.execution.concurrent.compiler.commons.CompilerMode;
import org.eclipse.gemoc.execution.concurrent.compiler.commons.ConcurrentCompilationContext;
import org.eclipse.gemoc.execution.concurrent.compiler.commons.ConcurrentCompilerExecutionPlatform;
import org.eclipse.gemoc.execution.concurrent.compiler.commons.ConcurrentCompilerRunConfiguration;
import org.eclipse.gemoc.execution.concurrent.compiler.fakeexecutor.FakeConcurrentExecutionEngine;
import org.eclipse.gemoc.execution.concurrent.compiler.ui.Activator;
import org.eclipse.gemoc.execution.concurrent.compiler.ui.templates.ForceCaseTemplate;
import org.eclipse.gemoc.executionframework.engine.commons.EngineContextException;
import org.eclipse.gemoc.executionframework.engine.core.RunConfiguration;
import org.eclipse.gemoc.executionframework.engine.ui.launcher.AbstractGemocLauncher;
import org.eclipse.gemoc.executionframework.extensions.sirius.services.AbstractGemocDebuggerServices;
import org.eclipse.gemoc.moccml.mapping.feedback.feedback.ActionModel;
import org.eclipse.gemoc.moccml.mapping.feedback.feedback.ActionResultCondition;
import org.eclipse.gemoc.moccml.mapping.feedback.feedback.Force;
import org.eclipse.gemoc.moccml.mapping.feedback.feedback.ModelSpecificEvent;
import org.eclipse.gemoc.moccml.mapping.feedback.feedback.When;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

import fr.inria.aoste.timesquare.ccslkernel.codegenerator.handlers.CCSLJavaCodeGenerator;
import fr.inria.aoste.timesquare.ccslkernel.codegenerator.helpers.PluginProjectHelper;
import toools.io.file.RegularFile;

public class Launcher extends AbstractGemocLauncher<MoccmlModelExecutionContext> {

	public final static String TYPE_ID = Activator.PLUGIN_ID + ".launcher";

	private FakeConcurrentExecutionEngine _executionEngine;

	private boolean withCoordinationAPI = false;
	private boolean withAspectsManagement = false;

	@Override
	public void launch(final ILaunchConfiguration configuration, final String mode, final ILaunch launch,
			IProgressMonitor monitor) throws CoreException {
			debug("About initialize the compiler");

			// We parse the run configuration
			final ConcurrentCompilerRunConfiguration runConfiguration = new ConcurrentCompilerRunConfiguration(configuration);

			ConcurrentCompilationContext concurrentexecutionContext = null;
			try {
				concurrentexecutionContext = new ConcurrentCompilationContext(runConfiguration);
			} catch (EngineContextException e1) {
				debug("Error when initializing the compiler");
				e1.printStackTrace();
			}
			concurrentexecutionContext.initializeResourceModel();
			ISolver _solver = null;
			try {
				_solver = concurrentexecutionContext.getMoccmlLanguageAdditionExtension().instanciateSolver();
				_solver.prepareBeforeModelLoading(concurrentexecutionContext);
				_solver.initialize(concurrentexecutionContext);
			} catch (CoreException e) {
				throw new CoreException(new Status(Status.ERROR, Activator.PLUGIN_ID,
						"Cannot instanciate solver from language definition", e));
			}

			//creates and setup the feedback model
			_executionEngine = new FakeConcurrentExecutionEngine(concurrentexecutionContext, _solver);
			 
			printImportantInformation(concurrentexecutionContext, (CcslSolver) _solver);
			
			debug("Compiler Initialization done, launching compiler...");

	}
	
	public String getQualifiedNamed(EObject eo, String separator) {
		if(eo == null) {
			return "null";
		}
		String res = getSimpledName(eo);
		if(eo.eContainer() == null) {
			return res;			
		}
		return getQualifiedNamed(eo.eContainer(), separator, res).replaceAll("-", "_");
	}

	private String getSimpledName(EObject eo) {
		if (eo == null) {
			return "null";
		}
		EStructuralFeature esf = ((EObject)eo).eClass().getEStructuralFeature("name");
		String res = "";
		if (esf == null) {
			res +="_";
		}else {
			res+= ((EObject)eo).eGet(esf);
		}
		return res.replaceAll("-", "_");
	}
	
	public String getQualifiedNamed(EObject eo, String separator, String initialString) {
		String n = getSimpledName(eo);
		if(eo.eContainer() == null) {
			return n+separator+initialString;			
		}
		return getQualifiedNamed(eo.eContainer(), separator, n+separator+initialString).replaceAll("-", "_");
	}
	

	public void printImportantInformation(ConcurrentCompilationContext concurrentCompilationContext, CcslSolver solver) {
		
		String projectName = (concurrentCompilationContext.getWorkspace().getProjectPath().removeLastSegments(1)+concurrentCompilationContext.resultProjectName).substring(1);
		withCoordinationAPI = concurrentCompilationContext.compilerMode == CompilerMode.COORDINATION;
		withAspectsManagement = withCoordinationAPI || concurrentCompilationContext.compilerMode == CompilerMode.EXECUTION;
		
		CCSLJavaCodeGenerator tsqCodeGen = new CCSLJavaCodeGenerator(projectName, withAspectsManagement, withCoordinationAPI);
		
		URI ccslUri = null;
		try {
			Field solverURIField = solver.getClass().getDeclaredField("solverInputURI");
			solverURIField.setAccessible(true);
			ccslUri = (URI) solverURIField.get(solver);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
		URL fileUrl = null;
		try {
			fileUrl = FileLocator.toFileURL(new URL(ccslUri.toString()));
		} catch (IOException e) {
			e.printStackTrace();
		} 
		IFile ccslFile = ResourcesPlugin.getWorkspace().getRoot().getFileForLocation( new Path(fileUrl.getPath()));
		
		
		
		try {
			tsqCodeGen.createProjectAndGenerate(ccslFile);
		} catch (CoreException e) {
			e.printStackTrace();
		}
		
		try {
			RegularFile model = new RegularFile((tsqCodeGen.getProject().getLocationURI()+"/src/"+concurrentCompilationContext.getResourceModel().getURI().lastSegment()).substring(5));		
			concurrentCompilationContext.getResourceModel().save(model.createWritingStream() , null);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		if(withAspectsManagement) {
			
			IProject p = tsqCodeGen.getProject();
			PluginProjectHelper pluginHelper = new PluginProjectHelper();
			Dictionary<String, String> allDSLBundleHeader = concurrentCompilationContext.getDslBundle().getHeaders();
			String stringOfRequiredBundles = allDSLBundleHeader.get("Require-Bundle");
			String[] tmpListOfRequiredBundles = stringOfRequiredBundles.split(",");
			try {
				for(String dep : tmpListOfRequiredBundles) {
					if(dep.contains(";")) {
						pluginHelper.addRequiredBundle(p, dep.split(";")[0]);
					}else {
						pluginHelper.addRequiredBundle(p, dep);
					}
				}
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
		
		tsqCodeGen.generateMainHeader(tsqCodeGen.srcFolder);
		
		if (withAspectsManagement) {
			generateAspectAndModelIntializationAndBeginOfMain(concurrentCompilationContext, tsqCodeGen);
		}
		if(withAspectsManagement && withCoordinationAPI) {
			tsqCodeGen.generateEndOfMainWithAspectManagementAndCoordination();
			RegularFile forcedCaseFile = new RegularFile(tsqCodeGen.srcFolder.getLocation().toPortableString()+"/ForceCase.java");
			try {
				forcedCaseFile.setContent(ForceCaseTemplate.content.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else
		if(withAspectsManagement && !withCoordinationAPI) {
			tsqCodeGen.generateEndOfMainWithAspectManagement();
		}else
		if(!withAspectsManagement && !withCoordinationAPI) {
			tsqCodeGen.generateBeginOfMain(concurrentCompilationContext.getLanguageDefinitionExtension().getName());
			tsqCodeGen.generateEndOfMain();
		}else
			
			
			
		return;		
	}

	private void generateAspectAndModelIntializationAndBeginOfMain(ConcurrentCompilationContext concurrentCompilationContext,
			CCSLJavaCodeGenerator tsqCodeGen) {
		tsqCodeGen.generateAspectList(new ArrayList<Class<?>>(((ConcurrentCompilerExecutionPlatform)concurrentCompilationContext.getExecutionPlatform()).allAspects));
		tsqCodeGen.generateBeginOfMain(concurrentCompilationContext.getLanguageDefinitionExtension().getName());
		
		StringBuilder builder = new StringBuilder();
		
//		builder.append("\n\n\n       Map<Clock, String> clockToMethodName= new HashMap<>();\n");
//		builder.append("      Map<Clock, String> clockToEObject = new HashMap<>();\n");
//		
		for(ModelSpecificEvent mse : concurrentCompilationContext.getFeedbackModel().getEvents()) {
			if (mse == null) {
				System.out.println("warning, a null MSE");
				continue;
			}
			if (mse.getAction() == null) {
				continue; //nothing to do here
			}
			builder.append("theEngine.clockToEObject.put("+getQualifiedNamed(mse.getSolverEvent(), "_")+", \""+mse.getCaller().eResource().getURIFragment(mse.getCaller())+"\");\n");
			builder.append("theEngine.clockToMethodName.put("+getQualifiedNamed(mse.getSolverEvent(), "_")+", \""+mse.getAction().getName()+"\");\n");
		}
		
		String modelExtension = "";
		List<String> modelExtensions = concurrentCompilationContext.getLanguageDefinitionExtension().getFileExtensions();
		if(modelExtensions.size() > 0){
			modelExtension = concurrentCompilationContext.getLanguageDefinitionExtension().getFileExtensions().get(0);
		}else {
			String modelString = concurrentCompilationContext.getResourceModel().toString();
			modelExtension = modelString.substring(modelString.lastIndexOf('.')+1);
			modelExtension = modelExtension.substring(0,modelExtension.length()-1);
		}

		
		String rootClassName = concurrentCompilationContext.getResourceModel().getContents().get(0).toString();
		rootClassName = rootClassName.substring(0, rootClassName.indexOf('@'));

		String initMethodName = concurrentCompilationContext.getRunConfiguration().getModelInitializationMethod();
		initMethodName = initMethodName.substring(initMethodName.lastIndexOf('.')+1);
		
		
		builder.append(("\n\n\n"+
				"	org.eclipse.emf.ecore.resource.ResourceSet resourceSet = new org.eclipse.emf.ecore.resource.impl.ResourceSetImpl();\n" + 
				"\n" + 
				"	resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(\n" + 
				"			\""+modelExtension+"\", new org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl());\n" + 
				"\n" + 
				"	// Register the package -- only needed for stand-alone!\n"
				));
		
		Set<Class<?>> uniquePackageClassSet = new HashSet<>();
		TreeIterator<EObject> it = concurrentCompilationContext.getResourceModel().getAllContents();
		while(it.hasNext()) {
			EObject eo = it.next();
			uniquePackageClassSet.add(eo.eClass().getEPackage().getClass().getInterfaces()[0]);
		}
		
		for(Class<?> c : uniquePackageClassSet) {
			String packageName = c.toString();
			packageName = packageName.substring(packageName.indexOf(' '));
			builder.append(("	"+packageName+" "+packageName.substring(packageName.lastIndexOf('.')+1).toLowerCase()+" = "+packageName+".eINSTANCE;\n\n"));
		}
		
		String modelName = concurrentCompilationContext.getResourceModel().getURI().lastSegment();
		builder.append("RegularFile model = new RegularFile(\""+modelName+"\");\n" + 
				"	try {\n" + 
				"		JavaResource.exportToFile(\"/"+modelName+"\", model);\n" + 
				"	} catch (java.io.IOException e) {\n" + 
				"		e.printStackTrace();\n" + 
				"	}");
		
		builder.append(("	org.eclipse.emf.ecore.resource.Resource resource = resourceSet.getResource(org.eclipse.emf.common.util.URI.createFileURI(\""+modelName+"\"), true);\n" + 
				"	\n" + 
				"	"+rootClassName+" root = ("+rootClassName+") resource.getContents().get(0);\n"));
		
		
		if(initMethodName.length()>0) {
			builder.append(("	ArrayList<Object> param = new ArrayList<Object>();\n" + 
					"	param.add(new org.eclipse.emf.ecore.util.BasicInternalEList<String>(String.class));\n" + 
					"	theEngine.execute(root, \""+initMethodName+"\", param);\n\n"
					));
		}
		
		//manage feedback
		ActionModel feedBackModel = concurrentCompilationContext.getFeedbackModel();
		for(When a : feedBackModel.getWhenStatements()) {
			ModelSpecificEvent onClock = a.getSource();
			ModelSpecificEvent clockToForce = ((Force)a.getAction()).getEventToBeForced();
			Boolean comparisonValue = (Boolean) ((ActionResultCondition)a.getCondition()).getComparisonValue();
			builder.append(("theEngine.forcedClock.put("+getQualifiedNamed(onClock.getSolverEvent(), "_")+", new ForceCase("+getQualifiedNamed(clockToForce.getSolverEvent(), "_")+", "+comparisonValue.booleanValue()+"));\n"));
		}
		
		tsqCodeGen.injectMethod(builder);
	}


	protected void debug(String message) {
		getMessagingSystem().debug(message, getPluginID());
	}

	protected void info(String message) {
		getMessagingSystem().info(message, getPluginID());
	}

	protected void warn(final String message) {
		getMessagingSystem().warn(message, getPluginID());
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						"GEMOC Engine Compiler", message);
			}
		});
	}

	protected void error(final String message) {
		getMessagingSystem().error(message, getPluginID());
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						"GEMOC Engine Compiler", message);
			}
		});
	}

	private MessagingSystem getMessagingSystem() {
		return Activator.getDefault().getMessaggingSystem();
	}

	@Override
	protected String getLaunchConfigurationTypeID() {
		return TYPE_ID;
	}

	@Override
	protected EObject getFirstInstruction(ISelection selection) {
		return EcorePackage.eINSTANCE;
	}

	@Override
	protected EObject getFirstInstruction(IEditorPart editor) {
		return EcorePackage.eINSTANCE;
	}

	@Override
	protected EObject getFirstInstruction(ILaunchConfiguration configuration) {
		return EcorePackage.eINSTANCE;
	}


	@Override
	protected String getDebugTargetName(ILaunchConfiguration configuration, EObject firstInstruction) {
		return "Gemoc compiler target";
	}

	@Override
	protected List<IDSLCurrentInstructionListener> getCurrentInstructionListeners() {
		List<IDSLCurrentInstructionListener> result = super.getCurrentInstructionListeners();
		result.add(AbstractGemocDebuggerServices.LISTENER);
		return result;
	}

	@Override
	protected String getDebugJobName(ILaunchConfiguration configuration, EObject firstInstruction) {
		return "Gemoc Concurrent debug job";
	}

	@Override
	protected String getPluginID() {
		return Activator.PLUGIN_ID;
	}

	@Override
	public String getModelIdentifier() {
		return null;
	}

	@Override
	protected ILaunchConfiguration[] createLaunchConfiguration(IResource file, EObject firstInstruction, String mode)
			throws CoreException {
		ILaunchConfiguration[] launchConfigs = super.createLaunchConfiguration(file, firstInstruction, mode);

		if (launchConfigs.length == 1) {
			// open configuration for further editing
			if (launchConfigs[0] instanceof ILaunchConfigurationWorkingCopy) {
				ILaunchConfigurationWorkingCopy configuration = (ILaunchConfigurationWorkingCopy) launchConfigs[0];

				String selectedLanguage = configuration.getAttribute(RunConfiguration.LAUNCH_SELECTED_LANGUAGE, "");
				if (selectedLanguage.equals("")) {

					// TODO try to infer possible language and other attribute
					// from project content and environment
					configuration.setAttribute(ConcurrentCompilerRunConfiguration.LAUNCH_SELECTED_DECIDER,
							ConcurrentCompilerRunConfiguration.DECIDER_ASKUSER_STEP_BY_STEP);
					final ILaunchGroup group = DebugUITools.getLaunchGroup(configuration, mode);
					if (group != null) {
						ILaunchConfiguration savedLaunchConfig = configuration.doSave();
						// open configuration for user validation and inputs
						DebugUITools.openLaunchConfigurationDialogOnGroup(
								PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
								new StructuredSelection(savedLaunchConfig), group.getIdentifier(), null);
					}
				}
			}
		}
		return launchConfigs;

	}

	@Override
	public IExecutionEngine getExecutionEngine() {
		return _executionEngine;
	}

}
