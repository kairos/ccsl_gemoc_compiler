package org.eclipse.gemoc.execution.concurrent.compiler.ui.launcher;


import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.xml.sax.SAXException;

import toools.config.ConfigurationException;
import toools.io.file.Directory;
import toools.io.file.RegularFile;

public class LucWorkspace
{
	private final Directory directory;
	private final Set<LucProject> projects = new HashSet<LucProject>();
	public IJavaProject eclipseProject;

	public LucWorkspace(Directory workspaceDirectory, IJavaProject eclipseProject) throws IOException,
			SAXException, ConfigurationException
	{
		this.directory = workspaceDirectory;
		this.eclipseProject = eclipseProject;
		rescanProjects();
	}

	public void rescanProjects() throws IOException, SAXException, ConfigurationException
	{
		this.projects.clear();

//		for (Directory projectDir : getLocation().listDirectories())
//		{
//			try
//			{
//				findProject(projectDir.getName());
//			}
//			catch (IllegalArgumentException | JavaModelException e)
//			{
//				System.err.println("can't load project " + projectDir.getName());
//			}
//		}
	}


	public Directory getLocation()
	{
		return this.directory;
	}

	public Set<LucProject> getProjects()
	{
		return projects;
	}

	public LucProject findProject(String projectName) throws IOException,
			SAXException, ConfigurationException, JavaModelException
	{
		for (LucProject p : getProjects())
		{
			if (p.getName().equals(projectName))
			{
				return p;
			}
		}

		Directory projectDir = new Directory(directory, projectName);

		if (new RegularFile(projectDir, ".project").exists()
				&& new RegularFile(projectDir, ".classpath").exists())
		{
			LucProject p = new LucProject(this, projectName, eclipseProject);
			this.projects.add(p);
			return p;
		}
		else
		{
			throw new IllegalArgumentException("Not a project directory: " + projectDir);
		}		
	}

}
