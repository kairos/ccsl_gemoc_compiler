/*******************************************************************************
 * Copyright (c) 2017 INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     INRIA - initial API and implementation
 *     I3S Laboratory - API update and bug fix
 *******************************************************************************/
package org.eclipse.gemoc.execution.concurrent.compiler.ui.launcher.tabs;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.gemoc.commons.eclipse.emf.URIHelper;
import org.eclipse.gemoc.commons.eclipse.ui.dialogs.SelectAnyIFileDialog;
import org.eclipse.gemoc.dsl.debug.ide.launch.AbstractDSLLaunchConfigurationDelegate;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.dsa.executors.ICodeExecutor;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.extensions.languages.MoccmlLanguageAdditionExtension;
import org.eclipse.gemoc.execution.concurrent.ccsljavaxdsml.api.extensions.languages.MoccmlLanguageAdditionExtensionPoint;
import org.eclipse.gemoc.execution.concurrent.compiler.commons.ConcurrentCompilerRunConfiguration;
import org.eclipse.gemoc.execution.concurrent.compiler.ui.Activator;
import org.eclipse.gemoc.execution.concurrent.compiler.ui.launcher.LauncherMessages;
import org.eclipse.gemoc.executionframework.engine.commons.DslHelper;
import org.eclipse.gemoc.executionframework.engine.core.RunConfiguration;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

public class LaunchConfigurationMainTab extends LaunchConfigurationTab {

	protected Composite _parent;

	protected Text _modelLocationText;
	protected Text _modelInitializationMethodText;
	protected Text _modelInitializationArgumentsText;
	protected Text _resultingProjectNameText;
	protected Combo _languageCombo;
    protected Combo _compilerMode;
	protected Text modelofexecutionglml_LocationText;

	public int GRID_DEFAULT_WIDTH = 200;

	@Override
	public void createControl(Composite parent) {
		_parent = parent;
		Composite area = new Composite(parent, SWT.NULL);
		GridLayout gl = new GridLayout(1, false);
		gl.marginHeight = 0;
		area.setLayout(gl);
		area.layout();
		setControl(area);

		Group modelArea = createGroup(area, "Model:");
		createModelLayout(modelArea, null);

		Group languageArea = createGroup(area, "Language:");
		createLanguageLayout(languageArea, null);
		
		Group compilerModeArea = createGroup(area, "API:");
		createCompilerModeLayout(compilerModeArea, null);
		
		Group resultModeArea = createGroup(area, "result:");
		createResultingProjectNameLayout(resultModeArea, null);

	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(ConcurrentCompilerRunConfiguration.LAUNCH_DELAY, 1000);
		configuration.setAttribute(ConcurrentCompilerRunConfiguration.LAUNCH_SELECTED_DECIDER,
				ConcurrentCompilerRunConfiguration.DECIDER_ASKUSER_STEP_BY_STEP);
		configuration.setAttribute(ConcurrentCompilerRunConfiguration.RESULT_PROJECT, "");
	}

	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		try {
			ConcurrentCompilerRunConfiguration runConfiguration = new ConcurrentCompilerRunConfiguration(configuration);
			_modelLocationText.setText(URIHelper.removePlatformScheme(runConfiguration.getExecutedModelURI()));
			_languageCombo.setText(runConfiguration.getLanguageName());
			_modelInitializationArgumentsText.setText(runConfiguration.getModelInitializationArguments());
			_compilerMode.setText(runConfiguration.getCompilerMode());
			_resultingProjectNameText.setText(runConfiguration.getResultProjectName());

		} catch (CoreException e) {
			Activator.error(e.getMessage(), e);
		}
	}

	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(AbstractDSLLaunchConfigurationDelegate.RESOURCE_URI,
				this._modelLocationText.getText());
		configuration.setAttribute(ConcurrentCompilerRunConfiguration.LAUNCH_SELECTED_LANGUAGE, this._languageCombo.getText());
		configuration.setAttribute(ConcurrentCompilerRunConfiguration.LAUNCH_INITIALIZATION_METHOD,
				_modelInitializationMethodText.getText());
		configuration.setAttribute(ConcurrentCompilerRunConfiguration.LAUNCH_INITIALIZATION_ARGUMENTS,
				_modelInitializationArgumentsText.getText());
		configuration.setAttribute(ConcurrentCompilerRunConfiguration.COMPILER_MODE, this._compilerMode.getText());
		configuration.setAttribute(ConcurrentCompilerRunConfiguration.RESULT_PROJECT, this._resultingProjectNameText.getText());
	}

	@Override
	public String getName() {
		return "Main";
	}

	// -----------------------------------

	/**
	 * Basic modify listener that can be reused if there is no more precise need
	 */
	private ModifyListener fBasicModifyListener = new ModifyListener() {
		@Override
		public void modifyText(ModifyEvent arg0) {
			updateLaunchConfigurationDialog();
		}
	};

	// -----------------------------------

	/***
	 * Create the Field where user enters model to execute
	 * 
	 * @param parent
	 * @param font
	 * @return
	 */
	public Composite createModelLayout(Composite parent, Font font) {
		createTextLabelLayout(parent, "Model to execute");
		// Model location text
		_modelLocationText = new Text(parent, SWT.SINGLE | SWT.BORDER);
		_modelLocationText.setLayoutData(createStandardLayout());
		_modelLocationText.setFont(font);
		_modelLocationText.addModifyListener(fBasicModifyListener);
		Button modelLocationButton = createPushButton(parent, "Browse", null);
		modelLocationButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent evt) {
				// handleModelLocationButtonSelected();
				// TODO launch the appropriate selector

				SelectAnyIFileDialog dialog = new SelectAnyIFileDialog();
				if (dialog.open() == Dialog.OK) {
					String modelPath = ((IResource) dialog.getResult()[0]).getFullPath().toPortableString();
					_modelLocationText.setText(modelPath);
					updateLaunchConfigurationDialog();
				}
			}
		});
		createTextLabelLayout(parent, "Model initialization method");
		_modelInitializationMethodText = new Text(parent, SWT.SINGLE | SWT.BORDER);
		_modelInitializationMethodText.setLayoutData(createStandardLayout());
		_modelInitializationMethodText.setFont(font);
		_modelInitializationMethodText.setEditable(false);
		createTextLabelLayout(parent, "");
		createTextLabelLayout(parent, "Model initialization arguments");
		_modelInitializationArgumentsText = new Text(parent, SWT.MULTI | SWT.BORDER |  SWT.WRAP | SWT.V_SCROLL);
		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.heightHint = 40;
		_modelInitializationArgumentsText.setLayoutData(gridData);
		//_modelInitializationArgumentsText.setLayoutData(createStandardLayout());
		_modelInitializationArgumentsText.setFont(font);
		_modelInitializationArgumentsText.setEditable(true);
		_modelInitializationArgumentsText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				updateLaunchConfigurationDialog();
			}
		});
		createTextLabelLayout(parent, "");
		return parent;
	}

	

	private GridData createStandardLayout() {
		return new GridData(SWT.FILL, SWT.CENTER, true, false);
	}

	/***
	 * Create the Field where user enters the language used to execute
	 * 
	 * @param parent
	 * @param font
	 * @return
	 */
	public Composite createLanguageLayout(Composite parent, Font font) {
		// Language
				createTextLabelLayout(parent, "DSL languages");
				_languageCombo = new Combo(parent, SWT.NONE);
				_languageCombo.setLayoutData(createStandardLayout());

				List<String> languagesNames = DslHelper.getAllLanguages();
				String[] empty = {};
				_languageCombo.setItems(languagesNames.toArray(empty));
				_languageCombo.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						updateLaunchConfigurationDialog();
					}
				});
				createTextLabelLayout(parent, "");

				return parent;
	}
	
	/***
	 * Create the Field where user enters the compiler mode used to compile
	 * 
	 * @param parent
	 * @param font
	 * @return
	 */
	public Composite createCompilerModeLayout(Composite parent, Font font) {
		// Compiler mode
				createTextLabelLayout(parent, "compiler Mode");
				_compilerMode = new Combo(parent, SWT.NONE);
				_compilerMode.setLayoutData(createStandardLayout());

				List<String> compilerModes = new ArrayList<String>( 
			            Arrays.asList("executable with coordination API", 
		                          	  "executable with execution API", 
		                          	  "executable with MoCCML only (no aspects)")); 
				String[] empty = {};
				_compilerMode.setItems(compilerModes.toArray(empty));
				_compilerMode.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						updateLaunchConfigurationDialog();
					}
				});
				createTextLabelLayout(parent, "");

				return parent;
	}

	/***
	 * Create the Field where user enters the name of the resulting project
	 * 
	 * @param parent
	 * @param font
	 * @return
	 */
	public Composite createResultingProjectNameLayout(Composite parent, Font font) {
		// Compiler mode
				createTextLabelLayout(parent, "resulting project name");
				_resultingProjectNameText = new Text(parent, SWT.MULTI | SWT.BORDER |  SWT.WRAP | SWT.V_SCROLL);
				GridData gridData = new GridData(GridData.FILL_BOTH);
				gridData.heightHint = 40;
				_resultingProjectNameText.setLayoutData(gridData);
				//_modelInitializationArgumentsText.setLayoutData(createStandardLayout());
				_resultingProjectNameText.setFont(font);
				_resultingProjectNameText.setEditable(false);
//				_resultingProjectNameText.addModifyListener(new ModifyListener() {
//
//					@Override
//					public void modifyText(ModifyEvent e) {
//						updateLaunchConfigurationDialog();
//					}
//				});
				createTextLabelLayout(parent, "");
				return parent;
	}
	

	@Override
	protected void updateLaunchConfigurationDialog() {
		super.updateLaunchConfigurationDialog();
		// modelInitializationMethod must come from the xdsml, maybe later we would
		// allows an "expert mode" where we will allow to change it there
		MoccmlLanguageAdditionExtension languageDefinitionExtPoint = MoccmlLanguageAdditionExtensionPoint
				.findMoccmlLanguageAdditionForLanguage(_languageCombo.getText());

		if (languageDefinitionExtPoint != null) {
			_modelInitializationMethodText.setText(getModelInitializationMethodName(languageDefinitionExtPoint));
		} else {
			_modelInitializationMethodText.setText("");
		}
		
		if(_modelLocationText.getText().trim().length() > 0) {
			String projName = _modelLocationText.getText().substring(1).replaceAll("/", ".")+".executable";
			if(_compilerMode.getText().trim().length() ==0
			 ||_compilerMode.getText().contains("coordination") ) {
				projName+=".coordination";
			}else
			if(_compilerMode.getText().contains("MoCCML") ) {	
				projName+=".moccml";
			}
			_resultingProjectNameText.setText(projName);
			_resultingProjectNameText.setEditable(true);
		}

	}
	
	protected String getModelInitializationMethodName(MoccmlLanguageAdditionExtension languageDefinitionExtension){
		
		ICodeExecutor codeExecutor;
		try {
			codeExecutor = languageDefinitionExtension.instanciateCodeExecutor();
		
			URI uri = URI.createPlatformResourceURI(_modelLocationText.getText(), true);
			Object caller = EMFResource.getFirstContent(uri);
			ArrayList<Object> parameters = new ArrayList<Object>();
			// try with String[] agrs
			parameters.add(new String[1]);
			List<Method> methods = codeExecutor.findCompatibleMethodsWithAnnotation(caller, parameters, fr.inria.diverse.k3.al.annotationprocessor.InitializeModel.class);
			// try with List<String>
			parameters.clear();
			parameters.add(new ArrayList<String>());
			methods.addAll(codeExecutor.findCompatibleMethodsWithAnnotation(caller, parameters, fr.inria.diverse.k3.al.annotationprocessor.InitializeModel.class));
			// try with EList<String>
			parameters.clear();
			parameters.add(new BasicEList<String>());
			methods.addAll(codeExecutor.findCompatibleMethodsWithAnnotation(caller, parameters, fr.inria.diverse.k3.al.annotationprocessor.InitializeModel.class));
			
			if(!methods.isEmpty()){
			
				Method selectedMethod = methods.get(0);
				return selectedMethod.getDeclaringClass().getCanonicalName()+"."+selectedMethod.getName();
				
			}
		} catch (CoreException e) {
			Activator.warn(e.getMessage(), e);
		}
		return "";
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.debug.ui.AbstractLaunchConfigurationTab#isValid(org.eclipse.debug.core.ILaunchConfiguration)
	 */
	@Override
	public boolean isValid(ILaunchConfiguration config) {
		setErrorMessage(null);
		setMessage(null);
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		String modelName = _modelLocationText.getText().trim();
		if (modelName.length() > 0) {
			
			IResource modelIResource = workspace.getRoot().findMember(modelName);
			if (modelIResource == null || !modelIResource.exists()) {
				setErrorMessage(NLS.bind(LauncherMessages.ConcurrentMainTab_model_doesnt_exist, new String[] {modelName})); 
				return false;
			}
			if (modelName.equals("/")) {
				setErrorMessage(LauncherMessages.ConcurrentMainTab_Model_not_specified); 
				return false;
			}
			if (! (modelIResource instanceof IFile)) {
				setErrorMessage(NLS.bind(LauncherMessages.ConcurrentMainTab_invalid_model_file, new String[] {modelName})); 
				return false;
			}
		}
		if (modelName.length() == 0) {
			setErrorMessage(LauncherMessages.ConcurrentMainTab_Model_not_specified); 
			return false;
		}
		
		String languageName = _languageCombo.getText().trim();
		if (languageName.length() == 0) {
			setErrorMessage(LauncherMessages.ConcurrentMainTab_Language_not_specified); 
			return false;
		}
		
		String compilerMode = _compilerMode.getText().trim();
		if (compilerMode.length() == 0) {
			setErrorMessage(LauncherMessages.ConcurrentMainTab_Invalid_compiler_mode); 
			return false;
		}
		
		String resultProjectName = _resultingProjectNameText.getText().trim();
		if (resultProjectName.length() == 0) {
			setErrorMessage(LauncherMessages.ConcurrentMainTab_Invalid_result_project_name); 
			return false;
		}

		return true;
	}
}
