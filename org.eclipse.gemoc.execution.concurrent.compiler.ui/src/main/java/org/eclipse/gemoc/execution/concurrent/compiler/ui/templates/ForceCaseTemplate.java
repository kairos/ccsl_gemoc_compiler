package org.eclipse.gemoc.execution.concurrent.compiler.ui.templates;

public class ForceCaseTemplate {
	static public String content = "" +
			"import org.eclipse.gemoc.moccml.compiled.library.Clock;\n" + 
			"\n"
			+ "public class ForceCase{\n" + 
			"	public Clock clockToForce;\n" + 
			"	public Boolean associatedResValue; //too restrictive, I know\n" + 
			"	public ForceCase(Clock c, boolean resValue) {\n" + 
			"		clockToForce = c;\n" + 
			"		associatedResValue = resValue;\n" + 
			"	}\n" + 
			"}";	
}
