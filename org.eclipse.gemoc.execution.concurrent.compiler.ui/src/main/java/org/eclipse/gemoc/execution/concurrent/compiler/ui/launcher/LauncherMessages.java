/*******************************************************************************
 *  Copyright (c) 2000, 2017 INRIA and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *  IBM - Initial API and implementation
 *  BEA - Daniel R Somerfield - Bug 88939
 *  Remy Chi Jian Suen - Bug 221973
 *******************************************************************************/
package org.eclipse.gemoc.execution.concurrent.compiler.ui.launcher;

import org.eclipse.gemoc.execution.concurrent.compiler.ui.Activator;
import org.eclipse.osgi.util.NLS;

public class LauncherMessages extends NLS {
	private static final String BUNDLE_NAME = Activator.PLUGIN_ID+".launcher.LauncherMessages";//$NON-NLS-1$

	

	public static String ConcurrentMainTab_Model_not_specified = "model not specified";
	public static String ConcurrentMainTab_invalid_model_file = "model invalid file";
	public static String ConcurrentMainTab_model_doesnt_exist = "model does not exist";
	

	public static String ConcurrentMainTab_Language_not_specified = "Language_not_specified";
	public static String ConcurrentMainTab_incompatible_model_extension_for_language = "incompatible_model_extension_for_language";
	public static String ConcurrentMainTab_missing_language = "please select the semantics to be used for compilation (choose a language)";
	public static String ConcurrentMainTab_Invalid_Language_missing_xdsml = "language is invalid (dsl file is missing)";
	public static String ConcurrentMainTab_Invalid_Language_missing_xdsml_with_error = "language is invalid (dsl file has error)";
	public static String ConcurrentMainTab_Invalid_compiler_mode = "please choose a compiler mode in the list" ;
	public static String ConcurrentMainTab_Invalid_result_project_name = "please fill the result project name";



	static {
		// load message values from bundle file
		NLS.initializeMessages(BUNDLE_NAME, LauncherMessages.class);
	}


}
