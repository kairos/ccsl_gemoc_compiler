//============================================================================
// Name        : main.cpp
// Author      : Julien Deantoni
// Version     : the best of ones I had :)
// Copyright   : http://i3s.unice.fr/~deantoni
// Description : Code generated for your CCSL specification, modern C++ style
//============================================================================
#include "GeneratedSystem.h"

#include <avr/io.h>
#include <util/delay.h>
const uint8_t led   = _BV(PC7);
const uint8_t led3   = _BV(PC6);
void task1(){
    DDRC |= led;
    PORTC |= led;
    PORTC &= ~led3;
}


void task2(){
    DDRC |= led3;
    PORTC |= led3;
    PORTC &= ~led;
}


void setupClockFunction(){
    MySpec_main_c1.assignFunction(task1);
    MySpec_main_c2.assignFunction(task2);
}