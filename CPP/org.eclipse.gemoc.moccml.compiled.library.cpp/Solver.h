//
// Created by jdeanton on 2/20/19.
//

#ifndef SRC_SOLVER_H
#define SRC_SOLVER_H

#include <vector>
#if not (defined (__AVR__))
    #include <iostream>
#endif
#include <set>
#include "Clock.h"
#include "constraints/Constraint.h"

class Solver {

public:
    std::vector<Clock *> clocks;
    std::set<Constraint *> constraintsButExclusions;
    std::set<Constraint *> exclusions;
    std::set<Constraint * > allConstraints;

    Solver(std::vector<Clock *> allClocks, std::set<Constraint *> allConstraintsButExclusions, std::set<Constraint *> allExclusions);

    virtual void solve();
    virtual void simulate(const unsigned int nbSteps= 200, bool logSteps = true, bool logClocks = true, bool hideExpressionsInLog = true);
    virtual void propagatesChoice();
    virtual void propagatesDeath();

    virtual ~Solver() = default;

#if not (defined (__AVR__))
    void logClockToCout(bool doNotLogExpressions) const;
#endif
    void doStep();
};


#endif //SRC_SOLVER_H
