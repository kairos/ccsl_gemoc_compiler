//
// Created by jdeanton on 4/27/20.
//

#ifndef COMPILEDCCSL_GENERATEDSYSTEM_H
#define COMPILEDCCSL_GENERATEDSYSTEM_H

#include "CCSLUtils.h"
#include "ClockDefinitions.h"

void run();
void setupClockFunction();

#endif //COMPILEDCCSL_GENERATEDSYSTEM_H
