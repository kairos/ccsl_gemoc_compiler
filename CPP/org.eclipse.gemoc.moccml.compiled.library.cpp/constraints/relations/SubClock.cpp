//
// Created by jdeanton on 4/19/20.
//

#include "SubClock.h"

SubClock::SubClock(Clock& l, Clock& r, std::string name)
        :Relation(MOVE(name)), left(l), right(r)
{
}

bool SubClock::evaluate() {
    return false;
}

void SubClock::rewrite() {
}

bool SubClock::propagatesChoice() {
//		System.out.println("Sublock::propagatesChoice() "+ this+ "c1 = "+c1.status+" and c2 is "+c2.status);
    if (right.status == POSSIBLY && left.status == POSSIBLY){
        return true;
    }

    if (right.status == FALSE && left.status == FALSE){
        return true;
    }
    if (right.status == TRUE && left.status == FALSE){
        return true;
    }

    if (right.status == TRUE && left.status == POSSIBLY){
        return true;
    }

    if (right.status == TRUE && left.status == TRUE){
        return true;
    }
    if (right.status == POSSIBLY && left.status == FALSE){
        return true;
    }

    if (right.status == FALSE){
        if (left.status == POSSIBLY){
            left.status = FALSE;
            return false;
        }
    }
    if (left.status == TRUE){
        if (right.status == POSSIBLY){
            right.status = TRUE;
            return false;
        }
    }
    //backtrack
    if (left.status == TRUE){
#if not (defined (__AVR__))
        std::cout << "Backtrack since c1 is " << left.status+" and c2 is " <<right.status<< std::endl;
#endif
        if (right.status == FALSE){
            left.status = FALSE;
            return false;
        }
    }


    exit(-15);
    return true; //never propagates anything...
}

bool SubClock::propagatesDeath() {
    if (right.isDead && !left.isDead){
        left.isDead = true;
        return false;
    }
    return true;
}

void SubClock::reset() {
}

#if not (defined (__AVR__))
    ostream& SubClock::toStream(ostream& os) const {
        os << /*name <<*/ "(" << left.name << " " << u8"\u2286" << " " << right.name << ")";
        return os;
    }
#endif