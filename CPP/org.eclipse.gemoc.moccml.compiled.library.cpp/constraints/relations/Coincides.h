//
// Created by jdeanton on 2/14/19.
//
#ifndef COINCIDES_H
#define COINCIDES_H

#include "../../Clock.h"
#include "Relation.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include <string>

class Coincides: public Relation{
	
public:
    Clock& c1;
    Clock& c2;

    Coincides(Clock& l, Clock& r, std::string name = "defaultCoincidesName");
    bool evaluate() override;
    void rewrite() override;
    bool propagatesChoice() override;
    bool propagatesDeath() override;
	void reset() override ;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
};

#endif