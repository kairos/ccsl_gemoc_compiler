//
// Created by jdeanton on 2/14/19.
//
#ifndef CAUSES_H
#define CAUSES_H

#include "../../Clock.h"
#include "Relation.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include <string>

class Causes: public Relation{
	
public:
    int delta = 0;
    Clock& left;
    Clock& right;

    Causes(Clock& l, Clock& r, std::string name= "defaultCausesName"): Relation(MOVE(name)), left(l), right(r){
    }
    bool evaluate() override;
    void rewrite() override;
    /**
     *
     * @return true is stability is reached, false otherwise
     */
    bool propagatesChoice() override;
    /**
     *
     * @return true is stability is reached, false otherwise
     */
    bool propagatesDeath() override;
	void reset() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
};
#endif