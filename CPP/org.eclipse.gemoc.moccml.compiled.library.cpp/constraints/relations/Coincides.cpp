//
// Created by jdeanton on 4/19/20.
//

#include "Coincides.h"


Coincides::Coincides(Clock& l, Clock& r, std::string name) : Relation(MOVE(name)), c1(l), c2(r){
}

bool Coincides::evaluate() {
    return false;
}

void Coincides::rewrite() {
    return;
}

bool Coincides::propagatesChoice() {
    if (c1.status == c2.status){
        return true;
    }

    if (c1.status == POSSIBLY && c2.status != POSSIBLY){
        c1.status = c2.status;
        return false;
    }

    if (c1.status != POSSIBLY && c2.status == POSSIBLY){
        c2.status = c1.status;
        return false;
    }

    //there is a conflict, propagates FALSE
    if(c1.status != POSSIBLY && c2.status != POSSIBLY) {
        c1.status = FALSE;
        c2.status = FALSE;
//  		    std::clog << "warning, back track !");
        return false;
    }

    exit(-12);
    return true; //never reached
}

bool Coincides::propagatesDeath() {
    if (c1.isDead && !c2.isDead){
        c2.isDead = true;
        return false;
    }
    if (c2.isDead && !c1.isDead){
        c1.isDead = true;
        return false;
    }
    return true;
}

void Coincides::reset() {
}

#if not (defined (__AVR__))
    ostream& Coincides::toStream(ostream& os) const {
        os << /*name <<*/ "(" << c1.name << u8" \uFF1D " << c2.name << ")";
        return os;
    }
#endif