//
// Created by jdeanton on 4/19/20.
//

#include "Causes.h"

bool Causes::evaluate() {
    canBeRewrote = true;
    return false;
}

void Causes::rewrite() {
    if (!canBeRewrote) return;

    if (left.status == TRUE){
        delta++;
    }
    if(right.status == TRUE){
        delta--;
    }
    canBeRewrote = false;
}

bool Causes::propagatesChoice() {
    if (delta == 0){
        if (left.status == FALSE && right.status == POSSIBLY){
            right.status = FALSE;
            return false;
        };
        if (left.status == POSSIBLY && right.status == TRUE){
            left.status = TRUE;
            return false;
        };
    }
    return true; //never propagates anything...
}

bool Causes::propagatesDeath() {
    if (left.isDead && !right.isDead && delta == 0){
        right.isDead = true;
        return false;
    }
    return true;
}

void Causes::reset() {
    delta = 0;
}

#if not (defined (__AVR__))
    ostream& Causes::toStream(ostream& os) const {
        os << /*name <<*/ "(" << left.name << " " << u8"\u2264" << " " << right.name << ")";
        return os;
    }
#endif