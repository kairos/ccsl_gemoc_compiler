//
// Created by jdeanton on 4/19/20.
//

#include "Precedes.h"

bool Precedes::evaluate() {
    if (!isActive) return false;

    canBeRewrote = true;
    if (delta == 0){
        right.status = FALSE;
    }
    return false;
}

void Precedes::rewrite() {
    if (!canBeRewrote) return;

    if (left.status == TRUE){
        delta++;
    }
    if(right.status == TRUE){
        delta--;
    }
    canBeRewrote = false;
}

bool Precedes::propagatesChoice() {
    return true; //never propagates anything...
}

bool Precedes::propagatesDeath() {
    if (left.isDead && !right.isDead && delta == 0){
        right.isDead = true;
        return false;
    }
    return true;
}

void Precedes::reset() {
}

Precedes::Precedes(Clock& l, Clock& r, std::string name)
        : Relation(MOVE(name)), left(l), right(r){}

#if not (defined (__AVR__))
    ostream& Precedes::toStream(ostream& os) const {
        os << /*name <<*/ "(" << left.name << " < " << right.name << ")";
        return os;
    }
#endif
