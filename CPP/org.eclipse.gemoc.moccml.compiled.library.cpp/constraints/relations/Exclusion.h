//
// Created by jdeanton on 5/28/17.
//
#ifndef EXCLUSION_H
#define EXCLUSION_H

#include "../../Clock.h"
#include "Relation.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include <string>

class Exclusion: public Relation{
	
public:
    Clock& c1;
	Clock& c2;

    Exclusion(Clock& l, Clock& r, std::string name = "ExclusionDefaultName");

	bool evaluate() override;
	void rewrite() override;
	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	bool propagatesChoice() override;
	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	bool propagatesDeath() override;
	void reset() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif

};

#endif