/*
 * PrecedesRelation.h
 *
 *  Created on: Aug 17, 2016
 *      Author: jdeanton
 */
#ifndef PRECEDES_H
#define PRECEDES_H

#include "../../Clock.h"
#include "Relation.h"
#include "../Constraint.h"
#include "../../utils/QuantumBoolean.h"

#include <string>
#include <iostream>

class Precedes: public Relation{
	
public:
    int delta = 0;
    Clock& left;
    Clock& right;

	Precedes(Clock& l, Clock& r, std::string name = "defaultPrecedesName");

	bool evaluate() override;
	void rewrite() override;
	bool propagatesChoice() override ;
	bool propagatesDeath() override ;
	void reset() override ;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
};

#endif