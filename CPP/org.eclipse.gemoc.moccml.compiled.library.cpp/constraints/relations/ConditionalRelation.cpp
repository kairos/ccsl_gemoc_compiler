//
// Created by jdeanton on 4/20/20.
//

#include "ConditionalRelation.h"

ConditionalRelation::ConditionalRelation(Relation& trueCase) : root(&trueCase) {
}

ConditionalRelation::ConditionalRelation() = default;

void ConditionalRelation::connect(Relation& trueCase) {
    root = &trueCase;
}

#if not (defined (__AVR__))
    std::ostream& ConditionalRelation::toStream(ostream& os) const {
        os << "Cond: " << root;
        return os;
    }
#endif

void ConditionalRelation::reset() {
    root->reset();
}

bool ConditionalRelation::evaluate() {
    canBeRewrote = true;
    return false;
}

bool ConditionalRelation::propagatesChoice() {
    return root->propagatesChoice();
}

void ConditionalRelation::rewrite() {
    root->rewrite();
    canBeRewrote = false;
}

bool ConditionalRelation::propagatesDeath() {
    return root->propagatesDeath();
}
