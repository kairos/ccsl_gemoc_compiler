//
// Created by jdeanton on 4/19/20.
//

#include "Exclusion.h"

Exclusion::Exclusion(Clock& l, Clock& r, std::string name) : Relation(MOVE(name)), c1(l), c2(r){
}

bool Exclusion::evaluate() {
    return false;
}

void Exclusion::rewrite() {
}

bool Exclusion::propagatesChoice() {
//    	    std::cout  <<  "Exclusion::propagatesChoice "  <<  " c1 = " <<  c1.status  <<  " c2 = "  <<  c2.status  <<  std::endl;

    if(  (c1.status == TRUE && c2.status == FALSE)
         ||(c1.status == FALSE && c2.status == TRUE)
         ||(c1.status == POSSIBLY && c2.status == POSSIBLY)
         ||(c1.status == POSSIBLY && c2.status == FALSE)
         ||(c1.status == FALSE && c2.status == POSSIBLY)
         ||(c1.status == FALSE && c2.status == FALSE)
            ){
        return true;
    }

    if (c1.status == TRUE && c2.status == POSSIBLY){
//    	        std::cout  <<  c2  <<  std::endl;
//    	        assert(c2.status != TRUE);
        c2.status = FALSE;
        return false;
    }
    if (c2.status == TRUE && c1.status == POSSIBLY){
//    	        assert(c1.status != TRUE);
        c1.status = FALSE;
        return false;
    }

    //backtrack
    if (c2.status == TRUE && c1.status == TRUE){

        exit(-13);
        return false;
    }


    exit(-14);
    return true;
}

bool Exclusion::propagatesDeath() {
    return true;
}

void Exclusion::reset() {
}

#if not (defined (__AVR__))
    ostream& Exclusion::toStream(ostream& os) const {
        os << /*name <<*/ "(" << c1.name << " # " << c2.name << ")";
        return os;
    }
#endif