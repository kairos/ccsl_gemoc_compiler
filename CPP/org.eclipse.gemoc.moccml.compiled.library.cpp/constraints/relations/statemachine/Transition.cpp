//
// Created by jdeanton on 4/20/20.
//

#include "Transition.h"
#include "State.h"

Transition::Transition(State& src, State& tgt) :source(&src), target(&tgt) {
}

std::ostream& Transition::toStream(std::ostream& os) const {
    os << "Transition from "<< source->name << " to " << target->name;
    return os;
}
