#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "../Relation.h"
#include "State.h"
#include "vector"
#include <algorithm>
#include <set>

class StateMachine: Relation {

public:
    std::vector<State> states;
	std::vector<Clock*> allClocks;
	State* currentState = nullptr;
	State* initialState = nullptr;
	std::vector<int> vars;
	std::vector<Transition*> guardEnabledTrantions;
    std::set<Transition*> fireableTransition;



	void reset() override;
	bool evaluate() override;
	void dealWithInitialTransition();
	bool propagatesChoice() override;
	void rewrite() override;
	bool propagatesDeath() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
};
#endif
