#ifndef TRANSITION_H
#define TRANSITION_H

#include <vector>
#include "../../../Clock.h"
#include "../../../classicalexpressions/BooleanExpression.h"
#include "../../../classicalexpressions/IntegerExpression.h"

class State;

class Transition {

public:
    std::vector<Clock*> clocks;
	BooleanExpression* guard = nullptr;
	std::vector<IntegerExpression*> actions;
	State* source;
	State* target;
	
	Transition(State& src, State& tgt);

    std::ostream& toStream(std::ostream& os) const;
};
#endif
