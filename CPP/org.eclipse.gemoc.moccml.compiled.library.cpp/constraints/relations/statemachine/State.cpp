//
// Created by jdeanton on 4/20/20.
//

#include "State.h"

State::State(std::string n) :NamedElement(MOVE(n)) {
}

#if not (defined (__AVR__))
    std::ostream& State::toStream(ostream& os) const {
        os << "State " << name;
        return os;
    }
#endif
