//
// Created by jdeanton on 4/20/20.
//

#include "StateMachine.h"

#if (defined (__AVR__))
namespace std {
    template<class InputIt, class UnaryPredicate>
    constexpr InputIt find_if_not(InputIt first, InputIt last, UnaryPredicate q) {
        for (; first != last; ++first) {
            if (!q(*first)) {
                return first;
            }
        }
        return last;
    }

    template<class InputIt, class UnaryPredicate>
    constexpr bool all_of(InputIt first, InputIt last, UnaryPredicate p) {
        return find_if_not(first, last, p) == last;
    }

    template<class InputIt, class UnaryPredicate>
    constexpr bool any_of(InputIt first, InputIt last, UnaryPredicate p) {
        return std::find_if(first, last, p) != last;
    }
}
#endif

void StateMachine::reset() {
    currentState = initialState;
}

bool StateMachine::evaluate() {
    dealWithInitialTransition();
    guardEnabledTrantions.clear();
    for(Transition* t : currentState->outgoingTransitions) {
        if (t->guard == nullptr || t->guard->evaluate()) {
            guardEnabledTrantions.push_back(t);
        }else {
//				if(t.guard != null) {System.out.print("FALSE:"+ t.guard.prettyPrint()+ "when "); t.clocks.stream().forEach(c -> System.out.print(c.name+";")); System.out.println("");}
        }
    }

    std::vector<Clock*> usedClock;
    for(Transition* t : guardEnabledTrantions) {
        usedClock.insert(usedClock.end(), t->clocks.begin(), t->clocks.end());
    }

    //put false all clocks unused by the transitions
    std::vector<Clock*> falseClocks = {allClocks};
    for (Clock* c: usedClock){
        auto posC = std::find(falseClocks.begin(), falseClocks.end(), c);
        if (posC != falseClocks.end()){
            falseClocks.erase(posC);
        }
    }
    for(Clock* c : falseClocks) {
        c->status = FALSE;
    }

    fireableTransition.clear();
    return false;
}

void StateMachine::dealWithInitialTransition() {
    for(Transition* t : currentState->outgoingTransitions) {
        if (t->guard == nullptr && t->clocks.empty()) {
            for(IntegerExpression* expr: t->actions) {
                expr->evaluate();
            }
            currentState = t->target;
            return;
        }
    }
}

bool StateMachine::propagatesChoice() {
    bool isStable = true;
    std::set<Transition*> tempFireableTransition = {fireableTransition};
    for(Transition* t : tempFireableTransition) {
        //changed occurs so check if correct
        if (!std::all_of(t->clocks.begin(), t->clocks.end(), [](Clock* c){return c->status == TRUE;}))
        { //not enabled anymore but not disabled completely
            fireableTransition.erase(t);
//            std::cout << "old enabled disabled " << t->source->name << " -> " << t->target->name <<"(" << fireableTransition.size() <<")" << std::endl;
            std::for_each(t->clocks.begin(), t->clocks.end(), [](Clock* c){c->status = FALSE;});
            isStable = false;
        }
    }

    for(Transition* t : guardEnabledTrantions) {
        if (std::all_of(t->clocks.begin(), t->clocks.end(), [](Clock* c){return c->status == TRUE;})) {
            if(fireableTransition.insert(t).second) { //if not already there
//					System.out.println("new fireable "+t.source.name+" -> "+t.target.name);
                //here we directly disallow all others
                std::vector<Clock*> clockToDisallow = {allClocks};
                //do in c++ clockToDisallow.removeAll(t.clocks);
                for (Clock* c: t->clocks){
                    auto posC = std::find(clockToDisallow.begin(), clockToDisallow.end(), c);
                    if (posC != clockToDisallow.end()){
                        clockToDisallow.erase(posC);
                    }
                }
                for(Clock* c : clockToDisallow) {
                    if (c->status != FALSE) {
                        c->status = FALSE;
                    }
                }
                isStable = false;
            }

        }
    }

    //check all mixed "FALSE" and "TRUE"

    if(fireableTransition.empty()
       && (!std::any_of(allClocks.begin(), allClocks.end(), [](Clock* c){return c->status == POSSIBLY;}))
       && (!std::all_of(allClocks.begin(), allClocks.end(), [](Clock* c){ return c->status == FALSE;})))
    {
        isStable = false;
        std::for_each(allClocks.begin(), allClocks.end(), [](Clock* c){c->status = FALSE;});
    }
    return isStable;
}

void StateMachine::rewrite() {
    for(Transition* t : fireableTransition) {
        for(IntegerExpression* expr : t->actions) {
            expr->evaluate();
        }
        currentState = t->target;
    }
}

bool StateMachine::propagatesDeath() {
    //TODO ?
    return true;
}

#if not (defined (__AVR__))
    std::ostream& StateMachine::toStream(ostream& os) const {
        os << "StateMachine prettyprinter todo...";
        return os;
    }
#endif
