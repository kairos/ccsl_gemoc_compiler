#ifndef STATE_H
#define STATE_H

#include "../../../utils/NamedElement.h"
#include "Transition.h"
#include <vector>

class State: public NamedElement {
public:
	std::vector<Transition*> outgoingTransitions;

    explicit State(std::string n = "DefaultStateName");

#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override ;
#endif
    ~State() = default;
};
#endif