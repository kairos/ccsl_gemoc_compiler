#ifndef _RELATION_H
#define _RELATION_H

#include "../Constraint.h"
#include <string>

class Relation : public Constraint {
public:
    Relation(std::string name = "DefaultRelationName"):Constraint(MOVE(name)){};
};

#endif
