//
// Created by jdeanton on 5/24/17.
//
#ifndef SUBCLOCK_H
#define SUBCLOCK_H

#include "../../Clock.h"
#include "Relation.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include <string>


class SubClock : public Relation{
	
public:
    Clock& left;
	Clock& right;

    SubClock(Clock& l, Clock& r, std::string name = "DefaultSubClockName");
	bool evaluate() override ;
	void rewrite() override ;
	/**
	 *
	 * @return true if stability is reached, false otherwise
	 */
	bool propagatesChoice() override ;
	/**
	 *
	 * @return true is stability is reached, false otherwise
	 */
	bool propagatesDeath() override;
	void reset() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
};

#endif