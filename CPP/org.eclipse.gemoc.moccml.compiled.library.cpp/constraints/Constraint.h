//
// Created by jdeanton on 5/24/17.
//

#ifndef SRC_RELATION_H
#define SRC_RELATION_H

#include "../utils/NamedElement.h"

#if not (defined (__AVR__))
    class Constraint;
    ostream& operator<<(ostream& os, Constraint& c);
#endif

class Constraint : public NamedElement{
public:
    bool isActive = true;
    bool canBeRewrote = false;
    explicit Constraint(std::string name = "DefaultConstraintName"):NamedElement(MOVE(name)){};

    virtual  void reset();
    virtual  bool evaluate()=0;
    virtual  bool propagatesChoice()=0;
    virtual  void rewrite()=0;
    virtual  bool propagatesDeath()=0;

#if not (defined (__AVR__))
    virtual ostream& toStream(ostream& os) const = 0;
#endif
    virtual ~Constraint() = default;
};


#endif //SRC_RELATION_H
