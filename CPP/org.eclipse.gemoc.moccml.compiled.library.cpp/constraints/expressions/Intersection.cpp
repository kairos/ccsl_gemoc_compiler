//
// Created by jdeanton on 4/20/20.
//

#include "Intersection.h"

Intersection::Intersection(Clock c11, Clock c22, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)),c1(&c11), c2(&c22){
}

Intersection::Intersection(std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)){
}

void Intersection::connect(Clock& c11, Clock& c22) {
    c1 = &c11;
    c2 = &c22;
}

bool Intersection::evaluate() {
    canBeRewrote = true;
    if(isDead){
        status = FALSE;
        return false;
    }
    return false;
}

void Intersection::rewrite() {
}

bool Intersection::propagatesChoice() {
    if (status == TRUE && (c1->status == TRUE && c2->status == TRUE)) {
        return true;
    }
    if (status == FALSE && (c1->status == FALSE || c2->status == FALSE)) {
        return true;
    }
    if (status == POSSIBLY
        && (c1->status == POSSIBLY  || c2->status == POSSIBLY)) {
        return true;
    }

    if (status == FALSE && (c1->status == POSSIBLY && c2->status == POSSIBLY)) {
        return true;
    }

    //if here, something must be done

    if (c1->status == TRUE  && c2->status == TRUE){
        status = TRUE;
        return false;
    }
    if (c1->status == FALSE || c2->status == FALSE) {
        status = FALSE;
        return false;
    }
    if(status == TRUE){
        assert(c1->status != FALSE);
        c1->status = TRUE;
        assert(c2->status != FALSE);
        c2->status = TRUE;
        return false;
    }

    if (status == FALSE && (c1->status == TRUE && c2->status == POSSIBLY)) {
        c2->status = FALSE;
        return false;
    }
    if (status == FALSE && (c1->status == POSSIBLY && c2->status == TRUE)) {
        c1->status = FALSE;
        return false;
    }



    exit(-5);
    return true;
}

void Intersection::reset() {
    Constraint::reset();
    Clock::reset();
}

bool Intersection::propagatesDeath() {
    if ((c1->isDead || c2->isDead) && !isDead){
        isDead = true;
        return false;
    }
    return true;
}

#if not (defined (__AVR__))
    ostream& Intersection::toStream(ostream& os) const {
        Clock::toStream(os);
        os << "= (" << c1->name << u8" \u2229 " << c2->name << ")";
        return os;
    }
#endif