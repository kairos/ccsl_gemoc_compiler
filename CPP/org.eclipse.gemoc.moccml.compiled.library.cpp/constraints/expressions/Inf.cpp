//
// Created by jdeanton on 4/20/20.
//

#include "Inf.h"

Inf::Inf(Clock lc, Clock rc, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)), leftClock(&lc), rightClock(&rc){
}

Inf::Inf(std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)){
}

void Inf::connect(Clock& lc, Clock& rc) {
    leftClock = &lc;
    rightClock = &rc;
}

void Inf::reset() {
    Constraint::reset();
    Clock::reset();
    delta = 0;
}

bool Inf::evaluate() {
    canBeRewrote = true;
    if(isDead){
        status = FALSE;
        return false;
    }
    return false;
}

void Inf::rewrite() {
    if (!canBeRewrote) return;

    if(leftClock->status == TRUE){
        delta--;
    }
    if(rightClock->status == TRUE){
        delta++;
    }
    canBeRewrote = false;
    return;
}

bool Inf::propagatesChoice() {
    if(isDead){
        return true;
    }

    if (status == POSSIBLY && (leftClock->status == POSSIBLY && rightClock->status == POSSIBLY)){
        return true;
    }

    if( delta < 0 && status == leftClock->status){
        return true;
    }

    if( delta > 0 && status == rightClock->status){
        return true;
    }

    if (delta == 0 && status != POSSIBLY && (leftClock->status == POSSIBLY && rightClock->status == POSSIBLY)){
        return true;
    }

    if( delta == 0
        && ((status == TRUE && (leftClock->status == TRUE || rightClock->status == TRUE))
            || (status == FALSE && (leftClock->status == FALSE && rightClock->status == FALSE))
            || (status == POSSIBLY && (leftClock->status == POSSIBLY && rightClock->status == FALSE))
            || (status == POSSIBLY && (leftClock->status == FALSE && rightClock->status == POSSIBLY)))
            ){
        return true;
    }

    if( delta < 0 && leftClock->status != POSSIBLY){
        status = leftClock->status;
        return false;
    }

    if( delta > 0 && rightClock->status != POSSIBLY){
        status = rightClock->status;
        return false;
    }

    if( delta < 0 && status != POSSIBLY){
        leftClock->status = status;
        return false;
    }

    if( delta > 0 && status != POSSIBLY){
        rightClock->status = status;
        return false;
    }

    if( delta == 0 && (status != POSSIBLY)){
        if (rightClock->status != POSSIBLY){
            leftClock->status = status;
            return false;
        }
        if (leftClock->status != POSSIBLY){
            rightClock->status = status;
            return false;
        }
        return false;
    }

    if( delta == 0 && (rightClock->status == TRUE || leftClock->status == TRUE)){
        status = TRUE;
        return false;
    }

    if( delta == 0 && rightClock->status == FALSE && leftClock->status == FALSE){
        status = FALSE;
        return false;
    }

    exit(-4);
    return true;
}

bool Inf::propagatesDeath() {
    if( delta > 0 && rightClock->isDead && !isDead){
        isDead = true;
        return false;
    }

    if( delta < 0 && leftClock->isDead && !isDead){
        isDead = true;
        return false;
    }

    if( delta == 0 && leftClock->isDead && rightClock->isDead && !isDead){
        isDead = true;
        return false;
    }

    return true;
}

#if not (defined (__AVR__))
    ostream& Inf::toStream(ostream& os) const {
        Clock::toStream(os);
        os << "= (" << leftClock->name << u8" \u2227 " << rightClock->name << ")";
        return os;
    }
#endif
