/*
 * UnionExpression.h
 *
 *  Created on: Aug 19, 2016
 *      Author: jdeanton
 */
#ifndef UNION_H
#define UNION_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include "../../classicalexpressions/IntegerExpression.h"
#include <string>


class Union: public Clock, public Constraint {
	
public:
    Clock* c1 = nullptr;
	Clock* c2 = nullptr;

	Union(Clock& c11, Clock& c22, std::string name = "defaultUnionName");
	explicit Union( std::string name = "defaultUnionName");

	void connect(Clock& c11, Clock& c22);
	bool evaluate() override;
	void rewrite() override;
	bool propagatesChoice() override;
	void reset() override;
	bool propagatesDeath() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
};

#endif