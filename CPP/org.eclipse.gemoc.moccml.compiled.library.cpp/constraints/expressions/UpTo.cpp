//
// Created by jdeanton on 4/20/20.
//

#include "UpTo.h"


UpTo::UpTo(Clock& toFollow, Clock& theKiller, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)),clockToFollow(&toFollow), killer(&theKiller) {
}

UpTo::UpTo(std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)){
}

void UpTo::connect(Clock& toFollow, Clock& theKiller) {
    clockToFollow = &toFollow;
    killer = &theKiller;
}

void UpTo::reset() {
    Constraint::reset();
    Clock::reset();
}

bool UpTo::evaluate() {
    canBeRewrote = true;
    if(isDead){
        status = FALSE;
        return false;
    }
    return false;
}

void UpTo::rewrite() {
    if (!canBeRewrote) return;

    if(killer->status == TRUE){
        isDead = true;
    }
    canBeRewrote = false;
    return;
}

bool UpTo::propagatesChoice() {
    if(isDead){
        return true;
    }

    if( status == clockToFollow->status && killer->status != TRUE){
        return true;
    }

    if( status == FALSE && killer->status == TRUE){
        return true;
    }

    if( status == POSSIBLY && clockToFollow->status == POSSIBLY && killer->status != TRUE){
        return true;
    }

    if( status == POSSIBLY && clockToFollow->status != POSSIBLY && killer->status != TRUE){
        status = clockToFollow->status;
        return false;
    }

    if( status == FALSE && clockToFollow->status == POSSIBLY && killer->status == FALSE){
        clockToFollow->status = FALSE;
        return false;
    }

    if( status == FALSE && clockToFollow->status == POSSIBLY && killer->status == POSSIBLY){
        clockToFollow->status = FALSE;
        return false;
    }

    if( killer->status == TRUE){
        status = FALSE;
        return false;
    }


    exit(-9);
    return true;
}

bool UpTo::propagatesDeath() {
    if (clockToFollow->isDead && !isDead){
        isDead = true;
        return false;
    }
    return true;
}

#if not (defined (__AVR__))
    ostream& UpTo::toStream(ostream& os) const {
        Clock::toStream(os);
        os << /*name <<*/ "= (" << clockToFollow->name << u8" \u21af " << killer->name << ")";
        return os;
    }
#endif