#ifndef CONDITIONAL_EXPRESSION_H
#define CONDITIONAL_EXPRESSION_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include "../../classicalexpressions/IntegerExpression.h"
#include "../../classicalexpressions/BooleanExpression.h"
#include <string>


class Case{
public:
    Clock* constraint;
    BooleanExpression* test;

    Case(const Case& c) = default;
    Case(Clock& c, BooleanExpression& be): constraint(&c),test(&be){
    }
};


class ConditionalExpression: public Clock, public Constraint {

public:
	std::vector<Case*> allCases;
	Clock* root = nullptr;
	
    ConditionalExpression(std::vector<Case> cases, std::string name);
	explicit ConditionalExpression(std::string name);
	
	void connect(std::vector<Case> cases);
	void reset() override;
	bool evaluate() override;
	bool propagatesChoice() override;
	void rewrite() override;
	bool propagatesDeath() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
    ~ConditionalExpression();
private:
    void assignAllCasesAndRoot(std::vector<Case>& cases);
};
#endif
