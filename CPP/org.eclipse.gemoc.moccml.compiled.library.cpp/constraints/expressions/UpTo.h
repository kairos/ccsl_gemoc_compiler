//
// Created by jdeanton on 2/16/19.
//
#ifndef UPTO_H
#define UPTO_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include "../../classicalexpressions/IntegerExpression.h"
#include <string>

class UpTo: public Clock, public Constraint {

public:
    Clock* clockToFollow = nullptr;
	Clock* killer = nullptr;

	UpTo(Clock& toFollow, Clock& theKiller, std::string name = "defaultUpToName");
	UpTo(std::string name = "defaultUpToName");
	void connect(Clock& toFollow, Clock& theKiller);
	void reset() override;
	bool evaluate() override;
	void rewrite() override;
	bool propagatesChoice() override;
	bool propagatesDeath() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
};

#endif

