//
// Created by jdeanton on 2/16/19.
//
#ifndef INF_H
#define INF_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include "../../classicalexpressions/IntegerExpression.h"
#include <string>

class Inf: Clock, Constraint {
	
public:
    Clock* leftClock = nullptr;
	Clock*  rightClock = nullptr;
    int delta = 0;

    Inf(Clock lc, Clock rc, std::string name);
    Inf(std::string name);

    void connect(Clock& lc, Clock& rc);
	void reset() override;
	bool evaluate() override;
	void rewrite() override;
	bool propagatesChoice() override;
	bool propagatesDeath() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
};

#endif
