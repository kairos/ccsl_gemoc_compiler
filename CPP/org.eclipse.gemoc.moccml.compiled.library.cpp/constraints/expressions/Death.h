//
// Created by jdeanton on 2/16/19.
//
#ifndef DEATH_H
#define DEATH_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../utils/Sequence.h"
#include "../../classicalexpressions/IntegerExpression.h"
#include <string>

class Death: public Clock, public Constraint {
public:
    Death(std::string name);

	void reset() override;
	bool evaluate() override;
	void rewrite() override;
	bool propagatesChoice() override;
	bool propagatesDeath() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
};

#endif
