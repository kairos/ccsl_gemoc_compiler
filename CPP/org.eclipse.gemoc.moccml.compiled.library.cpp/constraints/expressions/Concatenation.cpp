//
// Created by jdeanton on 4/20/20.
//

#include "Concatenation.h"

Concatenation::Concatenation(Clock& lc, Clock& rc, std::string name, bool leftIsConstraint, bool rightIsConstraint, bool rightIsRecursiveCall)
        :Clock(MOVE(name)), Constraint(MOVE(name)), leftClock(&lc), rightClock(&rc), leftIsAConstraint(leftIsConstraint),
        rightIsAConstraint(rightIsConstraint),rightIsRecursiveCall(rightIsRecursiveCall)
{
    if (rightClock == this){
        isRecursive = true;
    }
}

Concatenation::Concatenation(std::string name)
        :Clock(MOVE(name)), Constraint(MOVE(name)) {
}

void Concatenation::connect(Clock& lc, Clock& rc, bool leftIsConstraint, bool rightIsConstraint, bool rightIsRecursiveCall) {
    leftClock = &lc;
    rightClock = &rc;
        leftIsAConstraint = leftIsConstraint;
        rightIsAConstraint = rightIsConstraint;
    if (rightClock == this){
        isRecursive = true;
    }

    this->rightIsRecursiveCall= rightIsRecursiveCall;
}

void Concatenation::connect(Clock& lc, Clock& rc, bool rightIsRecursiveCall, ClassicalExpression& recursionParam, bool leftIsConstraint, bool rightIsConstraint){
    connect(lc,rc, leftIsConstraint, rightIsConstraint,rightIsRecursiveCall);
    this->recursionExpression = &recursionParam;
}


bool Concatenation::evaluate() {
    canBeRewrote = true;
    if(isDead){
        status = FALSE;
        return false;
    }

    if(followLeft && rightIsAConstraint  && rightClock != leftClock  && !isRecursive){
        rightClock->status = FALSE;
    }
    return false;
}

void Concatenation::rewrite() {
    if (!canBeRewrote) return;
//		System.out.println("--> concat rewrite");
    if (leftClock->isDead && rightIsRecursiveCall && recursionExpression != nullptr) {
        leftClock->isDead = false;
#if not (defined (__AVR__))
        if(dynamic_cast<SequenceExpression*>(recursionExpression) != nullptr) {
            //TODO check this ugly stuff here
            if (dynamic_cast<Wait*>(leftClock) != nullptr) {
                IntegerExpression* ie = new SeqHead{((SequenceExpression*)recursionExpression)->evaluate()};
                if(ie->evaluate() == -1){
                    isDead = true;
                    leftClock->isDead = true;
                    canBeRewrote = false;
                    delete ie;
                    ie = nullptr;
                    return;
                }
                ((Wait*)leftClock)->setExpression(*ie);
                delete ie;
                ie = nullptr;
            }else {
                ((SequenceExpression*)recursionExpression)->evaluate();
            }
            }else {

            exit(-2);
        }
        leftClock->reset();

#else
            //TODO check this ugly stuff here
                IntegerExpression* ie = new SeqHead{((SequenceExpression*)recursionExpression)->evaluate()};
                if(ie->evaluate() == -1){
                    isDead = true;
                    leftClock->isDead = true;
                    canBeRewrote = false;
                    delete ie;
                    ie = nullptr;
                    return;
                }
                ((Wait*)leftClock)->setExpression(*ie);
                delete ie;
                ie = nullptr;
                leftClock->reset();

#endif

    }
    if (leftClock->isDead && isRecursive){
        leftClock->isDead = false; //should be in reset of leftClock
        leftClock->reset();

    }
    if (leftClock->isDead && rightIsRecursiveCall){
        leftClock->isDead = false;
        leftClock->reset();
    }
    if (leftClock->isDead && followLeft){
        followLeft = false;
        if(rightClock == leftClock){
            rightClock->reset();
        }
    }
    if (!followLeft && rightClock->isDead){
        isDead = true;
    }
    canBeRewrote = false;
}

bool Concatenation::propagatesChoice() {
    if(followLeft){ //follow left
        if (leftClock->status != POSSIBLY && status == POSSIBLY){
            status = leftClock->status;
            return false;
        }
        if (leftClock->status == POSSIBLY && status != POSSIBLY){
            leftClock->status = status;
            return false;
        }
        if (leftClock->status == status ){
            return true;
        }

        //there is a conflict, propagates FALSE
        if(leftClock->status != POSSIBLY &&  this->status != POSSIBLY) {
            leftClock->status = FALSE;
            this->status = FALSE;
            return false;
        }

    }else{ //follow right
        if (rightClock->status != POSSIBLY && status == POSSIBLY){
            status = rightClock->status;
            return false;
        }
        if (rightClock->status == POSSIBLY && status != POSSIBLY){
            rightClock->status = status;
            return false;
        }

        if (rightClock->status == status ){
            return true;
        }

        //there is a conflict, propagates FALSE
        if(rightClock->status != POSSIBLY &&  this->status != POSSIBLY) {
            rightClock->status = FALSE;
            this->status = FALSE;
            return false;
        }
    }



    exit(-2);
    return true;
}

void Concatenation::reset() {
    Constraint::reset();
    Clock::reset();
    followLeft = true;
}

bool Concatenation::propagatesDeath() {
    if (!followLeft && rightClock->isDead && !isDead){
        isDead = true;
        return false;
    }
    if (leftClock->isDead && rightClock->isDead && !isDead){
        isDead = true;
        return false;
    }
    return true;
}

#if not (defined (__AVR__))
    ostream& Concatenation::toStream(ostream& os) const {
        Clock::toStream(os);
        os << "= (" << leftClock->name << u8" \u25CF " << rightClock->name << ")";
        return os;
    }
#endif