#ifndef USER_DEFINED_EXPRESSION_H
#define USER_DEFINED_EXPRESSION_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include "../../classicalexpressions/IntegerExpression.h"
#include <string>

class UserDefinedExpression: public Clock, public Constraint {
public:
	Clock* root = nullptr;
	
	UserDefinedExpression(Clock rootExpression, std::string name);
	explicit UserDefinedExpression(std::string name);
	
	void connect(Clock& rootExpression);
	void reset() override;
	bool evaluate() override;
	bool propagatesChoice() override;
	void rewrite() override;
	bool propagatesDeath() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
};
#endif