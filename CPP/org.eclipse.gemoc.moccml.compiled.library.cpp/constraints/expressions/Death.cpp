//
// Created by jdeanton on 4/20/20.
//

#include "Death.h"

Death::Death(std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)){
    isDead = true;
}

void Death::reset() {
    Constraint::reset();
    Clock::reset();
    isDead = true;
}

bool Death::evaluate() {
    canBeRewrote = true;
    status = FALSE;
    return false;
}

void Death::rewrite() {
    return;
}

bool Death::propagatesChoice() {
    return true;
}

bool Death::propagatesDeath() {
    return true;
}

#if not (defined (__AVR__))
    ostream& Death::toStream(ostream& os) const {
        Clock::toStream(os);
        os << "= (" << u8"\u2620)";
        return os;
    }
#endif