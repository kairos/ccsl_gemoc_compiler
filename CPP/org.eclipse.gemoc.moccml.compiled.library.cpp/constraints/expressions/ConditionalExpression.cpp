//
// Created by jdeanton on 4/20/20.
//

#include "ConditionalExpression.h"



void ConditionalExpression::assignAllCasesAndRoot(std::vector<Case>& cases) {
    size_t size = cases.size();
    for(int i = 0; i < size; i++) {
        Case* pt_c = new Case(cases.at(i));
        allCases.push_back(pt_c);
        if (pt_c->test->evaluate()) {
            root = pt_c->constraint;
        }else {
            pt_c->constraint->isDead = true;
        }
    }
}

ConditionalExpression::ConditionalExpression(std::vector<Case> cases, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)) {
    assignAllCasesAndRoot(MOVE(cases));
}


ConditionalExpression::ConditionalExpression(std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)){
}

void ConditionalExpression::connect(std::vector<Case> cases) {
    assignAllCasesAndRoot(cases);
}

void ConditionalExpression::reset() {
    Constraint::reset();
    Clock::reset();
    for(Case* c : allCases) {
        if (c->test->evaluate()) {
            root = c->constraint;
//				root->reset();
        }else {
            c->constraint->isDead = true;
        }
    }
}

bool ConditionalExpression::evaluate() {
    canBeRewrote = true;
    if(isDead){
        status = FALSE;
        return false;
    }
    return false;
}

bool ConditionalExpression::propagatesChoice() {
    if(root == nullptr) {
        return true; //recursiveCall
    }
    if(root->status == this->status) {
        return true;
    }

    //something to do here
    if(root->status == POSSIBLY &&  this->status != POSSIBLY) {
        root->status = this->status;
        return false;
    }

    if(root->status != POSSIBLY &&  this->status == POSSIBLY) {
        this->status = root->status;
        return false;
    }

    //there is a conflict, propagates FALSE
    if(root->status != POSSIBLY &&  this->status != POSSIBLY) {
        root->status = FALSE;
        this->status = FALSE;
//			System.out.println("warning, back track !");
        return false;
    }

    exit(-3);
    return true;
}

void ConditionalExpression::rewrite() {
    if (!canBeRewrote) return;

    for(Case* c : allCases) {
        if (c->test->evaluate()) {
            root = c->constraint;
//				((Constraint)c.constraint).rewrite(); --> done in a main loop
            break;
        }
    }
    canBeRewrote = false;
}

bool ConditionalExpression::propagatesDeath() {
    if(root == nullptr) {
        return true; //recursiveCall
    }
    if (root->isDead && this->isDead) {
        return true;
    }
    if (!root->isDead && !this->isDead) {
        return true;
    }
    root->isDead = true;
    this->isDead = true;
    return false;
}
#if not (defined (__AVR__))
    ostream& ConditionalExpression::toStream(ostream& os) const {
        if(root != nullptr) {
            os << "Cond: " << *root;
        }
        return os;
    }
#endif

ConditionalExpression::~ConditionalExpression() {
    for(Case* pt_c : allCases){
        delete pt_c;
    }
}
