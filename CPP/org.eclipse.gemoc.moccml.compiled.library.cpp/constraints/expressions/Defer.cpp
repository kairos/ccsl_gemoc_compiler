//
// Created by jdeanton on 4/20/20.
//

#include "Defer.h"

Defer::Defer(Clock& bc, Clock& dc, Sequence seq, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)), baseClock(&bc), delayClock(&dc), sigma(seq){
}

Defer::Defer(std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)){
}

void Defer::connect(Clock& bc, Clock& dc, Sequence seq) {
    baseClock = &bc;
    delayClock = &dc;
    sigma = seq;
}

void Defer::reset() {
    Constraint::reset();
    Clock::reset();
    sigma.clear();
    ds.clear();
}

bool Defer::evaluate() {
    canBeRewrote = true;
    if(isDead){
        status = FALSE;
        return false;
    }
    bool beta = getBeta();
    if (! beta) {
        status = FALSE;
    }

    return false;
}

void Defer::rewrite() {
    if (!canBeRewrote) return;

    if (isDead){
    }

    if (baseClock->status == TRUE) {
        if (delayClock->status == TRUE) {
            // RWDefer3
            nextDelay();
            int next = sigma.nextHead();
            if (next != -1) {
                sched(next, 0);
            }
        }
        else {
            // RWDefer2
            int next = sigma.nextHead();
            if (next != -1) {
                sched(next, 0);
            }
        }
    }
    else if (delayClock->status == TRUE) {
        // RWDefer1 nextDelay()
        nextDelay();
    }

    if ( (ds.empty() && sigma.empty())) {
        isDead = true;
    }

    canBeRewrote = false;
}


bool Defer::propagatesChoice() {
    bool beta = getBeta();
    if (beta) {
//            cout << "beta is true" << endl;
        if (delayClock->status != POSSIBLY && status == POSSIBLY){
            status = delayClock->status;
            return false;
        }
        if (delayClock->status == POSSIBLY && status != POSSIBLY){
            delayClock->status = status;
            return false;
        }
        //there is conflict, propagate false
        if (delayClock->status != POSSIBLY && status != POSSIBLY && delayClock->status != status){
            delayClock->status = FALSE;
            status = FALSE;
            return false;
        }
    }
    return true;
}

bool Defer::propagatesDeath() {
    if (delayClock->isDead && !isDead){
        isDead = true;
        return false;
    }

    if (baseClock->isDead && ds.empty()){
        isDead = true;
        return false;
    }

    return true;
}

#if not (defined (__AVR__))
    ostream& Defer::toStream(ostream& os) const {
        Clock::toStream(os);
        os << "= (" << baseClock->name << " $ " << sigma << " on " << delayClock->name << ")";
        return os;
    }
#endif


//PRIVATE

bool Defer::getBeta() {
    bool beta = false;
    if (ds.empty()) {
        beta = false;
    }
    else {
        beta = (getDelay() == 1 || getDelay() == 0);
    }
    return beta;
}

int Defer::getDelay() {
    if (ds.empty()) {
        return std::numeric_limits<int>::max();
    }
    return ds.front();
}

void Defer::sched(int next, int start) {
    auto it = ds.begin();
    std::advance(it, start);
    if (ds.size() == (size_t) start) {
        std::advance(it, start);
        ds.insert(it, next);
    }
    else {
        std::advance(it, start);
        int head = *it;
        if (next == head) {
            return;
        }
        else if (next < head) {
            int rem = head - next;
            *it = next;
            it++;
            ds.insert(it, rem);
        }
        else { // next > head
            int rem = next - head;
            sched(rem, start + 1);
        }
    }
}

void Defer::nextDelay() {
    if (! ds.empty()) {
        int head = ds.front();
        if (head == 1 || head == 0) {
            ds.pop_front();
        }
        else {
            auto it = ds.begin();
            *it = head - 1;
        }
    }
}
