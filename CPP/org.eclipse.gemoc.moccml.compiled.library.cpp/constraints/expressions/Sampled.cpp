//
// Created by jdeanton on 4/20/20.
//

#include "Sampled.h"

Sampled::Sampled(Clock& sampled, Clock& sampler, std::string name, bool strict)
        : Clock(MOVE(name)), Constraint(MOVE(name)), sampledClock(&sampled), samplingClock(&sampler), isStrict(strict), sampledSeen(false){
}

Sampled::Sampled(std::string name, bool strict)
        : Clock(MOVE(name)), Constraint(MOVE(name)), isStrict(strict), sampledSeen(false){
}

void Sampled::connect(Clock& sampled, Clock& sampler) {
    sampledClock = &sampled;
    samplingClock = &sampler;
}

void Sampled::reset() {
    Constraint::reset();
    Clock::reset();
    sampledSeen = false;
}

bool Sampled::evaluate() {
    canBeRewrote = true;
    if(isDead){
        status = FALSE;
        return false;
    }
    if (!sampledSeen && isStrict){
        status = FALSE;
    }
    return false;
}

void Sampled::rewrite() {
    if (!canBeRewrote) return;

    if(status == TRUE){
        sampledSeen = false;
        isDead = true;
    }

    if (sampledClock->status == TRUE && isStrict){
        sampledSeen = true;
    }

    if (sampledClock->status == TRUE && !isStrict && samplingClock->status != TRUE){
        sampledSeen = true;
    }
    canBeRewrote = false;
    return;
}

bool Sampled::propagatesChoice() {

    if( status == TRUE && sampledSeen && samplingClock->status == TRUE){
        return true;
    }

    if( status == TRUE && sampledClock->status == TRUE && samplingClock->status == TRUE && !isStrict){
        return true;
    }

    if( status == TRUE && !sampledSeen && !isStrict && samplingClock->status == TRUE && sampledClock->status == TRUE){
        return true;
    }

    if( status == POSSIBLY && samplingClock->status == POSSIBLY){
        return true;
    }
    if( status == POSSIBLY && !sampledSeen && !isStrict && samplingClock->status == TRUE && sampledClock->status == POSSIBLY){
        return true;
    }

    if( status == FALSE && !sampledSeen && isStrict){
        return true;
    }

    if( status == FALSE && !sampledSeen && !isStrict && (sampledClock->status != TRUE || samplingClock->status != TRUE)){
        return true;
    }

    if( status == FALSE && !sampledSeen && !isStrict && sampledClock->status == POSSIBLY && samplingClock->status == POSSIBLY){
        return true;
    }

    if( status == FALSE && sampledSeen && samplingClock->status == FALSE){
        return true;
    }

    //do something from here
    if( status == POSSIBLY && sampledSeen && samplingClock->status == TRUE){
        status = TRUE;
        return false;
    }

    if( status == POSSIBLY && samplingClock->status == TRUE && !isStrict && sampledClock->status == TRUE){
        status = TRUE;
        return false;
    }

    if( status == POSSIBLY && samplingClock->status == FALSE){
        status = FALSE;
        return false;
    }

    if( status == POSSIBLY && sampledSeen && samplingClock->status == FALSE){
        status = FALSE;
        return false;
    }

    if( status == TRUE && sampledSeen && samplingClock->status == POSSIBLY){
        samplingClock->status = TRUE;
        return false;
    }

    if( status == TRUE && !sampledSeen && samplingClock->status == POSSIBLY && sampledClock->status == TRUE && !isStrict){
        samplingClock->status = TRUE;
        return false;
    }

    if( status == TRUE && !sampledSeen && samplingClock->status == TRUE && sampledClock->status == POSSIBLY && !isStrict){
        sampledClock->status = TRUE;
        return false;
    }


    if( status == FALSE && sampledSeen && samplingClock->status == POSSIBLY){
        samplingClock->status = FALSE;
        return false;
    }

    if( status == FALSE && !sampledSeen && !isStrict && samplingClock->status == POSSIBLY && sampledClock->status == TRUE){
        samplingClock->status = FALSE;
        return false;
    }

    if( status == FALSE && !sampledSeen && !isStrict && samplingClock->status == TRUE && sampledClock->status == POSSIBLY){
        sampledClock->status = FALSE;
        return false;
    }


    //backtrack
    if( status == TRUE && sampledSeen && samplingClock->status == FALSE){
        status = FALSE;
        return false;
    }

    if( status == FALSE && sampledSeen && samplingClock->status == TRUE){
        samplingClock.status = FALSE;
        return false;
    }

    //TODO: deal with non strict backward...

    exit(-6);
    return true;
}

bool Sampled::propagatesDeath() {
    if (!sampledSeen && sampledClock->isDead && !isDead){
        isDead = true;
        return false;
    }
    if (sampledSeen && samplingClock->isDead && !isDead){
        isDead = true;
        return false;
    }

    return true;
}

#if not (defined (__AVR__))
    ostream& Sampled::toStream(ostream& os) const {
        Clock::toStream(os);
        os << "= (" << sampledClock->name << u8" \u2b6d\u2b6d " << samplingClock->name << ")";
        return os;
    }
#endif