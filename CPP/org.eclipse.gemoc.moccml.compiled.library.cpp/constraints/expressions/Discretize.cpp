//
// Created by jdeanton on 4/20/20.
//

#include "Discretize.h"
#if not (defined (__AVR__))
    #include <thread>
    #include <mutex> //to be changed with semaphore once C++20 is correctly supported
    std::mutex synchroMutex;


    Discretize::Discretize(std::string name, std::chrono::milliseconds periodInMs)
    : Clock(MOVE(name)), Constraint(MOVE(name)), period(periodInMs), executionIsRunning(true){
        periodicThread = std::thread(&Discretize::periodicTempo, this);
    }

    ostream& Discretize::toStream(ostream& os) const {
        Clock::toStream(os);
        os << "= discretize " << period.count() << "ms";
        return os;
    }

    void Discretize::periodicTempo(){
        auto begin = std::chrono::system_clock::now();
        while(executionIsRunning) {
            auto dur = std::chrono::duration_cast<std::chrono::milliseconds>((std::chrono::high_resolution_clock::now() - begin)).count();
                std::cout << "wake up @" << dur <<  std::endl;
                synchroMutex.unlock();  //unlock tick
                std::this_thread::sleep_until(begin + nbTick*period);
        }
    }

    Discretize::~Discretize() {
        executionIsRunning = false;
        periodicThread.join();
    }

    void Discretize::ticks() {
        Clock::ticks();
        synchroMutex.lock(); //wait for the time to spend....
    }

#else

    #define __DELAY_BACKWARD_COMPATIBLE__
    #include <util/delay.h>

    Discretize::Discretize(std::string name, uint32_t periodInMs)
            : Clock(MOVE(name)), Constraint(MOVE(name)), period(periodInMs), executionIsRunning(true){

    }
    Discretize::~Discretize() {
        executionIsRunning = false;
    }
    void Discretize::ticks() {
        Clock::ticks();
        _delay_ms(period); //TODO make it better by using HW timer and copy t=structure from x86_64
    }
    void Discretize::periodicTempo() {
        //should be done correctly (see TODO below)
    }

#endif
void Discretize::reset() {
    Constraint::reset();
    Clock::reset();
}

bool Discretize::evaluate() {
    status = POSSIBLY;
    return false;
}

void Discretize::rewrite() {
    return;
}

bool Discretize::propagatesChoice() {
    return true;
}

bool Discretize::propagatesDeath() {
    return true;
}

