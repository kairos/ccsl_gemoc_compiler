//
// Created by jdeanton on 4/20/20.
//

#include "UserDefinedExpression.h"

UserDefinedExpression::UserDefinedExpression(Clock rootExpression, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)), root(&rootExpression) {
}

UserDefinedExpression::UserDefinedExpression(std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)) {
}

void UserDefinedExpression::connect(Clock& rootExpression) {
    root = &rootExpression;
}

void UserDefinedExpression::reset() {
    Constraint::reset();
    Clock::reset();
}

bool UserDefinedExpression::evaluate() {
    canBeRewrote = true;
    if(isDead){
        status = FALSE;
        return false;
    }
    return false;
}

bool UserDefinedExpression::propagatesChoice() {
    if(root->status == this->status) {
        return true;
    }

    //something to do here
    if(root->status == POSSIBLY &&  this->status != POSSIBLY) {
        root->status = this->status;
        return false;
    }

    if(root->status != POSSIBLY &&  this->status == POSSIBLY) {
        this->status = root->status;
        return false;
    }

    //there is a conflict, propagates FALSE
    if(root->status != POSSIBLY &&  this->status != POSSIBLY) {
        root->status = FALSE;
        this->status = FALSE;
//			System.out.println("warning, back track !");
        return false;
    }


    exit(-10);
    return true;
}

void UserDefinedExpression::rewrite() {
}

bool UserDefinedExpression::propagatesDeath() {
    if (root->isDead && this->isDead) {
        return true;
    }
    if (!root->isDead && !this->isDead) {
        return true;
    }
    root->isDead = true;
    this->isDead = true;
    return false;
}

#if not (defined (__AVR__))
    std::ostream& UserDefinedExpression::toStream(ostream& os) const {
        os << "UserDefined: " << *root;
        return os;
    }
#endif
