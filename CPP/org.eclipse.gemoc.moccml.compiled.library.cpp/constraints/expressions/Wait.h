//
// Created by jdeanton on 2/14/19.
//
#ifndef WAIT_H
#define WAIT_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include "../../classicalexpressions/IntegerExpression.h"
#include <string>

class Wait: public Clock, public Constraint {
	
public:
    Clock* c1 = nullptr;
    int N;
	int initialN;
private:
	IntegerExpression* theExpr = nullptr;
public:
	Wait(Clock& c11, int waitValue, std::string name = "defaultWaitName");
	Wait(int waitValue, std::string name = "defaultWaitName");
	Wait(Clock& c11, IntegerExpression& expr, std::string name = "defaultWaitName");
	Wait(IntegerExpression& expr, std::string name = "defaultWaitName");

	void setExpression(IntegerExpression& expr);
	void connect(Clock& c11);
	void reset() override;
	bool evaluate() override;
	void rewrite() override;
	bool propagatesChoice() override;
	bool propagatesDeath() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
    ~Wait() override;

};

#endif


