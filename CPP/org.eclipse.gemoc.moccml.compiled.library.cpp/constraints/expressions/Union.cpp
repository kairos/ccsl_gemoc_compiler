//
// Created by jdeanton on 4/20/20.
//

#include "Union.h"

Union::Union(Clock& c11, Clock& c22, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)),c1(&c11), c2(&c22){
}

Union::Union(std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)){
}

void Union::connect(Clock& c11, Clock& c22) {
    c1 = &c11;
    c2 = &c22;
}

bool Union::evaluate() {
    canBeRewrote = true;
    if(isDead){
        status = FALSE;
        return false;
    }
    return false;
}

void Union::rewrite() {
}

bool Union::propagatesChoice() {

//	    cout << "Union::propagatesChoice " << " c1 = "<< c1->status << " c2 = " << c2->status << " def = " << status << endl;

    if (status == TRUE && (c1->status == TRUE || c2->status == TRUE)) {
        return true;
    }
    if ((status == FALSE) && (c1->status == FALSE) && (c2->status == FALSE)) {
        return true;
    }
    if ((status == POSSIBLY)
        && ((c1->status == FALSE  && c2->status == POSSIBLY)
            || (c2->status == FALSE && c1->status == POSSIBLY)
            || (c2->status == POSSIBLY && c1->status == POSSIBLY))) {
        return true;
    }
    if (status == TRUE && (c1->status == POSSIBLY && c2->status == POSSIBLY)) {
        return true;
    }
    //if here, something must be done

    if ((c1->status == TRUE || c2->status == TRUE) && status == POSSIBLY){
        status = TRUE;
        return false;
    }
    if (c1->status == FALSE && c2->status == FALSE && status == POSSIBLY) {
        status = FALSE;
        return false;
    }
    if(status == FALSE &&
       ((c1->status == POSSIBLY && c2->status == FALSE)
        ||
        (c2->status == POSSIBLY && c1->status == FALSE)
        ||
        (c2->status == POSSIBLY && c1->status == POSSIBLY)
       )
            ){
        c1->status = FALSE;
        c2->status = FALSE;
        return false;
    }

    if (status == TRUE && (c1->status == FALSE) && c2->status == POSSIBLY) {
        c2->status = TRUE;
        return false;
    }
    if (status == TRUE && (c2->status == FALSE)&& c1->status == POSSIBLY) {
        c1->status = TRUE;
        return false;
    }

    //here there is a conflict
    if ((c1->status == TRUE || c2->status == TRUE) && status == FALSE){
        c1->status = FALSE;
        c2->status = FALSE;
        return false;
    }

    if ((c1->status == FALSE && c2->status == FALSE) && status == TRUE){
        status = FALSE;
        return false;
    }


//    std::cout << "ERROR: in Union Expression " << Clock::name << " a case is missing: c1 = " << c1->status << " c2 = " << c2->status << " def = " << status << std::endl;
    exit(-8);
    return true;
}

void Union::reset() {
    Constraint::reset();
    Clock::reset();
}

bool Union::propagatesDeath() {
    if (c1->isDead && c2->isDead && !isDead){
        isDead = true;
        return false;
    }
    return true;
}

#if not (defined (__AVR__))
    ostream& Union::toStream(ostream& os) const {
        Clock::toStream(os);
        os << "= (" << c1->name << u8" \u222A " << c2->name << ")";
        return os;
    }
#endif