//
// Created by jdeanton on 2/15/19.
//
#ifndef SAMPLED_H
#define SAMPLED_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include "../../classicalexpressions/IntegerExpression.h"
#include <string>

class Sampled: public Clock, public Constraint {
	
public:
    Clock* sampledClock = nullptr;
	Clock* samplingClock = nullptr;
    bool isStrict = false;
    bool sampledSeen = false;

    Sampled(Clock& sampled, Clock& sampler, std::string name = "defaultSampledName", bool strict = false);
    explicit Sampled(std::string name = "defaultSampledName", bool strict = false);
    
    void connect(Clock& sampled, Clock& sampler);
    void reset() override;
    bool evaluate() override;
    void rewrite() override;
    bool propagatesChoice() override;
    bool propagatesDeath() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
};

#endif