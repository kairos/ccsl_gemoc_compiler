//
// Created by jdeanton on 2/16/19.
//
#ifndef DISCRETIZE_H
#define DISCRETIZE_H

#include "../../Clock.h"
#include "../Constraint.h"

#if not (defined (__AVR__))
    #include <chrono>
    #include <thread>
#endif

class Discretize: public Clock, public Constraint {
public:
#if not (defined (__AVR__))
    std::chrono::milliseconds period;
    Discretize(std::string name, std::chrono::milliseconds periodInMs);
#else
    const uint32_t period;
    Discretize(std::string name, uint32_t periodInMs);
#endif

    void ticks() override;
	void reset() override;
	bool evaluate() override;
	void rewrite() override;
	bool propagatesChoice() override;
	bool propagatesDeath() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
    ~Discretize() override;

private:
    bool executionIsRunning = false;
#if not (defined (__AVR__))
        std::thread periodicThread;
#endif
    void periodicTempo();
};

#endif
