//
// Created by jdeanton on 4/19/20.
//

#include "Wait.h"


Wait::Wait(Clock& c11, int waitValue, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)), c1(&c11){
    N = waitValue;
    initialN = waitValue;
}

Wait::Wait(int waitValue, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)){
    N = waitValue;
    initialN = waitValue;
}

Wait::Wait(Clock& c11, IntegerExpression& expr, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)), c1(&c11){
    theExpr = expr.clone();
    N = expr.evaluate();
    initialN = N;
}

Wait::Wait(IntegerExpression& expr, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)){
    theExpr = expr.clone();
    N = expr.evaluate();
    initialN = N;
}

void Wait::connect(Clock& c11) {
    c1 = &c11;
}

void Wait::reset() {
    Constraint::reset();
    Clock::reset();
    if(theExpr != nullptr) {
        N = theExpr->evaluate();
    }else {
        N = initialN;
    }
}

bool Wait::evaluate() {
    canBeRewrote = true;
    if(isDead){
        status = FALSE;
        return false;
    }
    if (N > 1){
        status = FALSE;
    }
    return false;
}

void Wait::rewrite() {
    if (!canBeRewrote) return;

    if (isDead){
    }

    if (status == TRUE){
        isDead = true;
        N = -1;
    }

    if (c1->status == TRUE){
        N--;
//	        cout << "N-- : " <<N << endl;
    }
    canBeRewrote = false;
}

bool Wait::propagatesChoice() {
    if (isDead) {
        return true;
    }

    if (N <= 0 && status == FALSE) {
        return true;
    }

    if (N >1 && status == FALSE) {
        return true;
    }
    if ((N == 1) && status == c1->status) {
        return true;
    }


    if (N == 1) {
        if (status != POSSIBLY && c1->status == POSSIBLY) {
            c1->status = this->status;
            return false;
        }
        if (status == POSSIBLY && c1->status != POSSIBLY) {
            this->status = c1->status;
            return false;
        }
    }

    //there is a conflict, propagates FALSE
    if ((N == 1) && status != c1->status) {
        c1->status = FALSE;
        this->status = FALSE;
//			std::cout << "warning, back track ! (N==" << N <<")" << std::endl;
        return false;
    }


    exit(-11);
    return true;
}

bool Wait::propagatesDeath() {
    if (c1->isDead && !isDead){
        isDead = true;
        return false;
    }

    return true;
}

#if not (defined (__AVR__))
    ostream& Wait::toStream(ostream& os) const {
        Clock::toStream(os);
        os << "= (" << c1->name << " wait " << N << ")";
        return os;
    }
#endif

Wait::~Wait() {
    delete theExpr;
}

void Wait::setExpression(IntegerExpression& expr) {
    delete theExpr;
    theExpr = expr.clone();
}
