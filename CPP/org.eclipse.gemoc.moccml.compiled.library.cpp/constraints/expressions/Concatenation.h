//
// Created by jdeanton on 6/2/17.
//
#ifndef CONCATENATION_H
#define CONCATENATION_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../classicalexpressions/ClassicalExpression.h"
#include "../../classicalexpressions/SequenceExpression.h"
#include "../../classicalexpressions/SeqHead.h"
#include "Wait.h"
#include <string>

class Concatenation: public Clock, public Constraint {
	
public:
    Clock* leftClock = nullptr;
	Clock* rightClock = nullptr;

    bool rightIsAConstraint = false;
    bool leftIsAConstraint = false;
    bool isRecursive = false;
    bool followLeft = true;
    bool rightIsRecursiveCall = false;
    ClassicalExpression* recursionExpression = nullptr;

    Concatenation(Clock& lc, Clock& rc, std::string name = "defaultConcatName", bool leftIsConstraint = false, bool rightIsConstraint = false, bool rightIsRecursiveCall = false);
    explicit Concatenation(std::string name = "defaultConcatName");

    void connect(Clock& lc, Clock& rc, bool rightIsRecursiveCall, ClassicalExpression& recursionParam, bool leftIsConstraint = false, bool rightIsConstraint = false);
    void connect(Clock& lc, Clock& rc, bool leftIsConstraint = false, bool rightIsConstraint = false, bool rightIsRecursiveCall = false);
    bool evaluate() override;
	void rewrite() override;
	bool propagatesChoice() override;
	void reset() override;
	bool propagatesDeath() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
};
#endif

