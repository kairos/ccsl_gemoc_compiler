//
// Created by jdeanton on 2/14/19.
//
#ifndef INTERSECTION_H
#define INTERSECTION_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include "../../classicalexpressions/IntegerExpression.h"
#include <string>

class Intersection: Clock, Constraint {

public:
    Clock* c1 = nullptr;
	Clock* c2 = nullptr;

	Intersection(Clock c11, Clock c22,std::string name);
	Intersection(std::string name);
	
	void connect(Clock& c11, Clock& c22);
	bool evaluate() override;
	void rewrite() override;
	bool propagatesChoice() override;
	void reset() override;
	bool propagatesDeath() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
};

#endif