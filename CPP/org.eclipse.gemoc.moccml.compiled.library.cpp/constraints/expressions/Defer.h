//
// Created by jdeanton on 5/29/17.
//
#ifndef DEFER_H
#define DEFER_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../utils/Sequence.h"
#include "../../classicalexpressions/IntegerExpression.h"
#include <string>
#include <vector>
#include <limits>

class Defer: public Clock, public Constraint {
	
public:
    Clock* baseClock = nullptr;
	Clock* delayClock = nullptr;
    Sequence sigma;
private:
    std::list<int> ds;

public:
    Defer(Clock& bc, Clock& dc, Sequence seq, std::string name = "defaultDeferName");
    explicit Defer(std::string name = "defaultDeferName");

    void connect(Clock& bc, Clock& dc, Sequence seq);
    void reset() override;
    bool evaluate() override;
    void rewrite() override;
    bool propagatesChoice() override;
    bool propagatesDeath() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
private:
    bool getBeta();
    int getDelay();
    void sched(int next, int start);
	void nextDelay();

};

#endif