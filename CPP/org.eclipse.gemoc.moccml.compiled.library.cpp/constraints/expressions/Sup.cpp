//
// Created by jdeanton on 4/20/20.
//

#include "Sup.h"

Sup::Sup(Clock& lc, Clock& rc, std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)), leftClock(&lc), rightClock(&rc){
}

Sup::Sup(std::string name) : Clock(MOVE(name)), Constraint(MOVE(name)){
}

void Sup::connect(Clock& lc, Clock& rc) {
    leftClock = &lc;
    rightClock = &rc;
}

void Sup::reset() {
    Constraint::reset();
    Clock::reset();
    delta = 0;
}

bool Sup::evaluate() {
    canBeRewrote = true;
    if(isDead){
        status = FALSE;
        return false;
    }
    return false;
}

void Sup::rewrite() {
    if (!canBeRewrote) return;

    if(leftClock->status == TRUE){
        delta--;
    }
    if(rightClock->status == TRUE){
        delta++;
    }
    canBeRewrote = false;
    return;
}

bool Sup::propagatesChoice() {
    if(isDead){
        return true;
    }

    if (status == POSSIBLY && (leftClock->status == POSSIBLY && rightClock->status == POSSIBLY)){
        return true;
    }

    if (status != POSSIBLY && (leftClock->status != POSSIBLY && rightClock->status != POSSIBLY)){
        return true;
    }

    if( delta > 0 && status == leftClock->status){
        return true;
    }

    if( delta < 0 && status == rightClock->status){
        return true;
    }

    if (delta == 0 && status != POSSIBLY && (leftClock->status == POSSIBLY && rightClock->status == POSSIBLY)){
        return true;
    }

    if( delta == 0
        && ((status == TRUE && (leftClock->status == TRUE || rightClock->status == TRUE))
            || (status == FALSE && (leftClock->status == FALSE && rightClock->status == FALSE))
            || (status == POSSIBLY && (leftClock->status == POSSIBLY && rightClock->status == TRUE))
            || (status == POSSIBLY && (leftClock->status == TRUE && rightClock->status == POSSIBLY)))
            ){
        return true;
    }

    if( delta > 0 && leftClock->status != POSSIBLY){
        status = leftClock->status;
        return false;
    }

    if( delta < 0 && rightClock->status != POSSIBLY){
        status = rightClock->status;
        return false;
    }

    if( delta > 0 && status != POSSIBLY){
        leftClock->status = status;
        return false;
    }

    if( delta < 0 && status != POSSIBLY){
        rightClock->status = status;
        return false;
    }

    if( delta == 0 && (status == FALSE)){
        if (rightClock->status != POSSIBLY){
            leftClock->status = status;
            return false;
        }
        if (leftClock->status != POSSIBLY){
            rightClock->status = status;
            return false;
        }
        return false;
    }

    if( delta == 0 && (status == TRUE)){
        leftClock->status = status;
        rightClock->status = status;
        return false;
    }

    if( delta == 0 && (rightClock->status == TRUE && leftClock->status == TRUE)){
        status = TRUE;
        return false;
    }

    if( delta == 0 && (rightClock->status == FALSE || leftClock->status == FALSE)){
        status = FALSE;
        return false;
    }


    exit(-7);
    return true;
}

bool Sup::propagatesDeath() {
    if( delta < 0 && rightClock->isDead && !isDead){
        isDead = true;
        return false;
    }

    if( delta > 0 && leftClock->isDead && !isDead){
        isDead = true;
        return false;
    }

    if( delta == 0 && leftClock->isDead && rightClock->isDead && !isDead){
        isDead = true;
        return false;
    }

    return true;
}

#if not (defined (__AVR__))
    ostream& Sup::toStream(ostream& os) const {
        Clock::toStream(os);
        os << "= (" << leftClock->name << u8" \u2228 " << rightClock->name << ")";
        return os;
    }
#endif