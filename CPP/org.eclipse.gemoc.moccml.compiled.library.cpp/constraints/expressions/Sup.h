//
// Created by jdeanton on 2/17/19.
//
#ifndef SUP_H
#define SUP_H

#include "../../Clock.h"
#include "../Constraint.h"
#include "../../utils/utils.h"
#include "../../classicalexpressions/IntegerExpression.h"
#include <string>


class Sup: Clock, Constraint {
	
public:
    Clock* leftClock = nullptr;
	Clock* rightClock = nullptr;
    int delta = 0;

    Sup(Clock& lc, Clock& rc, std::string name);
    Sup(std::string name);
    
    void connect(Clock& lc, Clock& rc);
    void reset() override;
    bool evaluate() override;
    void rewrite() override;
    bool propagatesChoice() override;
    bool propagatesDeath() override;
#if not (defined (__AVR__))
    ostream& toStream(ostream& os) const override;
#endif
};
#endif
