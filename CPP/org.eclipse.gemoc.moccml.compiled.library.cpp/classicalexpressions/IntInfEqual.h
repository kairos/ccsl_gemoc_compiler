#ifndef INT_INF_EQUAL_H
#define INT_INF_EQUAL_H

#include "IntegerExpression.h"
#include "BooleanExpression.h"

class IntInfEqual: public BooleanExpression {

    IntegerExpression& leftOperand;
    IntegerExpression& rightOperand;

    IntInfEqual(IntegerExpression& liexpr, IntegerExpression& riexpr);

    bool evaluate() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
    BooleanExpression* clone() const override;
};
#endif
