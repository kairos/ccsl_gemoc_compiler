#ifndef INT_ASSIGN_H
#define INT_ASSIGN_H

#include "IntegerExpression.h"
#include "IntRef.h"

class IntAssign: public IntegerExpression {

	IntRef& leftOperand;
	IntegerExpression& rightOperand;
	
	IntAssign(IntRef& lint, IntegerExpression& riexpr);
	
	int evaluate() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
    IntegerExpression* clone() const override;
};
#endif
