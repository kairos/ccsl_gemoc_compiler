#ifndef SEQ_TAIL_EXPRESSION_H
#define SEQ_TAIL_EXPRESSION_H

#include <string>
#include "../utils/Sequence.h"
#include "SequenceExpression.h"

class SeqTail: public SequenceExpression {

	Sequence seq;
	
public:
    SeqTail(Sequence s);
	
	Sequence evaluate() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
    SequenceExpression* clone() const override;
	
};
#endif
