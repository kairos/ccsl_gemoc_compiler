#ifndef NOT_H
#define NOT_H

#include "BooleanExpression.h"



class Not: public BooleanExpression {
private:
	BooleanExpression& operand;
public:
	explicit Not(const BooleanExpression& op);
	Not(const Not& n);

	void setOperand(BooleanExpression& op);
	bool evaluate() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
    BooleanExpression* clone() const override;

    ~Not() override;
};
#endif
