//
// Created by jdeanton on 4/20/20.
//

#include "Or.h"

Or::Or(BooleanExpression& lexpr, BooleanExpression& rexpr) : leftOperand(lexpr), rightOperand(rexpr) {
}

bool Or::evaluate() {
    return leftOperand.evaluate() || rightOperand.evaluate();
}

#if not (defined (__AVR__))
    std::ostream& Or::toStream(std::ostream& os) const {
        os << "(" << leftOperand << ") || (" <<rightOperand  << ")";
        return os;
    }
#endif

BooleanExpression* Or::clone() const {
    return new Or(*this);
}
