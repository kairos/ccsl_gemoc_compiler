//
// Created by jdeanton on 4/20/20.
//


#include "IntMinus.h"

IntMinus::IntMinus(IntegerExpression& liexpr, IntegerExpression& riexpr) : leftOperand(liexpr), rightOperand(riexpr){
}

int IntMinus::evaluate() {
    return leftOperand.evaluate() - rightOperand.evaluate();
}

#if not (defined (__AVR__))
    std::ostream& IntMinus::toStream(std::ostream& os) const {
        os << "(" << leftOperand << " - " <<rightOperand  << ")";
        return os;
    }
#endif

IntegerExpression* IntMinus::clone() const {
    return new IntMinus(*this);
}
