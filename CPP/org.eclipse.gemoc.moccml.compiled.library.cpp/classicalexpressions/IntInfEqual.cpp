//
// Created by jdeanton on 4/20/20.
//

#include "IntInfEqual.h"

IntInfEqual::IntInfEqual(IntegerExpression& liexpr, IntegerExpression& riexpr) : leftOperand(liexpr), rightOperand(riexpr) {
}

bool IntInfEqual::evaluate() {
    return leftOperand.evaluate() <= rightOperand.evaluate();
}
#if not (defined (__AVR__))
    std::ostream& IntInfEqual::toStream(std::ostream& os) const {
        os <<  "(" << leftOperand << " <= " << rightOperand << ")";
        return os;
    }
#endif

BooleanExpression* IntInfEqual::clone() const {
    return new IntInfEqual(*this);
}
