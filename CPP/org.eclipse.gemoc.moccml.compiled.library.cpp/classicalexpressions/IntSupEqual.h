#ifndef INT_SUPEQUAL_H
#define INT_SUPEQUAL_H

#include "IntegerExpression.h"
#include "BooleanExpression.h"

class IntSupEqual: public BooleanExpression {

    IntegerExpression& leftOperand;
    IntegerExpression& rightOperand;

    IntSupEqual(IntegerExpression& liexpr, IntegerExpression& riexpr);

    bool evaluate() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
};
#endif
