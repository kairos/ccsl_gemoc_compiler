//
// Created by jdeanton on 4/20/20.
//

#include "SeqTail.h"

SeqTail::SeqTail(Sequence s) :seq(MOVE(s)) {
}

Sequence SeqTail::evaluate() {
    return MOVE(seq.getTail());
}

#if not (defined (__AVR__))
    std::ostream& SeqTail::toStream(std::ostream& os) const {
        os << "tail(" << seq.name << ")";
        return os ;
    }
#endif

SequenceExpression* SeqTail::clone() const {
    return new SeqTail(*this);
}
