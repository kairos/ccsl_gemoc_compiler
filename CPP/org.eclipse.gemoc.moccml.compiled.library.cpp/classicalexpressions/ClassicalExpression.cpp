//
// Created by jdeanton on 4/20/20.
//

#include "ClassicalExpression.h"

#if not (defined (__AVR__))
    std::ostream& operator<<(std::ostream& os, const ClassicalExpression& ce) {
        return ce.toStream(os);
    }
#endif