//
// Created by jdeanton on 4/20/20.
//

#include "IntRef.h"

IntRef::IntRef(int op) :ref(MOVE(op)) {
}

int IntRef::evaluate() {
    return ref;
}
#if not (defined (__AVR__))
    std::ostream& IntRef::toStream(std::ostream& os) const {
        os << ref;
        return os;
    }
#endif

IntegerExpression* IntRef::clone() const {
    return new IntRef(*this);
}
