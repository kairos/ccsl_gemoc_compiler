//
// Created by jdeanton on 4/20/20.
//

#include "And.h"

And::And(BooleanExpression& lexpr, BooleanExpression& rexpr) : leftOperand(lexpr), rightOperand(rexpr) {
}

bool And::evaluate() {
    return leftOperand.evaluate() && rightOperand.evaluate();
}

#if not (defined (__AVR__))
    std::ostream& And::toStream(std::ostream& os) const {
       os << "(" << leftOperand << ") && (" <<rightOperand  << ")";
      return os;
    }
#endif

BooleanExpression* And::clone() const {
    return new And(*this);
}
