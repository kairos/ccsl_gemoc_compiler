#ifndef SEQUENCE_EXPRESSION_H
#define SEQUENCE_EXPRESSION_H

#include <ostream>
#include "ClassicalExpression.h"
#include "../utils/Sequence.h"

class SequenceExpression: public ClassicalExpression {

public:
    virtual Sequence evaluate() = 0;
#if not (defined (__AVR__))
    virtual std::ostream& toStream(std::ostream& os) const = 0; //From ClassicalExpression
#endif
    virtual ClassicalExpression* clone() const override = 0;
};

#endif
