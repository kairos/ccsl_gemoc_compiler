#ifndef INT_ADD_H
#define INT_ADD_H

#include "IntegerExpression.h"

class IntAdd: public IntegerExpression {

	IntegerExpression& leftOperand;
	IntegerExpression& rightOperand;
	
	IntAdd(IntegerExpression& liexpr, IntegerExpression& riexpr);
	
	int evaluate() override;
    #if not (defined (__AVR__))
        std::ostream& toStream(std::ostream& os) const override;
    #endif
    IntegerExpression* clone() const override;
	
};
#endif
