//
// Created by jdeanton on 4/20/20.
//

#include "SeqHead.h"

SeqHead::SeqHead(Sequence s) : seq(MOVE(s)) {
}

int SeqHead::evaluate() {
    return seq.nextHead();
}

#if not (defined (__AVR__))
    std::ostream& SeqHead::toStream(std::ostream& os) const {
        os << "seqHead(" << seq.name << ")";
        return os;
    }
#endif

IntegerExpression* SeqHead::clone() const {
    return new SeqHead(*this);
}
