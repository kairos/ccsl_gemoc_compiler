#ifndef SEQ_EMPTY_EXPRESSION_H
#define SEQ_EMPTY_EXPRESSION_H

#include <string>
#include "../utils/Sequence.h"
#include "BooleanExpression.h"

class SeqEmpty: public BooleanExpression {
private:
	Sequence seq;

public:
    explicit SeqEmpty(Sequence s);
	
	bool evaluate() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
    BooleanExpression* clone() const override;
};
#endif
