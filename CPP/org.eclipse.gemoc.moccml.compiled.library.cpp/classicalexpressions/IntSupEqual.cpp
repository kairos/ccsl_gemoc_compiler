//
// Created by jdeanton on 4/20/20.
//


#include "IntSupEqual.h"

IntSupEqual::IntSupEqual(IntegerExpression& liexpr, IntegerExpression& riexpr) : leftOperand(liexpr), rightOperand(riexpr){
}

bool IntSupEqual::evaluate() {
    return leftOperand.evaluate() >= rightOperand.evaluate();
}

#if not (defined (__AVR__))
    std::ostream& IntSupEqual::toStream(std::ostream& os) const {
        os << "(" << leftOperand << " >= " <<rightOperand  << ")";
        return os;
    }
#endif