#ifndef INT_MINUS_H
#define INT_MINUS_H

#include "IntegerExpression.h"

class IntMinus: public IntegerExpression {

    IntegerExpression& leftOperand;
    IntegerExpression& rightOperand;

    IntMinus(IntegerExpression& liexpr, IntegerExpression& riexpr);

    int evaluate() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
    IntegerExpression* clone() const override;
};
#endif

