//
// Created by jdeanton on 4/20/20.
//

#include "IntInf.h"

IntInf::IntInf(IntegerExpression& liexpr, IntegerExpression& riexpr) : leftOperand(liexpr), rightOperand(riexpr) {
}

bool IntInf::evaluate() {
    return leftOperand.evaluate() < rightOperand.evaluate();
}

#if not (defined (__AVR__))
    std::ostream& IntInf::toStream(std::ostream& os) const {
     os <<  "(" << leftOperand << " < " << rightOperand << ")";
     return os;
    }
#endif

BooleanExpression* IntInf::clone() const {
    return new IntInf(*this);
}
