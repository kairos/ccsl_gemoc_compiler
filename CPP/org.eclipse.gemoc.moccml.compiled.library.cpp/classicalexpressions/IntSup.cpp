//
// Created by jdeanton on 4/20/20.
//


#include "IntSup.h"

IntSup::IntSup(IntegerExpression& liexpr, IntegerExpression& riexpr) : leftOperand(liexpr), rightOperand(riexpr){
}

bool IntSup::evaluate() {
    return leftOperand.evaluate() > rightOperand.evaluate();
}

#if not (defined (__AVR__))
    std::ostream& IntSup::toStream(std::ostream& os) const {
        os << "(" << leftOperand << " > " <<rightOperand  << ")";
        return os;
    }
#endif

BooleanExpression* IntSup::clone() const {
    return new IntSup(*this);
}
