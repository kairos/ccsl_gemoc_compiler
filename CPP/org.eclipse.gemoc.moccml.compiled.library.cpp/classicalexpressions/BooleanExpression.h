#ifndef BOOLEAN_EXPRESSION_H
#define BOOLEAN_EXPRESSION_H

#include "ClassicalExpression.h"

class BooleanExpression: public ClassicalExpression {

public:
    virtual bool evaluate() = 0;
#if not (defined (__AVR__))
    virtual std::ostream& toStream(std::ostream& os) const override = 0;
#endif
    virtual BooleanExpression* clone() const override = 0;
};
#endif
