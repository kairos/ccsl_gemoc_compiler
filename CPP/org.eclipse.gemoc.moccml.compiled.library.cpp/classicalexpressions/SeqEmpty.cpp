//
// Created by jdeanton on 4/20/20.
//

#include "SeqEmpty.h"

SeqEmpty::SeqEmpty(Sequence s) :seq(MOVE(s)) {
}

bool SeqEmpty::evaluate() {
    return seq.empty();
}

#if not (defined (__AVR__))
    std::ostream& SeqEmpty::toStream(std::ostream& os) const {
        os <<  "seqEmpty(" << seq.name << ")";
     return os;
    }
#endif

BooleanExpression* SeqEmpty::clone() const {
    return new SeqEmpty(*this);
}
