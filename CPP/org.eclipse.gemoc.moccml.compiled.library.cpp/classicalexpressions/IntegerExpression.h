#ifndef INTEGER_EXPRESSION_H
#define INTEGER_EXPRESSION_H

#include "ClassicalExpression.h"
#if not (defined (__AVR__))
    #include <iostream>
#endif

class IntegerExpression: public ClassicalExpression {

public:
    virtual int evaluate() = 0;
    #if not (defined (__AVR__))
        virtual std::ostream& toStream(std::ostream& os) const = 0;
    #endif
    virtual ~IntegerExpression() = default;
    virtual IntegerExpression* clone() const override = 0;
};

#endif
