#ifndef INT_REF_H
#define INT_REF_H

#include "IntegerExpression.h"

class IntRef: public IntegerExpression {

public:
    int ref;
	
	IntRef(int op);
	
	int evaluate() override;
    #if not (defined (__AVR__))
        std::ostream& toStream(std::ostream& os) const override;
    #endif
    IntegerExpression* clone() const override;
};
#endif