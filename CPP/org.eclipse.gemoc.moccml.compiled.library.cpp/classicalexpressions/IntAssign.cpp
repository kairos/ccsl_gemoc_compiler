//
// Created by jdeanton on 4/20/20.
//

#include "IntAssign.h"

IntAssign::IntAssign(IntRef& lint, IntegerExpression& riexpr) :leftOperand(lint), rightOperand(riexpr) {
}

int IntAssign::evaluate() {
    leftOperand.ref = rightOperand.evaluate();
    return 0; //by convention
}
#if not (defined (__AVR__))

    std::ostream& IntAssign::toStream(std::ostream& os) const {
        os << leftOperand <<" := (" << rightOperand << ")";
        return os;
    }
#endif

IntegerExpression* IntAssign::clone() const {
    return new IntAssign(*this);
}
