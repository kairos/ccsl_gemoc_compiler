//
// Created by jdeanton on 4/20/20.
//

#include "Not.h"

Not::Not(const BooleanExpression& op) :operand(*op.clone()) {
}

bool Not::evaluate() {
    return !operand.evaluate();
}

#if not (defined (__AVR__))
    std::ostream& Not::toStream(std::ostream& os) const {
        os << "!(" << operand << ")";
        return os;
    }
#endif

Not::~Not() {
    delete &operand;
}

BooleanExpression* Not::clone() const {
    return new Not(*this);
}

void Not::setOperand(BooleanExpression& op) {
    delete &operand;
    operand = *op.clone();
}

Not::Not(const Not& n) :operand(*n.operand.clone()){
}
