#ifndef OR_H
#define OR_H

#include "BooleanExpression.h"

class Or: public BooleanExpression {
public:
	BooleanExpression& leftOperand;
	BooleanExpression& rightOperand;
	
    Or(BooleanExpression& lexpr, BooleanExpression& rexpr);
	
	bool evaluate() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
    BooleanExpression* clone() const override;
};
#endif
