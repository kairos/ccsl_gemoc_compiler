#ifndef SEQ_HEAD_EXPRESSION_H
#define SEQ_HEAD_EXPRESSION_H

#include "../utils/Sequence.h"
#include "IntegerExpression.h"

class SeqHead: public IntegerExpression {
private:
	Sequence seq;
public:
    SeqHead(Sequence s);
	
	int evaluate() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
    IntegerExpression* clone() const override;
};
#endif
