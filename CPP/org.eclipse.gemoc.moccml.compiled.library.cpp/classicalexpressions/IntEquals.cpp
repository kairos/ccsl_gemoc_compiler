//
// Created by jdeanton on 4/20/20.
//

#include "IntEquals.h"

IntEquals::IntEquals(IntegerExpression& liexpr, IntegerExpression& riexpr) : leftOperand(liexpr), rightOperand(riexpr) {
}

bool IntEquals::evaluate() {
    return leftOperand.evaluate() == rightOperand.evaluate();
}

#if not (defined (__AVR__))
    std::ostream& IntEquals::toStream(std::ostream& os) const {
     os << "(" << leftOperand << " == " << rightOperand << ")";
     return os;
    }
#endif

BooleanExpression* IntEquals::clone() const {
    return new IntEquals(*this);
}
