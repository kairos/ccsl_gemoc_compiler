#ifndef AND_H
#define AND_H


#include "BooleanExpression.h"

class And: public BooleanExpression {
public:
	BooleanExpression& leftOperand;
	BooleanExpression& rightOperand;
	
	And(BooleanExpression& lexpr, BooleanExpression& rexpr);
	
	bool evaluate() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
    BooleanExpression* clone() const override;
	
};
#endif
