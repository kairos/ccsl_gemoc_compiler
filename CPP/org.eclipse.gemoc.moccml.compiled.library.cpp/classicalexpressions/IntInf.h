#ifndef INT_INF_H
#define INT_INF_H

#include "IntegerExpression.h"
#include "BooleanExpression.h"

class IntInf: public BooleanExpression {

	IntegerExpression& leftOperand;
	IntegerExpression& rightOperand;
	
	IntInf(IntegerExpression& liexpr, IntegerExpression& riexpr);
	
	bool evaluate() override;
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
    BooleanExpression* clone() const override;
};
#endif
