#ifndef CLASSICAL_EXPRESSION_H
#define CLASSICAL_EXPRESSION_H

#if not (defined (__AVR__))
    #include <ostream>
    #define MOVE(VAR) MOVE(VAR)
#endif
#if (defined (__AVR__))
    #include <pnew.h>
    #define MOVE(VAR) VAR
#endif

class ClassicalExpression {
	
public:
    #if not (defined (__AVR__))
        virtual std::ostream& toStream(std::ostream& os) const = 0;
	    friend std::ostream& operator<<(std::ostream& os, const ClassicalExpression& ce);
    #endif
	virtual ClassicalExpression* clone() const = 0;
	virtual ~ClassicalExpression() = default;
};

#endif
