//
// Created by jdeanton on 4/20/20.
//

#include "IntAdd.h"

IntAdd::IntAdd(IntegerExpression& liexpr, IntegerExpression& riexpr) : leftOperand(liexpr), rightOperand(riexpr){
}

int IntAdd::evaluate() {
    return leftOperand.evaluate() + rightOperand.evaluate();
}

#if not (defined (__AVR__))
    std::ostream& IntAdd::toStream(std::ostream& os) const {
      os << "(" << leftOperand << " + " <<rightOperand  << ")";
     return os;
    }
#endif

IntegerExpression* IntAdd::clone() const {
    return new IntAdd(*this);
}
