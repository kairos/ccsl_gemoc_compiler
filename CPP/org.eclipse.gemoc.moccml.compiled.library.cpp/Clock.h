/*
 * Clock.h
 *
 *  Created on: Aug 17, 2016
 *      Author: jdeanton
 */

#ifndef _CLOCK_H
#define _CLOCK_H

#include "utils/QuantumBoolean.h"
#include "utils/NamedElement.h"
#if not (defined (__AVR__))
    #include <iostream>
#else
    #include <new>
#endif
#include <string>
#include <functional>

using std::ostream;
using std::string;

class Clock : public NamedElement{
	
public:
    int nbTick = 0;
//	Random random = new Random();
	bool isDead = false;
	QuantumBoolean status = POSSIBLY;
#if not (defined (__AVR__))
    std::function<void()> functionToCallOnTick;
    Clock(string name, std::function<void()> f);
    void assignFunction(std::function<void()> f);
    ostream& toStream(ostream& os) const override;

#else
    void (*functionToCallOnTick)() = nullptr;
    Clock(string name, void(*f)());
    void assignFunction(void(*f)());
#endif

	Clock(string name);

	virtual void reset();
	virtual void ticks();
	/**
	 * @note in this method we can implement a simulation policy. For instance to choose true or false according to some probabilities
	 */
	void chooseStatus();


};


#endif