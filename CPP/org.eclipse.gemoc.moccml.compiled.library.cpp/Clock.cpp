//
// Created by jdeanton on 4/19/20.
//
#include "Clock.h"

#include <utility>

Clock::Clock(string name) :NamedElement(MOVE(name)){
}

#if not (defined (__AVR__))

    Clock::Clock(string name, std::function<void()> f):Clock(name) {
        functionToCallOnTick = f;
    }

    ostream &Clock::toStream(ostream &os) const{
        if(isDead){
            os <<  u8"\u271E ";
        }
        os <<  "Clock " << this->name <<  ": "  <<  nbTick  <<  ", "  <<  status;
        return os;
    }
    void Clock::assignFunction(std::function<void()> f) {
        functionToCallOnTick = f;
    }
    void Clock::ticks() {
    this->nbTick++;
    if(functionToCallOnTick){
        functionToCallOnTick();
    }
}
#else
    Clock::Clock(string name, void (*f)()):Clock(name) {
        functionToCallOnTick = f;
    }
    void Clock::assignFunction(void (*f)()) {
        functionToCallOnTick = f;
    }
void Clock::ticks() {
    this->nbTick++;
    if(functionToCallOnTick != nullptr){
        functionToCallOnTick();
    }
}
#endif

void Clock::reset() {
    this->isDead = false;
}



void Clock::chooseStatus() {
//	  int r = (random.nextInt(2));
//		if (r == 1 ){
    status = TRUE;
//		}else{
//			status = QuantumBoolean.FALSE;
//		}
}






