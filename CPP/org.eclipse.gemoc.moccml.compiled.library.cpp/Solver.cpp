//
// Created by jdeanton on 2/20/19.
//

#include "Solver.h"

#include <utility>
#include <algorithm>
#if not (defined (__AVR__))
    #include <execution>
    #include <thread>
#endif
#include "utils/utils.h"
#include "constraints/expressions/Concatenation.h"


Solver::Solver(std::vector<Clock *> allClocks, std::set<Constraint *> allConstraintsButExclusions, std::set<Constraint*> allExclusions)
 :clocks(MOVE(allClocks)), constraintsButExclusions(MOVE(allConstraintsButExclusions)), exclusions(MOVE(allExclusions)),
  allConstraints(MOVE(constraintsButExclusions + exclusions))
 {}


#include <avr/io.h>
#include <util/delay.h>
const uint8_t led   = _BV(PC7);
const uint8_t led3   = _BV(PC6);
void toggle4(){
    DDRC |= (led | led3);
    PORTC ^= led;
    PORTC ^= led3;
}
void Solver::solve() {

    for(Constraint* pt_r: allConstraints) {
        pt_r->evaluate();
    }

    propagatesChoice();

    for(Clock* pt_c : clocks) {
        if (pt_c->status == POSSIBLY) {
            pt_c->status = TRUE;
            pt_c->chooseStatus();
            propagatesChoice();
        }
//
    }
    propagatesDeath();

    for(Clock* pt_c: clocks){
        if (pt_c->status == TRUE) {
            pt_c->ticks();
        }
    }
}


void Solver::simulate(const unsigned int nbSteps, bool logSteps, bool logClocks, bool hideExpressionsInLog) {
    for (unsigned int currentStep= 0; currentStep < nbSteps; currentStep++) {
        if (logSteps || logClocks) {
#if not (defined (__AVR__))
            cout << "-------------step " << currentStep << endl;
#endif
        }

        doStep();

        if(logClocks) {
#if not (defined (__AVR__))
            logClockToCout(hideExpressionsInLog);
#endif
        }
//        std::random_shuffle ( clocks.begin(), clocks.end() );
    }
}

void Solver::doStep() {
    for(Clock* pt_c : clocks){
        pt_c->status = POSSIBLY;
    }

    solve();
#if not (defined (__AVR__))
    for(Constraint* pt_r: constraintsButExclusions + exclusions) {
        if(dynamic_cast<Concatenation*>(pt_r) == nullptr){ //not a concat
            pt_r->rewrite();
        }
    }
    for(Constraint* pt_r: constraintsButExclusions + exclusions) {
        if(dynamic_cast<Concatenation*>(pt_r) != nullptr){ //a concat
            pt_r->rewrite();
        }
    }
#else
    for(Constraint* pt_r: constraintsButExclusions + exclusions) {
            pt_r->rewrite();
    }
#endif

}

#if not (defined (__AVR__))
    void Solver::logClockToCout(bool doNotLogExpressions) const {
        for(Clock* pt_c: clocks) {
         if (doNotLogExpressions){
             if (dynamic_cast<Constraint*>(pt_c) != nullptr){
                 continue;
             }
         }
         cout << *pt_c <<endl;
        }
    }
#endif


void Solver::propagatesChoice() {
    bool fixPointReached = true;
    do{
        fixPointReached = true;
        for(Constraint* pt_c : exclusions + constraintsButExclusions){
            bool isStable = pt_c->propagatesChoice();
            fixPointReached = fixPointReached && isStable;
        }
    }while (! fixPointReached);
}

void Solver::propagatesDeath() {
    bool fixPointReached = true;
    do{
        fixPointReached = true;
        for(Constraint* pt_c : exclusions + constraintsButExclusions){
            bool isStable = pt_c->propagatesDeath();
            fixPointReached = fixPointReached && isStable;
        }
    }while (! fixPointReached);
}