//
// Created by jdeanton on 4/21/20.
//

#ifndef SRC_CCSLUTILS_H
#define SRC_CCSLUTILS_H

#include "constraints/relations/Precedes.h"
#include "constraints/relations/SubClock.h"
#include "constraints/relations/Exclusion.h"
#include "constraints/relations/Coincides.h"
#include "constraints/relations/Causes.h"
#include "constraints/relations/ConditionalRelation.h"

#include "constraints/relations/statemachine/StateMachine.h"
#include "constraints/relations/statemachine/State.h"
#include "constraints/relations/statemachine/Transition.h"

#include "constraints/expressions/Wait.h"
#include "constraints/expressions/UpTo.h"
#include "constraints/expressions/Union.h"
#include "constraints/expressions/ConditionalExpression.h"
#include "constraints/expressions/UserDefinedExpression.h"
#include "constraints/expressions/Concatenation.h"
#include "constraints/expressions/Death.h"
#include "constraints/expressions/Defer.h"
#include "constraints/expressions/Intersection.h"
#include "constraints/expressions/Sampled.h"
#include "constraints/expressions/Inf.h"
#include "constraints/expressions/Sup.h"

#include "classicalexpressions/SeqTail.h"
#include "classicalexpressions/Not.h"
#include "classicalexpressions/SeqEmpty.h"
#include "classicalexpressions/IntegerExpression.h"
#include "classicalexpressions/IntInf.h"
#include "classicalexpressions/IntEquals.h"
#include "classicalexpressions/IntAssign.h"
#include "classicalexpressions/IntRef.h"
#include "classicalexpressions/IntAdd.h"
#include "classicalexpressions/Not.h"
#include "classicalexpressions/Or.h"
#include "classicalexpressions/And.h"
#include "classicalexpressions/SeqHead.h"
#include "classicalexpressions/IntInfEqual.h"
#include "classicalexpressions/IntMinus.h"
#include "classicalexpressions/IntSup.h"
#include "classicalexpressions/IntSupEqual.h"

#include "constraints/expressions/Discretize.h"

#include "constraints/Constraint.h"
#include "Clock.h"
#include "Solver.h"
#include <vector>
#include <set>


#include "utils/utils.h"

#if not (defined (__AVR__))
    #include "utils/argengine/Argengine.h"
    #include <iostream>
    #include <chrono>

    void manageCommandLineArguments(int argc, char ** argv, int& nbSteps, bool& logClocks, bool& logStepsOnly);
#else
    #include <pnew.h>
#endif
#endif //SRC_CCSLUTILS_H
