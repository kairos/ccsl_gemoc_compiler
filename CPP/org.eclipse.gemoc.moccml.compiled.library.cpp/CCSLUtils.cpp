//
// Created by jdeanton on 4/21/20.
//
#include "CCSLUtils.h"


#if not (defined (__AVR__))

    void manageCommandLineArguments(int argc, char** argv, int& nbSteps, bool& logClocks, bool& logStepsOnly) {
        juzzlin::Argengine ae(argc, argv);
        ae.addOption(
                { "-n", "--nbSteps" }, [&](std::string stepNumber) {
                    std::string::size_type sz;
                    nbSteps = std::stoi(stepNumber, &sz);
                },
                false, "number of steps to execute (-1 for infinite)");
        ae.addOption(
                { "-l", "--logClocksAndSteps" }, [&logClocks] {
                    logClocks = true;
                }, false, "log clock status and execution steps");
        ae.addOption(
                { "-s", "--logStepsOnly" }, [&logStepsOnly] {
                    logStepsOnly = true;
                }, false, "log only execution steps");

        juzzlin::Argengine::Error error;
        ae.parse(error);

        if (error.code != juzzlin::Argengine::Error::Code::Ok) {
            #if not (defined (__AVR__))

    #endif
                      << std::endl;
            ae.printHelp();
            exit(-2);
        }
    }
#else
    extern "C" void __cxa_pure_virtual() { }

    __extension__ typedef int __guard __attribute__((mode (__DI__)));

    extern "C" int __cxa_guard_acquire(__guard *);
    extern "C" void __cxa_guard_release (__guard *);
    extern "C" void __cxa_guard_abort (__guard *);

    int __cxa_guard_acquire(__guard *g) {return !*(char *)(g);};
    void __cxa_guard_release (__guard *g) {*(char *)g = 1;};
    void __cxa_guard_abort (__guard *) {};

#endif