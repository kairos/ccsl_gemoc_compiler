//
// Created by jdeanton on 4/27/20.
//

#include "GeneratedSystem.h"

extern void setupClockFunction();

int main(int argc, char ** argv)
{
    setupClockFunction();
    run();
    return 0;
}

void run() {
    int nbSteps = 200; //by default"
    bool logClocks = false;
    bool logStepsOnly = false;

#if not (defined (__AVR__))
    manageCommandLineArguments(argc,argv,nbSteps,logClocks,logStepsOnly);
#endif

    std::vector<int> finite_MySpec_main_r1_AlternatesDef_seqOneInfinite;
    std::vector<int> infinite_MySpec_main_r1_AlternatesDef_seqOneInfinite;
    infinite_MySpec_main_r1_AlternatesDef_seqOneInfinite.push_back(1);
    Sequence MySpec_main_r1_AlternatesDef_seqOneInfinite = {finite_MySpec_main_r1_AlternatesDef_seqOneInfinite,infinite_MySpec_main_r1_AlternatesDef_seqOneInfinite,"MySpec_main_r1_AlternatesDef_seqOneInfinite"};

    Defer MySpec_main_r1_AlternatesDef_Alt_c1DelayedByOne{"MySpec_main_r1_AlternatesDef_Alt_c1DelayedByOne"};
    Precedes MySpec_main_r1_AlternatesDef_Alt_c1PrecedesC2 = {MySpec_main_c1,MySpec_main_c2};
    Precedes MySpec_main_r1_AlternatesDef_Alt_c2precedesC1DelayedByOne = {MySpec_main_c2,MySpec_main_r1_AlternatesDef_Alt_c1DelayedByOne};
    MySpec_main_r1_AlternatesDef_Alt_c1DelayedByOne.connect(MySpec_main_c1,MySpec_main_c1,MySpec_main_r1_AlternatesDef_seqOneInfinite);

    Discretize phyClock{"phyClock", 1000};
    std::set<Constraint *> allExclusions = {};
    std::set<Constraint *> allConstraintsButExclusions;
    {
        Constraint * array[] = {&phyClock, &MySpec_main_r1_AlternatesDef_Alt_c1PrecedesC2, &MySpec_main_r1_AlternatesDef_Alt_c2precedesC1DelayedByOne, &MySpec_main_r1_AlternatesDef_Alt_c1DelayedByOne, &MySpec_main_r1_AlternatesDef_Alt_c1DelayedByOne};
        for(Constraint * ptc : array){
            allConstraintsButExclusions.insert(ptc);
        }
    }
    std::vector<Clock *> allClocks;
    {
        Clock* array[] = {&MySpec_main_c1, &MySpec_main_c2, &MySpec_main_r1_AlternatesDef_Alt_c1DelayedByOne, &phyClock} ;
        for(Clock* ptc : array){
            allClocks.push_back(ptc);
        }
    }

    Solver solver{allClocks, allConstraintsButExclusions, allExclusions};

    #if not(defined (__AVR__))
        solver.simulate(nbSteps, logStepsOnly, logClocks);
    #else
        while(true){
            solver.doStep();
        }
    #endif
}
