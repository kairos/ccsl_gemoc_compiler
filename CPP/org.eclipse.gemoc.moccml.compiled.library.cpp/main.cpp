////============================================================================
//// Name        : main.cpp
//// Author      : Julien Deantoni
//// Version     : the best of ones I had :)
//// Copyright   : http://i3s.unice.fr/~deantoni
//// Description : Code generated for your CCSL specification, C++11 style
////============================================================================
//
//
//#include "constraints/relations/Precedes.h"
//#include "constraints/relations/SubClock.h"
//#include "constraints/relations/Exclusion.h"
//#include "constraints/relations/Coincides.h"
//#include "constraints/relations/Causes.h"
//#include "constraints/relations/ConditionalRelation.h"
//
//#include "constraints/relations/statemachine/StateMachine.h"
//#include "constraints/relations/statemachine/State.h"
//#include "constraints/relations/statemachine/Transition.h"
//
//#include "constraints/expressions/Wait.h"
//#include "constraints/expressions/UpTo.h"
//#include "constraints/expressions/Union.h"
//#include "constraints/expressions/ConditionalExpression.h"
//#include "constraints/expressions/UserDefinedExpression.h"
//#include "constraints/expressions/Concatenation.h"
//#include "constraints/expressions/Death.h"
//#include "constraints/expressions/Defer.h"
//#include "constraints/expressions/Intersection.h"
//#include "constraints/expressions/Sampled.h"
//#include "constraints/expressions/Inf.h"
//#include "constraints/expressions/Sup.h"
//
//#include "constraints/Constraint.h"
//#include "Clock.h"
//#include "Solver.h"
//#include <vector>
//#include <iostream>
//
//#include "utils/utils.h"
//#include "utils/argengine/Argengine.h"
//using std::vector;
//
//void manageCommandLineArguments(int argc, char ** argv, int& nbSteps, bool& logClocks, bool& logStepsOnly){
//    juzzlin::Argengine ae(argc, argv);
//    ae.addOption(
//            { "-n", "--nbSteps" }, [&](std::string stepNumber) {
//                std::string::size_type sz;
//                nbSteps = std::stoi(stepNumber, &sz);
//            },
//            false, "number of steps to execute (-1 for infinite)");
//    ae.addOption(
//            { "-l", "--logClocksAndSteps" }, [&logClocks] {
//                logClocks = true;
//            }, false, "log clock status and execution steps");
//    ae.addOption(
//            { "-s", "--logStepsOnly" }, [&logStepsOnly] {
//                logStepsOnly = true;
//            }, false, "log only execution steps");
//
//    juzzlin::Argengine::Error error;
//    ae.parse(error);
//
//    if (error.code != juzzlin::Argengine::Error::Code::Ok) {
//
//                  << std::endl;
//        ae.printHelp();
//        exit(-1);
//    }
//}
//
//
//int main(int argc, char ** argv)
//{
//    int nbSteps = 200; //by default"
//    bool logClocks = false;
//    bool logStepsOnly = false;
//
//    manageCommandLineArguments(argc,argv,nbSteps,logClocks,logStepsOnly);
//
////	    int simpleTest1_main_b_PeriodicDef_plusOne = 4;
////	    int CCSL_CCSL_Expressions_three = 3;
////	    int CCSL_CCSL_Expressions_two = 2;
//	    Clock a =  {"a"};
//	    Clock b =  {"b"};
//	    Clock c =  {"c"};
//	    Clock d =  {"d"};
//	    Clock e =  {"e"};
//        Clock f =  {"f"};
//        Clock g = {"g"};
//
//	    Precedes p1{a,b, "a < b"};
//	    Precedes p2{b, c, "b < c"};
//	    SubClock s1{d, c, "d subOf c"};
//	    Exclusion e1{d, e, "d # e"};
//        Coincides cc1{e, f, "e = f"};
//        Causes c1{b,f, "b causes f"};
//
//        Wait w1{a,2, "a_wait_2"};
//        UpTo u1{g,w1, "g_upto_c1"};
//        Union un1{b, f, "b+f"};
//
//	    vector<Constraint *> allExclusions = {&e1};
//	    vector<Constraint *> allConstraintsButExclusions = {&p1,&p2,&s1, &cc1, &c1, &w1, &u1, &un1};
//	    vector<Clock *> allClocks = {&a,&b,&c,&d,&e,&f,&g,&w1,&u1, &un1};
////
//	    Solver solver = {allClocks, allConstraintsButExclusions, allExclusions};
//
//	    std::cout << vector<NamedElement*>(allConstraintsButExclusions.begin(),allConstraintsButExclusions.end())<< std::endl;
//
//	    solver.simulate(nbSteps, logStepsOnly, logClocks);
//	    std::cout <<
//	         "/**********************\n" <<
//	         "*     simulation ends      *\n" <<
//	         "**********************/" << std::endl;
//
//	    return 0;
//}
