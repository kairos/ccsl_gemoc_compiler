//
// Created by jdeanton on 5/29/17.
//

#ifndef SRC_SEQUENCE_H
#define SRC_SEQUENCE_H


#include "NamedElement.h"
#include <vector>
#include <list>
#include <string>
#include <iostream>

class Sequence: public NamedElement {

public:
    std::vector<int> finitePart;
    std::vector<int> infinitePart;
    int finiteIndex = 0;
    int infiniteIndex = 0;

    Sequence(std::vector<int> finitePart = {}, std::vector<int> infinitePart = {}, std::string name = "defaultSeqName");

    int popHead();
    Sequence getTail();
    /**
     * give the next int in the sequence
     * @return the next int or -1 if empty
     */
    int nextHead() const;
    /**
    * give the next int in the sequence
    * @return the next int or -1 if empty
    */
    int popNext();
    bool hasNext() const;
    bool isFinite() const;
    bool isInfinite() const;
    bool empty() const;
    void clear();
#if not (defined (__AVR__))
    std::ostream& toStream(std::ostream& os) const override;
#endif
    ~Sequence() = default;
};


#endif //SRC_SEQUENCE_H
