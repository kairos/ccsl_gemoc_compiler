//
// Created by jdeanton on 4/19/20.
//
#include "utils.h"
#include <string>

#if not (defined (__AVR__))

    ostream& operator<<(ostream& os, const vector<Clock*>& vect_pt_c) {
        std::string sep = "";
        for(Clock* pt_c : vect_pt_c){
            assert(pt_c != nullptr);
            os << sep << *pt_c ;
            sep = "   |   ";
        }
        return os;
    }

    ostream& operator<<(ostream& os, const vector<NamedElement*>& vect_pt_c) {
        std::string sep = "";
        for(NamedElement* pt_c : vect_pt_c){
            assert(pt_c != nullptr);
            os << sep << *pt_c ;
            sep = "   |   ";
        }
        return os;
    }
#endif

vector<Constraint *> operator+(const vector<Constraint *>& v1, const vector<Constraint *>& v2){
    vector<Constraint *> all = v1;
    for(Constraint* pt_c : v2){
        all.push_back(pt_c);
    }
    return all;
}

std::set<Constraint *> operator+(const std::set<Constraint *>& v1, const std::set<Constraint *>& v2){
    std::set<Constraint *> all = v1;
    for(Constraint* pt_c : v2){
        all.insert(pt_c);
    }
    return all;
}