//
// Created by jdeanton on 5/29/17.
//

#include "Sequence.h"


Sequence::Sequence(std::vector<int> finitePart, std::vector<int> infinitePart, std::string name)
:NamedElement(MOVE(name)), finitePart(finitePart), infinitePart(infinitePart), finiteIndex(0),infiniteIndex(0)
{
}

int Sequence::popHead() {
    return popNext();
}

Sequence Sequence::getTail() {
    this->popNext();
    return *this;
}

int Sequence::nextHead() const{
    if (((size_t)finiteIndex) < finitePart.size()) {
        return finitePart.at(finiteIndex);
    }
    else if (((size_t)infiniteIndex) < infinitePart.size()) {
        return infinitePart.at(infiniteIndex);
    }
    return -1;
    exit(-16);
}

int Sequence::popNext() {
    if (((size_t)finiteIndex) < finitePart.size()) {
        int result = nextHead();
        finiteIndex++;
        return result;
    }
    else if (((size_t)infiniteIndex) < infinitePart.size()) {
        int result = nextHead();
        infiniteIndex++;
        if (((size_t)infiniteIndex) == infinitePart.size()) {
            infiniteIndex = 0;
        }
        return result;
    }
    return -1;
    exit(-17);
}

bool Sequence::hasNext() const {
    return ( (((size_t)finiteIndex) < finitePart.size()) || isInfinite() );
}

bool Sequence::isFinite() const {
    return (infinitePart.empty());
}

bool Sequence::isInfinite() const {
    return (! infinitePart.empty());
}

bool Sequence::empty() const {
    return ((finitePart.empty()) && (infinitePart.empty()));
}

void Sequence::clear() {
    infiniteIndex = 0;
    finiteIndex = 0;
}
#if not (defined (__AVR__))

    std::ostream& Sequence::toStream(std::ostream& os) const {
        char sep = ' ';
        for(int v : finitePart){
            os << sep << v;
            sep = ';';
        }
    //    os << ' ';
        sep = '(';
        for(int v : infinitePart){
            os << sep << v;
            sep = ';';
        }
        os << u8")\u221E";
        return os;
    }
#endif


