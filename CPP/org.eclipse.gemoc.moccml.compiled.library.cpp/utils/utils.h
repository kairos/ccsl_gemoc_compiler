//
// Created by jdeanton on 2/20/19.
//

#ifndef SRC_UTILS_H
#define SRC_UTILS_H

#if not (defined (__AVR__))
    #include <iostream>
    #include <cassert>
#endif

#include <vector>
#include <set>
#include "../Clock.h"
#include "../constraints/Constraint.h"
#include "../utils/NamedElement.h"


using std::ostream;
using std::vector;

ostream& operator<<(ostream& os, const vector<Clock*>& vect_pt_c);
ostream& operator<<(ostream& os, const vector<NamedElement*>& vect_pt_c);

vector<Constraint *> operator+(const vector<Constraint *>& v1, const vector<Constraint *>& v2);
std::set<Constraint *> operator+(const std::set<Constraint *>& v1, const std::set<Constraint *>& v2);


#endif //SRC_UTILS_H
