/*
 * NamedElement.h
 *
 *  Created on: Aug 17, 2016
 *      Author: jdeanton
 */

#ifndef NAMED_ELEMENT_H_
#define NAMED_ELEMENT_H_

#if not (defined (__AVR__))
    #include <iostream>
    #define MOVE(VAR) MOVE(VAR)
#endif
#if (defined (__AVR__))
    #define MOVE(VAR) VAR
    #include <stdlib.h>
#endif

#include <string>



class NamedElement {
public:
	std::string name;


	explicit NamedElement(std::string name);
#if not (defined (__AVR__))
    virtual std::ostream& toStream(std::ostream& os) const;
    friend std::ostream& operator<<(std::ostream& os, const NamedElement& ne);
#endif
	virtual ~NamedElement();
};

#endif /* NAMEDELEMENT_H_ */
