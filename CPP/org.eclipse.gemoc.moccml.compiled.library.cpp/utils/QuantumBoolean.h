/*
 * QuanticBoolean.h
 *
 *  Created on: Aug 25, 2016
 *      Author: jdeanton
 */

#ifndef QUANTUM_BOOLEAN_H_
#define QUANTUM_BOOLEAN_H_

#if not (defined (__AVR__))
    #include "iostream"
#endif
enum QuantumBoolean{
		TRUE,
		POSSIBLY,
		FALSE
};
#if not (defined (__AVR__))
    std::ostream& operator<<(std::ostream& os, const QuantumBoolean& v);
#endif

#endif
