/*
 * NamedElement.cpp
 *
 *  Created on: Aug 17, 2016
 *      Author: jdeanton
 */

#include "NamedElement.h"


NamedElement::NamedElement(std::string name): name(MOVE(name)) {
}

NamedElement::~NamedElement() = default;

#if not (defined (__AVR__))
    std::ostream& NamedElement::toStream(std::ostream& os) const{
        os << "NamedElement: " << name << std::endl;
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const NamedElement& ne){
        return ne.toStream(os);
    }
#endif